#!/usr/local/bin/perl


=pod 
=head1 NAME

ComputeOrganismStatistics.pl - This program computes statistics about all replicons of the strain of the organism in the bank PeRNA.

=head1  SYNOPSIS 

./ComputeOrganismStatistics.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom "Bacteria" --organism "Escherichia coli" --strain K12 --word_max_length 2 --win_min_length 70 --win_max_length 70 --step 10 --shift_size 10
./ComputeOrganismStatistics.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom "Archea" --organism "Methanococcus jannaschii" --strain "DSM_2661" --word_max_length 2 --win_min_length 40 --win_max_length 40 --step 10 --shift_size 10

=head1 DESCRIPTION

ComputeOrganismStatistics.pl computes statistics about all the replicons of the strain of a organism 
For each replicon, it calls the different statistic computing. It computes statistics about 
the complet genomic sequence, 
intergenic regions, 
the ncrna, 
genomic windows : 
	min size of the window is win_min_length, max size is win_max_length
	step is the additional size between two windows 
	and shift_size is the shift of the moving window about the genome.
It computes each k-nucleotid number with 1<=k<=word_max_length
the PeRNA bank is completed with statistics files in the different directories:
<replicon>
    global_statistics
	complet_genome_stat
   		A_seq
		C_seq
		AA_seq
		AC_seq...
	IgRegionNumber
	ig_complet{1..w}_stat
		Positions
		A_seq
		C_seq
		AA_seq
		AC_seq...
	ig_leading{1..w}_stat
	ig_lagging{1..l}_stat
	tmp
    basic_statistics
	win{win_min_length..win_max_length}_stat
		A_seq
		C_seq...
		AA_seq...
		A_struct...
		GC_pair...
		FreeEnergy
		PairInformation
    known_RNA
	<family>{1..n}
	    RNA{1..p}
		RNA_stat	
		   A_seq
		   C_seq
		   AA_seq
		   A_struct
		   C_struct
		   GC_pair
		   PairInformation

=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;
use ToolBox;
use ParamParser;
use Logger;



MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help:s", "cfg=s","kingdom=s", "organism=s", "strain=s",
	"word_max_length=i","win_min_length=i","win_max_length=i","step=i","shift_size=i"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain',
	'word_max_length','win_min_length','win_max_length','step','shift_size'));
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	my $DATA_DIR		= $rh_param->Get('DATA_DIR');
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');

	# check that some parameters are integer
	$rh_param->AssertInteger('word_max_length');
	$rh_param->AssertInteger('win_min_length');
	$rh_param->AssertInteger('win_max_length');
	$rh_param->AssertInteger('step');
	$rh_param->AssertInteger('shift_size');
	
	my $word_max_length 	= $rh_param->Get('word_max_length');
	my $win_min_length  	= $rh_param->Get('win_min_length');
	my $win_max_length  	= $rh_param->Get('win_max_length');
	my $step  		= $rh_param->Get('step');
	my $shift_size 		= $rh_param->Get('shift_size');
	
	my $cmd;
	
	# search the name of the directory which contains the data about the organism
	my $data_directory = &GetDirWay($DATA_DIR, $kingdom, $organism, $strain);
	opendir(DATA_DIR, $data_directory ) or die "Can't open the directory $data_directory !!!\n";
	
	# for each replicon of the organism, compute all the statistics
	while ( defined(my $replicon = readdir DATA_DIR) ) {
		next if $replicon =~ /^\.\.?/;		# don't consider the directories . and ..
		next if (-f $data_directory.$replicon); # don't consider files
		
		print "Compute replicon $replicon...\n";
		
		print "  Compute genomic statistics...\n";
		# compute statistics on the genomic sequence of the replicon
		$cmd = "./ComputeGenomicStatistics.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --word_max_length $word_max_length";
 		&SystemLaunch($cmd);
	
		# compute statistics on the rna of the replicon
		print "  Compute RNA statistics...\n";
		$cmd = "./ComputeRNAStatistics.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --word_max_length $word_max_length";
 		&SystemLaunch($cmd);
	
		# compute statistics on the intergenic sequence of the replicon
		print "  Compute intergenic region statistics...\n";
		$cmd = "./ComputeIntergenicStatistics.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --word_max_length $word_max_length";
 		&SystemLaunch($cmd);
	
		# compute statistics on the window of the genomic sequence
		print "  Compute windows statistics...\n";
		$cmd = "./ComputeWindowStatistics.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --word_max_length $word_max_length --win_min_length $win_min_length --win_max_length $win_max_length --step $step --shift_size $shift_size";
		&SystemLaunch($cmd);

	}
	closedir(DATA_DIR);	
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--word_max_length : max length of k-nucleotids that we count ( 1 <= k <= word_max_length)
		--win_min_length : min length of the window which moves about the genome to compute statistics 
		--win_max_length : max length of the window which moves about the genome to compute statistics  
		--step 		: additional size between two windows 
		--shift_size 	: shift of the moving window about the genome
END
}
