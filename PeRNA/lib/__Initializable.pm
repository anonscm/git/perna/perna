#
# $Id: __Initializable.pm,v 1.1.1.1 2007-07-19 08:25:30 cros Exp $
#

package __Initializable;

=pod

=head1 NAME

__Initializable - Every object should derive from this class

This class is useful ONLY to provide the new method. new calls __Init, thus initializing the stuff for the derived classes.

=cut

use strict;
use warnings;

BEGIN
{
    our $VERSION = do {my @r = (q$Rev: 4 $ =~ /\d+/g); $r[0]};
}

sub New
{
    my ($class, @args) = @_;

    #print "KOUKOU $class\n";
    my $self = bless {}, ref($class) || $class;
    $self->__Init(@args);
    return $self;
}

1;

