#!/usr/local/bin/perl



=pod 
=head1 NAME

StockOrganism.pl - This program creates the tree structure for a strain of an organism

=head1  SYNOPSIS 

./StockOrganism.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom "Bacteria" --organism "Escherichia coli" --strain K12


=head1 DESCRIPTION

StockOrganism.pl creates the tree structure of a strain of an organism for its replicons :
it creates the tree structure and extracts from different files the genomic datas and the rna datas
The data files about areplicon of the organism have to be in the directory external_data/<kingdom>/<organism>[_strain]/<replicon>
See UserManual.pdf

=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use ToolBox;

our $EXTERNAL_DATA_DIR;

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help:s", "cfg=s","kingdom=s", "organism=s", "strain=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain'));
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$EXTERNAL_DATA_DIR	= $rh_param->Get('EXTERNAL_DATA_DIR');
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	
	my $ext_data_directory;			# external_data directory of the organism
	my $ext_data_replicon_directory;	# external data directory of the replicon
	
	# search the name of the directory which contains the external data about the organism
	$ext_data_directory = &GetDirWay($EXTERNAL_DATA_DIR, $kingdom, $organism, $strain);
	
	opendir(EXTERNAL_DIR, $ext_data_directory ) or die "Can't open the directory $ext_data_directory !!!\n";
	
	# for each replicon of the organism
	while ( defined(my $replicon = readdir EXTERNAL_DIR) ) {
		next if $replicon =~ /^\.\.?/;	# don't treate the files and the directories . and ..
		
		# external data directory of the current replicon 
		$ext_data_replicon_directory = &GetRepliconDir($EXTERNAL_DATA_DIR, $kingdom, $organism, $strain,$replicon);
		
		# the way of the accession number file 
		my @acc_file	= &GetExtensionFiles($ext_data_replicon_directory, ".ac");
		my $acc_way	= $ext_data_replicon_directory.$acc_file[0];
		if ( $#acc_file < 0) {
			die "ERROR : the accession file should be exist in the directory $ext_data_replicon_directory !!!";
		}
		# extract the data about the genome
		&StockGenBankData($ext_data_replicon_directory,$kingdom,$organism, $strain, $replicon);
		# extract the data about the rnas from the Rfam bank
		&StockRfamData($kingdom,$organism, $strain, $replicon, $acc_way);
		# extract the data about the ribosomal rnas from the European Ribosomal RNA database
		&StockRibosomalData($ext_data_replicon_directory, $kingdom,$organism, $strain, $replicon, $acc_way);
		# extract teh data about the rna from NonCode bank
		&StockNonCodeData($ext_data_replicon_directory, $kingdom,$organism, $strain, $replicon, $acc_way);
		# count the number of rna
		&CountRna($kingdom,$organism, $strain, $replicon);
	}
	
	closedir(EXTERNAL_DIR);
	
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
END
}


=head2 procedure  StockGenBankData

 Title        : StockGenBankData
 Usage        : StockGenBankData($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon )
 Prerequisite : none
 Function     : save the data of the genbank file, create the tree structure for the organism
 Returns      : none
 Args         : $ext_data_replicon_dir : directory of the genbank file
 		$kingdom : name of the kingdom of the organism
		$organism : the name of the organism
		$strain : the name of the strain
		$replicon : name of the replicon
 Globals      : none
 
=cut 

sub StockGenBankData
{
	my ($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon) = @_;
	# search the genbank file 
	my @gbk_file = &GetExtensionFiles($ext_data_replicon_dir, ".gbk");
		
	if ( $#gbk_file < 0) {
		die "ERROR : the genbank file should be exist in the directory $ext_data_replicon_dir !!!";
	}
	# the way of the genbank file 
	my $gbk_way 	= $ext_data_replicon_dir.$gbk_file[0];
	# creation of the tree structure in the data directory
	my $cmd 	= "./StockGenBankData.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --file $gbk_way";
 	&SystemLaunch($cmd);
}



=head2 procedure  StockRfamData

 Title        : StockRfamData
 Usage        : StockRfamData($kingdom,$organism, $strain, $replicon,$acc_way )
 Prerequisite : none
 Function     : extract the data from rfam bank(search the data in the file Rfam.full), create the tree structure for the organism
 Returns      : none
 Args         : $kingdom : name of the kingdom of the organism
 		$organism : the name of the organism
 		$strain : the name of the strain
		$replicon : name of the replicon
		$acc_way : the way of the accession number file 
 Globals      : none
 
=cut 

sub StockRfamData
{
	my ($kingdom,$organism, $strain, $replicon,$acc_way ) = @_;
	my $cmd = "./StockRfamData.pl --cfg \$PeRNA/cfg/Conf.cfg --RNAfamilies \$PeRNA/cfg/RNAfamiliesRfam.cfg --kingdom $kingdom --organism \"$organism\"  --strain $strain --replicon $replicon --accession_number $acc_way"; 
	&SystemLaunch($cmd);
}

=head2 procedure  StockRibosomalData

 Title        : StockRibosomalData
 Usage        : StockRibosomalData($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon, $acc_way )
 Prerequisite : none
 Function     : extract the ribosomal rnas from the files .ssu ans .lsu : create the tree structure for the organism
 Returns      : none
 Args         : $ext_data_replicon_dir : way of the lus and ssu files
 		$kingdom : name of the kingdom of the organism
		$organism : the name of the organism
		$strain : the name of the strain
		$replicon : name of the replicon
		$acc_way : the way of the accession number file 
 Globals      : none
 
=cut 

sub StockRibosomalData
{
	my ($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon, $acc_way ) = @_;
	my @lsu_file = &GetExtensionFiles($ext_data_replicon_dir, ".lsu");
	my @ssu_file = &GetExtensionFiles($ext_data_replicon_dir, ".ssu");
	my $cmd;
	
	# lsu treatment
	if ( $#lsu_file < 0) {
		print "WARNING : there is no lsu file in the directory $ext_data_replicon_dir !\n";
	} else {
		# the way of the lsu file 
		my $lsu_way 	= $ext_data_replicon_dir.$lsu_file[0];
		$cmd 		= "./StockRibosomalData.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --rna_file $lsu_way --rna_family_name lsu --accession_number $acc_way";
		&SystemLaunch($cmd);
	}
	
	#ssu treatment
	if ( $#ssu_file < 0) {
		print "WARNING : there is no ssu file in the directory $ext_data_replicon_dir !\n";
	} else {
		# the way of the ssu file 
		my $ssu_way 	= $ext_data_replicon_dir.$ssu_file[0];
		$cmd 		= "./StockRibosomalData.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --rna_file $ssu_way --rna_family_name ssu --accession_number $acc_way";
		&SystemLaunch($cmd);
	}
}



=head2 procedure  StockNonCodeData

 Title        : StockNonCodeData
 Usage        : StockNonCodeData($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon, $acc_way )
 Prerequisite : none
 Function     : extract the misc rnas from stockholm files 
 Returns      : none
 Args         : $ext_data_replicon_dir : way of the stockhoml files
 		$kingdom : name of the kingdom of the organism
 		$organism : the name of the organism
 		$strain : the name of the strain
		$replicon : name of the replicon
		$acc_way : the way of the accession number file 
 Globals      : none
 
=cut 

sub StockNonCodeData
{
	my ($ext_data_replicon_dir, $kingdom,$organism, $strain, $replicon, $acc_way ) = @_;
	my @sto_file = &GetExtensionFiles($ext_data_replicon_dir, ".sto");
	my $sto_way;
	my $cmd;

	if ( $#sto_file < 0) {
		print "WARNING : there is no stockholm files in the directory $ext_data_replicon_dir!!!\n";
	} else {
		# for each stockholm file
		for (my $i=0 ; $i <= $#sto_file ; $i++) {
			# the way of the  file 
			$sto_way 	= $ext_data_replicon_dir.$sto_file[$i];
			$cmd 		= "./StockNonCodeData.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon --misc_file $sto_way --accession_number $acc_way";
			&SystemLaunch($cmd);
		}
	}
}


=head2 procedure CountRna

 Title        : CountRna
 Usage        : CountRna($kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : count the number of rnas of the replicon and create a file which contains this number
 Returns      : none
 Args         : $kingdom : name of the kingdom of the organism
 		$organism : the name of the organism
		$strain : the name of the strain
		$replicon : name of the replicon
 Globals      : none
 
=cut 

sub CountRna
{
	my ($kingdom,$organism, $strain, $replicon) = @_;
	my $cmd = "./CountRna.pl --cfg \$PeRNA/cfg/Conf.cfg --kingdom $kingdom --organism \"$organism\" --strain $strain --replicon $replicon";
	&SystemLaunch($cmd);
}
