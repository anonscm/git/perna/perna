#!/usr/local/bin/perl 

##[
#>Project:      iANT integrated ANnotation Tool (1999-2000)
#>Release:      1.0
#>Lab:          INRA-CNRS LBMRPM, BP 27 Chemin de Borde Rouge, 31326 Castanet Cedex France
#>Authors:      Patricia.Thebault@toulouse.inra.fr and Jerome.Gouzy@toulouse.inra.fr
#>This script:  Patricia.Thebault@toulouse.inra.fr
#>Last updated: October 31, 2000
#>Citation:
#
#Aim: Reformat fasta format (cut lines and format the header line)
#
#
#Updated:       
#
#Who:   
#
##]

$script_ok = ( $0 =~ /\.ok\./ ) ? ".ok" : "";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jConfig$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jLibPerl$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/pLibPerl$script_ok.pl";




### Constants
$HEADER=0;
$SIZE=1;
$SEQUENCE=2;
$NAME=3;
$COM=4;
$AC=5;

########################################################################
#[pReformatUsage
#Aim:   Usage script 
#Global:
#In: Error Msg to print out
#Out: 
#Update:
#Prerequisite:     
#]
sub pReformatUsage 
{
local ($msg) = @_ ;
     print STDERR "$msg\n";    
     print STDERR 
	" USAGE : $0 
	
Required
	IN=Input file (fasta format required)
	OUT=Output file
Optionnal
	LINE=Length of the line                 [50]
	SUBST=Unknown letters are replaced      [NO]
              by N for DNA, and X for AA  
              VALUE=NO|YES  
	TYPE=Sequence type                      [DNA]
              VALUE=AA|DNA[DNA]
	CHECK=Check sequence consistency        [YES]
              VALUE=YES|NO|QUICK          
	VERBOSE=Verbose mode                    [YES]
              VALUE=YES|NO    
\n";
        exit(0);
}


########################################################################
#[pPrintOneSequence
#Aim: Print one sequence
#Global:
#In: $name,$ac,$com,$sequence,$size_line
#Out: 
#Update: 
#Prerequisite:     
#]
sub pPrintOneSequence
{
local($name,$ac,$com,$sequence,$size_line)=@_;
my($size_sequence)=length($sequence);
my($nb_lines)=$size_sequence/$size_line;
my($j)=0;
    $nb_lines=int($nb_lines)+1 if ($nb_lines=~ /\./);
    print  ">$name | $size_sequence | $ac |  $com\n";
    for ($j=0;$j<$nb_lines;$j++)
    {
	my($sub_sequence)=substr($sequence,($j)*$size_line,$size_line);
	print  "$sub_sequence\n";
    }    
}

########################################################################
#[pFormatFastaFile
#Aim:            Read fasta file with ONE OR SEVERAL sequenceS, remove newlines and join them into a string
#Global:
#In: $file,$check,$subst,$type,$size_line,$verbose
#Out: 
#Update:
#Prerequisite:     
#]
sub pFormatFastaFile
{
local ($file,$check,$subst,$type,$size_line,$verbose) = @_;
my(@L)=();
my(%LETTER)=();	
    open(FASTA, $file) || &pPrintErrorScript ("Error while  opening $file\n");
    if ($type eq "AA")
    {
	@L=split('',"ARNDCQEGHILKMFPSTWYVBZ");	###### ATTENTION, si chgt , penser a changer aussi listsearch plus bas
    }
    else
    {
	@L=split('',"ATGCU"); ##### ATTENTION, si chgt , penser a changer aussi listsearch plus bas
    }
    my($l)=0;
    for ($l=0;$l<=$#L;$l++)
    {
	$LETTER{$L[$l]}=1;
    }
    my($save_separator)=$/;
    $/="\n>";
    my($bloc)="";
    while($bloc =<FASTA>)
    {
	chop($bloc);
	if ($bloc ne "")
	{
	    $bloc=~ s/^>//;
	    my (@S)=split("\n",$bloc,2);
	    $S[1]=~ s/\n//g;
	    chomp($S[0]);
	    my($SEQ_header)=$S[0];
	    my($nb_delimiter);
	    $nb_delimiter=$SEQ_header=~ s/ \| / \| /g;
	    if ($nb_delimiter==3) # HEADER deja formate
	    {
	    	my(@F)=split(" \\| ",$SEQ_header);
		$SEQ_name=$F[0];
		$SEQ_com=$F[3];	
		$SEQ_AC=$F[2];	
	    }
	    else
	    {
		my(@FF)=split(' ',$SEQ_header,2); 
		$SEQ_name=$FF[0];
		$SEQ_com=$FF[1];
		$SEQ_AC="AC;";	
	    }
	    $S[1]=~tr/a-z/A-Z/;
	    chomp($S[1]);
	    my($SEQ_sequence)=$S[1];
	    if ($check eq "YES")
	    {
		my(@TAB_SEQ)=split('',$SEQ_sequence);
		my($c)=0;
		for ($c=0;$c<=$#TAB_SEQ;$c++)
		{
		    if ($TAB_SEQ[$c] eq " ")
		    {
		    	$TAB_SEQ[$c]="";
		    }
		    elsif (!defined ($LETTER{$TAB_SEQ[$c]}))
		    {
			#print STDERR "#$TAB_SEQ[$c]# at position $c is unknown within sequence : $SEQ_name\n" if ($verbose);
			if ($subst eq "YES")
			{
			    $TAB_SEQ[$c]= ($type eq "AA")?("X"):("N");
			}
			else
			{
			    $TAB_SEQ[$c]="";
			}
		    }
		    
		}
		$SEQ_sequence=join('',@TAB_SEQ);
	    }
	    elsif ($check eq "QUICK")
	    {
		if ($subst eq "YES")
		{
		    if ($type eq "AA")
		    {
			$SEQ_sequence=~ tr/ARNDCQEGHILKMFPSTWYVBZ/X/c;
		    }
		    else
		    {
			$SEQ_sequence=~ tr/ATGCU/N/c;
		    }
		}
		else
		{
		    if ($type eq "AA")
		    {
			$SEQ_sequence=~ tr/ARNDCQEGHILKMFPSTWYVBZ//dc;
		    }
		    else
		    {
			$SEQ_sequence=~ tr/ATGCU//dc;
		    }
		}
	    }
	    &pPrintOneSequence($SEQ_name,$SEQ_AC,$SEQ_com,$SEQ_sequence,$size_line);
	}
    }
    close (FASTA);
    $/=$save_separator;
}

########################################################################
#[pInitReformat
#Aim:      Init parameters      
#Global:
#In:      
#Update:*param
#Prerequisite:     
#]
sub pInitReformat
{
local(*param)=@_;    
    $param{"IN"} = "";
    $param{"OUT"} = "";
    $param{"LINE"} = 50;
    $param{"TYPE"} = "DNA"; #DNA|AA
    $param{"SUBST"} = "YES";#YES|NO
    $param{"CHECK"} = "YES";#YES|NO|QUICK
    $param{"VERBOSE"} = "YES";#YES|NO
}

########################################
###################### MAIN ############
########################################

&pInitReformat(*GidxParam_tmp);
local(%GidxArg)=&pTab2Indx(@ARGV);
local(%GidxParam)=(%GidxParam_tmp,%GidxArg);
&pReformatUsage("FATAL : IN is required\n") if ($GidxParam{IN} eq "");
&pReformatUsage("FATAL : IN does not exist\n") if (!&pFileExiste($GidxParam{IN}));
&pReformatUsage("FATAL : OUT is required\n") if ($GidxParam{OUT} eq "");
open (OUT,">$GidxParam{OUT}") || &pPrintErrorScript("Error while opening $GidxParam{OUT}\n");
select (OUT);
&pFormatFastaFile($GidxParam{IN},$GidxParam{CHECK},$GidxParam{SUBST},$GidxParam{TYPE},$GidxParam{LINE},$GidxParam{VERBOSE});
close (OUT);
