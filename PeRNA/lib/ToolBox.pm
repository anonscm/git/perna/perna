

=pod 
=head1 NAME

ToolBox 

=head1  SYNOPSIS 

ToolBox.pm 

=head1 DESCRIPTION

ToolBox.pm is a library which contains different procedures 

=cut

use strict;
use warnings;
use Carp;


=head1 SUBROUTINES

=cut



=head2 prodecure SystemLaunch

 Title        : SystemLaunch
 Usage        : SystemLaunch($cmd)
 Prerequisite : none
 Function     : Execute the unix command
 Returns      : none
 Args         : $cmde : unix command
 Globals      : none

=cut 
sub SystemLaunch
{
	my $cmd = shift;
	system $cmd;
}


=head2 function GetNameFastaFile

 Title        : GetNameFastaFile
 Usage        : GetNameFastaFile($organism, $strain, $replicon)
 Prerequisite : none
 Function     : return the name of the fasta file of the complete genome of the organism 
 Returns      : none
 Args         : $organism : the name of the organism
 		$strain : the strain of the organism
		$replicon : the replicon
 Globals      : none
 
=cut 
sub GetNameFastaFile
{
	my ($organism, $strain, $replicon) = @_;
	my $name_file = &GetDirName($organism, $strain);
	$name_file = $name_file."_".FormateString($replicon).".fna";
	return $name_file;
}


=head2 function GetNameFormatedFastaFile

 Title        : GetNameFormatedFastaFile
 Usage        : GetNameFormatedFastaFile($organism, $strain, $replicon)
 Prerequisite : none
 Function     : return the name of the formated fasta file of the complete genome of the organism 
 Returns      : none
 Args         : $organism : the name of the organism
 		$strain : the strain of the organism
		$replicon : the replicon
 Globals      : none
 
=cut 
sub GetNameFormatedFastaFile
{
	my ($organism, $strain, $replicon) = @_;
	my $name_file = &GetDirName($organism, $strain);
	$name_file = $name_file."_".FormateString($replicon)."_formated.fna";
	return $name_file;
}


=head2 procedure CreateDirectory

 Title        : CreateDirectory
 Usage        : CreateDirectory($directory_name, $parent_directory )
 Prerequisite : none
 Function     : create the directory directory_name in the $parent_directory
 Returns      : None
 Args         : $directory_name the name of the directory that we want to create
 		$parent_directory : the way of the parent directory
 Globals      : none

=cut
sub CreateDirectory
{
	my ($directory_name, $parent_directory) = @_;
	# go in the parent directory
	chdir($parent_directory) || die "Impossible to go to the directory $parent_directory\n";

	mkdir($directory_name, 0777) || die "Cannot mkdir $directory_name";
	#print "Directory $directory_name created in $parent_directory\n";
}


=head2 function ExistDirectory

 Title        : ExistDirectory
 Usage        : ExistDirectory($directory_name, $parent_directory )
 Prerequisite : none
 Function     : test if the directory exists
 Returns      : 0 if the directory doesn't exist, 1 else
 Args         : $directory_name the name of the directory that we are looking for
 		$parent_directory : the way of the parent directory
 Globals      : none

=cut
sub ExistDirectory
{
	my ($directory_name, $parent_directory) = @_;
	
	if (-e $parent_directory.$directory_name) {
		return 1;
	} else {
		return 0;
	}
}


=head2 function GetNextDirectoryNumber

 Title        : GetNextDirectoryNumber
 Usage        : GetNextDirectoryNumber($directory_way )
 Prerequisite : none
 Function     : look for the tallest number of directory " RNA" in the $directory_way and return the next number 
 Returns      : the next number of the directory "RNA"
 Args         : $directory_way the way of the directory in which we search the tallest number of this directory
 		if this directory contains the directories :	
			RNA1
			RNA2
			RNA3
			--> return 4
 Globals      : none

=cut
sub GetNextDirectoryNumber
{
	my ($directory_way) = @_;
	my $latest_number = 0;
	opendir(DIRHANDLE, $directory_way) || die "Cannot opendir $directory_way";
  	while (my $dir_name = readdir(DIRHANDLE)){
		my @number = split (/RNA/, $dir_name);
		if ( ($number[1]) && ($number[1] > $latest_number) ) {
			$latest_number = $number[1];
		}
	}

	return ($latest_number+1);
}


=head2 function FormateString

 Title        : FormateString
 Usage        : FormateString($string)
 Prerequisite : none
 Function     : lower case version of the string, delete the space and the characters different from [a..z0..9]
 Returns      : the formated string
 Args         : $string : the string to format
 Globals      : none

=cut
sub FormateString
{
	my ($string) = @_;
	# lower case version
	$string = lc($string);
	# delete the space at the begin and the end
	$string =~ s/(^[ \t]+)|([ \t]+$)//gm;
	# delete the specific characters
	$string =~ tr/a-z0-9 .//cd;
	return ($string);
}


=head2 function FormateStrain

 Title        : FormateStrain
 Usage        : FormateStrain($string)
 Prerequisite : none
 Function     : replace the space by '_' in the $strain string
 Returns      : the formated string
 Args         : $strain : the string of the strain to formate
 Globals      : none

=cut
sub FormateStrain
{
	my ($strain) = @_;
	$strain =~s/ /\_/g;
	return ($strain);	
}


=head2 function GetDirName

 Title        : GetDirName
 Usage        : GetDirName($org, $strain)
 Prerequisite : none
 Function     : return the name of the directory for the organism and its strain
 		$org_$strain where space are replaced by "_"
 Returns      : the name of the directory
 Args         : $org : the name of the organism
 		$strain : the name of the strain
 Globals      : none

=cut
sub GetDirName
{
	my ($org, $strain) = @_;
	my $dir;
	$dir = $org;
	# replace " " by "_"
	$dir =~s/ /\_/g;
	if ( $strain ne "") {
		$strain = &FormateStrain($strain);
		# add the name of the strain
		$dir = $dir."_".$strain;
	}
	return ($dir);
}


=head2 function GetKingdomDir

 Title        : GetKingdomDir
 Usage        : GetKingdomDir($data, $kingdom)
 Prerequisite : none
 Function     : return the way of the directory of the kingdom
 		
 Returns      : the way of the directory of the kingdom
 Args         : $data : directory of the data
 		$kingdom : name of the kingdom of the organism
 Globals      : none

=cut
sub GetKingdomDir
{
	my ($data, $kingdom) = @_;
	my $way = $data.$kingdom."/";
	return $way;
}

=head2 function GetDirWay

 Title        : GetDirWay
 Usage        : GetDirWay($data, $kingdom, $org, $strain)
 Prerequisite : none
 Function     : return the way of the directory for the organism and its strain
 		name of the directory : $org_$strain where space are replaced by "_"
 Returns      : the way of the directory
 Args         : $data : directory of the data
 		$kingdom : name of the kingdom of the organism
 		$org : the name of the organism
 		$strain : the name of the strain
 Globals      : none

=cut
sub GetDirWay
{
	my ($data, $kingdom, $org, $strain) = @_;
	my $way = GetKingdomDir($data, $kingdom);
	$way = $way.GetDirName($org, $strain)."/";
	return $way; 
}


=head2 function GetRepliconDir

 Title        : GetRepliconDir
 Usage        : GetRepliconDir($data, $kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : return the way of the directory of the replicon
 Returns      : the way of the directory of the kingdom
 Args         : $data : directory of the data
 		$kingdom : name of the kingdom of the organism
		$organism : the name of the organism
 		$strain : the name of the strain
		$replicon : name of the replicon
 Globals      : none

=cut
sub GetRepliconDir
{
	my ($data, $kingdom, $organism, $strain, $replicon) = @_;
	my $way = &GetDirWay($data, $kingdom, $organism, $strain);
	$way = $way.FormateString($replicon)."/";
	return $way;
}



=head2 function GetDirKnownRna

 Title        : GetDirKnownRna
 Usage        : GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : return the way of the directory "knownRNA
 Returns      : the way of the directory
 Args         : $data_dir : directory of the data
 		$kingdom : name of the kingdom of the organism
 		$organism : the name of the organism
 		$strain : the name of the strain
		$replicon : name of the replicon
 Globals      : none

=cut
sub GetDirKnownRna
{
	my ($data_dir, $kingdom, $organism, $strain, $replicon) = @_;
	my $way = &GetRepliconDir($data_dir, $kingdom, $organism, $strain,$replicon);
	$way = $way."knownRNA/";
	return $way;
	
}

=head2 function GetDirTmp

 Title        : GetDirTmp
 Usage        : GetDirTmp($data_dir, $kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : return the way of the directory "tmp"
 Returns      : the way of the directory
 Args         : $data_dir : directory of the data
 		$kingdom : name of the kingdom of the organism
 		$organism : the name of the organism
 		$strain : the name of the strain
		$replicon : name of the replicon
 Globals      : none

=cut
sub GetDirTmp
{
	my ($data_dir, $kingdom, $organism, $strain, $replicon) = @_;
	my $way = &GetRepliconDir($data_dir, $kingdom, $organism, $strain,$replicon);
	$way = $way."tmp/";
	return $way;
}

=head2 function GetStructureFileName

 Title        : GetStructureFileName
 Usage        : GetStructureFileName()
 Prerequisite : none
 Function     : return the name of the temporary structure file
 Returns      : the name of the temporary structure file
 Args         : none
 Globals      : none

=cut
sub GetStructureFileName
{
	return ("structure");
}
	
=head2 function GetExtensionFiles

 Title        : GetExtensionFiles
 Usage        : GetExtensionFiles($directory, $extension)
 Prerequisite : none
 Function     : return an array of the files which have the extension $extension in the directory $directory
 Returns      : an array of the found files
 Args         : $directory : directory in where looking for the files
 		$extension : the extension of the searched files
 Globals      : none

=cut
sub GetExtensionFiles
{
	my ($directory, $extension) = @_;
	opendir(GBK_DIR, $directory);
	my @a_files = grep { /$extension$/ } readdir(GBK_DIR);
	closedir(GBK_DIR);
	return @a_files;
}

=head2 function TreatSequence

 Title        : TreatSequence
 Usage        : TreatSequence($seq)
 Prerequisite : none
 Function     : extract and return the sequence and the alignment sequence
 Returns      : the sequence and the alignment sequence
 Args         : $seq : string containing the sequence : GG.G...C...C...C..G........U...A...G
 Globals      : none

=cut
sub TreatSequence
{
	my ($seq) = @_;
	# delete the end character and the caracters of the secondary structure : [, ], {, },(,), ^ 
	chomp $seq;
	$seq =~ s/ //g; 
	$seq =~s/\[|\]|\^|\{|\}|\(|\)//g;
	
	# sequence in the alignment format
	my $sequence_alignment = $seq;	# GG.G...C...C...C..G........U...A...G
	# nucleotidic sequence
	$seq =~ s/\.//g;				# GGGCCCGUAG
	$seq =~ s/\-//g;
	# substitution of o by N 
	# usefull for the rRNA which contains this symbol
	$seq =~ s/o/N/g;

	return ($seq, $sequence_alignment);
}


=head2 procedure CreateTreeStructureRNAs

 Title        : CreateTreeStructureRNAs
 Usage        : CreateTreeStructureRNAs($a_rna, $way_directory, $directory_name, $genome_lib, $source, $CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ)
 Prerequisite : none
 Function     : create the tree structure for all the rna 
 Returns      : None
 Args         : $a_rna : an array which contains the rna objects
 		$way_directory : the way where to create the directory
		$directory_name : the name of the directory to create
		$genome_lib : the way of the file uses for a library ; it's necessary to do a wu-blast about the genome
		$source : the name of the bank of the datas
		$CMD_REFORMAT_DB: the command to execute reformat
		$CMD_WU_BLAST : command to execute Blast
		$CMD_FILTER : command to execute a filter
		$IDENTITY : percentage of identity for Blast
		$PCQ : pcq for Blast
				
 Globals      : none

=cut
sub CreateTreeStructureRNAs
{
	my ($a_rna, $way_directory, $directory_name, $genome_lib, $source, $CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ) = @_;
	my $RNA_CPT;

	my $current_directory = $way_directory.$directory_name."/";
	if ( &ExistDirectory($directory_name, $way_directory) == 1) {
		$RNA_CPT = &GetNextDirectoryNumber($current_directory);
	}else{ 
		# create the directory $directory_name in the $way_directory
		&CreateDirectory($directory_name, $way_directory);
		$RNA_CPT = 1;
	}
	
	#go in the created directory of the type of rna
	chdir($current_directory);
	
	# for each rna
	foreach my $rna ( @{$a_rna} ) {
		#creation of the tree structure for the current rna
		$rna->CreateTreeStructureRNA(($RNA_CPT++), $current_directory, $CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ, $genome_lib, $source); 
	}
}


	


###################################################################################################################"
#  Functions created with the statistics - 27 juil.
###################################################################################################################"

=head2 procedure CreateFile

 Title        : CreateFile
 Usage        : CreateFile($file_name, $file_dir, $data)
 Function     : create a file named $file_name in the directory $file_dir, which contains the data $data
 Returns      : None
 Args         : $file_name : name of the file to create
 		$file_dir : directory where creating the file
		$data : content of the file		
 Globals      : none

=cut
sub CreateFile
{
	my ($file_name, $file_dir, $data) = @_;
	open(FILE, ">".$file_dir.$file_name);
	print FILE $data;
	close(FILE);
}

=head2 procedure UpdateFile

 Title        : UpdateFile
 Usage        : UpdateFile($file_name, $file_dir, $data)
 Function     : update a file named $file_name in the directory $file_dir, add the data $data at the end
 Returns      : None
 Args         : $file_name : name of the file to update
 		$file_dir : directory where is the file
		$data : content to add at the end of the file	
 Globals      : none

=cut
sub UpdateFile
{
	my ($file_name, $file_dir, $data) = @_;
	if (-e $file_dir.$file_name) {
		open(FILE, "+<".$file_dir.$file_name);
		seek(FILE,0,2);
		print FILE $data;
		close(FILE);
	}else {
		die "File $file_dir.$file_name doesn't exist !\n";
	}
}

=head2 function CreateAndOpenFile

 Title        : CreateAndOpenFile
 Usage        : CreateAndOpenFile($file_name, $file_dir)
 Function     : create a file named $file_name in the directory $file_dir
 		and return its descriptor (the file stays opened)
 Returns      : the descriptor of the created file
 Args         : $file_name : name of the file to create
 		$file_dir : directory where creating the file
 Globals      : none

=cut
sub CreateAndOpenFile
{
	my ($file_name, $file_dir) = @_;
	my $file_desc = new IO::File(">".$file_dir.$file_name) or die "Can't create $file_name in $file_dir\n";
	return $file_desc;
}

=head2 procedure UpdateOpenedFile

 Title        : UpdateOpenedFile
 Usage        : UpdateOpenedFile($file_desc, $data)
 Function     : update a file which has the descriptor $file_desc ; add the data $data at the end
 Returns      : None
 Args         : $file_desc : descriptor of the file
		$data : data to add at the end of the file		
 Globals      : none

=cut
sub UpdateOpenedFile
{
	my ($file_desc, $data) = @_;
	print $file_desc $data;
	
}

=head2 procedure DeleteFile

 Title        : DeleteFile
 Usage        : DeleteFile($file_way)
 Function     : delete the file $file_way
 Returns      : None
 Args         : $file_way : way of the file	
 Globals      : none

=cut
sub DeleteFile
{
	my ($file_way) = @_;
	unlink($file_way) or die "ERROR when deleting the file $file_way\n";
}



=head2 function GetPrefixFiles

 Title        : GetPrefixFiles
 Usage        : GetPrefixFiles($directory, $prefix)
 Prerequisite : none
 Function     : return an array of the files which have the prefix $prefix in the directory $directory
 Returns      : an array of the found files
 Args         : $directory : directory in where looking for the files
 		$prefix : the prefix of the searched files
 Globals      : none

=cut
sub GetPrefixFiles
{
	my ($directory, $extension) = @_;
	opendir(GBK_DIR, $directory);
	my @a_files = grep { /^$extension/ } readdir(GBK_DIR);
	closedir(GBK_DIR);
	return @a_files;
}


=head2 function GetCombination

 Title        : GetCombination
 Usage        : GetCombination($length, $ra_alphabet)
 Prerequisite : none
 Function     : return an array composed of all the words that we can create with letters of $ra_alphabet 
 		with the length $length
 		Example : $ra_alphabet refers to (A, C, G, T), $length = 2 
		=> AA, AC, AG, AT, CA, CC, CG, CT, GA, GC, GG, GT, TA, TC, TG, TT 
 Returns      : an array composed of all the words that we can create with letters of $ra_alphabet 
 		with the length $length
 Args         : $length : length of the searched words
 		$ra_alphabet : reference of an array of the alphabet
 Globals      : none
=cut 	
sub GetCombination
{
	my ($length, $ra_alphabet) = @_;
	if ($length == 1) {
		return @{$ra_alphabet};
	}else {
		my @a_end = &GetCombination(($length-1), $ra_alphabet);
		my @a_res;
		for ( my $i = 0 ; $i <= $#$ra_alphabet ; $i++) {
			for (my $j = 0 ; $j <= $#a_end ; $j++) {
				push(@a_res, $ra_alphabet->[$i].$a_end[$j]);
			}
		}
		return (@a_res);
	}
}


1
