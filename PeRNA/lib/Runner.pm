#
# $Id: Runner.pm,v 1.1.1.1 2007-07-19 08:25:30 cros Exp $
#

package Runner;

=pod

=head1 NAME

 Runner - Run an external program with timeout and TERM signals
          Run an external program without waiting for its death

 This package is NOT an object oriented package
 
=head1 EXPORTED FUNCTIONS

=head2 RunExt

 my ($status, $killed) = RunExt(-cmd $cmd,
                                 -stdin  $stdin,
				                 -stdout $stdout,
				                 -stderr $sterr,
				                 -timeout $timeout);

 PARAMETERS:

   -cmd     = The external command whith its parameters.
   -stdin   = The standard input, if redirected   (a file name, or anything you can pass to open)
   -stdout  = The standard output, if redirected  (a file name, or anything you can pass to open)
   -stderr  = The standard error, if redirected   (a file name, or anything you can pass to open)
   -timeout = The timeout in seconds. No timeout if -1


 RETURNED VALUES:
   $status  ==> the return value of the child
   $timeout ==> 0 if OK
               -1 if the child was killed by timeout
			   -2 if the child was killed by TERM (ctrl-C, qdel, ...)

 The cmd is executed, it may be killed if a TERM signal or an ALMR is received
 The child is then killed with a TERM, followed by a KILL signal: it should return in any circumstance
 
=head2 RunExtNoWait

 my $pid = RunExtNoWait(-cmd $cmd,
                        -stdin  $stdin,
				        -stdout $stdout,
				        -stderr $sterr,
				        -timeout $timeout);

 PARAMETERS:

   -cmd     = The external command whith its parameters.
   -stdin   = The standard input, if redirected   (a file name, or anything you can pass to open)
   -stdout  = The standard output, if redirected  (a file name, or anything you can pass to open)
   -stderr  = The standard error, if redirected   (a file name, or anything you can pass to open)


 RETURNED VALUE:
   $pid ==> the process Id of the child

 The cmd is executed, but the sub returns without waiting for the child's death.
 The children started may be killed if a TERM signal or an ALARM signal are received
 The child is then killed with a TERM signal. NO KILL signal is sent to the child, 
 terminating the child in a graceful way. If I don't have to wait, I am less violent...
			   
=cut

use strict;
use warnings;
use Carp;
use IO::File;

BEGIN
{
    use Exporter ();
    use vars qw($VERSION @ISA @EXPORT);

    our $VERSION = do {my @r = (q$Rev: 6 $ =~ /\d+/g); $r[0]};
    @ISA         = qw(Exporter);
    @EXPORT      = qw(&RunExt &RunExtNoWait);
    $SIG{'TERM'} = \&KillProcess;
    $SIG{'ALRM'} = \&KillProcess;
}

our $RUN_PID   = 0;     # Only one process ran by RunExt
our @RUN_PID   = ();    # Many processes ran by RunExtNoWait
our $KILLED_BY = '';

#
# EXPORTED function
#

sub RunExt
{
    my %args = @_;
    croak("Runner.pm internal error (RUN_PID $RUN_PID)") unless ($RUN_PID == 0);
    croak("The cmd to run is NOT specified")             unless (exists($args{'-cmd'}));

    if ($KILLED_BY eq 'TERM')
    {
        return (0, -2);    # a TERM signal was received JUST BEFORE calling RunExt
    }
    else
    {
        $KILLED_BY = '';
    }

    my @arguments = split(' ', $args{'-cmd'});

    if (exists $args{'-timeout'})
    {                      # Program a timeout, if necessary
        my $timeout = $args{'-timeout'} + 0;
        if ($timeout > 0)
        {
            alarm($args{'-timeout'});
        }
    }
    $RUN_PID = fork();

    if ($RUN_PID == 0)
    {                      # T H E   C H I L D
        my ($path, @arguments) = split(' ', $args{'-cmd'});
        if (exists $args{'-stdin'})
        {
            close(STDIN);
            my $stdin = $args{'-stdin'};
            open(STDIN, $stdin) or die "CANNOT OPEN $stdin";
        }
        if (exists $args{'-stdout'})
        {
            close(STDOUT);
            my $stdout = $args{'-stdout'};
            open(STDOUT, $stdout) or die "CANNOT OPEN $stdout";
        }
        if (exists $args{'-stderr'})
        {
            close(STDERR);
            my $stderr = $args{'-stderr'};
            open(STDERR, $stderr) or die "CANNOT OPEN $stderr";
        }
        exec($path, @arguments) || croak("$path not found");
    }

    wait();    # T H E   P A R E N T
    my $sts = $?;
    $RUN_PID = 0;

    if ($KILLED_BY eq 'ALRM')
    {          # The child was killed by a timeout
        return (0, -1);
    }
    elsif ($KILLED_BY eq 'TERM')
    {          # The child was killed by a TERM signal (qdel)
        return (0, -2);
    }
    else
    {          # The child returned, with or without an error
        return ($sts, 0);
    }
}

#
# EXPORTED function
#

sub RunExtNoWait
{
    my %args = @_;
    croak("The cmd to run is NOT specified") unless (exists($args{'-cmd'}));

    if ($KILLED_BY eq 'TERM')
    {
        return 0;    # a TERM signal was received JUST BEFORE calling RunExtNoWait
    }
    else
    {
        $KILLED_BY = '';
    }

    my @arguments = split(' ', $args{'-cmd'});

    my $run_pid = fork();

    if ($run_pid == 0)
    {                # T H E   C H I L D
        my ($path, @arguments) = split(' ', $args{'-cmd'});
        if (exists $args{'-stdin'})
        {
            close(STDIN);
            my $stdin = $args{'-stdin'};
            open(STDIN, $stdin) or die "CANNOT OPEN $stdin";
        }
        if (exists $args{'-stdout'})
        {
            close(STDOUT);
            my $stdout = $args{'-stdout'};
            open(STDOUT, $stdout) or die "CANNOT OPEN $stdout";
        }
        if (exists $args{'-stderr'})
        {
            close(STDERR);
            my $stderr = $args{'-stderr'};
            open(STDERR, $stderr) or die "CANNOT OPEN $stderr";
        }
        exec($path, @arguments) || croak("$path not found");
    }

    push(@RUN_PID, $run_pid);

    return $run_pid;

}

#
# Title: KillProcess
# Usage: kill the process whose PID is in $RUN_PID.
#        Send him a TERM signal, then a KILL signal
#        kill the processes whose PID are in @RUN_PID, and remove them from @RUN_PID
#        Send them ONLY a TERM signal (less violence...).
#        Called by the ALRM and the TERM signal handlers
#

sub KillProcess
{
    my $signame = shift;
    if ($RUN_PID != 0)
    {
        kill 'TERM', $RUN_PID;
        kill 'KILL', $RUN_PID;

        #	warn "$$ killed $RUN_PID";
    }
    while (my $pid = shift(@RUN_PID))
    {
        kill 'TERM', $pid;

        #  	warn "$$ killed $pid";
    }
    $KILLED_BY = $signame;
}

1;

