#!/usr/local/bin/perl

=pod 
=head1 NAME

StockRibosomalData.pl - This program extracts the RNA from the european ribosomal rna database

=head1  SYNOPSIS 

./StockRibosomalData.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome --accession_number $PeRNA/external_data/Bacteria/Escherichia_coli_K12/chromosome/U00096.ac --rna_file $PeRNA/external_data/Bacteria/Escherichia_coli_K12/chromosome/U00096.lsu --rna_family_name lsu 
	
=head1 DESCRIPTION

StockRibosomalData.pl extracts RNA from the european ribosomal rna database (in the rna_file)

for each rna, it creates the specific tree structure (in the directory knownRNA of the organism tree structure)
    data
     <organism>
	<replicon>
           knownRNA
	      <rna_family_name>
		  RNA1
		  RNA2
		       RNA2.fna
		       structure
		       information
		       tmp
	
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use Rna;


our $DATA_DIR; 
our $PCQ;
our $IDENTITY;

MAIN:
{
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s","accession_number=s","rna_file=s", "rna_family_name=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$DATA_DIR   		= $rh_param->Get('DATA_DIR');
	my $CMD_REFORMAT_DB	= $rh_param->Get('CMD_REFORMATDB');
	my $CMD_WU_BLAST	= $rh_param->Get('CMD_WU_BLAST');
	my $CMD_FILTER		= $rh_param->Get('CMD_FILTER');
	$PCQ			= 95;
	$IDENTITY		= 98;
	
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $accession_numbers 	= $rh_param->Get('accession_number');
	my $rna_file		= $rh_param->Get('rna_file');
	my $rna_family_name  	= $rh_param->Get('rna_family_name');
	my @a_set_rna;	

	@a_set_rna		= &CreateRibosomalRNA($rna_file, $accession_numbers, $organism, $strain, $rna_family_name);
	if ( $#a_set_rna >= 0) {		
		my $known_rna_way	= Rna::GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon);
		my $tmp_way 		= Rna::GetDirTmp($DATA_DIR, $kingdom, $organism, $strain, $replicon);
		my $genome_lib		= $tmp_way.Rna::GetNameFormatedFastaFile($organism, $strain, $replicon);
	
		#save of the ribosomal rna data 
		Rna::CreateTreeStructureRNAs(\@a_set_rna, $known_rna_way, $rna_family_name, $genome_lib, 
		"europeanrRNAdb", $CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ);
	}
}


=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		-kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--accession_number	: file which contains the names of the accession numbers of the strain of the organism
		--rna_file 	: the file of the european rRNA database
		--rna_family_name	: the name of the rna family
		
END
}


=head2 function CreateRibosomalRNA

 Title        : CreateRibosomalRNA
 Usage        : CreateRibosomalRNA($rrna_file, $acc_nb_file, $organism, $strain, $rna_family)
 Prerequisite : the rrna_file in parameter contains one family of RNA for one organism
 		for instance, all the ssu RNA for Escherichia Coli
 Function     : Create the RNAs containing in the rrna_file ; the file contains the structure of the rna  
 Returns      : return a set of rna in a hashtable
 Args         : $rrna_file : file containing the secondary structure of the rnas in the printable 
		alignment format in the european rna database
					 U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli O157:H7 H 
 					 U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli A AE000460
					 U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli A M87049
					 - - - - - - - 1 - - - - - - - - - - - - -2- - - - - -1' - - - - - - -  Helix numbering pro                     

 					- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli O157:H7 H 
 					- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli A AE000460
 					- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli A M87049
					 - - -3- - - - - - - - - - - - - - - - -4- - - - - - - - - - - - - - -  Helix numbering pro 
		$acc_nb_file : file which contains the accession numbers of the searched organsim
		$organism : the name of the organism of these rna
		$strain : the searched strain of the organism
		$rna_family : the name of the rna family (lsu or ssu)                    

 Globals      : none

=cut 


sub CreateRibosomalRNA
{
	my ($rrna_file, $acc_nb_file, $organism, $strain, $rna_family) = @_;						
	my @a_rnas;		# an array of the created rnas
	my $ra_seq_ok;		# set of sequence numbers which belongs to the searched strain
	my $ra_acc_ok;		# set of accession numbers
	
	my $in 		= new IO::File($rrna_file) or die "Can't open $rrna_file";
	my @a_rrna 	= <$in>;
	
	# searches the numbers of the sequences which belongs to the organism and the accession numbers
	($ra_seq_ok, $ra_acc_ok) = SelectSequences(\@a_rrna, $acc_nb_file);
	
	# create an array of RNA objects
	@a_rnas = &CreateSelectedRNA($ra_seq_ok, \@a_rrna, $organism, $strain, $rna_family, $ra_acc_ok);
	return (@a_rnas);
	
}


=head2 function SelectSequences

 Title        : SelectSequences
 Usage        : SelectSequences($a_rrna, $contig_file)
 Prerequisite : None
 Function     : Select the numbers of the sequences which belong to the organism. 
 		See the accession number at the end of each line and, if it belongs to the file 
 		$contig_file or $contog_file.Ribosomal, the number of the line is added to the array
 Returns      : return a ref about an array composed of the numbers of the lines 
 		which contain a RNA from the searched organism, 
		and a ref about an array which contains the accession numbers
 Args         : $a_rrna : reference about the array containing the ribosomal rnas in this format :
			U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli O157:H7 H 
 			U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli A AE000460
			U G A A[G A - G U U]U - - - - - G A[U - C A U]-[G - G C U C]- A[G A U  Escherichia coli A M87049
			- - - - - - - 1 - - - - - - - - - - - - -2- - - - - -1' - - - - - - -  Helix numbering pro                     

 			- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli O157:H7 H 
 			- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli A AE000460
 			- U{G}A A - - C G C U]- - - G[G C G - G C A - G - G]C - - - - C - U A  Escherichia coli A M87049
			- - -3- - - - - - - - - - - - - - - - -4- - - - - - - - - - - - - - -  Helix numbering pro 
		$contig_file : file which contains the accession numbers of the searched organsim                 
 Globals      : none

=cut 

sub SelectSequences
{
	my ($a_rrna, $contig_file) = @_;
	my @a_selected_seq_nb;	# set of the number of selected sequences
	my @a_selected_seq_acc;	# set of the accession numbers od selected sequences
	
	my @a_data;
	my $acc_number;
	
	# array of the data about the ribosomal rnas
	my @a_struct = @{$a_rrna};
 
	# open the file containing accession numbers of the organism
	open(CONTIG_FILE,"<$contig_file") || die "Problem to open file $contig_file ";
	# save all the lines in an array
	my @a_contig_acc_nb = <CONTIG_FILE>;
	close(CONTIG_FILE);
	
	# open the file containing accession numbers found in the ribosomal files : <$contig_file>.Ribosomal
	my $ribosomal_ac_file_name = $contig_file.".Ribosomal";
	if ( open(AC_RIBOSOMAL_FILE,"<$ribosomal_ac_file_name") ) {
		# add all the lines in the array
		push(@a_contig_acc_nb,<AC_RIBOSOMAL_FILE>);
		close(AC_RIBOSOMAL_FILE);
	} else {
		print "WARNING: no file $ribosomal_ac_file_name !\n";
	}
	
	# delete the empty lines at the begin of the array of the sequences
	DeleteEmptyLines(\@a_struct);
	my $sequence_nb = GetTextSize(\@a_struct);
	
	# for each line, search if the accession number at the end belongs to the contig_file
	for ( my $i = 0 ; $i < ($sequence_nb-1) ; $i++){
		# extract the accession number of the line
		@a_data 	= split(/ /,$a_struct[$i]);
		$acc_number	= $a_data[$#a_data];
		chomp($acc_number);

		# search the accession number in the contig accession number array
		if ( grep(/$acc_number/, @a_contig_acc_nb) != 0 ) {
			push (@a_selected_seq_nb, $i); 		# save the number of the line
			push (@a_selected_seq_acc,$acc_number);	# save the accession number
		}
	}
	return (\@a_selected_seq_nb,\@a_selected_seq_acc);
}


=head2 function CreateSelectedRNA

 Title        : CreateSelectedRNA
 Usage        : CreateSelectedRNA($a_num_seq, $a_data, $organism, $strain, $rna_family, $ra_acc_numbers)
 Prerequisite : none
 Function     : Create the RNA which belongs to the strain : which have a number in the array @seq_ok
 Returns      : return a set of Rna objects 
 Args         : $a_num_seq : an array containing gthe numer of the choosen rna
 		$a_data : array containing the ribosomal RNA
		$organism : the name of the organism of these rna
		$strain : the searched strain of the organism 
		$rna_family : name of the rna family (ssu / lsu)
		$ra_acc_numbers : ref about an array which contains the accession numbers of the sequences
 			
 Globals      : none

=cut 
sub CreateSelectedRNA
{
	my ($a_num_seq, $a_data, $organism, $strain, $rna_family, $ra_acc_numbers) = @_;
	my @lines;		# array containing the structure data of the rnas
	my $line;		# one line
	
	my $textSize;		# size of a part of the text
	my @a_struct_2D;	# array which contains the name of an helice and its position in the complete sequence 
				#[[1, 18][1', 25][2, 35]]
	
	my @a_sequences ;	# array which contains the entiere structure sequence for each rna

	my $nb_lines_per_seq;
	my $index_choosen_seq;
	
	# 1 - extraction of the sequences and of the consensus structure
	
	# delete the empty lines at the begin of the array
	DeleteEmptyLines($a_data);
	$textSize 	= &GetTextSize($a_data);
	@a_struct_2D 	= &GetHelixPositions($a_data,$textSize); # get the helix name and their position
	
	#delete the end of each line : the name of the specie
	DeleteSpecieName($a_data);

	@lines 			= @{$a_data};
	$nb_lines_per_seq 	= ($#lines+1)/($textSize -1);

	# concatenation of the parts of each sequence
	for ( my $i=0 ; $i <= $#{$a_num_seq} ; $i++ ) {
		$a_sequences[$i] 	= "";
		$index_choosen_seq 	= ${$a_num_seq}[$i];	# the number of the sequence that we want to treate
		
		for ( my $j = 0 ; $j <= $nb_lines_per_seq ; $j++) {
			$line = $lines[$j*($textSize-1)+$index_choosen_seq];
			if ($line) {
				$a_sequences[$i] = $a_sequences[$i].$line;
			}
		}
	}
	
	
	# 2 - creation of the rna objects
	my $seq;
	my $seq_alignment;
	my $r_couples;
	my $rna1;
	my @a_rnas;		# array containing the created rna objects
	
	# for each sequence
	for ( my $i = 0 ; $i <= $#a_sequences ; $i++ ) {
		($seq,$seq_alignment)	= Rna::TreatSequence($a_sequences[$i]); # the nucleotide sequence and the alignment sequence
		$r_couples 		= &CreateStructOneSeq($a_sequences[$i],\@a_struct_2D, $seq); # search the secondary structure
		$rna1 			= Rna->New($seq,$seq_alignment, 
					$organism, $strain, $rna_family, ${$ra_acc_numbers}[$i]); # creation of the rna
		$rna1->SetStructure($r_couples); # save its structure
		push(@a_rnas, $rna1);  # save in the set on rna
	}
	
	return @a_rnas;
}


=head2 function CreateStructOneSeq

 Title        : CreateStructOneSeq
 Usage        : CreateStructOneSeq( $seq,\@a_struct_2D, $primary_seq);
 Prerequisite : none
 Function     : create the structure 2D for the sequence according to the pos_helices
 Returns      : an array composed of couple of position of nucleotids which are paired together
 Args         : $seq : the sequence  U G A A[G A - G U U]U - - G A[U - C A U]
 		@a_struct_2D : array containing the name and the position of each helice
		$primary_seq : the sequence without structure characters UGAAGAGUUUGAUCAU
 Globals      : none

=cut

sub CreateStructOneSeq
{
	my ($seq,$a_struct_2D, $primary_seq) = @_;
	my $length_seq = length($seq);	# length of the sequence
	
	my $nucleotid;			# one nucleotid
	my $state_save_helice = 0;	# the state of the reading : 1 if we have to save the current nucleotid in a helice, else 0
	my $current_name_helice ="";	# the name of the current treated helice
	my @a_current_helice;		# array which contains the positions of the nucleotids of the current helice
	my %h_helices;			# hastable which contains, for each helice, the positions of its nucleotids
	my $pos_nucleotidic_seq = 0;	# current position in the nucleotidic sequence

	# for each nucleotid
	for ( my $i = 0 ; $i < $length_seq ; $i++ ) {
		$nucleotid = substr($seq, $i, 1);
		if ( $nucleotid eq '[' ){
			$state_save_helice = 1;
			# search the name of the helice
			$current_name_helice = &GetHelixName($i, $a_struct_2D);
		} elsif ($nucleotid eq ']') {
			$state_save_helice = 0;
			# save the current helice
			$h_helices{$current_name_helice} =  [ @a_current_helice ];
			# reinit
			$current_name_helice = '';
			@a_current_helice = ();
		} elsif ($nucleotid eq '{') {
			$state_save_helice = 0;
		} elsif ($nucleotid eq '}') {
			$state_save_helice = 1;
		} elsif ($nucleotid eq '^') {
			# save the current helice
			$h_helices{$current_name_helice} =  [@a_current_helice];
			# reinit
			$current_name_helice = &GetHelixName($i, $a_struct_2D);
			@a_current_helice = ();
		} elsif ($nucleotid eq '-') {
		} elsif ( ($nucleotid ne '(') && ($nucleotid ne ')') && ($nucleotid ne ' ') ){
			# case it's a letter
			$pos_nucleotidic_seq++;
			#if it's an helice
			if ( $state_save_helice == 1 )
			{
				# save the nucleotid in the current helice
				push(@a_current_helice,$pos_nucleotidic_seq);
			}
		}
	}
	
	# compute pairs
	my $ra_couple = &ComputePair(\%h_helices, $primary_seq);
	
	return $ra_couple;
}



=head2 function ComputePair

 Title        : ComputePair
 Usage        : ComputePair($rh_helices, $primary_seq)
 Prerequisite : none
 Function     : extract the pairs of the hashtab composed of 1/2 helices. 
 		for each 1/2 helice, search its second 1/2 helice and create the pairs with the two parts
 Returns      : a reference about an array composed of the pairs
 Args         : $rh_helices : ref about an hastab composed of 1/2 helices : key : the name of a 1/2 helice, values : the nucleotids of the 1/2 helice
 		$primary_seq : the sequence of the rna
 Globals      : none

=cut
sub ComputePair{
	
	my ($rh_helices, $primary_seq) = @_;
	my @a_couple; 	# set of paired nucleotid
	my $pos1 ;
	my $pos2 ;
	my $first_nucl;
	my $second_nucl;
	my $helice_len;
	my @a_hel_sec_part;
	
	# creation of the pair
	foreach my $helice ( keys %{$rh_helices} ) {
		# if it's a first part on a helice : h
		if ( substr($helice, -1 ) ne '\'' ){
			# if there is a complementary part of the demi-helice =  h`
			if ( exists( $rh_helices->{$helice."\'"} ) ) {
				@a_hel_sec_part = @{$rh_helices->{$helice."\'"}} ;
				# for each nucleotid of the demi-helice : creation of the couple
				$helice_len = $#{$rh_helices->{$helice}};
				for ( my $i = 0 ; $i <= $helice_len ; $i++) {
					$pos1 		= @{$rh_helices->{$helice}}[$i];
					$pos2 		= $a_hel_sec_part[$helice_len-$i];
					$first_nucl	= substr($primary_seq,($pos1-1),1);
					$second_nucl	= substr($primary_seq,($pos2-1),1);
					
					# if the two nucleotids can be paired (A-U or G-C...)
					if ( Rna::ArePaired($first_nucl, $second_nucl) ) {
						push(@a_couple, [($pos1, $pos2)] );
					}
				}
			}
			else {
				print "ERROR: helix $helice doesn't paired in the structure of the RNA !\n";
			}
		} 
	}
	return \@a_couple;
}

=head2 function GetHelixName

 Title        : GetHelixName
 Usage        : GetHelixName($position, $a_struct_2D);
 Prerequisite : none
 Function     : return the name of the helix in which the nucleotide number $i belongs
 Returns      : the name of the helix in which the nucleotide number $i belongs
 Args         : $position : the position of the nucleotide
 				$a_struct_2D : array containing the name and the position of the helix
 Globals      : none

=cut
sub GetHelixName
{
	my ($position, $a_struct_2D) = @_;
	my $helix = 0;	# number of the helix
	# position of the current helix
	my $i = $a_struct_2D->[$helix][1];
	
	# while the position of the current helix is smaller than the searched position
	while ( $i < $position ) {
		$helix++;	# next helix
		$i =  $a_struct_2D->[$helix][1];
	}
	return  $a_struct_2D->[$helix][0];
	 
}



######################################################################################################"
#  procedure from J-Benoit
###########################################################################################################"


=head2 procedure DeleteEmptyLines

 Title        : DeleteEmptyLines
 Usage        : DeleteEmptyLines(\@array);
 Prerequisite : none
 Function     : delete the empty lines in the begin of the array
 Returns      : none
 Args         : $array : the array in which we want to delete the empty lines
 Globals      : none

=cut
sub DeleteEmptyLines {

  while($_[0]->[0] eq "\n")
    {
      #delete the empty line
      shift@{$_[0]};
    }
}


=head2 function GetTextSize

 Title        : GetTextSize
 Usage        : GetTextSize(\@array);
 Prerequisite : none
 Function     : return the size of a text part : until a \n
 Returns      : none
 Args         : $array : ref about the array which contains the content of a file
 Globals      : none

=cut
sub GetTextSize {
	my $size = 0;
	while($_[0]->[$size] ne "\n")
	{
		$size++;
	}
	return $size;
}


=head2 function GetHelixPositions

 Title        : GetHelixPositions
 Usage        : GetHelixPositions(\@lines,$size);
 Prerequisite : none
 Function     : find the position of each helice
 Returns      : a 2D array composed of couple (helixName, position in the complete sequence) 
 Args         : $lines : an array containing the lines of the file about the ribosomal RNA
 		$size : the number of lines which compose a set of sequences
 Globals      : none

=cut

sub GetHelixPositions {
	my $i;
	my $tmp;			# temporary string to find the helix names
	my $lignesHelix;		# the line of the helix numbers - - - - 1 - - 1'- - 2 - -
	my @nomsHelix;		# an array containing the name of all the helix
	my @Helix;			# an array containing the name of a helix and its position
	my $position = -1;  		# position of the part of a helix
	
	#get all the data about the helix : - - - - - - - 1 - - - - - - - - - - - - -2- - - - - -1' - - - - - -
	for($i = ($_[1]-1) ; $i <= $#{$_[0]} ; $i+=($_[1]+1) )  { 
		$lignesHelix = $lignesHelix.substr($_[0]->[$i],0,index($_[0]->[$i],'  Helix')); 
	}
	
	# save the name of the helix
	$tmp = $lignesHelix;
	$tmp =~ s/-/ /g;
	$tmp =~ s/^\s+//g;
	@nomsHelix = split('\s+',$tmp);

	# find the positions of each helix
	$lignesHelix=~ s/ /-/g;
	foreach $i (@nomsHelix) {
		$position = -1;
		if ( index($lignesHelix, "-".$i) != -1){
			$position = index($lignesHelix, "-".$i);
		}
		else {
			print "ERROR : position of an helix wasn't find !! \n"
		}
		# save the position in the array
		push(@Helix, [$i, ($position+1)]);
	}
    
	for( $i=($_[1]-1) ; $i <= $#{$_[0]} ; $i+=($_[1]-1) ){
		splice(@{$_[0]},$i,2);
	}
	
	return(@Helix);
  
}

=head2 function DeleteSpecieName

 Title        : DeleteSpecieName
 Usage        : DeleteSpecieName(\@lines);
 Prerequisite : none
 Function     : delete the name of the species in each line of the array
 Returns      : the same array but without the name of the species at the end of each line 
 Args         : $lines : an array containing the lines of the file about the ribosomal RNA
 Globals      : none

=cut

sub DeleteSpecieName {
	my $i;
	for( $i=0 ; $i<= $#{$_[0]} ; $i++){
		$_[0]->[$i] = substr($_[0]->[$i], 0, index($_[0]->[$i],'  '));
    }
}
