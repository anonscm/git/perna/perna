#!/usr/bin/perl

package Rna;

=pod 
=head1 NAME

Rna - This program is the class RNA

=head1  SYNOPSIS 

Rna.pm

=head1 DESCRIPTION

Rna.pm is the class representing a RNA
it contains the sequence, its structure, the name of its organism and the strain of its organism


=cut


our @list;


#list of rna
BEGIN { 
	@list=();
}
  

use strict;
use ToolBox;
use DnaToolBox;
use Time::localtime;



#global variable
our $STRANDPLUS = "+";
our $STRANDLESS = "-";

our $CMD_REFORMAT 	= "";
our $CMD_WU_BLAST 	= "";
our $CMD_FILTER 	= "";
our $IDENTITY;
our $PCQ;

use vars qw($UNIXMKDIR);

=head1 SUBROUTINES

=cut

=head2 function New

 Title        : New
 Usage        : New($sequence, $sequence_alignment, $organism, $strain)
 Prerequisite : none
 Function     : Creates a reference on a RNA
 Returns      : the reference of the created RNA
 Args         : $sequence : the sequence of the RNA
 		$sequence_alignment : the sequence in the alignment format : A---GU--T
		$organism : the name of the organism
 		$strain : strain of the organism
		$family : the family of the rna
		$acc_number : the accession number of the rna
 Globals      : 

=cut 

sub New {
	my ($class,$sequence, $sequence_alignment, $organism, $strain, $family, $acc_number) = @_;
	my $obj_ptr = {}; 
	$obj_ptr->{'sequence'}		= $sequence;
	$obj_ptr->{'sequence_alignment'}= $sequence_alignment;
	$obj_ptr->{'organism'}		= $organism;
	$obj_ptr->{'strain'}		= $strain;
	$obj_ptr->{'family'}		= $family;
	$obj_ptr->{'acc_number'}	= $acc_number;
	bless $obj_ptr;                    # reference of classe
	push(@list,$obj_ptr);
	return $obj_ptr;
}



=head2 function GetSequence

 Title        : GetSequence
 Usage        : GetSequence()
 Prerequisite : None
 Function     : return the sequence of RNA
 Returns      : return the sequence of RNA
 Args         : None
 Globals      : None
 
=cut 


sub GetSequence {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'sequence'});
}



=head2 function GetSequenceAlignment

 Title        : GetSequenceAlignment
 Usage        : GetSequenceAlignment()
 Prerequisite : None
 Function     : return the sequence of the alignment
 Returns      : return the sequence of the alignment
 Args         : None
 Globals      : None
=cut 

sub GetSequenceAlignment {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'sequence_alignment'});
}


=head2 function GetBeginPos

 Title        : GetBeginPos
 Usage        : GetBeginPos()
 Prerequisite : None
 Function     : return the begin position of the sequence about the genome
 Returns      : return the begin position of the sequence about the genome
 Args         : None
 Globals      : None
 

=cut 

sub GetBeginPos {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'pos_begin'});
}

=head2 function GetEndPos

 Title        : GetEndPos
 Usage        : GetEndPos()
 Prerequisite : None
 Function     : return the end position of the sequence about the genome
 Returns      : return the end position of the sequence about the genome
 Args         : None
 Globals      : None
=cut 

sub GetEndPos {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'pos_end'});
}


=head2 function GetOrganism

 Title        : GetOrganism
 Usage        : GetOrganism()
 Prerequisite : None
 Function     : return the organism
 Returns      : return the organism
 Args         : None
 Globals      : None
=cut 

sub GetOrganism {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'organism'});
}

=head2 function GetStrain

 Title        : GetStrain
 Usage        : GetStrain()
 Prerequisite : None
 Function     : return the strain
 Returns      : return the strain
 Args         : None
 Globals      : None
 

=cut 

sub GetStrain {
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'strain'});
}

=head2 function GetStrandPlus

 Title        : GetStrandPlus
 Usage        : GetStrandPlus()
 Prerequisite : None
 Function     : return the global variable STRANDPLUS
 Returns      : STRANDPLUS
 Args         : None
 Globals      : None
 

=cut 

sub GetStrandPlus
{
	shift;  
    return $STRANDPLUS;
}

=head2 function GetStrandLess

 Title        : GetStrandLess
 Usage        : GetStrandLess()
 Prerequisite : None
 Function     : return the global variable STRANDLESS
 Returns      : STRANDLESS
 Args         : None
 Globals      : None
 

=cut 
sub GetStrandLess
{
	shift;
	return $STRANDLESS;
}


=head2 function GetStructure

 Title        : GetStructure
 Usage        : GetStructure()
 Prerequisite : None
 Function     : return the rna structure 
 Returns      : the rna structure : a 2 dimensional array of couples of paired nucleotides
 Args         : None
 Globals      : None
 

=cut 
sub GetStructure
{
	my ($obj_ptr) = @_;
    return (@{$obj_ptr->{'structure'}});
}

 
 

=head2 procedure SetStructure

 Title        : SetStructure
 Usage        : SetStructure($struct)
 Prerequisite : None
 Function     : set the structure of the rna.
				Example : (1,12)(2,11) the nucleotides 1 and 12, 2 and 11, are paired.	
 Returns      : None
 Args         : $struct an array containing the pairs
 Globals      : none

=cut

sub SetStructure
{
	my ($obj_ptr,$struct) = @_;
	$obj_ptr->{'structure'} = $struct;
}

=head2 procedure SetFamily

 Title        : SetFamily
 Usage        : SetFamily($family)
 Prerequisite : None
 Function     : set the family name of the rna
 Returns      : None
 Args         : $family ; the name of the family
 Globals      : none

=cut
sub SetFamily
{
	my ($obj_ptr,$family) = @_;
	$obj_ptr->{'family'} = $family;
}


=head2 function GetFamily

 Title        : GetFamily
 Usage        : GetFamily()
 Prerequisite : None
 Function     : return the family
 Returns      : return the family
 Args         : None
 Globals      : None
 

=cut 

sub GetFamily{
    my ($obj_ptr) = @_;
    return ($obj_ptr->{'family'});
}


=head2 procedure SetInformation

 Title        : SetInformation
 Usage        : SetInformation($info)
 Prerequisite : None
 Function     : save the information about the rna
 Returns      : None
 Args         : $info : a string composed of inforation about the rna
 Globals      : none

=cut

sub SetInformation
{
	my ($obj_ptr,$info) = @_;
	$obj_ptr->{'information'} = $info;
}

=head2 procedure AddInformation

 Title        : AddInformation
 Usage        : AddInformation($info)
 Prerequisite : None
 Function     : add the information about the rna
 Returns      : None
 Args         : $info : a string composed of inforation about the rna
 Globals      : none

=cut

sub AddInformation
{
	my ($obj_ptr,$info) = @_;
	$obj_ptr->{'information'} = $obj_ptr->{'information'}.$info;
}


=head2 function GetInformation

 Title        : GetInformation
 Usage        : GetInformation()
 Prerequisite : None
 Function     : return the information about the rna
 Returns      : the information about the rna
 Args         : None
 Globals      : None
 
=cut 
sub GetInformation
{
	my ($obj_ptr) = @_;
	return ( $obj_ptr->{'information'});
}

=head2 procedure PrintRNA

 Title        : PrintRNA
 Usage        : PrintRNA()
 Prerequisite : None
 Function     : print the RNA information 
 Returns      : None
 Args         : None
 Globals      : None
 
=cut 

sub PrintRNA {
	my ($obj_ptr) = @_;
	print $obj_ptr->{'sequence'};
	print "Positions : ".$obj_ptr->{'pos_begin'}.",".$obj_ptr->{'pos_end'}."\n";
}


=head2 function GetRNA

 Title        : GetRNA
 Usage        : GetRNA()
 Prerequisite : None
 Function     : return the $number of rna
 Returns      : the $number th rna
 Args         : None
 Globals      : None
 
=cut 

sub GetRNA {
	my ($obj_ptr, $number) = @_;
	return ($list[$number]);
}


=head2 procedure ConcatSequence

 Title        : ConcatSequence
 Usage        : ConcatSequence($seq)
 Prerequisite : the rna possess a sequence
 Function     : concatene the current sequence with the sequence in parameter	
 Returns      : None
 Args         : $seq the sequence which is concatenate with the sequence of the rna
 Globals      : none

=cut
sub ConcatSequence
{
	my ($obj_ptr,$seq) = @_;
	$obj_ptr->{'sequence'} = $obj_ptr->{'sequence'}.$seq;
}


=head2 procedure ConcatSequenceAlignment

 Title        : ConcatSequenceAlignment
 Usage        : ConcatSequenceAlignment($seq)
 Prerequisite : the rna possess a sequence of the alignment
 Function     : concatene the current alignment sequence with the sequence in parameter
 Returns      : None
 Args         : $seq the sequence which is concatenate with the alignment sequence of the rna
 Globals      : none

=cut
sub ConcatSequenceAlignment
{
	my ($obj_ptr,$seq) = @_;
	$obj_ptr->{'sequence_alignment'} = $obj_ptr->{'sequence_alignment'}.$seq;
}


=head2 function getNomFastaFile

 Title        : getNomFastaFile
 Usage        : getNomFastaFile($i)
 Prerequisite : none
 Function     : find the name of the fasta file of the rna according to its number
 Returns      : the name of the fasta file
 Args         : $i : the number of the rna
 Globals      : none

=cut

sub getNomFastaFile
{
	my ($obj_ptr, $i ) = @_;
	my $name_fasta_file = "RNA".$i.".fna";
	return ($name_fasta_file);
}


=head2 procedure CreateStructure2D

 Title        : CreateStructure2D
 Usage        : CreateStructure2D($struct)
 Prerequisite : None
 Function     : compute the 2D structure of the rna: 
 		with the sequence of the rna and the structure given in parameter,
 		save an array composed of pair
 Returns      : none
 Args         : $struct : string which corresponds to the 2D structure in the stockholm format
		Example : <<<...>...>>>
 Globals      : none

=cut

sub CreateStructure2D
{
	my ($obj_ptr, $struct)  = @_;
	my $ra_pairs = &ExtractStructure($obj_ptr->{'sequence_alignment'}, $struct);
	$obj_ptr->{'structure'} = $ra_pairs;
}


=head2 procedure CreateTreeStructureRNA

 Title        : CreateTreeStructureRNA
 Usage        : CreateTreeStructureRNA($i, $parent_directory,$cmd_reformat, $cmd_wu_blast, $cmd_filter, $identity, $pcq, $genome_lib, $source)
 Prerequisite : none
 Function     : complete the tree structure PeRNA for the rna: create a directory and many files
 Returns      : None
 Args         : $i the index of the rna
 		$parent_directory : the way of the parent directory
		$cmd_reformat : the reformat command
		$cmd_wu_blast : the blast command
		$cmd_filter : the filter command
		$identity : percentage of identity for Blast
		$pcq : pcq for Blast
		$genome_lib : the genome library to use Blast
		$source : the bank of the datas
 Globals      : none

=cut

sub CreateTreeStructureRNA
{
	my ($obj_ptr, $i, $parent_directory,$cmd_reformat, $cmd_wu_blast, $cmd_filter, $identity, $pcq, $genome_lib, $source) = @_;
	
	$CMD_REFORMAT	= $cmd_reformat;
	$CMD_WU_BLAST	= $cmd_wu_blast;
	$CMD_FILTER	= $cmd_filter;
	$IDENTITY	= $identity;
	$PCQ		= $pcq;
	
	#create a directory for the current rna
	my $directory_name = "RNA".$i;
	&CreateDirectory($directory_name, $parent_directory);
	
	my $name_fasta_file = &getNomFastaFile($obj_ptr, $i);
	my $directory_way   = $parent_directory.$directory_name."/";
	
	# create a tmp directory
	&CreateDirectory("tmp", $directory_way);
	my $directory_tmp = $directory_way."tmp/";
	
	my $fasta_file_data = $obj_ptr->ComputeFastaFileContent($i);
	&CreateFile($name_fasta_file, $directory_way, $fasta_file_data);

	# compute the positions and the strand of the rna on the genomic sequence
	$obj_ptr->GetPositions($directory_tmp, $directory_way.$name_fasta_file, $genome_lib);
	
	# creation of a file containing the strand, the genomic position, the date 
	$obj_ptr->CreateInformationFile($directory_way,$source);
	
	# creation of a file containing the secondar structure of the rna
	$obj_ptr->CreateStructureFile($directory_way);
}


=head2 function ComputeFastaFileContent

 Title        : ComputeFastaFileContent
 Usage        : ComputeFastaFileContent($nb)
 Prerequisite : none
 Function     : compute the content of the fasta file of the rna
 Returns      : the content of the fasta file of the rna
 Args         : $nb : the number of the rna
 Globals      : none

=cut
sub ComputeFastaFileContent
{
	my ($obj_ptr, $nb) = @_;
	# compute the heading of the fasta file
	my $heading 	= ">RNA".$nb." from ".$obj_ptr->{'organism'}." ".$obj_ptr->{'strain'}."\n";
	my $body 	= $heading.$obj_ptr->{'sequence'}."\n";
	return ($body);
}

=head2 procedure CreateInformationFile

 Title        : CreateInformationFile
 Usage        : CreateInformationFile($directory,$source)
 Prerequisite : the positions of the sequence in the genome have to be computed ( procedure GetPositions )
 Function     : create a file containing information about the rna : its positions, its strand, 
 		the extraction date of this data
 Returns      : None
 Args         : $directory: the directory in which create the file
 		$source: the bank of the datas
 Globals      : none

=cut

sub CreateInformationFile
{
	my ($obj_ptr, $directory,$source) = @_;
	
	# open the file of information
	open(F_SEQ, ">".$directory."information");

	# save the positions
	my @a_positions = @{$obj_ptr->{'positions'}};
	for ( my $i = 0 ; $i < ($#a_positions+1) ; $i++ ) {
		print F_SEQ "POSITION $a_positions[$i][0] $a_positions[$i][1] $a_positions[$i][2]\n";
	}

	# save the date
	my $tm = localtime;
	print F_SEQ "EXTRACTION_DATE ".($tm->year+1900)."-".($tm->mon+1)."-".($tm->mday)."\n";
	
	# save the source of the rna datas
	print F_SEQ "SOURCE $source\n";

	# save the family of the rna
	print F_SEQ "FAMILY ".$obj_ptr->{'family'}."\n"; 
	
	#save the accession number of the rna
	print F_SEQ "ACC_NUMBER ".$obj_ptr->{'acc_number'}."\n";
	
	close(F_SEQ);					
}


=head2 procedure CreateStructureFile

 Title        : CreateStructureFile
 Usage        : CreateStructureFile($directory)
 Prerequisite : none
 Function     : create a file containing the secondary structure of the rna 
 		Example : (1,12) (2,11) ( 3, 10)
		it means that the nucleotids 1 and 12, 2 and 11, 3 and 10 are paired
 Returns      : None
 Args         : $directory: the directory in which creates the file
 Globals      : none

=cut

sub CreateStructureFile
{
	my ($obj_ptr, $directory) = @_;
	
	my $struct_file_name 	= &GetStructureFileName();
	my $data 		= &StructureToString($obj_ptr->{'structure'});
	&CreateFile($struct_file_name, $directory, $data);
}

					
=head2 procedure GetPositions

 Title        : GetPositions
 Usage        : GetPositions ($directory, $name_fasta_file, $genome_lib)
 Prerequisite : the fasta file of the rna must exist
 Function     : compute the positions of the current rna on the genome
 Returns      : none
 Args         : $directory : the directory where to execute WuBlast
 		$way_fasta_file : the way of the fasta file of the treated rna ; used to searched the position of the rna in the genome
		$genome_lib : the way of the file used as library to execute a wu-blast on the genome sequence
 Globals      : none

=cut
sub GetPositions
{
	my ($obj_ptr, $directory, $way_fasta_file, $genome_lib) = @_;
	my $cmde ="";
	my $all_file; # string which contains the output of blast
	my @a_rna;	# use to parse the output of Blast
	my @a_data;	#use to parse the output of Blast
	my @a_positions;
	my @a_strand;	# use to compute the strand
	my $strand;	# use to compute the strand
	
	my @a_results;	# array containing the positions of the sequence in the genome
	
	# execute a wu-blast
	$cmde = $CMD_REFORMAT." IN=".$way_fasta_file." OUT=".$directory."sequenceFormat.fas";
	&SystemLaunch($cmde);
	$cmde = $CMD_WU_BLAST." IN=".$directory."sequenceFormat.fas LIB=".$genome_lib." OUT=".$directory."res";
	&SystemLaunch($cmde);
	$cmde = $CMD_FILTER." FILE=".$directory."res IDENTITY=$IDENTITY PCQ=$PCQ > ".$directory."results";
	&SystemLaunch($cmde);

	#open the output file of blast
	open (F_BLAST,"<".$directory."results") || die "Problem to open file results!\n";
	# select all the lines
	while (defined(my $line = <F_BLAST>) ) {
		$all_file = $all_file.$line;
	}
	close(F_BLAST);
	
	# delete the temporary files
	#$cmde = "rm -f ".$rna_directory."results ".$rna_directory."res ".$rna_directory."sequenceFormat.fas";
	#&SystemLaunch($cmde);

	# an element of the table is an occurence of the sequence in the genome
	@a_rna = split( /\</, $all_file);
	#search the positions of the rna on the genome and the strand
	# for each apparition of the sequence
	for (my $i = 1 ; $i <= $#a_rna ; $i++) {
		@a_data = split(/\/\//, $a_rna[$i]);
		# delete the space at the begin and the end of the line
		$a_data[0] =~ s/(^[ \t]*)|([ \t]*$)//gm;
		$a_data[1] =~ s/(^[ \t]*)|([ \t]*$)//gm;
		
		#find positions
		@a_positions 	= split(/ +/, $a_data[1]);
		#find the strand
		@a_strand 	= split(/ +/, $a_data[0]);
		if ( $a_strand[1] < $a_strand[2] ) {
			$strand = Rna::GetStrandPlus();
		}else {
			$strand = Rna::GetStrandLess();
		}
		# save the positions and the strand
		push(@a_results, [($a_positions[1],$a_positions[2], $strand)]);
	}
	
	# save all the found positions
	$obj_ptr->{'positions'} = \@a_results;
}
1
