

require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/cgi-lib.pl";

$ServerName = "http://atg.toulouse.inra.fr"; # voir URL_SERVER


# Fonctions a reecrire en priorite
# sub jPrintMATCHversFichier
# sub jPrintMATCHversFichier4STS
# sub jPrintMATCHversFichier4EST
# fusionner ces fonctions en une seule bien ecrite
# Fait le 07-07-2000


###################################################
# Initialise le tableau GidxParam et l'environement
#
sub jInitEnv
{
my($ret)=0;

	if ( $0 =~ /\.cgi\./ )
	{
		$ret = &InitWWW();
		%GidxParam = %in;
		$HTML=$YES;
	}
	else
	{
		%GidxParam=&pTab2Indx(@ARGV);
		$HTML=$NO;
	}
	if ( ! defined($GidxParam{ORGA_CONFIG}) )
	{
		$GidxParam{ORGA_CONFIG} = $GidxParam{CFG};
	}
	$ret;
}

####################################################
#
#
sub jMin 
{
my($a,$b) = @_;

	return( ( $a <= $b ) ? $a : $b );
}

####################################################
#
#
sub jMax 
{
my($a,$b) = @_;

	return( ( $a >= $b ) ? $a : $b );
}

####################################################
# Init the list of ORF
#

sub jGetDIRList
{
local(*TDIRList,$extension) = @_;

	if ( $GidxParam{ALLREP} )
	{
		@TDIRList = `$UNIXCAT $GidxParam{ALLREP}`;
		chop(@TDIRList);
	}
	else
	{
		$date = time;
		$TmpFile = "/tmp/tmp_run_all_aa.$date";
		&jSYSTEM("$UNIXLS -d $GidxParam{DIR}/$ORFDIR/*_$extension > $TmpFile");
		@TDIRList = `$UNIXCAT $TmpFile`;
		chop(@TDIRList);
		&jSYSTEM("$UNIXRM $TmpFile");
	}
}

####################################################
# Init a list from file name pattern
#

sub jGetFILEList
{
local(*TDIRList,$extension) = @_;
my($date,$TmpFile);

	$date = time;
	$TmpFile = "/tmp/tmp_run_all_aa.$date";
	&jSYSTEM("$UNIXFIND $GidxParam{DIR}  -follow -name \"*$extension\" -print > $TmpFile");
	@TDIRList = `$UNIXCAT $TmpFile`;
	chop(@TDIRList);
	&jSYSTEM("$UNIXRM $TmpFile");
}



####################################################
# Get and clean one line using the TAG
# Can be used with a STRING or a FILE (default) 17-08-99
#

sub jGetTAG
{
local ($tag,$flux,$typeflux)=@_;

	if ( $typeflux eq "STRING" )
	{
		$restag = $flux;
		chomp($restag);
	}
	else # if ( $typeflux eq "FILE" ) default --> compatibility
	{
		$restag = `$UNIXGREP \"^$tag\" $flux`;
		chomp($restag);
	}
	$restag =~ s/^$tag\s+//g;
        $restag;
}

#####################################################
#
#
#

sub jCheckSupervisor
{
my($ip_supervisor) = jGetSpecificOrgaValue("IP_SUPERUSERS");
my(@F)=split(",",$ip_supervisor);
my($ip);

	foreach $ip (@F)
	{
		return (1) if ( $ENV{REMOTE_ADDR} =~ /$ip/ )
	}
	return(0);
}


sub jCheckAnnotators
{
my($ip_supervisor) = &jGetSpecificOrgaValue("IP_ANNOTATORS");
my(@F)=split(",",$ip_supervisor);
my($ip);

	# si la variable n'est pas initialise, il a le droit d'editer
	if( $ip_supervisor eq "" )
	{
		return(1);
	}
	foreach $ip (@F)
	{
		return (1) if ( $ENV{REMOTE_ADDR} =~ /$ip/ )
	}
	return(0);
}

##########################################################

sub SortBy1Field
{
my($begA) = split('\|',$a);
my($begB) = split('\|',$b);

        $begA<=>$begB;
}

##########################################################

sub SortByBeginPos4Coord
{
my($begA,$endA) = split('\|',$a);
my($begB,$endB) = split('\|',$b);
my($reverse);

	$reverse = ( $begA > $endA ) ? 1 : 0;
	

	if ( $begA == $begB )	# cas du PSIGNAL
	{
		# plus petite valeur de max d'abord
		if ( $reverse )
		{
			return($endA <=> $endB);
		}
		else
		{
			return($endB <=> $endA);
		}
	}
	else
	{
		# plus petite valeur de min d'abord
		return(&jMin($begA,$endA) <=> &jMin($begB,$endB));
	}
}


##########################################################
# Print the variable only in cmd mode
#

sub jPrintCmd
{
local ($lign)= @_;

	print STDOUT "$lign" if ( $0 !~ /\.cgi\./);
}


##########################################################
# Print HTML Code
#

sub jHtml
{
local ($lign)= @_;

	print STDOUT "$lign\n";

}


##########################################################

sub jGetRunDate
{
my(@date)=();

	@date=split(' ',`date`);
	return  "$date[2]-$date[1]-$date[$#date]";
}


##########################################################

sub jGetFileDate
{
my($filename) = @_;
my(@date)=();
my($t,$modif);

	($t,$t,$t,$t,$t,$t,$t,$t,$t,$modif,$t,$t,$t) = stat "$filename";
	($t,$t,$t,$jour,$mois,$an,$t,$t,$t) = localtime($modif);
	$an+=1900;
	$mois = (Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec) [$mois];
	if ( $an =~ /1970/ )	
	{
		return $ERROR_CODE;
	}
	return  "$jour-$mois-$an";
}

##########################################################

sub jSYSTEM
{
my($cde) =@_;

	if ( $debug && ($0 !~ /\.ok\./) )
	{
		print STDERR "cmd: #$cde#\n";
	}
	system("$cde");
}

sub Trace
{
local ($ligne) = @_;

	if ( $debug )
	{
		print STDERR "$ligne\n";
	}
}


sub jTraceWWW
{
local ($ligne) = @_;

	if ( $debug && ($0 !~ /\.ok\./) )
	{
		if ( $0 =~ /\.cgi\./ )
		{
			print STDOUT "$ligne<BR>";
		}
		else
		{
			print STDOUT "$ligne\n";
		}
	}
}

#########################################################
#croisant pour la frame + et decroissant pour la frame - 
#

sub SortByBeginPosFctFrame
{
my($ret);
my(@F);
my(@G);

	@F = split(' ',$a,9999);
        @G = split(' ',$b,9999);
	$ret = ( $a =~ / F=\+/ ) ? ($F[1]<=>$G[1]) : ($G[1]<=>$F[1]);
}

sub OpenHTML
{
local($param)=@_;
my(@F)=();
my($titre)= "";
my($fich);
my($i);

	@F=split(' ',$param);

	$fich = $F[0];
	foreach $i (1 .. $#F)
	{
		$titre = "$titre$F[$i] ";
	}
	if ( $titre eq "" )
	{
		$titre = $fich;
	}

	if ( $fich ne "stdout" )
	{
		open(HTML,">$fich");
		select(HTML); $| =1;
	}
	print<<END
<HTML>
<HEAD>
<TITLE>$titre</TITLE>
<LINK rel="stylesheet" type="text/css" media="screen" href="$IANT_CSS">
</HEAD>
<BODY>
<PRE>
END
}

#
# Mal ecrit
# ecore Utilisee ????
#
sub PrintHSP
{
local ($type_prg) = @_;

	$lignescore = "$_";
	my ($fini) = 0;
	chop($lignescore);
	$t = <IN>;
	chop($t);
	$lignescore .= "$t";
	@HSP=();
	$i = 0;
	$s_begin = "";
	$s_end = "";
	$q_begin = "";
	$q_end = "";	
	while (defined($_ = <IN>) && (! /^>/) && (!/Score = /) && (! /Database:/)  && (! /^Searching/) )
        {	
		@T = split;
		if ( /Query:/ )
		{
			if ( $q_begin eq "" )
			{
				$q_begin = $T[1];
			}
			$q_end = $T[$#T];
		}
		if ( /Sbjct:/ )
		{
			if ( $s_begin eq "" )
			{
				$s_begin = $T[1];
			}
			$s_end = $T[$#T];
		}
		#JER @HSP[$i++] = $_;
		#$HSP[$i++] = $_;
		$HSP[$i++] = "$_" if (! /Frame =/ && !/Strand/);
        }
	$pc_s = int (( $s_end - $s_begin + 1) / $s_length * 100);
	if ( $q_end > $q_begin )
	{
		$delta = $q_end - $q_begin;
	}
	else
	{
		$delta = $q_begin - $q_end;
	}
	$pc_q = int( ( $delta + 1)  / $q_length * 100);
	$lignescore =~ s/[\(\)\,]//g;
	@S = split(' ',$lignescore,9999);
        if ($lignescore =~  /Gaps/)
	{
	  print REF "<$q_name $q_begin $q_end // $s_name $s_begin $s_end // S=$S[4] E=$S[7] I=$S[11] P=$S[11] G=$S[15] ";
        }
        else
	{
	  print REF "<$q_name $q_begin $q_end // $s_name $s_begin $s_end // S=$S[4] E=$S[7] I=$S[11] P=$S[11] G=0% ";
        }
	if ( $commentaire =~ /\; \| / )
	{
		@S=split(/\; \| /,$commentaire,99999);
		$commentaire = $S[1];
	}
	print REF "LS=$s_length %S=$pc_s LQ=$q_length %Q=$pc_q $info_frame // $commentaire" ;
	print REF "\n@HSP";
	$fini =  ( (/^\s+Database:/)  || ( /^Searching/) ) ? 1 : 0;
	return($fini);
}

sub ReformatMegablast
{
local ($fich)=@_;

	open(REF,">$fich.reformat");
	open(IN,"$fich");
	chop($program = <IN>);
	while ( $_ = <IN> )
	{
		chop;
		@F=split;
		if ( /^Query=/ )
		{
			$q_name = $F[1];
		}
		if ( / letters\)/ )
		{
			$q_length = $F[0];
			$q_length =~ s/[\(\,]//g;
			print REF "QUERYFILE:   $fich\n";
			print REF "QUERY:       $q_name\n";
			print REF "QUERYLENGTH: $q_length\n";
			print REF "PROGRAM:     $program\n";
			print REF "DATABASE:    $db\n\n";
		}
		if ( /^Database:/ )
		{
			$db = "$F[1] $F[2]";
		}
		if ( /^>/ )
		{
			$fini = 0;
			@Z = split(' ');
			if ( $#Z == 3 )
                        {
                                $ligne2 = <IN>;
                                chop($ligne2);
                                $ligne2 =~ s/\s+/ /;
                                $_ = "$_ $ligne2";
                                @Z = split(' ');
                        }
			$s_name= ( $Z[4] ne "" && ($Z[4] ne  "AC;") && $Z[4] !~ /\d\d\d;/) ? $Z[4] :$Z[0]; # PAT 08-03-01 : pour remplacer AC par le nom de la seq			
			$s_name =~ s/>//;
			$s_length = $Z[2];
			print REF "$_\n";
			$commentaire = "$_";
			while (defined($_ = <IN>) && ! $fini )
			{
				while ( /Score = / )
				{
					$fini = &PrintHSP($program);
				}
				chop;
				@Z = split(' ');
				if ( /^>/ && $#Z == 3 )
                                {
                                        $ligne2 = <IN>;
                                        chop($ligne2);
                                        $ligne2 =~ s/\s+/ /;
                                        $_ = "$_ $ligne2";
                                        s/\s+/ /;
                                        @Z = split(' ');
                                }
				if ( /^>/ )
				{
					$s_name= ( $Z[4] ne "" && ($Z[4] ne  "AC;") && $Z[4] !~ /\d\d\d;/) ? $Z[4] :$Z[0];# PAT 08-03-01 : pour remplacer AC par le nom de la seq			
					$s_name =~ s/>//;
					$s_length = $Z[2];
					$commentaire = "";
				}
				if ( /Database:/ ) # je quitte sur database
				{
					$fini = 1;
					$version = "";
					$commentaire = "";
					$s_name = "";
					$q_name = "";
					$s_length = "";
					$q_length = "";
					$db = "";
					@Z=();
				}
				elsif ( ($_ ne "\n") && ! /Length =/ )
				{
					print REF "$_\n";
					$commentaire .= "$_";
					$commentaire =~ s/[ \n\t]+/ /g;
					$commentaire=~s/\/len=(\d+)//;
				}
			}
			$fini = 0;
		}
		if ( /^\s+Database:/ ) # je quitte sur database
		{	
			print REF "//\n\n";
		}
	}
	if ( /^\s+Database:/ ) # je quitte sur database
	{	
		print REF "//\n\n";
	}
	close(REF);
	close(IN);
	system("mv $fich.reformat $fich");
}

###################################################################

sub CloseHTML
{
local($fich)=@_;

	print HTML<<END
	</PRE>
	</BODY>
	</HTML>
END
}

###################################################################

sub WriteHTML
{
local ($ligne) = @_;

	print HTML "$ligne\n";
}

###################################################################
#
#

sub TraceIDX
{
	local (%idx) = @_;

	if ( $debug )
	{
		foreach $cour (keys(%idx))
		{
			print STDERR "$cour - $idx{$cour}\n";
		}
	}
}

###################################################################
#
#

sub SortByScoreDecroissant
{
my($scoreA,$scoreB);
my($lnameA,$lnameB);

        ($scoreA)  =  ($a =~ / S=(\d+) /);
        ($scoreB)  =  ($b =~ / S=(\d+) /);
        $scoreA = 0 if ( $scoreA eq "" );
        $scoreB = 0 if ( $scoreB eq "" );
	
	
	if ( $scoreB == $scoreA )	# en cas d'egalite de score, priorite aux sequences dt l'ID est le plus cours (SP,TR,TRNEW)
					# Ajoute le 09juin2000
	{
		@F=split(' ',$a);
		$lnameA = length($F[4]);
		@F=split(' ',$b);
		$lnameB = length($F[4]);
		return($lnameA<=>$lnameB);
	}
	else
	{
		$scoreB<=>$scoreA;
	}
}

###################################################################
#
#

sub SortByScoreCroissant
{
        ($scoreA)  =  ($a =~ / S=(\d+) /);
        ($scoreB)  =  ($b =~ / S=(\d+) /);
        $scoreA = 0 if ( $scoreA eq "" );
        $scoreB = 0 if ( $scoreB eq "" );

        $scoreA<=>$scoreB;
}

###################################################################
#
#

sub jScanFrameShift2Analyse
{
local (@TMP) = @_;
my($match)="";
my(@FrameMatchPlus)=();
my(@FrameMatchMoins)=();
my(@TOK)=();
my($quoi);

        # je regarde s'il y a au moins deux frame sur +
        $quoi = " F=\\+";
        @FrameMatchPlus = grep(/$quoi/,@TMP);
        if ( $#FrameMatchPlus >=1 )     #c'est bon j'ai au moins 2 match sur une frame +
        {
                @TOK = (@TOK,@FrameMatchPlus);
        }
        $quoi = " F=\\-";
        @FrameMatchMoins = grep(/$quoi/,@TMP);
        if ( $#FrameMatchMoins >=1 )    #c'est bon j'ai au moins 2 match sur une frame -
        {
                @TOK = (@TOK,@FrameMatchMoins);
        }
        @TOK;
}

###################################################################
#
#

sub Perm
{
local($d,$f) = @_;
my($tmp);

	$tmp=$f;
	$f=$d;
	$d=$tmp;
	($d,$f);
}


###################################################################
#
#


sub jChevauchementHSP
{
local ($deb_q,$fin_q,$deb_s,$fin_s) = @_;
my($C1,$C2);

	if ( $deb_q > $fin_q )
	{
		($deb_q,$fin_q) = &Perm($deb_q,$fin_q);
	}
	if ( $deb_s > $fin_s )
	{
		($deb_s,$fin_s) = &Perm($deb_s,$fin_s);
	}

        #print STDOUT "$deb_q,$fin_q,$deb_s,$fin_s\n";
        $C1 = ( ($deb_q - $fin_s )  > 0 ) ? 1 : 0;
        $C2 = ( ($deb_s - $fin_q )  > 0 ) ? 1 : 0;

        if (  ! ($C1 || $C2) )
        {
                return(1);
        }
        return(0);
}
#
sub jScanFrameShift3Print
{
local ($prog_meth,*ALLMATCH) = @_;
my($ligne)=();
my(@G)=();
my($cptGRP);
my(@GRPHSP)=();
my($group_name)="";
my($group_name_bak)="";

	print "<PRE><CENTER><Font Size=+2>AUTOMATIC frameshift PREDICTION based on blastx results<Font><BR>Warning: human expertise is needed</CENTER>\n";

        foreach $ligne (@ALLMATCH)
        {
            	@G=split(' ',$ligne);
		if ( $G[4] ne $group_name)
		{
			$group_name_bak = $group_name;
			$group_name=$G[4];
			&jScanFrameShift3AnalysePrint($prog_meth,$GidxParamAnal{"FILE"},$group_name_bak,@GRPHSP) if ($#GRPHSP >= 0);
			$cptGRP=0;
			@GRPHSP=();
		}
		$GRPHSP[$cptGRP++] = $ligne;
        }
	&jScanFrameShift3AnalysePrint($prog_meth,$GidxParamAnal{"FILE"},$group_name,@GRPHSP) if ($#GRPHSP >= 0);
}

###################################################################
#
#

sub jPrintTableFrameshift
{
local ($HSP1,$HSP2,$retq,$rets,$distq,$dists,$dists3,$frame1,$frame2,$somPCS)=@_;
my(@F)=();
my($chaine)=();
my($pcs1);
my($pcs2);
my(@G);
my($signe);

	@F=split(' ',$HSP1);
	$F[0] =~ s/<//;
	($pcs1) = ( $HSP1 =~ / %S=(\d+) /);
	@G=split(' ',$HSP2);
	($pcs2) = ( $HSP2 =~ / %S=(\d+) /);

	print "<TABLE  CLASS=\"linkzone\" STYLE=\"\"  BORDER=2> <TR><TH COLSPAN=3>Query/$F[0]<TH COLSPAN=3>Subject/$F[4]</TR> <TR><TD>$F[1]<TD>$F[2]<TD>$frame1<TD>$F[5]<TD>$F[6]<TD>%S=$pcs1</TR> <TR><TD>$G[1]<TD>$G[2]<TD>$frame2<TD>$G[5]<TD>$G[6]<TD>%S=$pcs2</TR>\n";

	$chaine = (abs(100-$somPCS) > 5) ? ("(-) SUM %S = $somPCS") : ("<Font Color=blue>(*) SUM %S = $somPCS</Font>");
	print "<TR><TD COLSPAN=6>$chaine</TR>";
	$chaine = ( $retq ) ? ("(-) Overlap on query positions") : ("(*) No overlap on query positions");
	print "<TR><TD COLSPAN=6>$chaine</TR>";
	$chaine = ( $rets ) ? ("(-) Overlap on subject positions") : ("(*) No overlap on subject positions");
	print "<TR><TD COLSPAN=6>$chaine</TR>";
	$signe = "(-)";
	if ( abs($distq-$dists3)<=21 )
	{
		$signe = "(*) COMPATIBLE";
	}
	$chaine = "$signe gap between hsps: Query=<Font Color=blue>$distq(pb)</Font> / Subject=$dists(aa)=<Font Color=blue>$dists3(pb)</Font>";
	if ( $frame1 eq $frame2 && $signe =~ /COMPATIBLE/ )
	{
		$chaine = "$chaine<BR>Compatible HSP on the same frame could be due to a compensating frameshift OR filtered (by seg program) sequences";
	}
	print "<TR><TD COLSPAN=6>$chaine</TR>";
	if (  abs(100-$somPCS) <= 5  && !$retq && !$rets && $signe =~ /COMPATIBLE/  )
	{
		print  "<TR><TD COLSPAN=6><Font Color=red>All automatic criteria are ok for a frameshift</Font></TR>\n";
	}
	print "</TABLE><HR>";
}

###################################################################
#
#

sub jScanFrameShift3AnalysePrint
{
local ($prog_meth,$unix_file,$comfiltre,@TMP) = @_;
my($match)="";
my(@TOK)=();
my($i)=0;
my($min_pos,$max_pos)=0;
my(@F)=();
my(@G)=();
my($somPCS)=0;
my($distq,$dists,$signe,$dists3);
my($PCS) = 0;
my(@frameF);
my(@frameG);
my($Frame);

        $min_pos= 99999999;
        $max_pos= -1;
        @TMP = sort SortByBeginPosFctFrame @TMP;

        @F=split(' ',$TMP[0]);
	$max_pos = $F[1] if ( $F[1] > $max_pos );
	$max_pos = $F[2] if ( $F[2] > $max_pos );
	$min_pos = $F[1] if ( $F[1] < $min_pos );
	$min_pos = $F[2] if ( $F[2] < $min_pos );
	($PCS) = ($TMP[$i] =~ / %S=(\d+) /);
	$somPCS += $PCS;
	($Frame) = ($TMP[$i] =~ / F=([+-]\d+) /);
        for ($i=1;$i<=$#TMP;$i++)
        {
                @G=split(' ',$TMP[$i]);
		$max_pos = $G[1] if ( $G[1] > $max_pos );
		$max_pos = $G[2] if ( $G[2] > $max_pos );
		$min_pos = $G[1] if ( $G[1] < $min_pos );
		$min_pos = $G[2] if ( $G[2] < $min_pos );
		($PCS) = ($TMP[$i] =~ / %S=(\d+) /);
		$somPCS += $PCS;
		($Frame) = ($TMP[$i] =~ / F=([+-]\d+) /);
		$retq = &jChevauchementHSP($F[1],$F[2],$G[1],$G[2]);
		$rets = &jChevauchementHSP($F[5],$F[6],$G[5],$G[6]);
                $distq = abs($F[2]-$G[1]);
                $dists = abs($F[6]-$G[5]);
                $dists3 = $dists*3;
                $signe = "(-)";
                @frameF = grep(/F=/,@F);
                @frameG = grep(/F=/,@G);

		# JER 06/04/00 if ( abs($distq-$dists3)<100 && ($distq <= 200) && $dists3 <= 200  )
		if ( abs($distq-$dists3)<100 && ($distq <= 200) && $dists3 <= 200  && ( $frameF[0] ne $frameG[0] ) )
		{
			if ($GidxParamAnal{"HTML"} == $YES )        # JER 18/01/99
			{
				if  ( $Frame =~ /\+/ )
				{
					print  "\n<A HREF=$WWW_SCRIPT_AA_DNA_FILTRE_BLAST?FILE=$GidxParamAnal{FILE}&BEGIN=$min_pos&END=$max_pos&COM=$comfiltre&ORGA_CONFIG=$GidxParamAnal{ORGA_CONFIG}&FORM_FRAME_SET=2>$prog_meth ($min_pos/$max_pos)</A><BR>\n";
				}
				else
				{
					print  "\n<A HREF=$WWW_SCRIPT_AA_DNA_FILTRE_BLAST?FILE=$GidxParamAnal{FILE}&BEGIN=$max_pos&END=$min_pos&COM=$comfiltre&&ORGA_CONFIG=$GidxParamAnal{ORGA_CONFIG}&FORM_FRAME_SET=2>$prog_meth ($max_pos/$min_pos)</A><BR>\n";
				}
				# verifier si dans pPrintMATCH cela peut se simplifier
				$_ = $TMP[$i-1];
				&pPrintMATCH(0,$prog_meth,$_,$unix_file,$GidxParamAnal{ORGA_CONFIG});
				$_ = $TMP[$i];
				&pPrintMATCH(0,$prog_meth,$_,$unix_file,$GidxParamAnal{ORGA_CONFIG});
			}
			else
			{
				print "$TMP[$i-1]\n";
				print "$TMP[$i]\n";
			}
			&jPrintTableFrameshift($TMP[$i-1],$TMP[$i],$retq,$rets,$distq,$dists,$dists3,$frameF[0],$frameG[0],$somPCS);
		}
                @F = @G;
        }
}

###################################################################
#
#

sub jScanFrameShift2
{
local (@ALLMATCH) = @_;
local(@NEWALL)=();
local($cptNEWALL)=0;
my($group_name)="";
local(@TMP) = ();
my($cptTMP)=0;
my($i);
my(@F);

        foreach(@ALLMATCH)
        {
                @F=split;
                if ( $F[4] ne $group_name)
                {
                        @NEWALL = (@NEWALL,jScanFrameShift2Analyse(@TMP));
                        $group_name=$F[4];
                        for ($i=0;$i<=$cptTMP;$i++)
                        {
                                $TMP[$i]="";
                        }
                        @TMP = ();
                        $cptTMP = 0;
                }
                $TMP[$cptTMP++] = $_;
        }
        @NEWALL = (@NEWALL,jScanFrameShift2Analyse(@TMP));
}

###################################################################
# 
#

sub jRedondant
{
local ($sens,$courant,$deb_red,$fin_red,*ALLMATCHREDONDANCE) = @_;
my(@F)=();
my($i)=0;
my($deb_query);
my($fin_query);

        @F=split(' ',$courant);
        $deb_query = $F[1];
        $fin_query = $F[2];

        for($i=$deb_red;$i<=$fin_red;$i++)
        {
                @F=split(' ',$ALLMATCHREDONDANCE[$i]);
                $deb_dejaok = $F[1];
                $fin_dejaok = $F[2];
                # $deb_dejaok $deb_query -- $fin_query $fin_dejaok
                #   100         200           400        1000
                #&Trace(" ($deb_dejaok  $deb_query) && $fin_query  $fin_dejaok");
                if ( $sens eq "+" &&  (($deb_dejaok-10) <= $deb_query) && $fin_query <= ($fin_dejaok+10) )
                #if ( $sens eq "+" &&  (($deb_dejaok) <= $deb_query) && $fin_query <= ($fin_dejaok) )
                {
                        return($YES);
                }
                # $deb_dejaok $deb_query -- $fin_query $fin_dejaok
                #   1000         400           200        100
                if ( $sens eq "-" &&  (($deb_dejaok+10) >= $deb_query) && $fin_query >= ($fin_dejaok-10) )
                #if ( $sens eq "-" &&  (($deb_dejaok) >= $deb_query) && $fin_query >= ($fin_dejaok) )
                {
                        return($YES);
                }
        }
        return ($NO);
}

##############################################################################################
# JER 25/01/99 sequence d'appel de jRedondanceINCLUSION

# il faut appeler jRedondanceINCLUSION AVANT le tri par begin position
# le tableau retourne par defaut est trie par score decroissant 
# mais si par la suite on doit trie par position on peut desactiver ce tri en sortie
#        $returntrieparscore = ($GidxParamAnal{"SORT"}!=$YES ) ? $YES : $NO;
#        @ALLMATCH = &jRedondanceINCLUSION(*ALLMATCH,$returntrieparscore);
# pour des question d'optimisation le tableau ALLMATCH (tres gros) est utilise avec un alias
# en lecture mais le tableau retourne a pour vocation d'effacer ALLMATCH


sub jRedondanceINCLUSION
{
local (*ALLMATCH,$returntrieparscore) = @_;
my($deb_red) = 0;
my($fin_red) = -1;
local(@ALLMATCHREDONDANCE) = ();        # ATTENTION il faut qu'il soit local pour etre lisible dans jRedondant
# my(@ALLMATCHTMP) = @_; Pourquoi 22091999 ???? 
my(@ALLMATCHTMP) = ();
my(@Frame) = ();
my(@FrameMatch) = ();
my($sens);
my($i)=0;

    @ALLMATCHTMP = sort SortByScoreDecroissant @ALLMATCH;
    @Frame = ('+1', '+2', '+3', '-1', '-2', '-3' );
    foreach $i (0 .. $#Frame)
    {
	$sens = ( $Frame[$i] =~ /^\+/ )?"+":"-";
	#&Trace("$sens\n");
	$quoi = " F=\\$Frame[$i] ";
	@FrameMatch = grep(/$quoi/,@ALLMATCHTMP);
	foreach $courant (@FrameMatch)
	{
	    if ( ! &jRedondant($sens,$courant,$deb_red,$fin_red,*ALLMATCHREDONDANCE) )
	    {
		$fin_red++;
		$ALLMATCHREDONDANCE[$fin_red] = $courant;
	    }
	}
	$deb_red = $fin_red+1;
	$fin_red = $fin_red;
    }
    if ( $returntrieparscore )
    {
	return (sort SortByScoreDecroissant @ALLMATCHREDONDANCE);
    }
    else
    {
	return(@ALLMATCHREDONDANCE);
    }
}

#
#
#

sub jRedondanceINCLUSION_BLASTP
{
local (*ALLMATCH,$returntrieparscore) = @_;
my($deb_red) = 0;
my($fin_red) = -1;
local(@ALLMATCHREDONDANCE) = ();        # ATTENTION il faut qu'il soit local pour etre lisible dans jRedondant
# my(@ALLMATCHTMP) = @_;  Encore pourquoi ??? 22/09/99
my(@ALLMATCHTMP) = (); 
my($courant);

	@ALLMATCHTMP = sort SortByScoreDecroissant @ALLMATCH;
	foreach $courant (@ALLMATCHTMP)
	{
	    	if ( ! &jRedondant("+",$courant,$deb_red,$fin_red,*ALLMATCHREDONDANCE) )
	    	{
			$fin_red++;
			$ALLMATCHREDONDANCE[$fin_red] = $courant;
	    	}
	}
	$deb_red = $fin_red+1;
	$fin_red = $fin_red;
	if ( $returntrieparscore )
	{
		return (sort SortByScoreDecroissant @ALLMATCHREDONDANCE);
	}
	else
	{
		return(@ALLMATCHREDONDANCE);
	}
}


#
# Tres Certainement obsolete
#
sub ReadParam
{
	local($PARAM)=@_;
	open(PARAM,"$PARAM") || die "fichier $PARAM introuvable\n";
	@idx_param = <PARAM>;
	chop(@idx_param);
	foreach $param (@idx_param)
	{
		@F = split(' ',$param,9999);
	 	$Idx{$F[0]} = $F[1];
		$Idx{$F[0]} =~ s/ROOT\//$Idx{"ROOT"}\//g;
	}
	%Idx;
}

#################################################
# Encore utilise dans les vieux programmes
#
sub InitParamWWW
{
local (%in) = @_;

	#$old = $*;  $* =1;
	foreach $key (%in)
	{
		if ( $debug_www )
		{
			print STDOUT "#$key#$in{$key}#\n";
		}
		if ( $in{$key} ne ""  )
		{
			#print STDOUT "\n$in{$key} -- $key\n";
			$Gidx_param{$key} = $in{$key};
		}
	}

}

#################################################
# Used by jInitEnv
#

sub InitWWW
{
my($ret);

	@httpdenv = (SERVER_SOFTWARE,
        SERVER_NAME,
        GATEWAY_INTERFACE,
        SERVER_PROTOCOL,
        SERVER_PORT,
        REQUEST_METHOD,
        HTTP_ACCEPT,
        PATH_INFO,
        PATH_TRANSLATED,
        SCRIPT_NAME,
        QUERY_STRING,
        REMOTE_HOST,
        REMOTE_ADDR,
        AUTH_TYPE,
        REMOTE_USER,
        REMOTE_IDENT,
        CONTENT_TYPE,
        CONTENT_LENGTH);
	$ret = &ReadParse;
        print &PrintHeader;
	#print STDOUT "DEBUG<P>";
        #print &PrintVariables(%in);
	#print STDOUT "<HR>";
	$ret;
}


# BACSU
# "
# Concerning the characterization of the transcription terminator sites,
# we used a method which has not yet been integrated into our prototype
# system. Palingol [16] is a programming
# language specialized in the treatment of DNA and RNA secondary
# structures which can be used to describe any arbitrary structure and to
# scan a sequence database to find all sequences
# that fold into this structure. In the case of the Rho-independent
# terminator patterns, we used the following description: one helix
# containing a stem (minimum 7 base pairs (bp) long
# with at least 4 GC pairs) and a loop (between 3 and 10 bases long); this
# helix must be followed by a T-rich region (at least 3 successive T
# nucleotides among the 6 following the helix)
# "


sub jChercheRHOstrand
{
local (*seq,$longueur_contig,$reverse_strand)=@_;
my ($i)=0;
my ($trouve)=0;
my ($cand) = "";
my ($number_ofGC) = 0;
my ($dec,$stemd,$polyT,$stemg) = "";
my (@Tpat) = ();
my ($tmp) = "";

        while ($seq =~ /(.{30})T{3,20}/g)
        {
                $cand = "$&";
                if ( $cand =~ /(\w{10})(T{3,20})/ )
                {
                        $trouve = 0;
                        for ($dec=0;($dec<=3 && !$trouve) ;$dec++)
                        {
                                $stemd = $1;
                                $polyT = $2;
                                $stemd = substr($stemd,$dec,7);
                                $number_ofGC = $stemd  =~ tr/GC/GC/ ;
                                if ( $number_ofGC >= 4 )
                                {
                                        ($stemg= $stemd) =~ tr/ATGC/TACG/;
                                        $stemg = reverse($stemg);
                                        if ( $cand =~ /($stemg\w{3,10}$stemd.{0,3}T{3,})/ )
                                        {
						$tmp = ( $reverse_strand ) ? "REV:" : "" ;
                                                $Tpat[$i++] = "$tmp$1 $stemg $stemd";
                                                $trouve = 1;
                                        }

                                }
                        }
                }
        }
        return @Tpat;
}


sub jChercheRHO
{
local (*sequence)=@_;
my ($i)=0;

	$sequence = "\U$sequence";
	$longueur_seq = length($sequence);
	@RHO_LIST_FOR = &jChercheRHOstrand(*sequence,$longueur_seq,0);
	($reverse_seq = $sequence ) =~ tr/ATGC/TACG/;
	$reverse_seq = reverse($reverse_seq);
	@RHO_LIST_REV = &jChercheRHOstrand(*reverse_seq,$longueur_seq,1);
	@RHO_LIST = (@RHO_LIST_FOR,@RHO_LIST_REV);
}

sub jPrintRHO
{
local ($seqname,*seq,*Tpat)=@_;
my ($i)=0;
my ($listeRHO)="";
my ($pos) = 0;
my ($rev)=0;
my ($endpos)=0;

	foreach $i (0..$#Tpat)
	{
		@F=split(' ',$Tpat[$i]);
		if ( $listeRHO !~ / $F[0] / )
		{
			$PATTERN = $F[0];
			if ( $PATTERN =~ /REV/ )
			{
				$PATTERN =~ s/REV://;
				$pattern = $PATTERN; # AV
				$PATTERN =~ tr/ATGC/TACG/;
				$PATTERN = reverse ($PATTERN);
				$reverse = $YES;
			}
			else
			{
				$pattern = $PATTERN;
				$reverse = $NO;
			}
			while ($seq =~ /$PATTERN/g)
			{
				$pos = length($`)+1;
				$endpos = $pos+length($pattern)-1;
				$begin = ( ! $reverse ) ? $pos : $endpos;
				$end = ( ! $reverse ) ? $endpos : $pos;
				print "<$seqname $begin $end // RHO_INDEP;$pattern // $pattern - $F[1] - $F[2]\n";
			}
			$listeRHO .= " $F[0] ";
		}
	}
}


##########################################
# Run a unix command (nice)
#

sub jRunMeth
{
local ($cde) = @_;
my($dat)="";

	$tim = `date`;
	chop($tim);
	if ( $0 =~ /\.cgi./ )
	{
		print  "<B>START:</B> <I>$tim</I>\n<BR>$cde";
	}
	else
	{
		print  "START: $tim\n$cde\n";
	}
	$res = `nice $cde`;
	$tim = `date`;
	chop($tim);
	if ( $0 =~ /\.cgi./ )
	{
		print  "\n<BR><B>END:</B> <I>$tim</I><BR>\n";
	}
	else
	{
		print  "END: $tim\n\n";
	}
}


#
#
#
sub jBackup_
{
        $GBACKUP_ = $_;
}

#
#
#
sub jRestore_
{
        $_ = $GBACKUP_;
}


#
# Search the given key into the Global GidxParam hash table
# if the key is not yet into the table. The subroutine update
# the table with the values into the specific config file (needed in this case)
#
sub jGetSpecificOrgaValue
{
local($cle) = @_;
my(%idxParam)=();
my($clef) = "";

	if ( ! defined($GidxParam{$cle}) )       # le programme precedent ne m'a pas donne l'organisme je dois donc
					    # le lire dans le fichier ORGA_CONFIG qui est necessaire
	{
		if ( ! -e $GidxParam{ORGA_CONFIG} ) 
		{
			if ( $cle eq "ORGANISM" )	# PATCH  pour compatibilite 210999
			{
				if ( $0 !~ /\.ok\./ )
				{
					print STDOUT "<I>Development Warning (don't worry !!!): No organism config file founded.";
					print STDOUT "rhime is used as ORGANISM value. Will be modified<BR>";
					print STDOUT "if you are working with another organism send me an email gouzy\@tlse</I>\n";
				}
				return("rhime");
			}
			else
			{
				return("ERROR");
			}
		}
		else
		{
			# update of the Global HashTable GidxParam with parameters
			# in the config file
			# the config file "win" if there are duplications
			# change le 06/10/2000
			#%GidxParam = (%GidxParam,&pReadConfig($GidxParam{ORGA_CONFIG}));
			%idxParam = &pReadConfig($GidxParam{ORGA_CONFIG});
			foreach $clef (keys(%idxParam))
			{
				if ( ! defined($GidxParam{$clef}) )
				{
					$GidxParam{$clef} = $idxParam{$clef};
				}
			}
		}
	}
	return($GidxParam{$cle}); 
}

#
# What: FILENAME|xxxx
#
sub jSetObjetType
{
my($What,$Value) = @_;

        if ( $What eq "FILENAME" )
        {
                if ( $Value =~ /EST$/ || ($Value =~ /GSS$/) || $Value =~ /STS$/ )
                {
                        return($GEST);
                }
                elsif ( $Value =~ /GC$/ )
                {
                        return($GGC);
                }
                elsif ( $Value =~ /IS$/ )
                {
                        return($GIS);
                }
                elsif ( $Value =~ /MISC$/ )
                {
                        return($GMISC);
                }
        }
        return($GPROTEIN);
}


sub jGetProteinID
{
my($lign)=@_;
my(@Tlign2);

        if ( $lign =~ /\:/ )
        {
                @Tlign2  = split(':',$lign);
        }
        else
        {
                @Tlign2  = split('_',$lign,3);
        }
        $Tlign2[2]=~s/\;//;
        if ( $Tlign2[2] ne "" )
        {
                return($Tlign2[2]);
        }
        else
        {
                $lign=~ s/\;//;
                return($lign);
        }
}


sub jReadSeq
{
local ($seqfile) = @_;
my($seq,$longueur);
my($l);
my(@F);

        open(SEQ,"$seqfile") || die "Unable to open $seqfile#\n";
        while(<SEQ>)
        {
                if ( /^>/ )
                {
                        @F=split;
                        $longueur = $F[2];
                }
                else
                {
                        $seq = "$seq$_";
                }

        }
        $seq = "\U$seq";
        $seq =~ s/[\d\s]+//g;
        close(SEQ);
        $l = length($seq);
#        if ( $l != $longueur )
#        {
#                print STDERR "ERROR: the real length of the sequence is not the same than in comment ($l instead of $longueur)\n";
#                exit;
#        }
#        ($seq,$longueur);
	($seq,$l);
}


sub jPrintSelectCompleteGenomes
{
my(@TList) = `$UNIXCAT $COMPLETE_CONFIG`;
my($elem) = "";
my(@F);

	print "<SELECT NAME=COORD>\n";
	print "<OPTION VALUE=>\n";
	chop(@TList);
	foreach $elem (@TList)
	{
		@F=split('\|',$elem);
		print "<OPTION VALUE=$F[0]>$F[$#F]\n";
	}
	print "<SELECT>\n";
}


sub jGetPrositeUrl
{
my($id,$ac)=@_;
my($num);
my($ps_tmp);

	($num) = ($ac =~ /PS(\d+)/ );
	$ps_tmp =  ( $num >= 50000 ) ? $psf_mask : $ps_mask;
	$ps_tmp =~ s/MASK/$ac/;
	$ps_tmp =~ s/MASK/$id/;
	return($ps_tmp);
}

sub jGetInterProUrl
{
my($id,$ac)=@_;
my($num);
my($ipr_tmp) = $ipr_mask;

	$ipr_tmp =~ s/MASK/$ac/;
	$ipr_tmp =~ s/MASK/$id/;
	return($ipr_tmp);
}


sub jGetSPUrl
{
my($id,$ac)=@_;
my($num);
my($sp_tmp);

	$sp_tmp =  ( length ($ac) > 6  ) ? $em_mask : $sp_mask;
	$sp_tmp =~ s/MASK/$ac/;
	$sp_tmp =~ s/MASK/$id/;
	return($sp_tmp);
}

########################################################################
#[jRefDB
#]
sub jRefDB
{
local($lien)=@_;
my($pd_tmp,$sp_tmp,$gi_tmp,$em_tmp);
my($sm_tmp); #a gerer avec ORGA_CONFIG
my(@J)=();

    if ( $lien =~ /;sp_/ && $lien =~ /gi_/  )
    {
         ($id_gi,$id_sp,$nom_sp) = / gi_(\d+);sp_(......)_(\w+)/;
    }
    elsif ( $lien =~ /;sp_/ && $lien =~ /pd_/ )
    {
         ($id_pd,$id_sp,$nom_sp) = / pd_(\w+);sp_(......)_(\w+)/;
    }
    elsif ( $lien =~ /sp_/ )
    {
         ($id_sp,$nom_sp) = / sp_(......)_(\w+)/;
    }
    elsif ($lien=~/em_/)
    {
        ($id_em) = /em_(\w+\d+)/; 
    }
    elsif ($lien=~/s[nm]:/)  # PAT 24-05-00 # JER n'est pas generique
    {
                ($sm_bac,$sm_entry) = /sm:(\w+):(\w+);/;# PAT 24-05-00
                @J=split('_',$sm_bac);
                $sm_lab = "\L$J[1]";
    }
    else 
    {
        ($id_gi) = / gi_(\d+);/;
    }

    $pd_tmp = $pd_mask;
    $sp_tmp = $sp_mask;
    $gi_tmp = $gi_mask;
    $em_tmp = $em_mask;
    $sm_tmp = $sm_mask;

    $pd_tmp =~ s/MASK/$id_pd/g;
    $sp_tmp =~ s/MASK/$id_sp/g;
    $gi_tmp =~ s/MASK/$id_gi/g;
    $em_tmp =~ s/MASK/$id_em/g;

    # JER 07-07-2000 N'est pas generique
    $sm_tmp =~ s/BACMASK/$sm_bac/g;
    $sm_tmp =~ s/LABMASK/$sm_lab/g;
    $sm_tmp =~ s/ENTRYMASK/$sm_entry/g;

    $lien =~ s/pd_$id_pd;/ pd$pd_tmp /;
    $lien =~ s/gi_$id_gi;/ gi$gi_tmp /;
    $lien =~ s/sp_${id_sp}_/ sp$sp_tmp /;
    $lien=~ s/em_$id_em/em$em_tmp/;
    $lien=~ s/s[nm]:$sm_bac:$sm_entry/sm $sm_tmp/;  # PAT SN-->SM

    return ($lien);
}


sub jParseMatch
{
my($hit)=@_;
my(@F) = ();
my(@G) = ();
my(%idxMATCH);

    ($idxMATCH{"lq"})    = ($hit =~ / LQ=(\d+) /);
    ($idxMATCH{"ls"})    = ($hit =~ / LS=(\d+) /);
    ($idxMATCH{"SCORE"}) = ($hit =~ / S=(\d+) /);

    ($idxMATCH{"PROBA"}) = ($hit =~ / E=([\w\-\.]+) /);
    if (($idxMATCH{"PROBA"}) =~ /^e/)
    {
        ($idxMATCH{"PROBA"}) = "1". ($idxMATCH{"PROBA"}) ;
    }
    $idxMATCH{"PROBA_INF"}=$idxMATCH{"PROBA"};## PAT 05-05-99 

    ($idxMATCH{"IDENTITE"}) 	= ($hit =~ / I=(\d+)% /);
    ($idxMATCH{"POS" })		= ($hit =~ / P=(\d+)% /);
    ($idxMATCH{"GAP"})		= ($hit =~ / G=(\d+)% /);
    ($idxMATCH{"PCS" })		= ($hit =~ / %S=(\d+) /);
    ($idxMATCH{"PCQ"}) 		= ($hit =~ / %Q=(\d+) /);
    ($idxMATCH{"FRAME"}) 	= ($hit =~ / F=([+-]\d+) /);

    @F = split("//",$_,9999);
    @G = split;
    $idxMATCH{"BEGIN" }	 	= $G[1];
    $idxMATCH{"END" }	 	= $G[2];
    $idxMATCH{"LONG_HSP"}	= abs ($G[2]-$G[1])+1;
    $idxMATCH{"id_query"}	= $G[0];
    $idxMATCH{"id_query"}=~ s/<//;

    $idxMATCH{"s_name"}		= $G[4];
    $idxMATCH{"comment"}	= $F[$#F];
    $idxMATCH{"lien"}		= $F[1];

    return %idxMATCH;
}


sub jPrintMATCH
{
local ($clone_ouput,$prog_meth,$line,$file,$hit) = @_;
my($comment,$program,$lien,$val);
my(@F);

my(%iparam) = &pParseMATCH(); 
my($id);

    $comment=$iparam{"comment"};
    $comment=~s/ +/ /g;
    &pColorRed(*comment);
    @F=split(' ',$iparam{"lien"});
    $lien = &jRefDB($F[0]);
    if ( $clone_ouput eq "STS"  || $clone_ouput eq "EST" || $clone_ouput eq "CONTIG" || $clone_ouput eq "AA" )
    {
            $line = "$ROOT/$line" if ( $line !~ /^$ROOT/ );
	    if ( $clone_ouput eq "STS" )
	    {
			$idxMATCH{id_query} = "$idxMATCH{id_query}_STS" if ( $idxMATCH{id_query} !~ /_STS$/ );
  	    }
	    elsif ( $clone_ouput eq "EST" )
	    {
			$idxMATCH{id_query} = "$idxMATCH{id_query}_EST" if ( $idxMATCH{id_query} !~ /_EST$/ );
            }
	    elsif ( $clone_ouput eq "AA" )
	    {
			$idxMATCH{id_query} = "$idxMATCH{id_query}_AA" if ( $idxMATCH{id_query} !~ /_AA$/ );
            }
	    $id = $idxMATCH{id_query};
	    if ( $clone_ouput eq "CONTIG" )
	    {
		    $val = sprintf "<A HREF=$WWW_SCRIPT_AA_DNA_FILTRE_BLAST?ORGA_CONFIG=$GidxParam{ORGA_CONFIG}&FILE=$file>$id</A> S=%5d E=%7s I=%3d P=%3d G=%2d \%S=%3d $lien $comment\n",$iparam{SCORE},abs($iparam{PROBA}), $iparam{IDENTITE},$iparam{POS},abs($iparam{GAP}),$iparam{"PCS"};	
	    }
	    else
 	    {
		    $id =~ s/_[SE]TS$//;
		    $val = sprintf "\n<LI><A HREF=$WWW_SCRIPT_AA_DNA_EDIT_ENTRY?ORGA_CONFIG=$GidxParam{ORGA_CONFIG}&DIR=$line&IN=$idxMATCH{id_query}>$id</A> <B>S=%5d E=%7s I=%3d P=%3d G=%2d \%S=%3d</B>\n\n\t$lien $comment\n",$iparam{SCORE},abs($iparam{PROBA}), $iparam{IDENTITE},$iparam{POS},abs($iparam{GAP}),$iparam{"PCS"};	
	    }
    }
    else
    {
	    $val = sprintf "<A HREF=$WWW_SCRIPT_AA_DNA_FILTRE_BLAST?FILE=$file&BEGIN=$iparam{BEGIN}&END=%d&SCORE=$iparam{SCORE}&IDENTITE=$iparam{IDENTITE}&COM=$iparam{\"s_name\"}&GAP=%d&POS=$iparam{POS}&ORGA_CONFIG=$GidxParam{ORGA_CONFIG}>$prog_meth</A> $iparam{FRAME} %6d-%6d\tS=%5d E=%7s I=%3d P=%3d Gap=%2d \%S=%3d $comment \n",abs($iparam{END}),abs($iparam{GAP}),$iparam{BEGIN},abs($iparam{END}),$iparam{SCORE},abs($iparam{PROBA}), $iparam{IDENTITE},$iparam{POS},abs($iparam{GAP}),$iparam{"PCS"};
    }
    print $val;
}

sub jPrintError
{
my($msg,$code)=@_;

        if ( $code == 1 )
        {
                print "<BR><Font COLOR=red>$msg</Font>\n";
        }
        elsif ( $code == 2 )
        {
                print "<P><Font COLOR=red>Some parameters are not correct, please go back to the previous form and correct them</Font>\n";
        }
}

sub jIsARealObj
{
my($lign)=@_;

	chomp($lign);
	if ( $lign =~ /RHO$/ || $lign =~ /\|PSIGNAL\|/ )
	{
		return(0);
	}
	return(1);
}


sub jGetDirFromSeqFileName
{
my($seqfile) = @_;
my($dir,$tmp)="";

	($dir) = ( $seqfile =~ /(.+)\/$ORFDIR\/.+\/.+/ );
	if ( $dir eq "" )
	{
		($dir) = ( $seqfile =~ /(.+)\/.+\/.+/ );
	}
	$dir =~ s/\/*$//;
	return($dir);
}

sub jGetDirAndEntryFromCompleteSeqFile
{
my($seqfile) = @_;
my($dir,$entry,$filename,$tmp)="";

	($dir,$entry,$filename) = ( $seqfile =~ /(.+)\/$ORFDIR\/(.+)\/(.+)/ );
	if ( $dir eq "" )
	{
		($dir,$entry,$filename) = ( $seqfile =~ /(.+)\/(.+)\/(.+)/ );
	}
	$dir =~ s/\/*$//;
	return($dir,$entry,$filename);
}

# JER 22-08-2000
# cette fonction a ete reecrite a partir de pTraduction car la fonction de Patricia
# renvoie une sequence traduite mais le premier caractere est different de M, le controle est (parfois!!) fait ailleurs.
# la fonction presente recoit en plus la liste des codons valides et un flag qui signale si l'on
# veut faire un controle du start ou pas
# si codon ds la liste (controle ou pas) ==> return (seq,longueur en aa)
# sinon
#	si controle return ("",0)
#	sinon return (sequence traduite sans modification du start, longueur en AA)

sub jTraduction
{
local($sequence,$code_lettre,$file_CODE,$liste_codon_start,$control)=@_;
my($i);
my($AA);
my($seq_traduite);
my($start_codon);
    
    	my($nAAs)	= length($sequence)/3;
    
	if ( ! -e $file_CODE ) 
    	{
        	$file_CODE = "$DEFAULT_FILE_CODE_GENETIQUE";
    	}


    	(*Idx1,*Idx3)=&pReadCode($file_CODE);


	$start_codon = substr($sequence,0,3);
	$start_codon = "\U$start_codon";

	for ($i=0, $seq_traduite=""; $i<$nAAs; $i++)
	{
		$triplet =substr($sequence,$i*3,3);
		$triplet = "\U$triplet";

		if ( &pCheckCodeTriplet($triplet) )
		{
			$AA = ($code_lettre==$CODE_UNE_LETTRE) ? $Idx1{$triplet} : $Idx3{$triplet};
		}
		else
		{
		    	$AA = "?";
		}
		$seq_traduite = "$seq_traduite$AA";
	}

	if ( $liste_codon_start =~ /$start_codon/i ) 
	{
		$seq_traduite =~ s/^\w/M/;
	}
	elsif ( $control )
	{
		$seq_traduite = "";
		$nAAs = "";
	}
	return ($seq_traduite,$nAAs);
}


1;

