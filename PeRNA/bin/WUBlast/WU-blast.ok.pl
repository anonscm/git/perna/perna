#!/usr/local/bin/perl

##[
#>Project:      iANT integrated ANnotation Tool (1999-2000)
#>Release:      1.0
#>Lab:          INRA-CNRS LBMRPM, BP 27 Chemin de Borde Rouge, 31326 Castanet Cedex France
#>Authors:      Patricia.Thebault@toulouse.inra.fr and Jerome.Gouzy@toulouse.inra.fr
#>This script:  Patricia.Thebault@toulouse.inra.fr
#>Last updated: October 31, 2000
#>Citation:
#
#Aim: Run WU-blast and reformat output to iANT results common format file
#
#
#Updated:       
#
#Who:   
#
##]

$script_ok = ( $0 =~ /\.ok\./ ) ? ".ok" : "" ;
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jConfig$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jLibPerl$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/pLibPerl$script_ok.pl";


########################################################################
#[pWU_blast_usage
#Aim:      Usage script      
#Global:  $CMD_SCRIPT_AA_DNA_REFORMATDB
#In: : Error Msg to print out
#Out: 
#Update:
#Prerequisite:     $GidxParam{IN} eq "" || $GidxParam{OUT} eq "" || $GidxParam{LIB} eq "" || $GidxParam{PROG} eq ""
#]

sub  pWU_blast_usage
{
local ($msg) = @_ ;
    print STDERR "$msg\n";	 
    print STDERR 
	" USAGE : $0 
	
Required	
        IN=Input File (Fasta format :  1 or x sequences)
        LIB=Database  (Fasta format and pressdb is required)	
        OUT=Output File 

WARNING : You have to reformat the 2 previous file with  $CMD_SCRIPT_AA_DNA_REFORMATDB

        PROG=Program Namet VALUE=blastn|tblastx|blastn                      [blastn]
             blastn:  query=nt base=nt
             tblastx: query=nt base=nt  (six-frame translations of a nucleotide query sequence against the six-frame translations of a nucleotide sequence database)
             tblastn: query=aa base=nt (compares a protein query sequence against a nucleotide sequence database dynamically translated in all reading frames)
Optional
        SUP_IDENTICAL=Suppress match if QUERY and SUBJECT sequences are identical 
	             (names,positions and identity=100) VALUE=YES|NO 	    [NO]
        PARAM=WU-BLAST parameters 	     \n";
	     
    exit(0);
}


########################################################################
#[pGetStatMatch
#Aim:   Get information from the header line (are different according to the program used)
#Global:
#In: $line, $program(tblastn,tblastx or blastn) 
#Out: $score,$proba,$iden,$pos,$frame
#Update:
#Prerequisite:     
#]

sub pGetStatMatch
{
my($line,$program)=@_;
    $line =~ s/[\(\)\,]//g;
    $line =~ s/\bSum\b//g;#Blast enleve parfois Sum!!!
    my(@S) = split(' ',$line);
    ##Score = 159 23.9 bits Expect = 7.6 P = 1.0 Identities = 211/363 58% Positives = 211/363 58% Strand = Minus / Plus
    my($score)=$S[2];
    my($proba)=$S[7];
    my($iden) =$S[14];
    my($pos)=$S[18];
    my($frame);
    if ($program =~ /tblastn/i)
    {
	$frame="F=$S[21]";
    }
    elsif ($program =~ /tblastx/i)
    {
	$frame="F=$S[21] F2=$S[23]";
    }
    else
    {
	$frame="F=0";
    }
    return($score,$proba,$iden,$pos,$frame);
}
########################################################################
#[pGetInfoSubject
#Aim:   Read the  match header line of a WU_blast output       
#Global:
#In: $line
#Out: $name,$length,$com
#Update:
#Prerequisite:     
#]
sub pGetInfoSubject
{
    local($line,$reformat)=@_;
    my(@Z) = split(' ',$line);
    my($name) ="";
    my($com) ="";
    if (!$reformat)
    {
	$name= ( $Z[4] ne "" && ($Z[4] ne  "AC;" && $Z[4] !~ /^SM[abc];/ && $Z[4] !~ /\d\d\d;/) ) ? "$Z[4]$Z[0]" : $Z[0]; # JER
    }
    else
    {
	$name=$Z[0];
	$com= ( $Z[4] ne "" && ($Z[4] ne  "AC;") && $Z[4] !~ /\d\d\d;/) ? $Z[4] :""; 
	
    }
    $name =~ s/>//;
    my($length) = $Z[2]; 
    my(@G) =split('\|',$line,4);
    $com .= $G[$#G];
    return($name,$length,$com);
}

########################################################################
#[pCalculateGap
#Aim:        Calculate gaps percentage for a query sequence  
#Global:
#In: $query
#Out: $n_gaps
#Update:
#Prerequisite:     
#]
sub pCalculateGap
{
    local($query)=@_;
    my($gaps)=0;
    my($n_gaps)=0;
    $gaps=($query=~ s/-/-/g);
    if ($gaps ==0 || $gaps eq "")
    {
	$n_gaps =0;
    }
    else
    {
	$n_gaps =$gaps;
    }
    if ($n_gaps != 0)
    {
	    $n_gaps = int(($gaps /length($query))*100);
	    $n_gaps=1 if ($n_gaps<1 && $n_gaps>0); # JER 29/09/1999
    }
    return $n_gaps;
}

########################################################################
#[ReformatWU_BLAST
#Aim:         Reformat the WU_blast output to the iANT common results format 
#Global:
#In: $infile,$fich,$prog,$db
#Out: 
#Update:
#Prerequisite:     
#]
sub ReformatWU_BLAST
{
    local ($infile,$fich,$prog,$db,$supp_identical,$reformat_position,$inverse_position)=@_;
    open(OUT,">>$fich");
    open(IN,"$infile");
    my($program)="";
    chop($program = <IN>);
    my($q_name)="";my($q_length)="";
    my($add_com)=$NO;
    my($n_gaps)=0;
    my($seq_query)="";my($q_begin)=0;my($q_end)=0;my($s_begin)=0;my($s_end)=0;
    my($header_subject_to_print)="";
    while ( $_ = <IN> )
    {
	chop;
	my(@F)=split;
	if ( /^Query=/ )
	{
	    $q_name = $F[1]	;
	    $line_q_name=$_;
	    $line_q_name=~ s/Query= //;
	    $line_q_name=~ s/^ //g;
	    $line_q_name=">$line_q_name";
	}
       
	if ( / letters\)/ )
	{
	    $q_length = $F[0];
	    $q_length =~ s/[\(\,]//g;
	}
	$db = $F[1] if ( /^Database:/ );
	$version = $_ if ( /^    Posted date:/ );
	if ( ($q_name ne "") && ($q_length ne "") )
	{
	    print OUT "QUERYFILE:   $fich\n";
	    print OUT "QUERY:       $q_name\n";
	    print OUT "QUERYLENGTH: $q_length\n";
	    print OUT "PROGRAM:     $program\n";
	    print OUT "DATABASE:    $db\n\n";
	    $add_match=$NO;
	    $add_com=$NO;
	    ($s_name,$s_length,$commentaire)=&pGetInfoSubject($_,$reformat_position);	    
	    my($i)=0;
	    my(@HSP)=();
	    while (($_ = <IN>) )
	    {	   
	        chomp;
	    	if (( /^>/ || /^ Score/ || /^Parameters/)&&($#HSP>0))
		{	
			if (($s_name eq $q_name)&&($q_begin == $s_begin) && ($q_end == $s_end) && ($iden == 100)&& ($supp_identical))
			{
				#### Suppression, rien ne se passe
				# SUP_IDENTICAL  ATTENTION ## si deux sequences identiques mais avec des N, I ne sera pas egale a 100 et donc sequences ne seront pas supprimmees.
			}		
			else
		  	{       #### Not identical :: alors j'imprime
			      #  print STDOUT "($s_name eq $q_name)&&($q_begin == $s_begin) && ($q_end == $s_end) && ($iden == 100)&& ($supp_identical)\n";
			       if ($header_subject_to_print ne "")
			       {
			       		print OUT $header_subject_to_print;
					$header_subject_to_print="";
			       }
			       if ($reformat_position && $s_name =~ /-(\d)+-(\d)+/)
			       {
				       	my(@D)=split('-',$s_name);
					my($reformat_s_begin)=0;
					my($reformat_s_end)=0;
					if ($q_begin< $q_end)
					{
					    $reformat_s_begin=$D[1]+$s_begin-1;
					    $reformat_s_end=$D[1]+$s_end-1;
					}
					else
					{
					    $reformat_s_begin=$D[1]+$s_end-1;
					    $reformat_s_end=$D[1]+$s_begin-1;
					    $tmp=$q_begin;
					    $q_begin=$q_end;
					    $q_end=$tmp;
					    $tmp=$s_begin;
					    $s_begin=$s_end;
					    $s_end=$tmp;
					}
					my($reformat_s_name)=$D[0];
					print OUT "\n<$q_name $q_begin $q_end // $s_name#$s_begin-$s_end $reformat_s_begin $reformat_s_end // S=$score E=$proba I=$iden P=$pos  ";				
			       }
			       elsif ($inverse_position)
			       {
				   my($reformat_s_begin)=$s_length-$s_begin+1;
				   my($reformat_s_end)=$s_length-$s_end+1;
				   print OUT "\n<$q_name $q_begin $q_end // $s_name $reformat_s_begin $reformat_s_end // S=$score E=$proba I=$iden P=$pos  ";
			       }
			       else
			       {
				   print OUT "\n<$q_name $q_begin $q_end // $s_name $s_begin $s_end // S=$score E=$proba I=$iden P=$pos  ";
			       }
			     
			       $n_gaps=&pCalculateGap($seq_query);
			       if ( $commentaire =~ /\; \| / )
			       {
			           @S=split(/\; \| /,$commentaire,99999);
			           $commentaire = $S[1];
			       }
			       $pc_s = int (abs( $s_end - $s_begin + 1) / $s_length * 100);
			       $pc_q = int( abs( $q_end - $q_begin + 1)  / $q_length * 100);
			       # JER Ajoute le 07-08-2000 pour avoir le commentaire de la sequence subject sur une ligne (chop non fait au momment de la lecture
			       $commentaire =~ s/\n//g;
			       $commentaire =~ s/ +/ /g;
			       # Fin JER
			       print OUT "G=$n_gaps% LS=$s_length %S=$pc_s LQ=$q_length %Q=$pc_q $frame // $commentaire\n" ;
			       for ($j=0;$j<=$#HSP;$j++)
			       {
			           print OUT "$HSP[$j]";
			       }
		    	}
			@HSP=();
			$i=0;
			$seq_query="";
			$q_begin=0;
			$q_end=0;
			$s_begin=0;
			$s_end=0;
			$add_match=$NO;
		}
		last if (/^Parameters/);
		if ( /^>/ )
		{  
		    $header_subject_to_print= "$_\n";
		    ($s_name,$s_length,$commentaire)=&pGetInfoSubject($_,$reformat_position); 
		    $add_com=$YES;
		    next;
		}		  
		if (($add_com)&&(! /Length =/))
		{
		    $header_subject_to_print.="$_\n";
		    $commentaire.="$_";		    
		    next;
		}
		if (/Length =/ )
		{
		    $add_com=$NO;
		    $add_score=$YES; 
		    next;
		}
		if ( /Score = /) # sur deux lignes seulement
		{
		    #chop;
		    $line_score=$_;
		    $_=<IN>;
		    chomp;
		    $line_score ="$line_score , $_";
		    ($score,$proba,$iden,$pos,$frame)=&pGetStatMatch($line_score,$prog);
		    $add_match=$YES;
		    $_=<IN>;
		    chomp(); 
		}
		
		if (($add_match)&&(!/Minus Strand HSPs/))
		{
		    
		    $HSP[$i] = "$_\n";	
		    if ($HSP[$i] =~ /^Query/)
		    {
			my(@Q)=split(" ",$HSP[$i]); 
			$seq_query.=$Q[2] ;
			$q_begin=$Q[1] if ($q_begin==0);
			$q_end=$Q[3];
		    }
		    elsif ($HSP[$i] =~ /^Sbjct/)
		    {
			my(@Q)=split(" ",$HSP[$i]);
			$s_begin=$Q[1] if ($s_begin==0);
			$s_end=$Q[3];
			if ($reformat_position || $inverse_position)
			{
			    $HSP[$i] =~ s/\d/ /g;
			}
		    }
		    
		    $i++;
		}
		   
	    }
	    last; #### SORT du premier while	    
	}
    }

    print OUT "//\n\n";
    close(OUT);
    close(IN);
}

########################################################################
#[pRunBlast
#Aim:       To run blast for ONE sequence   
#Global:
#In:        $in, $lib ,$out,$prog,$param
#Out: 
#Update:
#Prerequisite:     The library file has to been reformatted with ReformatDB and pressdb
#]
sub pRunBlast
{
    local($in, $lib ,$out,$prog,$param) = @_;
    if ($prog =~ /tblastn/i)
    {
	$prog =$WU_tBLASTN;
    }
    elsif ($prog =~ /tblastx/i)
    {
	$prog =$WU_tBLASTX;
    }
    else
    {
	$prog =$WU_BLASTN;
    }
    if ( $param !~ /B=/i )
    {
	$param = "$param B=50000";
    }
    $cde = "$prog  $lib $in $param > $out ";
    #print STDOUT "#$cde#######\n";
    system("$cde");
}

########################################################################
#[pRunBlastMultiSequences
#Aim:     To run blast for a file with more than ONE sequence     
#Global:  $UNIXGREP
#In:      $file,$lib,$prog,$param,$outfile
#Out: $nb_sequences
#Update:
#Prerequisite:     The library file has to been reformatted with ReformatDB and pressdb
#]
sub pRunBlastMultiSequences
{
     local($file,$lib,$prog,$param,$outfile,$supp_identical,$reformat_position)=@_;
     open(FILE,"$file") || print STDOUT "cannot open $file\n";
     my(@name_seq)="";
     my(@seq)="";
     my($nb_sequences)=`$UNIXGREP \">\" $file| wc -l`;
     my($i)=0;
     while ( $_ = <FILE> )
     {
	 if (/^>/ )
	 {
	     $i++;
	     $name_seq[$i]=$_;
	     $seq[$i]="";
	 }
	 else
	 {
	     $seq[$i].=$_;
	 }
     }
     close(FILE);
     my($num)=0;
     for ($i=1;$i<=$#seq;$i++)
     {
     	 $num++;
	 my ($date) = time;
	 my($tmp_file)="${file}_${date}_$num";
	 # FIN JER
	 open (OUT,">$tmp_file") || die "cannot open $tmp_file\n";
	 print OUT "$name_seq[$i]";
	 print OUT "$seq[$i]";
	 close OUT;
	# print STDOUT "#$tmp_file#$name_seq[$i]#########\n";
	 &pRunBlast($tmp_file,$GidxParam{LIB},"$tmp_file.ori",$prog,$param);
	 &ReformatWU_BLAST("$tmp_file.ori",$outfile,$prog,$lib,$supp_identical,$reformat_position);
	 system ("$UNIXRM $tmp_file");
	 system("$UNIXRM  $tmp_file.ori");
     }
     return $nb_sequences;
}

########################################################################
#[pInitWU_BLAST
#Aim:        Init Parameters   
#Global:     $WU_BLASTN , $ROOTTMP
#In: 
#Out: 
#Update:     *param
#Prerequisite:     
#]
sub pInitWU_BLAST
{
    local(*param)=@_;
    
    $param{IN}="";
    $param{OUT}="";
    $param{LIB}="";
    $param{TMP}=$ROOTTMP;
    $param{PROG}=$WU_BLASTN;
    $param{SUP_IDENTICAL}=$NO;
    $param{REFORMAT_POSITION}=$NO;
    $param{INVERSE_POSITION}=$NO;
    $param{PARAM}="";	
}

###############################MAIN############################
&pInitWU_BLAST(*GidxParam);
%GidxArg=&pTab2Indx(@ARGV);
&pFillParam(*GidxArg,*GidxParam);

if ($GidxParam{IN} eq "" || $GidxParam{OUT} eq "" || $GidxParam{LIB} eq "" || $GidxParam{PROG} eq ""  )
{
    &pWU_blast_usage("FATAL : Parameters are missing \n");
}


my($nb_seq)=`$UNIXGREP \">\" $GidxParam{IN} | wc -l`;
system ("$UNIXRM $GidxParam{OUT}") if (&pFileExiste($GidxParam{OUT}));

if ($nb_seq>1)
{
    &pRunBlastMultiSequences($GidxParam{IN},$GidxParam{LIB},$GidxParam{PROG},$GidxParam{PARAM},$GidxParam{OUT},$GidxParam{SUP_IDENTICAL},$GidxParam{REFORMAT_POSITION});
}
else
{
    my($date) = time;
    chomp($date);
    &pRunBlast($GidxParam{IN},$GidxParam{LIB},"$GidxParam{IN}.$date.blast.ori",$GidxParam{PROG},$GidxParam{PARAM});
    &ReformatWU_BLAST("$GidxParam{IN}.$date.blast.ori",$GidxParam{OUT},$GidxParam{PROG},$GidxParam{LIB},$GidxParam{SUP_IDENTICAL},$GidxParam{REFORMAT_POSITION},$GidxParam{INVERSE_POSITION});
    system("$UNIXRM  $GidxParam{IN}.$date.blast.ori ");
}




















