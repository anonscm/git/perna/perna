$script_ok = ( $0 =~ /\.ok\./ ) ? ".ok" : "" ;

$DEFAULT_ORGANISM = "rhime";	# JER doit etre supprime
$COMMON_DIR = "/www/Common";
$debug = 1;
$YES=1;
$NO=0;
$ERROR_CODE = -999.98765;


#$PROG_MAIL = "/usr/ucb/Mail"; # commented by MJO 18 Jul. 2005

# $GidxParam{OBJ} possible values (default = $PROTEIN)
# set in jSetObjetType
$GPROTEIN = 1;
$GEST     = 2;
$GGC      = 4; # cluster gene (cDNA)
$GSTS     = 3;
$GIS     = 5; # IS
$GMISC   = 6;

#$ROOT =	"/home/pat/public_html/"; # commented by MJO 18 Jul. 2005
$ROOT =	"/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/";  # MJO 18 Jul. 2005
#$ROOT =        "/cgi-bin/";
$iANT = "iANT";
#$ROOT_iANT =	"/home/pat/public_html/"; # commented by MJO 18 Jul. 2005
$ROOT_iANT =	"/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/"; # MJO 18 Jul. 2005
$iANT_LIST_ORGANISMS=   "$ROOT_iANT/iANT.LIST.ORGANISMS.txt";

#$ROOTTMP = "/home/pat/public_html/tmp";	# JER 14-07-2000 # commented by MJO 18 Jul. 2005
$ROOTTMP = "/bioinfo/www/bioinfo/perna/PeRNA/tmp";	# JER 14-07-2000  # MJO 18 Jul. 2005

$DIR_CONFIG     ="CONFIG";
$DIR_IMG	="$DIR_CONFIG/IMG";
$DIR_DOC_HTML   ="$DIR_CONFIG/DOC_HTML"; 
$BBA_HTML       ="BBA.html";

$IANT_BLANK_HTML = "/$iANT/$DIR_CONFIG/blank.html";
$MAIN_HTML      ="main.html";
$VIEWER_HTML      ="viewer.html"; # JER ajoute 19/05/2000
$ASSEMBLY_HTML	= "Assembly.html";	
$LOG_ASSEMBLY_HTML	= "LogAssembly.html";	
$CONTIG_LIST_HTML= "ContigList.html";	
$MENU_HTML      ="menu.html";
$DIR_ORF_DEL    = "ORF_DEL";

# JER 15-11 $DIR_VALIDATED_BACS="/www/rhime/lab2/VALIDATED_BACS";

#### Repertoire necessaire a chaque organism
$DIR_DOC        ="doc";
$DIR_DB         ="db";
$DIR_AC		="AC";

$DIR_TMP="$ROOTTMP/CARTE/";
$DIR_FOR_CARTE =$DIR_TMP;	# JER pourquoi 2 variables
#$WWW_DIR_TMP="/pat/tmp/CARTE/";	# JER 14-07 utiliser plutot $DIR_TMP en subsituant ^$ROOT  # commented by MJO 18 Jul. 2005
$WWW_DIR_TMP="/bioinfo/www/bioinfo/perna/PeRNA/tmp/CARTE/";	# JER 14-07 utiliser plutot $DIR_TMP en subsituant ^$ROOT  # MJO 18 Jul. 2005

#####################################
#### Fichier modele necessaire a l'installation d'une bacterie dans iANT
$MODELE_BACTERIE_CONFIGURE_1    ="$ROOT_iANT/$DIR_CONFIG/MASK.config.1";
$MODELE_BACTERIE_CONFIGURE_2    ="$ROOT_iANT/$DIR_CONFIG/MASK.config.2";
$MODELE_BACTERIE_CONFIGURE   ="$ROOT_iANT/$DIR_CONFIG/MASK.config";
$MODELE_FORM_BBA			="$ROOT_iANT/$DIR_CONFIG/MODEL_$BBA_HTML";
$DEFAULT_FILE_CLASSIFICATION  ="$ROOT_iANT/$DIR_CONFIG/CLASSIFICATION.modele";
$DEFAULT_FILE_CODE_GENETIQUE  = "$ROOT_iANT/$DIR_CONFIG/CODEGENETIQUE";
$DEFAULT_FILE_CLASS_COLOR     = "$ROOT_iANT/$DIR_CONFIG/CLASS_COLOR";
$DEFAULT_FILE_DNA_PATTERNS     = "$ROOT_iANT/$DIR_CONFIG/MASK.dna.PAT";
$DEFAULT_FILE_FEATURES     = "$ROOT/$DIR_CONFIG/FEATURES.config";
$INSTALLATION_LOG_FILE     = "installation.LOG";
$DEFAULT_rRNA_DB           = "$ROOT_iANT/db/rRNA.db";
$DEFAULT_IS_DB             = "$ROOT_iANT/db/is.db";
#####################################
### HELP
$FILE_HELP_CONFIGURATION   ="/$iANT/Common/HELP/HELP_configuration.html";
$FILE_HELP_MAIN   ="/$iANT/Common/HELP/HELP_main.html";
$FILE_HELP_INSTALL_ORG   ="/$iANT/Common/HELP/HELP_install_organism.html";
$FILE_HELP_CONTROL_PANEL ="/$iANT/Common/HELP/HELP_Control_Panel.html";
$FILE_HELP_NIP 	              = "/$iANT/Common/HELP/Help_nip.html";	
$FILE_HELP_VIEWER_WORK        ="/$iANT/Common/HELP/HELP_VIEWER_WORK.html";         	
$FILE_HELP_MAIN_WORK       ="/$iANT/Common/HELP/HELP_MAIN_WORK.html";
#####################################


######## A VIRER###############
$DIR_MODELE_HTML ="$ROOT/$DIR_CONFIG/modele_HTML";
$MAIN_MODELE     ="$DIR_MODELE_HTML/modele_$MAIN_HTML";
$DB_RRNA         ="$ROOTDB/rRNA.db";
$DB_DNA_PATTERN  ="$ROOTDB/pattern.dna.pscan";
$BBA_MODELE      ="$DIR_MODELE_HTML/modele_$BBA_HTML$script_ok";
$BBA_DEFAULT     ="$DIR_MODELE_HTML/default_$BBA_HTML$script_ok";
$FORM_DEL_ORF_MODELE          = "$DIR_MODELE_HTML/modele_$FORM_DEL_ORF";
$FORM_VAL_ORF_MODELE          = "$DIR_MODELE_HTML/modele_$FORM_VAL_ORF";
$FORM_DEL_ORF                 = "ORF-TRNA_deletion.html";
$FORM_VAL_ORF                 = "ORF-TRNA_validation.html";
$FORM_RUN_SCRIPTS             = "RUN_Scripts.html";
###############################

#$HTML_STYLE = "/~pat/CONFIG/base.css";  # commented by MJO 18 Jul. 2005
#$IANT_CSS   = "/~pat/CONFIG/base.css";  # commented by MJO 18 Jul. 2005
$HTML_STYLE = "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/base.css";
$IANT_CSS   = "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/base.css";


#####################################
# DEFAULT VALUES
#

$OVERLAP_QUERY_SUBJECT_CUTOFF = 80; 
$DISPLAY_SEQUENCE_LINE_SIZE   = 50;
$LIMIT_SIZE_TO_RUN_TRADUCTION = 10000;

$HTTP_genprotec	= "http://genprotec.mbl.edu/form_gene";
$URL_SERVER	= "sequence.toulouse.inra.fr";
$URL_SYNTENY_SERVER ="http://sequence.toulouse.inra.fr/CompleteGenomes/DRAW_MAP.html";

$SIGNAL_PEPTIDE = "PSIGNAL";

#### Mask pour les liens http ######

# doit etre ameliore, je le garde pour compatibilite (blast server)
$sn_mask = "<A HREF=http://sequence.toulouse.inra.fr/rhime/LABMASK/BACMASK/ORF/coordonnees.VIEW.html TARGET=ref_db>BACMASK</A> <A HREF=http://sequence.toulouse.inra.fr/aa_dna.script/edit_entry.cgi$script_ok.pl?MODE=VIEW&LIST=NO&DIR=/www/rhime/LABMASK/BACMASK&IN=ENTRYMASK TARGET=ref_db2>ENTRYMASK</A>";
$sm_mask = "<A HREF=http://sequence.toulouse.inra.fr/rhime/LABMASK/BACMASK/ORF/coordonnees.VIEW.html TARGET=ref_db>BACMASK</A> <A HREF=http://sequence.toulouse.inra.fr/aa_dna.script/edit_entry.cgi$script_ok.pl?MODE=VIEW&LIST=NO&DIR=/www/rhime/LABMASK/BACMASK&IN=ENTRYMASK TARGET=ref_db2>ENTRYMASK</A>";

$pd_mask = "<A HREF=http://www.toulouse.inra.fr/prodom/cgi-bin/ReqProdomII.pl?id_dom2=MASK TARGET=ref_db>MASK</A>";
$gi_mask = "<A HREF=http://www.ncbi.nlm.nih.gov/htbin-post/Entrez/query?uid=MASK&form=6&db=p&Dopt=g TARGET=ref_db>MASK</A>";
$gi_mask_DNA = "<A HREF=http://www.ncbi.nlm.nih.gov/htbin-post/Entrez/query?uid=MASK&form=6&db=n&Dopt=g TARGET=ref_db>MASK</A>";
$gi_mask_PROT = "<A HREF=http://www.ncbi.nlm.nih.gov/htbin-post/Entrez/query?uid=MASK&form=6&db=p&Dopt=g TARGET=ref_db>MASK</A>";

$sp_mask = "<A HREF=http://www.expasy.ch/cgi-bin/get-sprot-entry?MASK TARGET=ref_db>MASK</A>";
$em_mask="<A HREF=http://srs.ebi.ac.uk/srs6bin/cgi-bin/wgetz?-e+[EMBL-ALLTEXT:'MASK*'] TARGET=ref_db>MASK</A>";
$ec_mask = "<A HREF=http://www.expasy.ch/cgi-bin/get-full-entry?[enzyme-id:MASK] TARGET=ref_db>MASK</A>";
$ps_mask = "<A HREF=http://www.expasy.ch/cgi-bin/prosite-search-ac?MASK TARGET=ref_db>MASK</A>";
$psf_mask = "<A HREF=http://www.isrec.isb-sib.ch/cgi-bin/get_pstprf?MASK TARGET=ref_db>MASK</A>";
$rz_mask = "<A HREF=http://sequence.toulouse.inra.fr/srs5bin/cgi-bin/wgetz?-e+[RHIME-ID:'MASK']>MASK</A>";
$bac_mask ="<A HREF=http://sequence.toulouse.inra.fr/srs5bin/cgi-bin/wgetz?-lv+5000+-f+GeneName+-f+Description+[rhime-bac:MASK]>MASK</A>";
$ipr_mask = "<A HREF=http://srs6.ebi.ac.uk/srs6bin/cgi-bin/wgetz?-id+4Flds1EnCA0+[interpro-AccNumber:MASK]+-e TARGET=ref_db>MASK</A>";

#### Editable fields #####

$LIST_EDITABLE_FIELDS = " AN DE DF DC CL GN GC FA EC "; # " EX FA EC ";

		
#####################################
# FILES
#

@LIST_DIR_EXTENSION = ("TRNA","RRNA","RIME","MOTIF","IS","MISC","REP","OBJ","snRNA");	
%LIST_TYPE_ORF=(Protein=>"AA",TRNA=>"TRNA",tRNA=>"TRNA",rRNA=>"RRNA",RIME=>"RIME",MOTIF=>"MOTIF",IS=>"IS",MISC=>"MISC",Miscelleaneous=>"MISC",REP=>"REP",OBJ=>"OBJ",Object=>"OBJ");
@LIST_TYPE_ORF_CONFIGURE=(
			["AA","Proteins","RECTANGLE"],
			["REP","Unit repeats","TRIANGLE"],
			["MISC","Miscelleanous","MINI_RECT"],
			["OBJ","Objects","MINI_RECT"],
			);

#### Variable pour mask dna 
####### FORMAT type =>["code_lettre_dans_fichier_mask(O si rien),"couleur_de_representation_du_type] , "Texte pouor legende"]
%MASK_VALUE=(
	TRNA=>["1","black","Putative tRNA"],
	SHNDAL=>["2","purple","Putative Shine et Dalgarno sequence"],
	RHO_INDEP=>["3","green","Putative Terminaison Rho Independent site"],
	PIPBOX=>["4","pink","Putative PIPBOX sequence"],
	RIME=>["6","blue","Putative RIME sequence"],
	REP=>["9","blue","Putative Repetitive Extragenic Palindromes"],
	MOTIF=>["7","cyan","Putative MOTIF sequence"],
        BOUCLE=>["5","green","Putative BOUCLE structure"],
	DEFAULT	=>["8","grey","DNA Motif"],
		);
@LIST_OBJECT_DNA= (keys %MASK_VALUE);



$SUGGESTEDANN_TAG	     = "SuggestedAnnotation";
$LSTDIR_EXTENSION 	      = "lstdir";
$SANS_SEG_EXTENSION           = "sptr_sans_seg";	
$ORFDIR                       = "ORF";
$SEQUENCE_FILE_EXTENSION      = "AA";
$ENTRY_FILE_EXTENSION	      = "dat";
$STAT_FILE_EXTENSION	      = "stat";
$DNAINFO_FILE_EXTENSION       = "info";
$BLASTX_EXTENSION             = "blastx";
$NR                           = "nr";
$BLASTN_EXTENSION             = "blastn";
$BLASTX_FILTRE_EXTENSION      = "xnr";
$BLASTX_FILTRE_E1_EXTENSION   = "$BLASTX_FILTRE_EXTENSION.E:1e-2";    
$BLASTX_FILTRE_E2_EXTENSION   = "$BLASTX_FILTRE_EXTENSION.E:1e-4"; 
$BLAST_CUTOFF_E1_EXTENSION    = "E:1e-2";
$BLAST_CUTOFF_E2_EXTENSION    = "E:1e-4";
$FRAMED_EXTENSION             = "frameD";
$ORI_EXTENSION                = "ori";
$HTML_EXTENSION               = "html";
$DNA_MASK_EXTENSION           = "dna.mask";
$MOTIF_EXTENSION              = "motifs";
$FILTRE_EXTENSION             = "filtre";
$IS_EXTENSION		      = "is";
$TRNA_EXTENSION               = "tRNA";
$RRNA_EXTENSION               = "rRNA";
$DNA_PATTERN_EXTENSION        = "dna.PAT";
$GZ_EXTENSION                 = "gz";
$EXTRACTION_EXTENSION         = "extract";
####WARNING entension ===> extension
$ENTRY_FILE_ENTENSION         = $ENTRY_FILE_EXTENSION; # Variable a ne pas utiliser JER conserve temp pour compatibilite
$ANNOTATION_FILE_EXTENSION    = "aa.00.annotation";
$HISTORY_EXTENSION    	      = "history";
$LSTDIR_EXTENSION	      = "lstdir";
$ANNOTATION_FILE_ENTENSION    = $ANNOTATION_FILE_EXTENSION;	# Variable a ne pas utiliser JER
$PROSITE_FILE_EXTENSION       = "PS";
$PROFILE_FILE_EXTENSION       = "PSF";
$INTERPRO_FILE_EXTENSION     = "IPR";
$PROSITE_FILE_ENTENSION       = $ANNOTATION_FILE_EXTENSION;	# Variable a ne pas utiliser JER
$SEG_FILE_EXTENSION           = "seg";
$SEG_FILE_ENTENSION           = $SEG_FILE_EXTENSION;	# Variable a ne pas utiliser JER
$EMBL_EXTENSION               = "embl";
$RAW_EXTENSION                = "raw";
$GBK_EXTENSION               = "gbk";

####WARNING extention ===> extension

$PRODOM_FILE_EXTENTION        = "prodom";
$SPTR_FILE_EXTENTION          = "sptr";
$SPTR_RE_FILE_EXTENTION       = "10.sptr";
$NR_FILE_EXTENTION            = "nr";

$RHIME_FILE_EXTENSION         = "rhimedb";
$TM_FILE_EXTENSION            = "TM";

$BAC_PREVIOUS_EXTENSION	      = "previous";
$BAC_NEXT_EXTENSION	      = "next";


$HTML_INDEX_AA_FILE_NAME      = "index.html";
$HTML_COORDONNEES_TXT	      = "coordonnees.txt";
$HTML_COORDONNEES_EDIT        = "coordonnees.EDIT.html";
$HTML_COORDONNEES_VIEW        = "coordonnees.VIEW.html";
$HTML_LOG_AA		      = "LOG.aa.html";
$HTML_LOG_DNA		      = "LOG.dna.html";
$HTML_LISTE_PREDICTION	      = "liste.html";
$HTML_VIEW_LISTE_PREDICTION   = "View_liste.html";
$HTML_RAPPORT		      = "rapport.html";
$HTML_RAPPORT_DIR	      = "RAPPORT";

$LISTE_BLASTX_MASK	      = "BLASTX_MASK";

########## IMAGES #######################
$CONFIG_COLOR		      = "$ROOT_iANT/$DIR_CONFIG/DEFAULT_LIST_COLOR.txt";
$IMG_16S		      = "$ROOT_iANT/$DIR_IMG/16St.gif";
$IMG_23S                      = "$ROOT_iANT/$DIR_IMG/23St.gif";

$IMG_tRNA                     = "$ROOT_iANT/$DIR_IMG/tRNA.gif";
$IMG_tRNA_LARGEUR            = 10;
$IMG_tRNA_HAUTEUR            = 18;

$IMG_5S                       = "$ROOT_iANT/$DIR_IMG/5St.gif";

$IMG_SS                       = "$ROOT_iANT/$DIR_IMG/Signal.gif";
$IMG_SS_LARGEUR               = 8;  
$IMG_SS_HAUTEUR               = 30; 

$IMG_RHO_IND                  = "$ROOT_iANT/$DIR_IMG/rho_indep.gif";
$IMG_RHO_IND_INV              = "$ROOT_iANT/$DIR_IMG/rho_indep_reverse.gif";
$IMG_RHO_IND_LARGEUR          = 11;
$IMG_RHO_IND_HAUTEUR          = 15; 

$WWW_IMG_NEXT_ARROW           = "/$iANT/$DIR_IMG/next_arrow.gif";
$WWW_IMG_PREVIOUS_ARROW       = "/$iANT/$DIR_IMG/prev_arrow.gif";
#########################################


$LIST_AC		      = "LISTAC";
$LIST_AC_DEL	              = "LISTAC.del";


	
###
##################################
# UNIX CMD  updated by MJO 18 Nov. 2005 for cat machine
#

$UNIXFIND = "find";
$UNIXMORE = "more";
$UNIXSORT   = "sort";
$UNIXRM   = "rm -f";
$UNIXLS   = "ls";
$UNIXLN   = "ln -s";
$UNIXGREP = "grep";
$UNIXEGREP= "egrep";
$UNIXGREPBIS ="grep";
$UNIXgGREP= "grep";
$UNIXHEAD = "head";
$UNIXMV   = "mv";
$UNIXCP   = "cp";
$PERL 	  = "perl";
$FLY	  = "/bioinfo/appli/intel/pub/fly-1.6.5/fly";
$UNIXCAT   = "cat";
$UNIXCHMOD = "chmod";
$UNIXCHMOD_RO ="chmod 444";
$UNIXMKDIR  = "mkdir";
$UNIXTAIL  ="tail";
$UNIXWC ="wc";
$UNIXGZIP  ="gzip -f";
$UNIXDEZIP="gzip -cd";
$UNIXDEZIP_NO_STDOUT="gzip -df";
$crc64	   ="/usr/local/bin/crc64";


#####################################
# WEB SCRIPT
#
$WWW_SCRIPT_iANT                = "/$iANT/iANT.cgi$script_ok.pl";
$WWW_SCRIPT_MAKE_CONFIGURATION_FILE="/lib-pl/iANT.MakeConfigurationFile.cgi$script_ok.pl";

#$WWWROOTAA = "/~pat/cgi-bin/"; # commented by MJO 18 Jul. 2005
$WWWROOTAA = "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/";
$WWW_SCRIPT_AA_RUN_ALL_METHODS 	= "$WWWROOTAA/run_all_methods.cgi$script_ok.pl";
$WWW_SCRIPT_AA_CLASS_BEAUTY 	= "$WWWROOTAA/ClassBeauty.cgi$script_ok.pl";
$WWW_SCRIPT_AA_DNA_DISPLAYFAM   = "$WWWROOTAA/DisplayFam.cgi$script_ok.pl";

#$WWWROOTDNA = "/~pat/cgi-bin/"; # commented by MJO 18 Jul. 2005
$WWWROOTDNA = "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/";

$WWW_PROG_DRAW_MAP		= "$WWWROOTDNA/DrawMap.cgi$script_ok.pl";
$WWW_PROG_DESSINER_ORFS		= $WWW_PROG_DRAW_MAP;
$WWW_PROG_BACK_UP		= "$WWWROOTDNA/backup.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_INSTALL_NEW_SEQ = "$WWWROOTDNA/InstallNewSequence.cgi$script_ok.pl";
$WWW_SCRIPT_VIEW_WITH_NIP        = "$WWWROOTDNA/ViewNip.cgi$script_ok.pl";
$WWW_SCRIPT_RUN_SCRIPTS         = "$WWWROOTDNA/RunScripts.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_RUN_ALL_METHODS = "$WWWROOTDNA/RunAllMethods.cgi$script_ok.pl";
$WWW_SCRIPT_VALIDATION="$WWWROOTDNA/Validation.cgi$script_ok.pl";

$WWW_SCRIPT_MAKE_BAC_BROWSER    = "$WWWROOTDNA/makeBacBrowser.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_SCROLL 		= "$WWWROOTDNA/Scroll.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_ASSEMBLER       = "$WWWROOTDNA/Assembler.cgi$script_ok.pl";
$WWW_SCRIPT_IMPORT_ANNOTATION   = "$WWWROOTDNA/ImportAnnotation.cgi$script_ok.pl";

$WWW_SCRIPT_DNA_GCPC = "$WWWROOTDNA/GCpourcent.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_IMG_FRAMED = "$WWWROOTDNA/imgFrameD.cgi$script_ok.pl";
$WWW_SCRIPT_DNA_ITERE_FS_BLASTX_PRED = "$WWWROOTDNA/ItereDetectionFrameshift.cgi$script_ok.pl";


$WWW_SCRIPT_BUILD_MATRIX4FRAMED ="$WWWROOTDNA/ComputeMatrixForFrameD.cgi$script_ok.pl";
$URL_SCRIPT_BUILD_MATRIX4FRAMED ="http://sequence.toulouse.inra.fr/dna.script/ComputeMatrixForFrameD.cgi$script_ok.pl";


#$WWWROOTAA_DNA = "/~pat/cgi-bin/"; # commented by MJO 18 Jul. 2005
$WWWROOTAA_DNA = "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/";
$WWW_SCRIPT_AA_DNA_EDIT_ENTRY   = "$WWWROOTAA_DNA/edit_entry.cgi$script_ok.pl";
$WWW_SCRIPT_AA_EDIT_ENTRY 	= "$WWW_SCRIPT_AA_DNA_EDIT_ENTRY";  #Compatibilite
$WWW_SCRIPT_AA_DNA_MAKE_ENTRY 	= "$WWWROOTAA_DNA/make_entry.cgi$script_ok.pl";
$WWW_SCRIPT_AA_MAKE_ENTRY 	= "$WWW_SCRIPT_AA_DNA_MAKE_ENTRY";  #Compatibilite
$WWW_SCRIPT_AA_DNA_MAKE_DB 	  = "$WWWROOTAA_DNA/make_db.cgi$script_ok.pl";
$WWW_SCRIPT_AA_DNA_ANNOTATION_TRANSFER   = "$WWWROOTAA_DNA/annotation_transfer.cgi$script_ok.pl";
$WWW_SCRIPT_AA_DNA_2_DATABASES          ="$WWWROOTAA_DNA/iANT2DataBases.cgi$script_ok.pl";
$WWW_SCRIPT_AA_DNA_FILTER   = "$WWWROOTAA_DNA/Filter.cgi$script_ok.pl";

#
#####################################
# CMD SCRIPT
#

$ROOTDNA = "$ROOT";
$ROOTAA  = "$ROOT";
$ROOTAA_DNA = "$ROOT";

$CMD_SCRIPT_MAKE_BAC_BROWSER    = "$ROOTDNA/makeBacBrowser$script_ok.pl";
$CMD_SCRIPT_DNA_GC_PC 		= "$ROOTDNA/GCpourcent$script_ok.pl";
$CMD_SCRIPT_AA_RUN_ALL_METHODS 	= "$ROOTAA/run_all_methods$script_ok.pl";
$CMD_SCRIPT_AA_MAKE_PRODOM	= "$ROOTAA/make_prodom$script_ok.pl";
$CMD_SCRIPT_AA_TOPPRED 		= "$ROOTAA/TopPred$script_ok.pl";
$CMD_SCRIPT_AA_TMPRED 		= "$ROOTAA/TMpred$script_ok.pl";
$CMD_SCRIPT_AA_SEG 		= "$ROOTAA/seg$script_ok.pl";
$CMD_SCRIPT_AA_PROFILE_SCAN 	= "$ROOTAA/ProfileScan$script_ok.pl";
$CMD_SCRIPT_AA_PROSITE_SCAN 	= "$ROOTAA/prosite_scan$script_ok.pl";
$CMD_SCRIPT_AA_FILTRE_BLASTP	= "$ROOTAA/blastp_filtre$script_ok.pl";
$CMD_SCRIPT_AA_PSORT            = "$ROOTAA/pSort$script_ok.pl";
$CMD_SCRIPT_AA_INTERPROSCAN     = "$ROOTAA/InterProScan$script_ok.pl";
$CMD_SCRIPT_AA_STAT             = "$ROOTAA/stat$script_ok.pl";
$CMD_SCRIPT_AA_DNA_BLAST2FAS    = "$ROOTAA_DNA/blast2fas$script_ok.pl";
$CMD_SCRIPT_AA_DNA_SRS2FAS      = "$ROOTAA_DNA/srs2fas$script_ok.pl";
$CMD_SCRIPT_SORTDB		= "$ROOTAA_DNA/sortdb$script_ok.pl";
$CMD_SCRIPT_NCBI_REFORMAT	= "$ROOTAA_DNA/ncbi_blast_reformat$script_ok.pl";	

$CMD_SCRIPT_AA_DNA_DISPLAYFAM   = "$ROOTAA/DisplayFam$script_ok.pl";	# en principe Display Fam doit pouvoir marcher pour l'ADN
$CMD_SCRIPT_AA_DNA_2_DATABASES  = "$ROOTAA_DNA/iANT2DataBases$script_ok.pl";
$CMD_SCRIPT_EXTRACT		= "$ROOTDNA/Extract$script_ok.pl";
$CMD_SCRIPT_DRAW_ENTRY		="$ROOTAA_DNA/www_image$script_ok.pl -xdom_var /usr/local/Xdom ";
$PROG_PATCH_FrameD		="$ROOTDNA/patch_FrameD.pl"; # JER ne dois plus etre utilise	cf CMD_SCRIPT_DNA_IMG_FRAMED
$PROG_PATCH_GLIMMEUR		="$ROOTDNA/conv_thomas_prog$script_ok.pl";	# JER ne dois plus etre utilise  cf CMD_SCRIPT_DNA_IMG_FRAMED
$PROG_CUT_SEQ			="$ROOTDNA/cutLineFile$script_ok.pl";
$PROG_REFORMAT_NCBI_BLAST	="$ROOTDNA/ncbi_blast_reformat$script_ok.pl";
$PROG_REFORMATDB		="$ROOTAA_DNA/reformatdb";
$PROG_SORTDB			="$ROOTAA_DNA/sortdb";
$PROG_INTERACTIF		="$ROOTAA_DNA/interactif";
$CMD_SCAN_PATTERN		="$ROOTDNA/ScanPatterns$script_ok.pl";
$PROG_DRAW_MAP	                ="$ROOTDNA/DrawMap$script_ok.pl";
$CMD_SCRIPT_DNA_RUN_ALL_METHODS ="$ROOTDNA/RunAllMethods$script_ok.pl";
$CMD_SCRIPT_DNA_IMG_FRAMED      ="$ROOTDNA/imgFrameD$script_ok.pl";
$CMD_SCRIPT_GCPC      ="$ROOTDNA/GCpourcent$script_ok.pl";
$CMD_SCRIPT_DNA_ITERE_FS_BLASTX_PRED = "$ROOTDNA/ItereDetectionFrameshift$script_ok.pl";
$CMD_SCRIPT_DNA_WU_BLAST        ="$ROOTDNA/WU-blast$script_ok.pl";

$CMD_SCRIPT_AA_DNA_FILTER ="$ROOTAA_DNA/Filter$script_ok.pl";
$CMD_SCRIPT_AA_DNA_MAKE_DB      ="$ROOTAA_DNA/make_db$script_ok.pl";
$CMD_SCRIPT_AA_DNA_MAKE_ENTRY	= "$ROOTAA_DNA/make_entry$script_ok.pl";
$CMD_SCRIPT_AA_DNA_ANNOTATION_TRANSFER   = "$ROOTAA_DNA/annotation_transfer$script_ok.pl";

$CMD_SCRIPT_AA_DNA_REFORMATDB ="$ROOTAA_DNA/ReformatDB$script_ok.pl";

$CMD_SCRIPT_AA_MAKE_ENTRY	= "$CMD_SCRIPT_AA_DNA_MAKE_ENTRY";	# Compatibilite 30/12/99
$CMD_SCRIPT_AA_MAKE_ENTRY_CLUSTER = "$ROOTAA_DNA/make_entryCluster$script_ok.pl";

$PROG_BACK_UP                   ="$ROOTDNA/backup.cgi$script_ok.pl";



#####################################
# PROGRAMS
#

$PROG_SEARCH_tRNA               ="/usr/local/bin/tRNAscan-SE -P -o";
$PROG_tRNAscan_SE               ="/usr/local/bin/tRNAscan-SE -P ";
#
# --- FrameD++ ---
#
#$FRAMED_DIR = "/usr/local/FrameD++.v2/FrameD";	# CHANGE le 18/11/99
$FRAMED_DIR = "/usr/local/FrameD++/FrameD";	# CHANGE le 01/12/99
$FRAMED_PROG = "FrameD++ -a $FRAMED_DIR/FrameD.par ";
$FRAMED_PROG_TEST = "FrameD++.test -a $FRAMED_DIR/FrameD.par.test ";
$DIR_FOR_FrameD ="$ROOTTMP/FrameD++/";	
$PROG_FrameD ="/usr/local/FrameD++/FrameD++";	# JER 20/12/1999 en principe plus utilise
$FRAMED_IMG     ="FrameD_IMG.$HTML_EXTENSION";
$MODELE_FS_REPORT="$ROOT_iANT/$DIR_CONFIG/MODEL_FS_REPORT.$HTML_EXTENSION";
$FRAMED_FSD     ="FrameD_FSD.$HTML_EXTENSION";

#
# --- extract --- 
#
$DNA_EXTRACT ="/usr/local/bin/extract";
$FILE_DNA_EXTRACT="file_to_extract";
$DIR_DNA_EXTRACT="$ROOTTMP/extract";


#
# --- BLASTPGP ---
#
$FORMATDB  = "/usr/local/bin/formatdb";
$BLASTPGP  = "nice /usr/local/bin/blastpgp";
$BLASTPGP_PARAM = "-e 0.1 -G 9 -E 2 -a 4";
#$BLASTPGP_PARAM = "-e 0.1 -a 4";

$BLASTALL_BLASTX  = "nice /usr/local/bin/blastall  -p blastx -a 4 "; 

$PRESSDB          = "/usr/local/wu-blast/pressdb";


#
# --- WU-blast ---
#
$WU_BLASTN        = "/usr/local/bioinfo/wu-blast//blastn";
$WU_tBLASTN       = "/usr/local/bioinfo/wu-blast//tblastn";
$WU_tBLASTX       = "/usr/local/bioinfo/wu-blast//tblastx";



#
# --- SEG ---
#
$SEG   = "/usr/local/bin/seg";

#
# --- TMPRED ---
#
$TMPRED_DIR   = "/usr/local/TMpred";
$TMPRED_prog  = "$TMPRED_DIR/btmpred.bin";
$TMPREDmatrix = "$TMPRED_DIR/matrix.tab";

$TMPRED_DEFAULT_MINL = 19;
$TMPRED_DEFAULT_MAXL = 25;

#
# --- TopPred ---
#
$TOPPRED_PROG = "/usr/local/toppredm/release-1.0/bin/toppred";
$TOPPRED_PARAM = " -gif -q -p  ";
$TOPPRED_EXT = "TopPred";

#
# --- Generic Separator ----
#
$GEN_SEP = "___";

#
# --- Convert ----
#
$CONVERT_PROG = "/usr/local/bin/convert";

#
# --- WWWSEQ ---
#
$PRODOM_RELEASE 	= "2000.1";
$PRODOM_URL 		= "http:\/\/www.toulouse.inra.fr";
$WWWSEQ_DIR 		= "$ROOT/prodom/script/blast/";
$WWWSEQ_PROG		= "./wwwseq.p$PRODOM_RELEASE";
$PRODOM_FICHIER_INDEX   = "/www/prodom/image/p$PRODOM_RELEASE.fichier_index";

#
# madomain
#
$MADOMAIN_PROG		= "/multalin/cgi-bin/madomain.pl";

#
#multalin
#
$MULTALIN_DIR		= "/usr/local/multalin";
$MULTALIN		= "$MULTALIN_DIR/ma";
$MULTALIN_BLOSUM	= "$MULTALIN_DIR/blosum62.tab";
$MULTALIN_GIF150	= "$MULTALIN_DIR/gif150.cfg";

#
# DisplayFam
#
$WWW_DisplayFam = "/prodom/cgi-bin/DisplayFam.pl";

#
# --- PROSITE ---
#
$PROSITE_PSCAN 		= "$ROOT/db/prosite.pscan";

#
# --- SRS ---
#
$SRS_GETZ = "/usr/local/srs/bin/solaris/getz";
$SRS_INIT = "/usr/local/srs/etc/prep_srs";
#$SRS_UPDATE = "/usr/local/srs/etc/srsupdate";
# JER PB avec SRSUPDATE je patche de facon temporaire pour que le control-panel puisse marcher
#$SRSCHECK =  "/usr/local/srs/bin/solaris/srscheck 2> /tmp/errorsrscheck";
#$SRSBUILD =  "/usr/local/srs/bin/solaris/srsbuild";
$SRS_UPDATE = "/usr/local/srs/etc/jSRSUPDATE";
$HTML_SRS_CLASS = "<A HREF=http://$URL_SERVER/srs5bin/cgi-bin/wgetz?-f+GeneName+-f+Description+-f+GNConfidence+-f+DEConfidence+[ORGAMASQUE-CL:'MASK']>MASK</A>";


#
# OPTIONS
#
$PRINT_PRODOM_RELEASE_IMAGES = 1;
$PRINT_MULTALIN_LINK         = 1;
$SRS_DATABASE		     = 1;
$BLAST_SERVER		     = 1;

#
#--- NIP --- 
#
$CODE_UNE_LETTRE   = "1";
$CODE_TROIS_LETTRE = "3";


#
#--- readseq ---
#
$READSEQ           = "/usr/local/bin/readseq";
$IG_extension      = "ig";
$PSORT_prog        = "/usr/local/bin/psort";
$psort_EXTENSION   = "psort";
$PSORT_EXTENSION   = "PSORT";

#
#
#


$PFT_PFSCAN = "/usr/local/bin/pfscan";
$PFT_PROSITE_PROFILE_DB = "$ROOTDB/prosite_prerelease.prf";

$IPRSCAN = "/usr/local/iprscan/InterProScan.pl";
$GMAKE = "/usr/local/bin/gmake";

$NUM_PROCESSORS = 4;


@INDICE_CONFIDENCE=("hypothetical","putative","probable");


1;

