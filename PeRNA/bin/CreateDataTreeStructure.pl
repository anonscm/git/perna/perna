#!/usr/local/bin/perl



=pod 
=head1 NAME

CreateDataTreeStructure.pl - This program creates the tree structure for a strain of an organism in the directory $PeRNA/external_data/<Kingdom>

=head1  SYNOPSIS 

./CreateDataTreeStructure.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom "Bacteria" --organism "Escherichia coli" --strain K12 --replicon "chromosom" 

=head1 DESCRIPTION

CreateDataTreeStructure.pl creates the tree structure of a strain of the organism of the kingdom in the directory $PeRNA/external_data

external_data
	 __ <kingdow>
	 	__<Organism>[_<strain>]
			__ <replicon>
		
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use ToolBox;

our $EXTERNAL_DATA_DIR;

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s", "kingdom=s", "organism=s", "strain=s", "replicon=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain', 'replicon'));
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');

	$EXTERNAL_DATA_DIR	= $rh_param->Get('EXTERNAL_DATA_DIR');
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	
	# create the tree structure
	&CreateTreeStructure($kingdom, $organism, $strain, $replicon);
	
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help       	: this message
		--cfg		: the configuration file
		--kingdom	: kingdom of the organism
		--organism	: name of the organism
		--strain	: strain of the organism
		--replicon	: replicon of the organism
END
}


=head2 function CreateTreeStructure

 Title        : CreateTreeStructure
 Usage        : CreateTreeStructure($kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : Create the tree structure for the replicon of the specific organism and the specific strain in the directory $EXTERNAL_DATA
 Returns      : None
 Args         : $kingdom : kingdom of the organism
 		$organism : name of the organism
		$strain : strain of the organism
		$replicon : replicon of the organism
 Globals      : none
 
=cut 


sub CreateTreeStructure{
	my ($kingdom, $organism, $strain, $replicon) = @_;
	my $way;	# the way where to create the directory for the organism 
	my $dir_name;

	# go in the directory of the kingdom
	$way = &GetKingdomDir($EXTERNAL_DATA_DIR, $kingdom);
	chdir($way) or die "The kingdom $kingdom doesn't exist.\n";
	
	$dir_name = &GetDirName($organism, $strain);
	# create the directory for the organism
	if (&ExistDirectory($dir_name, $way) == 0) {
		&CreateDirectory($dir_name, $way);
	}
	
	$way = $way.$dir_name."/";
	$dir_name = &FormateString($replicon);
	# creation of the directory of the replicon
	if (&ExistDirectory($dir_name, $way) == 0) {
		&CreateDirectory($dir_name, $way);
	}
	
}

