require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/cgi-lib.pl";

##[
#>Project:      iANT integrated ANnotation Tool (1999-2000)
#>Release:      1.0
#>Lab:          INRA-CNRS LBMRPM, BP 27 Chemin de Borde Rouge, 31326 Castanet Cedex France
#>Authors:      Patricia.Thebault@toulouse.inra.fr and Jerome.Gouzy@toulouse.inra.fr
#>This script:  Patricia.Thebault@toulouse.inra.fr
#>Last updated: 
#>Citation:
#
#Aim: iANT Library
#
#Updated:       
#
#Who:   
#
##]

$INFINI=99999999999;
$YES=1;
$NO=0;
$OK=1;
$ERROR=0;
@STEM=("None","Probable","Medium","Strong");

########################################################################
#[pReadSeqFasta
#Aim: Read fasta file containing ONE sequence, remove newlines and join them into a string
#Global:
#In: $file
#Out: name and sequence
#Update:
#Prerequisite:     
#]
sub pReadSeqFasta
{
    local ($file) = @_;	
    if (&pFileExiste($file))
    {
	open(FASTA, $file) ||pPrintErrorScript ("Error while  opening $file\n");
	my ($def, @seq) = <FASTA>; # get the whole file at once
	chomp @seq;                # remove newlines from all sequence lines
	my $seq = join("", @seq);  # join lines into a string
	close (FASTA);
	$def=~s/\>//;
	my(@F)=split(' ',$def);
	return ($F[0], $seq);
    }
    else 
    {
	print "file $fich doesn't exist/n";
    }
}

########################################################################
#[pReadSequencesFasta
#Aim: Read fasta file containing several sequences, remove newlines and join them into a string
#Global:
#In: $file
#Out: tableau de hashage contenant les sequences (index=name)
#Update:
#Prerequisite:     
#]
sub pReadSequencesFasta
{
    local ($file) = @_;	
    my(%SEQUENCES)=();
    if (&pFileExiste($file))
    {
	open(FILE,"$file") || die "cannot open $file\n";
	my($line)="";
	my($i)=-1;
	while ( $line= <FILE> )
	{
	    chop($line);	 
	    if ($line=~ /^>/ )
	    {	
	        $name=$line;
		$name=~s/>//;
		$SEQUENCES{"$name"}="";
	    }
	    else
	    {
	        $SEQUENCES{"$name"}.="$line";
	    }
     }
	return(%SEQUENCES);
    }
    else 
    {
	print "file $fich doesn't exist/n";
    }

}

########################################################################
#[pParseMATCH
#Aim:            Parse blastx results
#Global: Current line
#In: Current line
#Out: %idxMATCH 
#Update:
#Prerequisite:     
#]
sub pParseMATCH
{
    my(@F) = ();
    my(@G) = ();
    ($idxMATCH{"lq"}) = / LQ=(\d+) /; 
    ($idxMATCH{"lq"}) = / LQ=(\d+) /;
    ($idxMATCH{"ls"}) = / LS=(\d+) /;
    ($idxMATCH{"SCORE"}) = / S=(\d+\.*\d*) /;
  
        ############ Pour snoscan   
	($idxMATCH{"C_SCORE"}) =/ Sc=(\d+\.*\d*) /;
    ($idxMATCH{"D_SCORE"}) = / Sd=(\d+\.*\d*) /;
    ($idxMATCH{"Dp_SCORE"}) = / Sd\'=(-?\d+\.*\d*) /;
   # print STDOUT "$idxMATCH{C_SCORE}#$idxMATCH{D_SCORE}##$idxMATCH{Dp_SCORE}##\n";
    ($idxMATCH{"STEM"}) = / stem=(\d+) /;
    ($idxMATCH{"MISMATCH"}) = / mismatch=(\d+) /;
    ($idxMATCH{"MATCH"}) = / match=(\d+) /;
   
        ############ 
	
    ($idxMATCH{"PROBA"}) = / E=([\w\-\.]+) /;
    if (($idxMATCH{"PROBA"}) =~ /^e/)
    {
	($idxMATCH{"PROBA"}) = "1". ($idxMATCH{"PROBA"}) ;
    }
    $idxMATCH{"PROBA"}=$idxMATCH{"PROBA"};
    $idxMATCH{"PROBA_INF"}=$idxMATCH{"PROBA"};
    ($idxMATCH{"IDENTITY"}) = / I=(\d+)% /;
    ($idxMATCH{"POS" })= / P=(\d+)% /;
     ($idxMATCH{"GAP"})= / G=(\d+)% /;
    ($idxMATCH{"PCS" })= / %S=(\d+) /;
    ($idxMATCH{"PCQ"}) = / %Q=(\d+) /;
    ($idxMATCH{"FRAME"}) = / F=([+-]\d+) /;
    @F = split("//",$_,9999);
    @G = split;
    ($idxMATCH{"BEGIN" })= $G[1];

    ($idxMATCH{"END" })= $G[2];
    ($idxMATCH{"LENGTH_HSP"})= abs ($G[2]-$G[1])+1;	
    ($idxMATCH{"id_query"})= $G[0];
    ($idxMATCH{"id_query"})=~ s/<//;
    ($idxMATCH{"s_begin"})= $G[5];
    ($idxMATCH{"s_end"})= $G[6];
    ($idxMATCH{"q_begin"})= $G[1];
    ($idxMATCH{"q_end"})= $G[2];
    ($idxMATCH{"s_name"})= $G[4];
    $idxMATCH{"comment"}= $F[$#F];
    $idxMATCH{"lien"}= $F[1];
    return %idxMATCH;
}

########################################################################
#[pREfDB
#Aim:            Write the hypertext link of an AC number with its database
#Global: 
#In:
#Out:
#Update:*lien
#Prerequisite:     
#]
sub pREfDB
{
    local(*lien)=@_;
    local($color);
     if ( $lien =~ /;sp_/ && $lien =~ /gi_/  )
    {
	 ($id_gi,$id_sp,$nom_sp) = / gi_(\d+);sp_(......)_(\w+)/;
	 if ( $nom_sp =~ /_RHI/ )
	 {
	     $color = "<Font Color=red>";
	 }
    }
    elsif ( $lien =~ /;sp_/ && $lien =~ /pd_/ )
    {
	 ($id_pd,$id_sp,$nom_sp) = / pd_(\w+);sp_(......)_(\w+)/;
    }
    elsif ( $lien =~ /sp_/ )
    {
	 ($id_sp,$nom_sp) = / sp_(......)_(\w+)/;
    }
    elsif ($lien=~/em_/)
    {
	($id_em) = /em_(\w+\d+)/; # PAT 29-01-99 ajout
    }
    elsif ($lien=~/s[nm]:/)  # PAT 24-05-00
    {
		($sm_bac,$sm_entry) = /sm:(\w+):(\w+);/;# PAT 24-05-00
		my(@J)=();
		@J=split('_',$sm_bac);
		$sm_lab = "\L$J[1]";
    }
    else 
    {
	($id_gi) = / gi_(\d+);/;
    }

    $pd_tmp = $pd_mask;
    $sp_tmp = $sp_mask;
    $gi_tmp = $gi_mask;
    $em_tmp = $em_mask;
    $sm_tmp = $sm_mask;
    $pd_tmp =~ s/MASK/$id_pd/g;
    $sp_tmp =~ s/MASK/$id_sp/g;
    $gi_tmp =~ s/MASK/$id_gi/g;
    $em_tmp =~ s/MASK/$id_em/g;
    $sm_tmp =~ s/BACMASK/$sm_bac/g;
    $sm_tmp =~ s/LABMASK/$sm_lab/g;
    $sm_tmp =~ s/ENTRYMASK/$sm_entry/g;
    $lien =~ s/pd_$id_pd;/ pd$pd_tmp /;
    $lien =~ s/gi_$id_gi;/ gi$gi_tmp /;
    $lien =~ s/sp_${id_sp}_/ sp$sp_tmp /;
    $lien=~ s/em_$id_em/em$em_tmp/;
    $lien=~ s/s[nm]:$sm_bac:$sm_entry/sm $sm_tmp/;  # PAT SN-->SM
    # $lien =~ s/$H[1] $H[2]//g;
    # return ($color,$lien);
}

########################################################################
#[pColorRedHeaderLines
#Aim:            Color in red lines that contain key words 
#Global:
#In:$keywords
#Out:
#Update:*comment
#Prerequisite: 
#]
sub pColorRedHeaderLines
{
    local ($keywords,*comment)=@_;
    $keywords=~ s/,/\|/g;
     if ( "\U$comment" =~ /$keywords/)
    {
	$comment = "<Font Color=red>$comment</Font>";
    }
}

########################################################################
#[pPrintMATCH
#Aim:         Print Header Match line of IANT results common format
#Global:
#In: $clone_ouput,$prog_meth,$line,$file,$config
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintMATCH
{
    local ($clone_ouput,$prog_meth,$line,$file,$config) = @_;
    local($comment,$program,$id_query,$sp_tmp,$gi_tmp,$lien,$color,$id_gi,$id_sp,$nom_sp,$val);
    my(%idxParamMatch)=&pParseMATCH();
    my(@FF) = split("###",$line);
    my($comment)="";
    my($db) = $FF[$#FF] if ($#FF > 0);
    if ( $db eq "" )
    {
	$db = "nr";
    }
    else
    {
	$db =~ s/ //g;
	$tmp = "### $db";
	$line =~ s/$tmp//g;
    }
    
    $comment=$idxParamMatch{"comment"};
    if ( ($comment=~ /\]/) &&$comment=~ /\[/)
    {
	$comment =~ s/\]/<TH>/;
    }
    else
    {
	$comment = <TH>.$comment;
    }
    &pColorRed(*comment);
    $comment =~ s/\[//;
    $comment =~ s/ +/ /g;#remplace +sieurs espaces par un seul
    my($id_query) = $idxParamMatch{"$id_query"};
    $id_query =~ s/<//; 
  
    my(@H)=split(' ',$lien);
    my($color)="";
    my(@F)=split(' ',$idxParamMatch{"lien"});
    ##### PAT 10-05-2000
    local($lien)=$F[0];
    &pREfDB(*lien); 
    my($couleur)="";
    if ( $color ne "" )
    {
	$couleur = "$color$lien</Font>";
    }
    else
    {
	$couleur = $lien;
    }
    my($filtre_nom_subject)=$idxParamMatch{"s_name"};
    &pREfDB(*filtre_nom_subject);
    $filtre_nom_subject=~ s/;//g;
    my(@F) = split(' ',$comment);
    my($filtre_com )= $F[0];
    $filtre_com =~ s/^ //;
    if ($prog_meth =~ /SNOSCAN/i)
    {
    	$comment=~ s/CUGA/<FONT color=green>CUGA<\/FONT>/g;
	my($id_subject)=$idxParamMatch{s_name};
	if (($id_subject=~ /^g/)||($id_subject=~ /^gi_/i))
        {
	$id_subject=~ s/^gi_//i;
	$id_subject=~ s/^g//i;
	@T=split("_",$id_subject);
	$id_subject=$T[0];
	$id_subject_tmp=$gi_mask_DNA;
	$id_subject_tmp=~s/MASK/$id_subject/g;
        } 
	else
	{
		$id_subject_tmp=$idxParamMatch{s_name};
	}
	my($link_prog)="<A HREF=$WWW_SCRIPT_AA_DNA_FILTER?&FILE=$file&BEGIN=$idxParamMatch{\"BEGIN\"}&END=$idxParamMatch{END}>$prog_meth</A>";
	my($methyl_base)=$idxParamMatch{s_begin};
	$methyl_base =~ s/(\D+)(\d+)/<FONT color=red> $1<\/FONT><FONT color=blue>$2<\/FONT>/;
	my($methyl_pos)=($idxParamMatch{s_begin}=~ /(\d+)/);
        $val = sprintf " $link_prog <B> $idxParamMatch{id_query} %10d %10d </B> // S=[$idxParamMatch{SCORE}]  MATCH=<B>$idxParamMatch{\"MATCH\"}</B> MISMATCH=<B>$idxParamMatch{\"MISMATCH\"}</B>  // $comment // $methyl_base   $id_subject_tmp\n",$idxParamMatch{"BEGIN"},$idxParamMatch{"END"};    
    }
    else
    {
    	$val = sprintf "<A HREF=$WWW_SCRIPT_AA_DNA_FILTER?&FILE=$file&BEGIN=$idxParamMatch{\"BEGIN\"}&END=%d&SCORE=$idxParamMatch{\"SCORE\"}&IDENTITY=$idxParamMatch{\"IDENTITY\"}&COM=$idxParamMatch{\"s_name\"}&GAP=%d&POS=$idxParamMatch{\"POS\"}&ORGA_CONFIG=$config>$prog_meth</A> $idxParamMatch{\"FRAME\"} %6d-%6d\tS=%5d E=%7s  I=%3d  P=%3d Gap=%2d pcS=%3d %s $comment \n",abs($idxParamMatch{"END"}),abs($idxParamMatch{"GAP"}),$idxParamMatch{"BEGIN"},abs($idxParamMatch{"END"}),$idxParamMatch{"SCORE"},abs($idxParamMatch{"PROBA"}), $idxParamMatch{"IDENTITY"},$idxParamMatch{"POS"},abs($idxParamMatch{"GAP"}),$idxParamMatch{"PCS"},$couleur;
    }
    $val =~ s/<TH>//g;
    print $val;
 }




########################################################################
#[pTab2Indx
#Aim:          Convert an array (KEY=param) to an hashtable (key=KEY and VALUE=param) (to initialize parameters)
#Global:
#In: @tab
#Out: %indx
#Update:
#Prerequisite:     
#]
sub pTab2Indx
{
    
    local(@tab)=@_;
    local(%indx);
    foreach $i (0 .. $#tab) 
    {
	($iden, $val) = split(/=/,$tab[$i],2); # splits on the first =.
	$iden =~ tr/a-z/A-Z/;
	$indx{$iden}=$val;
    }	
    return %indx;
}



########################################################################
#[pPrintLegende
#Aim:     Debug function  : print out the hastable parameters     
#Global:
#In: *to_print,$file
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintLegende
{
    local(*to_print,$file)=@_;
    $text="";
    foreach $key (keys %to_print)
    {
	    if ( &pStrContainsStr($0,"\.cgi\."))
	    {
		print STDOUT " <U>$key </U> ---><B>$to_print{$key}</B>\n<BR>";
	    }
	    else
	    {
		print STDOUT " $key   --->$to_print{$key}\n";
	    }
    }
}




########################################################################
#[SortByBeginPosition
#Aim:         Sort the matches by ascending order of the score 
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub SortByBeginPosition
{
	@F = split(' ',$a,9999);
	@G = split(' ',$b,9999);
	$F[1]<=>$G[1];
}

########################################################################
#[pSortHSPByName
#Aim:       Sort the matches by alphabetical order of the name
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pSortHSPByName
{
	@F = split(' ',$a,9999);
	@G = split(' ',$b,9999);
	$F[4] cmp $G[4];
}

########################################################################
#[pSortFirstString
#Aim:       To sort an array    on alphabetical order
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pSortFirstString
{
	@F = split(' ',$a,9999);
	@G = split(' ',$b,9999);
	$F[0] cmp $G[0];
}

########################################################################
#[pGetCandidateFrameShift
#Aim:    To get hsp which at least 2 same subject      
#Global:
#In:  @listeMatch 
#Out: @newList    
#Update:
#Prerequisite:     
#]
sub pGetCandidateFrameShift
{
    local($file,*listeMatch)=@_; 
    my($count_all) = 0;
    my(@F)=();
    my(@G)=();
    my($j)=0;
    my($i)=0;
    my($name)="";
    my($name_next)="";
    my($keep_next)=$NO;
    my(@newList)=();
    #my(@listeMatch_sorted)= sort pSortHSPByName @listeMatch;
        ######## PAT 27-07-00 : Optimisation
    my($date) = time;
    my($file_tmp)=&pGetDir($file)."/TMP_$date";
    open(TMP,">$file_tmp") || print STDOUT "###Error while opening $file_tmp\n";
    for ($i=0;$i<=$#listeMatch;$i++)
    {
	print TMP "$listeMatch[$i]\n" ;
    }
    close(TMP);
    @listeMatch=`$UNIXSORT +4 -5 $file_tmp`; #### Tri sur le nom du subject
    system("$UNIXRM $file_tmp");
    for ($j=0;$j<$#listeMatch;$j++) 
    {
	chomp($listeMatch[$j]);
	chomp($listeMatch[$j+1]);	# PAT 30-08-2000 MODIF
    	@F=split(' ', $listeMatch[$j]);
	$name= $F[4];
	    
	@G=split(' ', $listeMatch[$j+1]);
	$next_name=$G[4];
	if ($name eq $next_name)
	{
		$newList[$count_all]=$listeMatch[$j];
	    	$count_all++;
		$keep_next=$YES;
	}
	else
	{
		if ($keep_next)
		{
			$newList[$count_all]=$listeMatch[$j] ;
	    		$count_all++;
		}
		$keep_next=$NO;
	}
	
    }
    return @newList;
}

########################################################################
#[
#Aim:          
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pExtraction
{
    local($seq,$name,$begin,$end)=@_;
    my($sub_seq)="";
    my($sub_name)="";
    $seq =~ tr/a-z/A-Z/;
    $seq =~ s/ //g;
    $seq =~ s/\n//g;
    my($long_total)=length($seq);	
    if ($begin ==0) { 	$begin=1;     }
    if ($end ==0)   { 	$end=1;     }
    $begin=abs($begin);
    $end=abs($end);
    $end=$long_total if ($end > $long_total);
    if ($end<$begin)
    {
	$seq=~ tr/ACGT/TGCA/;
	$seq=reverse($seq);
	$sub_seq=substr($seq,$long_total-$begin,($long_total-$end)-($long_total-$begin)+1);	
    }
    else
    {
	$sub_seq=substr($seq,$begin-1,($end-$begin+1));
    }
    my($nombAA) = int (length($sub_seq)/3);
    my(@N) = split(/\|/,$name,3);
    if ($end<$begin)
    {
	$new_end=$begin-($nombAA*3)-1;
    }
    else
    {
	$new_end=$begin+($nombAA*3)-1;#Attention 1 ---> 3 (1+3)=4
    }
    $N[0]=~s/ //g;
    $sub_name=$N[0]."#".$begin."#".$new_end;
    return ($sub_seq,$sub_name);
}


########################################################################
#[pExit
#Aim:      Exit and print out an HTLM msg    
#Global:
#In: $script,$msg
#Out: 
#Update:
#Prerequisite:     
#]
sub pExit
{
    local ($script,$msg)=@_;   
    if (&pStrContainsStr($script,"cgi"))
    {
	print " <B><FONT SIZE=+3>" ;
    }
    print " $msg\n";
    if ( &pStrContainsStr($script,"cgi"))
    {
	print "</BODY></HTML>\n<BR>";
	&CloseHTML();
    }
    exit(0);
}

########################################################################
#[
#Aim:          
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pCalculFrame
{
local($q_begin,$q_end,$q_length)=@_;

	if ( $q_begin <= $q_end )
	{
		#DK 16/10/98 $frame = 1+$q_begin-int($q_begin/3)); 
		$frame = "+1" if ( ($q_begin - 1) % 3 == 0);
		$frame = "+2" if ( ($q_begin - 2) % 3 == 0);
		$frame = "+3" if ( ($q_begin - 3) % 3 == 0);
	}
	else
	{
		$frame = "-1" if ( (1+$q_length-$q_begin - 1) % 3 == 0);
		$frame = "-2" if ( (1+$q_length-$q_begin - 2) % 3 == 0);
		$frame = "-3" if ( (1+$q_length-$q_begin - 3) % 3 == 0);
	}

	return $frame;
}

################################################################
#FONCTION: pReturnDebutFrame1
#AIM: retourne le debut le plus proche correspondant a la phase 1 par rapport au debut propose
#INPUT: $q_begin $q_end
#INPUT: $q_length longueur de la sequence totale, utilisee pour le mode reverse
#OUTPUT: nouveau debut en phase 1 et end
################################################################

########################################################################
#[pReturnDebutFrame1
#Aim:     Return position correponding to the Frame 1     
#Global:
#In: $q_begin,$q_end,$q_length
#Out: $nouveau_debut
#Update:
#Prerequisite:     
#]
sub pReturnDebutFrame1
{
    local($q_begin,$q_end,$q_length)=@_;
  
    my($nouveau_debut);
    if ( $q_begin <= $q_end )
    {
	$q_begin=1 if ( $q_begin ==0); ## PAT 05-05-99 
	#DK 16/10/98 $frame = 1+$q_begin-int($q_begin/3)); 
	$nouveau_debut = $q_begin if ( ($q_begin - 1) % 3 == 0);
	$nouveau_debut = $q_begin-1 if ( ($q_begin - 2) % 3 == 0);
	$nouveau_debut = $q_begin-2 if ( ($q_begin - 3) % 3 == 0);
    }
    else
    {
	$nouveau_debut = $q_begin if ( (1+$q_length-$q_begin - 1) % 3 == 0);
	$nouveau_debut = $q_begin+1 if ( (1+$q_length-$q_begin - 2) % 3 == 0);
	$nouveau_debut = $q_begin+2 if ( (1+$q_length-$q_begin - 3) % 3 == 0);
    }
    return ($nouveau_debut);
}

########################################################################
#[pTranslation
#Aim:    Translate a sequence    and reverse it if begin>end 
#Global:
#In: $sequence,$debut,$fin,$code_lettre
#Out: 
#Update:
#Prerequisite:     
#]
sub pTranslation
{
    local($sequence,$debut,$fin,$code_lettre)=@_;
    my ($sequence_translated)="";
    if ($debut>$fin)
    {
	$sequence=reverse($sequence);
    }
    $sequence_translated=&pTraduction($sequence,$code_lettre);
    return $sequence_translated;
}

########################################################################
#[pTraduction
#Aim:   Translate a DNA sequence       to an AA sequence
#Global:
#In: $sequence,$code_lettre,$file_CODE
#Out: 
#Update:
#Prerequisite:     
#]
sub pTraduction
{
    local($sequence,$code_lettre,$file_CODE  )=@_;
    
    my ($long_seq)=int (length($sequence));
    my ($nAAs)=$long_seq/3;
    my ($seq_traduite)="";
    my($i);
    if (!&pFileExiste($file_CODE))
    {
	$file_CODE="/www/Common/CODEGENETIQUE.rhime";
	print STDOUT "<BR>Default value for the CODEGENETIQUE : /www/Common/CODEGENETIQUE.rhime <BR>" if ($0 !~ /ok/);
    }
    (*Idx1,*Idx3)=&pReadCode($file_CODE);
    foreach $i (1..$nAAs)
    {
	$triplet =substr($sequence,($i-1)*3,3);
	$triplet =~ tr/a-z/A-Z/;
	if (&pCheckCodeTriplet($triplet) == $YES)
	{
	    if ($code_lettre==$CODE_UNE_LETTRE)
	    {	
		$AA=$Idx1{$triplet};
	    }
	    else
	    {	
		$AA=$Idx3{$triplet};
	    }	
	}
	else{
	    $AA="?";
	}
#	$AA="M" if ($i==1) ; #PAT 23-04-99 prot commence toujours par une methionine
	$seq_traduite=$seq_traduite.$AA;
    }
    return $seq_traduite;
}

########################################################################
#[pTranslation_to_array
#Aim:          Translate a  string to an array 
#Global:
#In: $sequence,$debut,$fin,$code_lettre,$file_CODE
#Out: 
#Update:
#Prerequisite:     
#]
sub pTranslation_to_array
{
    my($sequence,$debut,$fin,$code_lettre,$file_CODE)=@_;
    my($long_seq)=int (length($sequence));
    my($nAAs)=$long_seq/3;
    my(@seq_traduite)=();
    (*Idx1,*Idx3)=pReadCode($file_CODE);
    foreach $i (1..$nAAs)
    {
	$triplet =substr($sequence,($i-1)*3,3);
	if (&pCheckCodeTriplet($triplet) == $YES)
		{
			if ($code_lettre==$CODE_UNE_LETTRE)
			{	
				$AA=$Idx1{$triplet};
			}
			else # ($code_lettre==$CODE_TROIS_LETTRE)
			{	
				$AA=$Idx3{$triplet};
			}
		}
		else{
			$AA="?";
		}	
	$seq_traduite[$i]=$AA;
    }
    #print "\n@seq_traduite\n";
    return @seq_traduite;
}

########################################################################
#[pCheckCodeTriplet
#Aim:    Check DNA sequence      
#Global:
#In: $aas
#Out: $YES $NO
#Update:
#Prerequisite:     
#]
sub pCheckCodeTriplet
{
	local($aas)=@_;
	if ($aas !~ /(?i)[CAGT]{3}/)     
	{
	#	print STDOUT " PROBLEME :base(s) inconnus $aas\n";
		#exit(0);
		return $NO;
	}
	
	#tester si triplet contient autres chose que acgt
	return $YES;
}

########################################################################
#[pReadCode
#Aim:   Read from a file the genetique code (format :   TTT Phe F)     
#Global:
#In: $file
#Out: Idx1 (for the 1 letter code) ,*Idx3 (for the 3 letters code)
#Update:
#Prerequisite:     
#]
sub pReadCode
{
    local($file)=@_;
    open (CODE,"$file")|| print SDOUT ("Error : Cannot open file --$file--\n");
    
    
    while (<CODE>)
    {
	chop;
	@F=split;
	$Idx3{$F[0]}=$F[1];
	$Idx1{$F[0]}=$F[2];
    }
    return (*Idx1,*Idx3);
}


########################################################################
#[pLongFastaFile
#Aim:  
#Global:
#In: $file
#Out:length
#Update:
#Prerequisite:     
#]
sub pLongFastaFile
{
    local($file)=@_;
    return &pLengthFastaFile($file);
}

########################################################################
#[pengthFastaFile
#Aim:Get the length from a fasta file (format : >name | length | AC; | comment)  
#Global:$INFINI
#In: $file
#Out: $length
#Update:
#Prerequisite:     
#]
sub pLengthFastaFile
{
    local($file)=@_;
    my (@F) = ();
    my($line) = `$UNIXGREP \"^>\" $file`;
    chop($line);
    $line=~s/ //g;
    @F=split('\|',$line);
    if ($F[1] =~ /(\d+)/)
    {
	return $F[1];
    }
    else 
    {
	return $INFINI;
    }
}

########################################################################
#[pLongFormatCommunFile
#Aim: Get the length from a file with the Commun format     
#Global:$UNIXGREP
#In: $file ( zipped file are allowed)
#Out: $length
#Update:
#Prerequisite:     
#]
sub pLongFormatCommunFile
{
    my($file)=@_;
    my (@F) = (); 
    my($line) = ($file =~ /$UNIXDEZIP/ ) ? "$file $UNIXGREP QUERYLENGTH " : "$UNIXGREP \"^QUERYLENGTH\" $file";
    $line=`$line`; 
    my($number);
    chop($line);
    $line=~s/ //g;
    @F=split('\:',$line);
    if ($F[1]=~ /(\d+)/)
    {
	return $F[1];
    }
    else 
    {
	return $INFINI;
    }   
}

########################################################################
#[pNameBAC
#Aim: Get name from a fasta file     (format : >name | length | AC; | comment)       
#Global:$UNIXGREP
#In: $file
#Out: $name
#Update:
#Prerequisite:     
#]
sub pNameBAC
{
    local($file)=@_;
    my (@F) = ();

    $line = `$UNIXGREP \"^>\" $file`;
    chop($line);
    @F=split('\|',$line);
    $name = $F[0];
    $F[0]=~ s/>//;
    $F[0]=~ s/ //g;
    return $F[0];
}

########################################################################
#[pGetNameFileNoExt
#Aim: Get the file name without its extension    
#Global:
#In: $file
#Out: $file with no extension
#Update:
#Prerequisite:     
#]
sub pGetNameFileNoExt
{ 
    local($file)=@_;
    my (@F) = ();
    @F=split('\.',$file); 
    return $F[0];
}


########################################################################
#[pGetIdBAC
#Aim:      Get a BAC id number from  its info file
#Global:
#In: $file
#Out: $id
#Update:
#Prerequisite:     
#]
sub pGetIdBAC
{
    local($file)=@_;
    my($name)="";
    my($line);
    my(@F);
    if (!&pFileExiste("$file.$DNAINFO_FILE_EXTENSION"))
    {	
	return("");
    }
    else
    {
	my($line)=`$UNIXGREP BAC $file.$DNAINFO_FILE_EXTENSION`;
	@F=split(' ',$line);
	$name=$F[1];
	return($name);
    }
}

########################################################################
#[pGetDir
#Aim:       Get the directory   of a file
#Global:
#In: $file
#Out: $rep
#Update:
#Prerequisite:     
#]
sub pGetDir
{ 
    local($file)=@_;
    my (@F) = ();
    @F=split('\/',$file); 
    $rep="";
    for ($i=1;$i<$#F;$i++)
    {
	$rep=$rep."/".$F[$i];
    }
    return $rep;
}
########################################################################
#[pGetNameFile
#Aim: Get the file name from the whole path         
#Global:
#In: $rep_file
#Out: $file
#Update:
#Prerequisite:     
#]
sub pGetNameFile
{ 
    local($rep_file)=@_;
    my (@F) = ();
    @F=split('\/',$rep_file); 
    my($file)=$F[$#F];
    return $file;
}
########################################################################
#[pGetLastNameInDirectory
#Aim:  Get the file in a directory        
#Global:
#In: $rep
#Out: $last_rep
#Update:
#Prerequisite:     
#]
sub pGetLastNameInDirectory
{ 
    local($rep)=@_;
    my (@F) = ();
 #   chop($rep_file) until ($rep_file !~ /\/$/);
    @F=split('\/',$rep); 
    my($last_rep)=$F[$#F];
    return $last_rep;
}

########################################################################
#[pFormatProteine
#Aim:     Format a proteine sequence by adding an end of line every 50 AA     
#Global:
#In: $prot
#Out: $prot_formate
#Update:
#Prerequisite:     
#]
sub pFormatProteine
{
    local($prot)=@_;
    my($prot_formate)="";
    my($nbAAS)=length($prot);
    my($nbLines)=$nbAAS/50;
    my($j)=0;
    for($j=0;$j<$nbLines;$j++)
    {
	$sub_line=substr($prot,$j*50,50);
	$prot_formate.="$sub_line\n";
    }
    return $prot_formate;
}

########################################################################
#[pTri_Date
#Aim:          Sort a dates array
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pTri_Date
{
    my (@A)=split ("_",$a);
    my (@B)=split ("_",$b);	
    #pour l'an 2000, en considerant qu'il n'y est pas de dates anterieurs a 1999
    if ($A[2]==99)
    {
	$A[2]=1999;
    }
    else
    {
	$A[2]="20$A[2]";
	
    }		
    if ($B[2]==99)
    {
	$B[2]=1999;
    }
    else
    {
	$B[2]="20$B[2]";
	
    }
    $VAL =	$A[2] cmp $B[2]; # compare l'annee
    $VAL =	$A[1] cmp $B[1] if ($VAL ==0 );#compare le mois
    $VAL =	$A[0] cmp $B[0] if ($VAL ==0 );#compare le jour
    return $VAL;
    
}
########################################################################
#[pFileExiste
#Aim:   Test if a file exist or not       
#Global:$YES ,$NO
#In: $myFile
#Out: $YES or $NO
#Update:
#Prerequisite:     
#]
sub pFileExiste
{
    local ($myFile)=@_;
    if (($myFile ne "")&&(-e $myFile)  )
    {
	return $YES;
    }
    else
    {
	#&jTraceWWW("WARNING in function pFileExiste   ::: #$myFile# does not exist OR is empty\n");
	return $NO;
    }
}
########################################################################
#[pFileIsEmpty
#Aim:      Test if a file is empty or not    
#Global:$YES ,$NO
#In: $myFile
#Out: $YES or $NO
#Update:
#Prerequisite:     
#]
sub pFileIsEmpty
{
    local ($myFile)=@_;
    if ((-z $myFile))
    {
	return $YES;
    }
    else
    {
	return $NO;
    }
}
########################################################################
#[pReadCouleur
#Aim:  Read the color configuration file     (format :   couleur(lettre)|R,G,B|code hexadecimal de la couleur) 
#Global:
#In: $file_couleur
#Out: %tab
#Update:
#Prerequisite:     
#]
sub pReadCouleur
{
    local($file_couleur)=@_;
    open(COULEUR,"$file_couleur") || print STDOUT  "ERROR opening file_couleur : $file_couleur in function pReadCouleur\n";
    my (%tab)=();
    while (<COULEUR>)
    {
	chomp;
	my(@F)=split('\|',$_);
	chomp($F[0]);
	chomp($F[2]);
	$F[1] =~ s/ // while ($F[1] =~ /^ /);
	$tab{$F[0]}[0]=$F[1];#couleur au format R,G,B
	$tab{$F[0]}[1]=$F[2];#code couleur hexadecimal
    }
    close (COULEUR);
    return %tab;
    
}

########################################################################
#[pReadConfig
#Aim:    Read ORGA_CONFIG      (format : VAR=value), and for value which are not set, set default value
#Global:
#In: $file
#Out: %idxVAR
#Update:
#Prerequisite:     
#]
sub pReadConfig
{
    local($file)=@_;
    if ((-e $file) && (!-z $file))
    {
	open (CONFIG,"$file") ||   print STDOUT "ERROR opening $file\n";
	while (<CONFIG>)
	{
	    if (!/^\#/)
	    {
		    chomp;
		    my(@F)=split('=',$_,2);	
		    if ($F[0] ne "" && $F[1] ne "" )
		    {
			$idxVAR{$F[0]}=$F[1];
		    }
		}
	}
	close(CONFIG);
    }
    else
    {
	print STDERR "ERROR while reading the file : $file\n";
	exit;
    }
    $idxVAR{DIR_ORGA_CONFIG}="$ROOT/$DIR_CONFIG" if ((! defined $idxVAR{DIR_ORGA_CONFIG}) || ($idxVAR{DIR_ORGA_CONFIG} eq ""));	
    ####################################
    #### Set VALUES empty to default values
    ####################################
     if ((! defined $idxVAR{FILE_CLASSIFICATION}) || ($idxVAR{FILE_CLASSIFICATION} eq "")|| (!&pFileExiste("$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASSIFICATION}")))
    {
        $idxVAR{FILE_CLASSIFICATION}=$DEFAULT_FILE_CLASSIFICATION;
    }
    else
    {
        $idxVAR{FILE_CLASSIFICATION}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASSIFICATION}";
    }
    if ((! defined $idxVAR{FILE_CODE_GENETIQUE}) || ($idxVAR{FILE_CODE_GENETIQUE} eq "")|| (!&pFileExiste("$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CODE_GENETIQUE}")))
    {
	$idxVAR{FILE_CODE_GENETIQUE}=$DEFAULT_FILE_CODE_GENETIQUE ; 
    }
    else
    {
	$idxVAR{FILE_CODE_GENETIQUE}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CODE_GENETIQUE}";
    }
    if ((! defined $idxVAR{FILE_CLASS_COLOR}) || ($idxVAR{FILE_CLASS_COLOR} eq "")|| (!&pFileExiste("$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASS_COLOR}")))
    {
	$idxVAR{FILE_CLASS_COLOR}=$DEFAULT_FILE_CLASS_COLOR;
    }
    else
    {
	$idxVAR{FILE_CLASS_COLOR}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASS_COLOR}";
    }
    if ((! defined $idxVAR{FILE_PATTERNS}) || ($idxVAR{FILE_DNA_PATTERNS} eq "")|| (!&pFileExiste("$idxVAR{DIR_DB_ORGA}/$idxVAR{FILE_DNA_PATTERNS}")))
    {
	$idxVAR{FILE_DNA_PATTERNS}=$DEFAULT_FILE_DNA_PATTERNS;
    }
    else
    {
	$idxVAR{FILE_DNA_PATTERNS}="$idxVAR{DIR_DB_ORGA}/$idxVAR{FILE_DNA_PATTERNS}";
    }
    if ((! defined $idxVAR{FILE_FEATURES}) || ($idxVAR{FILE_FEATURES} eq "")|| (!&pFileExiste("$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_FEATURES}")))
    {
	$idxVAR{FILE_FEATURES}=$DEFAULT_FILE_FEATURES;
    }
    else
    {
	$idxVAR{FILE_FEATURES}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_FEATURES}";
    }
    
    
    if ((! defined $idxVAR{DB_rRNA}) || ($idxVAR{DB_rRNA} eq "")|| (!&pFileExiste("$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_rRNA}")))
    {
	$idxVAR{DB_rRNA}=$DEFAULT_rRNA_DB;
    }
    else
    {
	$idxVAR{DB_rRNA}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_rRNA}";
    }
    if ((! defined $idxVAR{DB_IS}) || ($idxVAR{DB_IS} eq "")|| (!&pFileExiste("$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_IS}")))
    {
	$idxVAR{DB_IS}=$DEFAULT_IS_DB;
    }
    else
    {
	$idxVAR{DB_IS}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_IS}";
    }
    $idxVAR{DB_REP}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_REP}";
    $idxVAR{DB_REP}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_ORGA}";
    $idxVAR{DNA_DATABASE}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DNA_DATABASE}";
    $idxVAR{FILE_TABLE_FREQUENCE}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_TABLE_FREQUENCE}";
    return %idxVAR;
}

########################################################################
#[pReadConfig2Configure
#Aim:    Read ORGA_CONFIG     to write the configuration files
#Global:
#In: $file
#Out: %idxVAR
#Update:
#Prerequisite:     
#]
sub pReadConfig2Configure
{
    local($file)=@_;
    if ((-e $file) && (!-z $file))
    {
	open (CONFIG,"$file") ||   die "ERROR opening $file\n";
	while (<CONFIG>)
	{
	    if (!/^\#/)
	    {
		    chomp;
		    my(@F)=split('=',$_,2);	
		    if ($F[0] ne "" && $F[1] ne "" )
		    {
			$idxVAR{$F[0]}=$F[1];
		    }
		}
	}
	close(CONFIG);
    }
    else
    {
	print STDERR "ERROR while reading the file : $file\n";
	exit;
    }		       
    $idxVAR{DIR_ORGA_CONFIG}="$ROOT/$DIR_CONFIG" if ((! defined $idxVAR{DIR_ORGA_CONFIG}) || ($idxVAR{DIR_ORGA_CONFIG} eq ""));
    $idxVAR{FILE_CLASSIFICATION}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASSIFICATION}"; 
    $idxVAR{FILE_CODE_GENETIQUE}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CODE_GENETIQUE}"; 
    $idxVAR{FILE_CLASS_COLOR}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CLASS_COLOR}"; 
    $idxVAR{FILE_FEATURES}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_FEATURES}"; 
    $idxVAR{FILE_TABLE_FREQUENCE}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_TABLE_FREQUENCE}";
    $idxVAR{FILE_DNA_PATTERNS}="$idxVAR{DIR_DB_ORGA}/$idxVAR{FILE_DNA_PATTERNS}";  
    $idxVAR{DB_rRNA}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_rRNA}";
    $idxVAR{DB_IS}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_IS}";
    $idxVAR{DB_REP}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DB_REP}";
    $idxVAR{DNA_DATABASE}="$idxVAR{DIR_DB_ORGA}/$idxVAR{DNA_DATABASE}";
    $idxVAR{FILE_CODE_GENETIQUE}="$idxVAR{DIR_ORGA_CONFIG}/$idxVAR{FILE_CODE_GENETIQUE}";
    return %idxVAR;
}
########################################################################
#[pIsBigger
#Aim:    Test if var1 is bigger than var2      
#Global: $YES , $NO
#In: $var1,$var2
#Out: $YES or $NO
#Update:
#Prerequisite:     
#]
sub pIsBigger
{
    local($var1,$var2)=@_;
    if ($var1>$var2)
    {
	return $YES;
    }
    else
    {
	return $NO;
    }
}

########################################################################
#[pFillParam
#Aim:          Initialize Parameters 
#Global:
#In: *in
#Out: 
#Update:*out
#Prerequisite:     
#]
sub pFillParam
{

    local (*in,*out) = @_;
    my($i);
    my($key);
    foreach $key (keys %in)
    {
	$trouve=$NO;
	foreach $i (keys %out)
	{
	    if ($i eq $key)
	    { 
		$trouve=$YES;
		if (("\U$in{$key}" eq "NO"))
		{
		    $out{$i}=$NO;
		}
		elsif ("\U$in{$key}" eq "YES" )
		{
		    $out{$i}=$YES;
		}
		else
		{
		    $out{$i}=$in{$key};
		}
		last; 
	    }  	   
	}
    }
 
} 

########################################################################
#[pIsEmpty
#Aim:  Check if a value is empty or not        
#Global:
#In: $var
#Out: $YES or $NO
#Update:
#Prerequisite:     
#]
sub pIsEmpty
{
    local($var)=@_;
    if ($var eq "")
    {
	return $YES;
    }
    else
    {
	return $NO;
    }
}

########################################################################
#[pReadInfoBac
#Aim:       Read info contig file   
#Global:
#In: $file
#Out: $name,$who
#Update:
#Prerequisite:     
#]
sub pReadInfoBac
{
    local($file)=@_;
    if (!&pFileExiste($file))
    {	
	return("","");
    }
    else
    {
	my($line)=`$UNIXGREP BAC $file`;
	@F=split(' ',$line);
	$name=$F[1];
	$line=`$UNIXGREP ANNOTATOR $file`;
	@F=split(' ',$line);
	$who=$F[1];
	return($name,$who);
    }
}

########################################################################
#[pFillParamBlastx
#Aim:         Initialize Blastx Parameters 
#Global:
#In: 
#Out: 
#Update:*param,*paramOUT
#Prerequisite:     
#]
sub pFillParamBlastx
{
    local(*param,*paramOUT)=@_;  
    #intervalle de proba dans lequel filrer
    if ($param{PROBA} =~ /,/)
    {
	my(@F)=split (",",$param{PROBA});
	if ($F[0]<$F[1])
	{
	    $param{PROBA}=$F[1];
	    $param{PROBA_INF}=$F[0];
	}
	else
	{
	    $param{PROBA}=$F[0];
	    $param{PROBA_INF}=$F[1];
	}
    }
    &pFillParam(*param,*paramOUT);    
}


########################################################################
#[pFiltreMatchBlast
#Aim:     Filter out Blast Matchs     
#Global: 
#In: *idxFiltre,*idxIdenMatch,*idxRedun
#Out: $YES or $NO
#Update:
#Prerequisite:     
#]
sub pFiltreMatchBlast
{
    local(*idxFiltre,*idxIdenMatch,$Redundancy,*tab_redundancy)=@_;
    my(@FILTRE_BLAST_INFERIEUR) = ("GAP","PROBA","MISMATCH");
    my(@FILTRE_BLAST_SUPERIEUR) = ("SCORE","IDENTITY","POS","PCS","PCQ","LENGTH_HSP","PROBA_INF","C_SCORE","D_SCORE","Dp_SCORE","STEM","MATCH");
    my($BeginPosHSP);
    my($EndPosHSP);
    my($BeginFiltre);
    my($EndFiltre);
    $idxFiltre{"BEGIN"}=0 if ($idxFiltre{"BEGIN"} eq "");
    $idxFiltre{"END"}=$INFINI if ($idxFiltre{"END"} eq "");
   
    if ($idxIdenMatch{"BEGIN"}<$idxIdenMatch{"END"})
    {
	$BeginPosHSP=$idxIdenMatch{"BEGIN"};
	$EndPosHSP=$idxIdenMatch{"END"};
    }
    else
    {
	$BeginPosHSP=$idxIdenMatch{"END"};
	$EndPosHSP=$idxIdenMatch{"BEGIN"};
    }
    if ($idxFiltre{"BEGIN"}<$idxFiltre{"END"})
    {
	$BeginFiltre=$idxFiltre{"BEGIN"};
	$EndFiltre=$idxFiltre{"END"};
    }
    else
    {
	$BeginFiltre=$idxFiltre{"END"};
	$EndFiltre=$idxFiltre{"BEGIN"};
    }
    
    # step 1 :: filtre sur l'intervalle choisi selon deux modes, strict ou non
    if (!$idxFiltre{STRICT})
    { 
	#les HSP sont a gauche ou a droite de l'intervalle du filtre
	return $NO if ((($BeginPosHSP <$BeginFiltre)&&($EndPosHSP < $BeginFiltre)) || (($BeginPosHSP > $EndFiltre)&&($EndPosHSP > $EndFiltre)));
    }
    else
    {
	return $NO if (($BeginPosHSP <$BeginFiltre)||($EndPosHSP > $EndFiltre));
    }  
    # step 2 :: filtre sur l'overlap
    if (!&pIsEmpty( $idxFiltre{OVERLAP}))
    {
	return $NO if (($idxFiltre{OVERLAP}<$BeginPosHSP)||($idxFiltre{OVERLAP}>$EndPosHSP));
    }
    # step 3 :: filtre superieur (valeur du HSP doit etre inferieur)
    foreach $i (0 .. $#FILTRE_BLAST_INFERIEUR)
    {
    	
#	print STDOUT "#####$FILTRE_BLAST_INFERIEUR[$i]###$idxFiltre{$FILTRE_BLAST_INFERIEUR[$i]} vs match=$idxIdenMatch{$FILTRE_BLAST_INFERIEUR[$i]}\n"if (!&pIsEmpty( $idxFiltre{$FILTRE_BLAST_INFERIEUR[$i]}));
	return $NO if ((!&pIsEmpty( $idxFiltre{$FILTRE_BLAST_INFERIEUR[$i]}) ) &&($idxIdenMatch{$FILTRE_BLAST_INFERIEUR[$i]}>$idxFiltre{$FILTRE_BLAST_INFERIEUR[$i]}));
    }
    #step 4 :: filtre inferieur (valeur du HSP doit etre inferieur)
    foreach $i (0 .. $#FILTRE_BLAST_SUPERIEUR)
    {
#    	print STDOUT "#####$FILTRE_BLAST_SUPERIEUR[$i]###$idxFiltre{$FILTRE_BLAST_SUPERIEUR[$i]} vs match=$idxIdenMatch{$FILTRE_BLAST_SUPERIEUR[$i]}\n"if (!&pIsEmpty( $idxFiltre{$FILTRE_BLAST_SUPERIEUR[$i]}));
	return $NO if ((!&pIsEmpty( $idxFiltre{$FILTRE_BLAST_SUPERIEUR[$i]}) ) &&($idxIdenMatch{$FILTRE_BLAST_SUPERIEUR[$i]}<$idxFiltre{$FILTRE_BLAST_SUPERIEUR[$i]}));
   }
    #step 5 :: filtre sur mot cle
    if ( !&pIsEmpty($idxFiltre{COM}))
    {
	my(@mot_cles)= split (',', $idxFiltre{COM});
    	my($trouve_com)=$NO;
	for( $j =0;$j<=$#mot_cles;$j++)
	{
	   #print STDOUT "mot_cles#$idxIdenMatch{comment}##$idxIdenMatch{s_name}  $mot_cles[$j]###\n";
	    if (("\U$idxIdenMatch{comment}" =~/$mot_cles[$j]/i )||("\U$idxIdenMatch{s_name}" =~ /$mot_cles[$j]/i))
	    {
		$trouve_com=$YES;
	    }
	}
	return $NO if (!$trouve_com); #n'a trouve aucun mot cles
    }
    if("\U$Redundancy"  =~ /Inclusion/i)
    {
	my($j)=0;
	my($courant)=$#tab_redundancy+1;
	for ($j=0;$j<=$#tab_redundancy;$j++)
	{
#	    print STDOUT "#($idxIdenMatch{s_begin}>=$tab_redundancy[$j][0])&&($idxIdenMatch{s_end}<=$tab_redundancy[$j][1])&&($idxIdenMatch{id_query} eq $tab_redundancy[$j][2])####<BR>";
#	    print STDOUT "#return<BR>"if(($idxIdenMatch{s_begin}>=$tab_redundancy[$j][0])&&($idxIdenMatch{s_end}<=$tab_redundancy[$j][1])&&($idxIdenMatch{id_query} eq $tab_redundancy[$j][2]));
	    return $NO if(($idxIdenMatch{s_begin}>=$tab_redundancy[$j][0])&&($idxIdenMatch{s_end}<=$tab_redundancy[$j][1])&&($idxIdenMatch{id_query} eq $tab_redundancy[$j][2])) ;
	

	    
	}
	$tab_redundancy[$courant][0]=$idxIdenMatch{s_begin};# subject begin
	$tab_redundancy[$courant][1]=$idxIdenMatch{s_end};#subject end

	$tab_redundancy[$courant][2]=$idxIdenMatch{id_query};#name query 
    }
    if("\U$Redundancy"  =~ /4snoscan/i) ### Pour snoscan
    {
	my($j)=0;
	my($courant)=$#tab_redundancy+1;
	for ($j=0;$j<=$#tab_redundancy;$j++)
	{
	   return $NO if(($idxIdenMatch{q_begin}==$tab_redundancy[$j][0])&&($idxIdenMatch{q_end}==$tab_redundancy[$j][1])&&($idxIdenMatch{id_query} eq $tab_redundancy[$j][2])) ;
	

	    
	}
	$tab_redundancy[$courant][0]=$idxIdenMatch{q_begin};# subject begin
	$tab_redundancy[$courant][1]=$idxIdenMatch{q_end};#subject end
	$tab_redundancy[$courant][2]=$idxIdenMatch{id_query};#name query 
    }
    return $YES;
}

########################################################################
#[pGetACBac
#Aim:         Get AC number for a contig  
#Global:
#In: $bac,$num_AC
#Out: $AC_number
#Update:
#Prerequisite:     
#]
sub pGetACBac
{
    local($bac,$num_AC)=@_;
    my($ac)=0;my($version)=0;
    ($ac,$version)=($bac=~ /(\d{3})_(\d{2})$/);
    return "$num_AC$ac\_DNA";
}


########################################################################
#[pPrintMATCH_DBvsDB
#Aim:     Print out matchs filtered from Lassap (personnel)     | A verifier
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintMATCH_DBvsDB
{
    local ($match,$prog_meth,$file,$config) = @_;
    my(@F) = split("//",$match); 
    my(@QUERY) = split(' ',$F[0]);
    
    my($query_deb) = $QUERY[1];
    my($query_fin) = $QUERY[2];
#    print STDOUT "##in pPrintMATCH_DBvsDB #$query_deb#$query_fin\n";
 #   @G = split;
    my($id_query) = $QUERY[0];
    $id_query =~ s/[<;]//;
    my($link_genbank)=$gi_mask_PROT;
    if ($prog_meth =~ /blastn/i)
    {
	$link_genbank=$gi_mask_DNA;
    }
    my(@SUBJECT)=split(' ',$F[1]);
    my($id_subject)=$SUBJECT[0];
    $id_subject=~ s/;//;
    my($subject_deb)=$SUBJECT[1];
    my($subject_fin)=$SUBJECT[2];
    my($line_DATAS)=$F[2];
    my($length_hsp)=abs($query_deb-$query_fin);
    my($identite)=($match=~ / I=(\d+)% /);
    my($com)=$id_subject;
    if (($id_subject=~ /^g/)||($id_subject=~ /^gi_/i))
    {
	$id_subject=~ s/^gi_//i;
	$id_subject=~ s/^g//i;
	@T=split("_",$id_subject);
	$id_subject=$T[0];
	$id_subject_tmp=$link_genbank;
	$id_subject_tmp=~s/MASK/$id_subject/;
	$orga=$T[1];
	$orga=~ tr/a-z/A-Z/;
	$id_subject_tmp=~s/MASK/${orga}_$id_subject/;
    }
    else
    {
	$id_subject_tmp=&pMakelink_Map_SRS($id_subject,$config,$subject_deb,$subject_fin);
    }
    if (($id_query=~ /^g/)||($id_query=~ /^gi_/i))
    {
	$id_query=~ s/^gi_//i;		
	$id_query=~  s/^g//i;
	$id_query_tmp=$link_genbank;
	@T=split("_",$id_query);
	$id_query=$T[0];
	$id_query_tmp=~s/MASK/$id_query/;
	$orga=$T[2];
	$orga=~ tr/a-z/A-Z/;
	$id_query_tmp=~s/MASK/$T[1]_$orga/;
    }
    else
    {
	$id_query_tmp=&pMakelink_Map_SRS($id_query,$config,$query_deb,$query_fin);
    }
    my($length_line_DATAS)=length($line_DATAS);
#    $length_line_DATAS= 20 if ($length_line_DATAS<=20);
#    $length_line_DATAS= 60 if ($length_line_DATAS>20);
    @S=split(" ",$F[2]);
    $com =~ s/### \?//g;
    $val = sprintf ("<LI><A HREF=$WWW_SCRIPT_AA_DNA_FILTER?&FILE=$file&BEGIN=$query_deb&END=$query_fin&IDENTITY=$identite&COM=$com&LENGTH_HSP=$length_hsp&ORGA_CONFIG=$config>$prog_meth</A> //%20s %6d-%6d //<B>%${length_line_DATAS}s</B>// $id_subject_tmp <B>%6d-%6d</B> \n",$id_query_tmp,$query_deb,$query_fin,$line_DATAS,$subject_deb,$subject_fin);
    $val =~ s/<TH>//g;
    print $val;
}

########################################################################
#[pPrintMATCH_DrawMap
#Aim:       A verifier   
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintMATCH_DrawMap
{
    local ($match,$prog_meth,$file,$config) = @_;
    my(@F) = split("//",$match); 
    my(@QUERY) = split(' ',$F[0]);
    my($query_deb) = $QUERY[1];
    my($query_fin) = $QUERY[2];
    
 #   @G = split;
    my($id_query) = $QUERY[0];
    $id_query =~ s/<//;
    my($link_genbank)=$gi_mask_PROT;
    my(@SUBJECT)=split(' ',$F[1]);
    my($id_subject)=$SUBJECT[0];
    my($subject_deb)=$SUBJECT[1];
    my($subject_fin)=$SUBJECT[2];
    my($line_DATAS)=$F[2];
    my($length_hsp)=abs($query_deb-$query_fin);
    my($identite)=($match=~ / I=(\d+)% /);
    my($com)=$id_subject;
    my($comment)=$F[3];
    if (($id_subject=~ /^g/)||($id_subject=~ /^gi_/i))
    {
	$id_subject=~ s/;//;
	$id_subject=~ s/^gi_//i;
	$id_subject=~ s/^g//i;
	@T=split("_",$id_subject);
	$id_subject=$T[0];
	$id_subject_tmp=$link_genbank;
	$id_subject_tmp=~s/MASK/$id_subject/g;
    }
    else
    {
	$id_subject_tmp=$id_subject;
    }
    $id_query_tmp=$id_query;
    my($length_line_DATAS)=length($line_DATAS);
#    $length_line_DATAS= 20 if ($length_line_DATAS<=20);
#    $length_line_DATAS= 60 if ($length_line_DATAS>20);
    @S=split(" ",$F[2]);
    $com =~ s/### \?//g;
    $val = sprintf ("<LI><A HREF=$WWW_SCRIPT_AA_DNA_FILTER?&FILE=$file&BEGIN=$query_deb&END=$query_fin&IDENTITE=$identite&COM=$com&LENGTH_HSP=$length_hsp&ORGA_CONFIG=$config>$prog_meth</A> //%20s %6d-%6d //<B>%${length_line_DATAS}s</B>// $id_subject_tmp %6d-%6d<B>  $comment</B> \n",$id_query_tmp,$query_deb,$query_fin,$line_DATAS,$subject_deb,$subject_fin);
    $val =~ s/<TH>//g;
    print $val;
}


########################################################################
#[pGetTagConfig
#Aim: Get the value of a TAG in the ORGA_CONFIG          
#Global:$UNIXGREP
#In: $tag,$file
#Out: $value
#Update:
#Prerequisite:     
#]
sub pGetTagConfig
{
    local ($tag,$file) = @_;
    my($line) = `$UNIXGREP $tag $file`;
    chomp($line);
    my(@F)=split("=",$line,2); # Attention si ligne contient plusieurs signes =
    return ($F[1]); 
}

########################################################################
#[pGetExtensionFile
#Aim:   Get the extension of a file       
#Global:
#In: $file
#Out: $extension
#Update:
#Prerequisite:     
#]
sub pGetExtensionFile
{
    local ($file) = @_;
    chomp($file);
    my(@F) = split('\.',$file);
    my($extension)=$F[$#F];
    return ($extension); 
}

########################################################################
#[pStrContainsStr
#Aim:      Check if a string contains an other one 
#Global: 
#In:        $Str1,$Str2
#Out:       $YES or $NO
#Update:
#Prerequisite:     
#]
sub pStrContainsStr
{
    local($Str1,$Str2)=@_;
    $Str2=~ s/\./\\./g;
    if (($Str1 eq "")|| ($Str2 eq ""))
    {
	return $NO ;
    }
    elsif ($Str1 =~ /$Str2/i)
    { 
	return  $YES;
    }
    else
    {  
	return  $NO;
    }
}

########################################################################
#[pCutSentences
#Aim:     Preformat lines (SP or embl ...)
#Global: 
#In:        $size_line,$separateur (to consider to cut or not a word),$msg (the line),$starter (for embl and genbak features for example)
#Out:       $text_formate
#Update:
#Prerequisite:     
#]
sub pCutSentences
{
    local($size_line,$separateur,$msg,$starter)=@_;
    chomp($msg);
    my(@tab_msg)=split("$separateur",$msg);
    my($i)=0;
    my($text_formate)="";
    my($line_formate)="";
    my($line)="";
    for ($i=0;$i<=$#tab_msg;$i++)
    {
	$line="$line_formate"."$tab_msg[$i]";
	if ((length($line)-1)<$size_line)
	{
	    $line_formate.="$tab_msg[$i]";
	    $line_formate.="$separateur" if ($i!=$#tab_msg);
	    $text_formate.="$starter$line_formate\n" if ($i==$#tab_msg);
	}
	else
	{
	    $text_formate.="$starter$line_formate\n";
	    $line_formate="$tab_msg[$i]";
	    $line_formate=~ s/^ //; ### Si le separateur est ;
	    $text_formate.="$starter$line_formate\n" if ($i==$#tab_msg);
	    $line_formate.="$separateur" if ($i!=$#tab_msg);
	}
    }
    return ($text_formate);
}

########################################################################
#[pIsMultiple
#Aim:     Check if a number is multiple of an other one (i.e. : for the genetic code, $multiple=3)
#Global: 
#In:        $number,$multiple
#Out:       $YES,$NO
#Update:
#Prerequisite:     
#]
sub pIsMultiple
{
    local($number,$multiple)=@_;
    my($test)=(($number%$multiple) ==0) ?( $YES) : ($NO);
    return ($test);
}


########################################################################
#[pReadCoordonneesLine
#Aim:     Get data from a line in the format begin|end|nom_gene|class|num_accession
#Global: 
#In:       $line 
#Out:      num_field_in_line,begin,end,nom_gene,class,num_accession
#Update:
#Prerequisite:     
#]
sub pReadCoordonneesLine
{
    local($line)=@_;
	print STDOUT "$line\n";
    if (&pStrContainsStr($line,"\\:"))##### AU cas ou le resultat du grep donne la ligne+le nom du fichier
    {
    	my(@L)=split("\\:",$line);
	$line=$L[1];
    }
    my(@F)=split("\\|",$line);
    ######4,begin,end,nom_gene,class,num_accession
    return($#F,$F[0],$F[1],$F[2],$F[3],$F[4]);
}


########################################################################
#[pGetLastLineNotEmptyInFile
#Aim:   Get the last line in a file (not empty)
#Global: 
#In:       $file 
#Out:      $last_line
#Update:
#Prerequisite:     
#]
sub pGetLastLineNotEmptyInFile
{
	local($file)=@_;
	my($last_line)="";
	my($i)=0;
	my(@I)=split (' ',`$UNIXWC $file`);
	my($total_line)=$I[0];
	chomp($total_line);
	for($i=1;$i<=$total_line;$i++)
	{
		my(@tab)=`$UNIXTAIL -${i}l $file`;
		$lastline=$tab[0] ;
		chomp($lastline);
		last if ($lastline ne "");
	}
	return ($lastline);
}



########################################################################
#[pReadDatafile
#Aim:    Read a DAT file (with one or several proteins)
#Global: $PROT,$CLASS,$DNA_ANN,$BEGIN,$END,$VALUE_TO,$CRITERE,$DE,$EC,$SQ,$GENE,$FA
#In:      $file,$majuscule(to uppercade teh DE field)
#Out:     @sorted_list_ORFS
#Update:
#Prerequisite:     
#]
sub  pReadDatafile
{
    local($file,$majuscule)=@_;
    open (FILE,"$file") || print STDERR "ERROR while opening dat file:$file in pReadDatafile ($0)\n";
    my(@list_ORFS)=();
    my($num)=0;
     while ($line = <FILE>)
    { 
	chomp($line);
	
	$line=~ s/ $// while ($line=~ / $/);
	my(@F)=split (' ',$line);
	my(@G)=split (' ',$line,2);
	if ($line =~ /^ID(\s)+/)
	{
	    $list_ORFS[$num][$ID]=$F[1];
	    $list_ORFS[$num][$PROT]="";
	    $list_ORFS[$num][$CLASS]="";
	    $list_ORFS[$num][$DNA_ANN]="";
	    $list_ORFS[$num][$BEGIN]=0;
	    $list_ORFS[$num][$END]=0;
	    $list_ORFS[$num][$VALUE_TO_SORT]=0;
	    $list_ORFS[$num][$CRITERE]="";
	    $list_ORFS[$num][$DE]="";
	    $list_ORFS[$num][$EC]="";
	    $list_ORFS[$num][$SQ]="";
	    $list_ORFS[$num][$GENE]="";
	    $list_ORFS[$num][$FA]=""; 
	}
	elsif ($line =~ /^AC(\s)+/)
	{
	    $list_ORFS[$num][$AC]=$F[1];
	}
	elsif (($line =~ /^OR(\s)+/)&&(($F[1] eq "VAL_POS")))
	{
	    $list_ORFS[$num][$BEGIN]=$F[2] ;
	    $list_ORFS[$num][$END]=$F[3];
	    $list_ORFS[$num][$VALUE_TO_SORT]=($F[2]<$F[3]) ? ($F[2]):($F[3]);  
	}
	elsif (($line =~ /^OR(\s)+/)&&(($F[1] eq "VAL_ANN")))
	{
	    @F=split (' ',$line,3);
	    $list_ORFS[$num][$DNA_ANN]=$F[2];  
	}
	elsif (($line =~ /^OR(\s)+/)&&(($F[1] eq "VAL_CRT")))
	{
	    @F=split (' ',$line,3);
	    $F[2]=~ s/ //g;
	    $F[2]=~ s/Gene_prediction/FrameD/;
	    $list_ORFS[$num][$CRITERE]=$F[2];  
	}	
	elsif ($line =~ /^CL(\s)+/)
	{
	    $line=~ s/CL   //;
	    my(@F)=split (" ",$line);
	    $list_ORFS[$num][$CLASS] = $F[0];
	}
	elsif ($line =~ /^DC(\s)+/)# identificateur de confiance sur la description
	{
	    my($number)=$line;
	    $number=~ s/DC(\s)+(\d)/$2/;
	    if (($INDICE_CONFIDENCE[$number]ne "")&&($number<3))
	    {
		$list_ORFS[$num][$NOTE]="Product confidence : $INDICE_CONFIDENCE[$number]";
	    }
	}                        
	elsif ($line =~ /^DE(\s)+/)
	{
	    $line=~ s/DE   //i;
	    my($confidence)= sprintf ("@INDICE_CONFIDENCE");
	    $confidence=~ s/ /\|/g;
	    $line=~ s/transmembrane//i;
	    $line=~ s/unknown//i;
	    $line=~ s/\///i;
	    if ($line !~ /$confidence/i)
	    {
		$list_ORFS[$num][$DE] = "$list_ORFS[$num][$NOTE] ";
		$list_ORFS[$num][$DE] =~ s/^Product confidence : //;
	    }
	    $list_ORFS[$num][$DE] .="$line";
	}
	elsif ($line =~ /^DF(\s)+/)
	{
	    $line=~ s/DF   //;
	    if ($list_ORFS[$num][$DE] eq "")
	    {
		$list_ORFS[$num][$DE]="$list_ORFS[$num][$NOTE]";
		$list_ORFS[$num][$DE] =~ s/^Product confidence : //;
	    }
	    $list_ORFS[$num][$DE] .= " $line";
	}
	elsif ($G[0] eq "EC")
	{
	    $list_ORFS[$num][$EC]=$G[1]; 
	}
	elsif ($G[0] eq "FA")
	{
	    $line=~ s/family//i;
	    $list_ORFS[$num][$FA]="$G[1]"; 
	}
	elsif ($F[0] eq "GC")
	{
	    my($number)=$line;
	    $number=~ s/GC(\s)+(\d)/$2/;
	    if ((defined($INDICE_CONFIDENCE[$number]))&&($number<3))
	    {
		$list_ORFS[$num][$NOTE].=";" if ($list_ORFS[$num][$NOTE] ne "");
		$list_ORFS[$num][$NOTE].="Gene name confidence : $INDICE_CONFIDENCE[$number]"; 
	    }
	}
	elsif ($G[0] eq "GN")
	{
	    $G[1]=~ s/ / OR /g;
	    $list_ORFS[$num][$GENE]=$G[1]; 
	} 
	elsif ($F[0] eq "SQ")
	{
	    $list_ORFS[$num][$SQ]=$line; 
	}    
	elsif (&pStrContainsStr($line,"\^     ")) 
	{
	    $line=~ s/ //g;
	    $list_ORFS[$num][$PROT].="$line";
	}
	if ($F[0] =~ /\/\//)
	{
 	    if ($list_ORFS[$num][$AC]=~/_AA/)
	    {
		if ($list_ORFS[$num][$DE] eq "")
		{
		    $list_ORFS[$num][$DE] = "hypothetical protein";
		}
		elsif ($list_ORFS[$num][$DE] !~ /PROTEIN/i)
		{
		    $list_ORFS[$num][$DE] .= " protein";
		}
	    }
	    $list_ORFS[$num][$DE]=~ tr/a-z/A-Z/ if ($majuscule);
	    $list_ORFS[$num][$DE]=~ s/^ // while ($list_ORFS[$num][$DE]=~ /^ /);
	    $num++;
	}
    }
    local(@sorted_list_ORFS)= sort {$a->[$VALUE_TO_SORT]<=>$b->[$VALUE_TO_SORT]} (@list_ORFS);
    return @sorted_list_ORFS;
}

########################################################################
#[pWriteSwissProt
#Aim:   Write SwissProt format
#Global:
#In:$begin,$end,*orfs,$config
#Out: 
#Update:
#Prerequisite:     
#]

sub pWriteSwissProt
{
    local($begin,$end,*orfs,$config)=@_;
    my($longueur)=abs($end-$begin+1);
    my ($SP_NAME)=&pGetTagConfig("SP_NAME",$config);
    $SP_NAME="BBBB" if ($SP_NAME eq "");
    my($date) = &jGetRunDate();
    my(@D)=split("-",$date);
    my($mini_date)="$D[1]-$D[2]";
    my ($taxonomy)=&pGetTagConfig("TAXONOMY",$config);
    my($organism)=&pGetTagConfig("ORGANISM_NAME",$config);
    my($strain)=&pGetTagConfig("STRAIN",$config);
    my($description)=&pGetTagConfig("DESCRIPTION",$config);
    my($classification_file)=&pGetTagConfig("FILE_CLASSIFICATION",$config);
    my($dir_config)=&pGetTagConfig("DIR_ORGA_CONFIG",$config);
    my($comment)=&pGetTagConfig("COMMENT",$config);
    my($authors)=&pGetTagConfig("AUTHORS",$config);
    my($title)=&pGetTagConfig("TITLE",$config);
    my($journal)=&pGetTagConfig("JOURNAL",$config);
    $classification_file="$dir_config/$classification_file";
    my(%idxClassification)=&pGetIdxClassification($classification_file); 
    my($text)=""; 
    my($length_AA)=0;
    my($name_prot)="";
    my($to_print)=$NO;
    for ($i=0;$i<=$#orfs;$i++)
    {
	next if (!&pStrContainsStr($orfs[$i][$AC],"AA"));
	if (($begin ne "")&&($end ne ""))
	{
	    ($tmp,$begin_tronque,$end_tronque,$to_print)=&pWriteOrfPositions($orfs[$i][$BEGIN],$orfs[$i][$END],$begin,$end);
	    next if ($begin_tronque!=0 || $end_tronque!=0 || !$to_print);
	}
	$length_AA=length($orfs[$i][$PROT]);
	$name_prot=~ tr/a-z/A-Z/;
	my(@G)=split(" ",$orfs[$i][$GENE]);
	my($gene)=$G[0];
	print STDOUT "ID   $gene     PRELIMINARY;      PRT;   $length_AA AA.\n";
	print STDOUT "DT   01-XXX-2000 (Rel. XX, Created)\n";
	print STDOUT "DT   01-XXX-2000 (Rel. XX, Last sequence update)\n";
	print STDOUT "DT   01-XXX-2000 (Rel. XX, Last annotation update)\n";
	if ($orfs[$i][$EC] eq "")
	{
	    $text=&pCutSentences($SIZE," ","$orfs[$i][$DE].","DE   "); 
	}
	else
	{
	    my(@K)=split(" ",$orfs[$i][$EC]);
	    my($j)=0;
	    my($EC)="";
	    for ($j=0;$j<=$#K;$j++)
	    {
		$EC.="(EC $K[$j])";
	    }
 	    $text=&pCutSentences($SIZE," ","$orfs[$i][$DE] $EC.","DE   "); 
	}
	print STDOUT "$text";
	print STDOUT "GN   $orfs[$i][$GENE] OR $orfs[$i][$AC].\n";
	print STDOUT "OS   $organism.\n";
	$description= "" if (&pStrContainsStr($description,"chromosome"));
	print STDOUT "OG   $description.\n" if ($description ne "");
	my($text)=&pCutSentences($SIZE,";","$taxonomy.","OC   "); 
	print STDOUT "$text";
	print STDOUT "RN   [1]\n";
	print STDOUT "RP   SEQUENCE FROM N.A.  \n";
	print STDOUT "RC   STRAIN=$strain;\n" if ($strain ne "");
	if ($authors ne "")
	{
	    $authors=~ s/[;.,]$//;
	    $text=&pCutSentences($SIZE,",","$authors;","RA   "); 
	    print  "$text"; 
	}
	print STDOUT "RL   Submitted (XXX-2000) to the SWISS-PROT data bank.\n";
	if ($orfs[$i][$CLASS] !~ /^VI/)
	{
	    $text=&pCutSentences($SIZE," ","PATHWAYS: $idxClassification{$orfs[$i][$CLASS]}.","CC       ");
	    $text=~ s/CC       /CC   -!- /;
	    print $text;
	}
	if ($orfs[$i][$FA] ne "")
	{
	    $text=&pCutSentences($SIZE," ","SIMILARITY: BELONGS TO THE $orfs[$i][$FA] FAMILY.","CC       ");
	    $text=~ s/CC       /CC   -!- /;
	    print $text;
	}
	my($CODE_crc4)=`$crc64 $orfs[$i][$PROT]`;
	print STDOUT "$orfs[$i][$SQ]  $CODE_crc4 CRC64;\n";
	my($cds)=&pFormatSwissProtein(66,$orfs[$i][$PROT]);
	print $cds;
	print STDOUT "//\n";
    }
}

########################################################################
#[pFormatSwissProtein
#Aim:     Preformat a pretein sequence in SP format 
#Global:
#In:  $size_line,$sequence
#Out: $format_sequence
#Update:
#Prerequisite:     
#]
sub  pFormatSwissProtein
{
  local($size_line,$sequence)=@_;
  my($format_sequence)=""; 
  my($pre_format_sequence)=""; 
  my($j)=0;
  my($nbBlocks)=(length($sequence))/10;
  for($j=0;$j<=$nbBlocks;$j++)
  {
      $sub_prot=substr($sequence,$j*10,10);
      $pre_format_sequence.="$sub_prot ";
  }
  my($nbLines)=(length($pre_format_sequence))/$size_line;
  for($j=0;$j<=$nbLines;$j++)
  {
      $sub_prot=substr($pre_format_sequence,$j*$size_line,$size_line);
      $format_sequence.="     $sub_prot\n";
  } 
  return($format_sequence);
}

########################################################################
#[pPrintErrorScript
#Aim: Print an error message and return the script name and the number line where the error occured  
#In:  $msg
#Out: 
#Update:
#Prerequisite:     
#]
sub  pPrintErrorScript
{
    local($msg)=@_;
    my($t1,$t2,$t3)= caller;
    print STDERR "$msg in $t1 of $t2 at line $t3\n";
}

########################################################################
#[pWriteBlankPage
#Aim:      Print out a blank HTML page
#Global:
#In: 
#Out:
#Update:
#Prerequisite:     
#]
sub pWriteBlankPage
{
    select(STDOUT);
    print STDOUT "<HTML><HEAD><TITLE>iANT</TITLE></HEAD><BODY>\n";
    print STDOUT "\<BODY BGCOLOR=\"#FFFFFF\" \></BODY></HTML>";
}

########################################################################
#[pGetValidatedBacsList
#Aim:         Get the list of validated Bacs listed in DIR_VALIDATED_BACS (ORGA config tag)
#Global:      $UNIXLS
#In:          $BACS_DIR
#Out:      
#Update:   *Contigs: list of validated bacs or contigs
#Prerequisite:     
#]
sub pGetValidatedBacsList
{
    local ($BACS_DIR,*TAB_Contigs)=@_;
    my(@Contigs) =`$UNIXLS -l  $BACS_DIR `;
    my($num)=0;
    for ($j=0;$j<=$#Contigs;$j++)
    {
    	if ($Contigs[$j] =~ /->/)
	{
		my(@C)=split("->",$Contigs[$j]);
		$TAB_Contigs[$num]=$C[1];
		$num++;
	}
    }
    
}

########################################################################
#[pCheckAnnotators
#Aim:       Check if user is in the IP annotator list
#Global:
#In: $config
#Out: 
#Update:
#Prerequisite:     
#]
sub pCheckAnnotators
{
    local($config)=@_;
    my($ip_supervisor) = &pGetTagConfig("IP_ANNOTATORS",$config);
    my(@F)=split(",",$ip_supervisor);
    my($ip);
    # si la variable n'est pas initialise, il a le droit d'editer
    if( $ip_supervisor eq "" )
    {
	return(1);
    }
    foreach $ip (@F)
    {
	return (1) if ( $ENV{REMOTE_ADDR} =~ /$ip/ );
    }
    return(0);
}

########################################################################
#[pPrintMsgHTML
#Aim:     Print an HTML message, open and close the HTML document
#Global:
#In: msg
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintMsgHTML
{
	local($msg)=@_;
	print STDOUT "<HTML><HEAD></HEAD><BODY>\n";
	print STDOUT "<B>$msg</B>\n";
	print STDOUT "</BODY></HTML>\n";
}

########################################################################
#[pGetDirFromCFG
#Aim:     Get the organism directory from the configuration file
#Global:
#In: $config
#Out: $dir
#Update:
#Prerequisite:     
#]
sub pGetDirFromCFG
{
	local($config)=@_;
	my($dir)=&pGetDir($config);
	$dir=~ s/$DIR_DOC$//;
	return ($dir);
}
###############################################
###############################################
################# OBSOLETE ####################
###############################################
###############################################

sub pScanFrameShift
{	
    local($file,*listeMatch)=@_; 
    return &pGetCandidateFrameShift($file,*listeMatch);
}


########################################################################
#[pFiltreMATCH
#Aim:        Obsolete function, replaced by   pFiltreMatchBlast
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pFiltreMATCH
{ 
    local(*idxFiltre,*idxIdenMatch,*idxRedun)=@_;
    return(&pFiltreMatchBlast(*idxFiltre,*idxIdenMatch,*idxRedun)); 
}

########################################################################
#[pMakelink_Map_SRS         Obsolete
#Aim:  Obsolete Cree le lien hypertexte SRS + ZOOM sur carte en fonction de l'id d'une ORF ou de l'id d'un contig  
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pMakelink_Map_SRS
{
    local ($id,$config,$debut,$fin) = @_; 
    my($link)="";
    my($dir_validated_bacs)=&pGetTagConfig("DIR_VALIDATED_BACS",$config);
    if ($id =~ /SMc/) # A CHANGER : specifique de rhime
    { 
	$link="$rz_mask" ;
	$link=~ s/MASK/$id/g;
	my($dir_coordonnees_file) = &pGetDirectory_AC($id,"$dir_validated_bacs/ASSEMBLE/*/$ORFDIR/$HTML_COORDONNEES_TXT");
	$link.=" <A HREF=$WWW_PROG_DESSINER_ORFS?DIR=$dir_coordonnees_file&ZOOM_ORF=$id&ORGA_CONFIG=$config TARGET=MAP> View $id</A>";
    }
    elsif ((($id =~ /BAC/)||($id =~ /SEQ/))||($id =~ /\d\d\d /))
    {   
	if ($debut>$fin)
	{
	    my($tmp)=$debut;
	    $debut=$fin;
	    $end=$tmp;
	}

	my($begin) = (($debut-10000)>=0) ? ($debut-10000) :(1) ;
	my($end) =$fin+10000 ;
	my($dir_coordonnees_file)="";
	if (($id =~ /BAC/)||($id =~ /SEQ/))
	{
		$link=&pGetTagConfig("DB_MASK2",$config);
		$link=~ s/MASK/$id/g;
		$dir_coordonnees_file = &pGetDirectory_Contig($id,$dir_validated_bacs) ;
	}
	elsif ($id =~ /\b\d{3}\b/)
	{
		$dir_coordonnees_file= &pGetDirectory_Contig($id,"$dir_validated_bacs/ASSEMBLE/") ;
	}
	$link.=" <A HREF=http://sequence.toulouse.inra.fr/$WWW_PROG_DESSINER_ORFS?DIR=$dir_coordonnees_file&ORGA_CONFIG=$config&BEGIN=$begin&END=$end&HTML=YES&ZOOM=YES TARGET=MAP> View $id</A>";
    }
    else
    {	
#	$link.=" <A HREF=http://sequence.toulouse.inra.fr/$WWW_PROG_DESSINER_ORFS?DIR=$dir_coordonnees_file&ZOOM_ORF=$id&ORGA_CONFIG=$config TARGET=MAP> View $id</A>";
	$link.=" <B>$id</B>";
    }
    return ($link);
}

########################################################################
#[pGetDirectory_AC        Obsolete
#Aim:          
#Global:$UNIXGREP
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pGetDirectory_AC
{
    local ($ORF_AC,$file_coordonnees) = @_;
    my($line) = `$UNIXGREP $ORF_AC $file_coordonnees`;
    my(@F)=split(":",$line);
    my($dir)=&pGetDir($F[0]);
    return ($dir);
}

########################################################################
#[pGetDirectory_Contig             Obsolete
#Aim:          
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pGetDirectory_Contig
{
    local ($CONTIG_AC,$dir_VALIDATED_BACS) = @_;
    my($line) = `ls -l $dir_VALIDATED_BACS | $UNIXGREP $CONTIG_AC`;
    chomp($line);
    my(@F)=();
    my($dir)="";
    ###### Pour contigs valides
    if (&pStrContainsStr($line,"->"))
    {
    	@F=split("->",$line);
	$dir=$F[1];
	$dir =~ s/ //g;
    }
    ###### Pour contigs assembles
    else
    {
    	$dir="$dir_VALIDATED_BACS/$CONTIG_AC";
    }
    return ($dir); 
}

########################################################################
#[pPrintMATCH_with_IMG            Obsolete
#Aim:          Obsolete
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pPrintMATCH_with_IMG
{
    local ($match,$prog_meth,$file,$config) = @_;
    my(@F) = split("//",$match); 
    my(@QUERY) = split(' ',$F[0]);
    my($query_deb) = $QUERY[1];
    my($query_fin) = $QUERY[2];
    
 #   @G = split;
    my($id_query) = $QUERY[0];
    $id_query =~ s/<//;
    my($link_genbank)=$gi_mask_PROT;
    if ($prog_meth =~ /blastn/i)
    {
	$link_genbank=$gi_mask_DNA;
    }
    my(@SUBJECT)=split(' ',$F[1]);
    my($id_subject)=$SUBJECT[0];
    my($subject_deb)=$SUBJECT[1];
    my($subject_fin)=$SUBJECT[2];
    my($line_DATAS)=$F[2];
    my($length_hsp)=abs($query_deb-$query_fin);
    my($identite)=($match=~ / I=(\d+)% /);
    my($com)=$id_subject;
    my($comment)=$F[3];
    if (($id_subject=~ /^g/)||($id_subject=~ /^gi_/i))
    {
	$id_subject=~ s/;//;
	$id_subject=~ s/^gi_//i;
	$id_subject=~ s/^g//i;
	@T=split("_",$id_subject);
	$id_subject=$T[0];
	$id_subject_tmp=$link_genbank;
	$id_subject_tmp=~s/MASK/$id_subject/g;
    }
    else
    {
	$id_subject_tmp=$id_subject;
    }
    if (($id_query=~ /^g/)||($id_query=~ /^gi_/i))
    {
	$id_query=~ s/^gi_;//;	
	$id_query=~ s/^gi_//i;
	$id_query=~  s/^g//i;
	$id_query_tmp=$link_genbank;
	@T=split("_",$id_query);
	$id_query=$T[0];
	$id_query_tmp=~s/MASK/$id_query/g;
    }
    else
    {
	$id_query_tmp=$id_query;
    }
    my($length_line_DATAS)=length($line_DATAS);
#    $length_line_DATAS= 20 if ($length_line_DATAS<=20);
#    $length_line_DATAS= 60 if ($length_line_DATAS>20);
    @S=split(" ",$F[2]);
    $com =~ s/### \?//g;
    if ($query_deb>$query_fin)
    {
	$img_name="REV_gi_$id_subject"."_$id_query.gif";
    }
    else
    {
	$img_name="gi_$id_subject"."_$id_query.gif";
    }
    $val = sprintf ("<LI><A HREF=$WWW_SCRIPT_AA_DNA_FILTER?&FILE=$file&BEGIN=$query_deb&END=$query_fin&IDENTITY=$identite&COM=$com&LENGTH_HSP=$length_hsp&ORGA_CONFIG=$config>$prog_meth</A>// <A HREF=http://sequence.toulouse.inra.fr/rhime/lab2/TEST_PAT/EST/MULT/$img_name>ALN</A>%20s %6d-%6d //<B>%${length_line_DATAS}s</B>// $id_subject_tmp %6d-%6d<B>  $comment</B> \n",$id_query_tmp,$query_deb,$query_fin,$line_DATAS,$subject_deb,$subject_fin);
    $val =~ s/<TH>//g;
    print $val;
}

########################################################################
#[pColorRed Obsolete
#Aim:            Color in red lines that contain key words 
#Global:
#In:
#Out:
#Update:*comment
#Prerequisite: 
#]
sub pColorRed
{
    local (*comment)=@_;
     if ( "\U$comment" =~ /RHIZOBIUM/  || "\U$comment" =~ /R.MELILOTI/)
    {
	$comment = "<Font Color=red>$comment</Font>";
    }
}


########################################################################
#[pGetLab
#Aim:          OBSOLETE
#Global:
#In: 
#Out: 
#Update:
#Prerequisite:     
#]
sub pGetLab
{ 
    local($file)=@_;
    my(@F)=split('\/',$file); 
    my($rep)="";
    for ($i=1;$i<$#F;$i++)
    {
	$_=$F[$i];
	if (/lab/g)
	{
	    $rep= $F[$i];
	}
    }
    $rep=~ tr/a-z/A-Z/;
    return $rep;
}

1;








