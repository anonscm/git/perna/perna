#!/usr/local/bin/perl

=pod 
=head1 NAME

StockNonCodeData.pl - This program extracts the RNA from the stockholm files corresponding to the NonCode database

=head1  SYNOPSIS 
./StockNonCodeData.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome --accession_number $PeRNA/external_data/Bacteria/Escherichia_coli_K12/chromosome/U00096.ac --misc_file $PeRNA/external_data/Bacteria/Escherichia_coli_K12/chromosom/RF50011.sto 

=head1 DESCRIPTION

StockNonCodeData.pl extracts RNAs from files containing RNAs from noncode bank (stockholm files)
for each rna, it creates the specific tree structure (in the directory knownRNA of the organism tree structure)

data
     <organism>
	<replicon>
           knownRNA
	      misc
		  RNA1
		  RNA2
		       RNA2.fna
		       structure
		       information
		       tmp

=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;

use Rna;

our $DATA_DIR; 
our $PCQ;
our $IDENTITY;


MAIN:
{
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s", "kingdom=s", "organism=s", "strain=s", "replicon=s", "accession_number=s", "misc_file=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$DATA_DIR   		= $rh_param->Get('DATA_DIR');
	my $CMD_REFORMAT_DB	= $rh_param->Get('CMD_REFORMATDB');
	my $CMD_WU_BLAST	= $rh_param->Get('CMD_WU_BLAST');
	my $CMD_FILTER		= $rh_param->Get('CMD_FILTER');
	$PCQ			= 97;
	$IDENTITY		= 100;

	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $accession_number_file = $rh_param->Get('accession_number');	
	my $misc_file 		= $rh_param->Get('misc_file');
	
	my $ref_a_set_rna;
	my $genome_lib;
	
	
	# open the file containing contig numbers
	open (CONTIG_FILE,"<$accession_number_file") || die "Problem to open file $accession_number_file ";
	# save all the lines in an array
	my @a_acc_nb = <CONTIG_FILE>;
	close (CONTIG_FILE);
	
	# open the file containing accession numbers which was found in the noncode files
	# $accession_number_file.NonCode
	my $non_code_ac_file_name = $accession_number_file.".NonCode";
	if ( open (AC_NONCODE_FILE,"<$non_code_ac_file_name") ) {
		# add all the lines in the array
		push(@a_acc_nb,<AC_NONCODE_FILE>);
		close(AC_NONCODE_FILE);
	} else {
		print "WARNING: no file $non_code_ac_file_name !\n";
	}
	
	$ref_a_set_rna 		= &CreateMiscRNA($misc_file, $organism, $strain, \@a_acc_nb);
	
	my $known_rna_way	= Rna::GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	my $tmp_way		= Rna::GetDirTmp($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	$genome_lib		= $tmp_way.Rna::GetNameFormatedFastaFile($organism, $strain, $replicon);
	
	#save rna data in the directory misc 
	Rna::CreateTreeStructureRNAs($ref_a_set_rna, $known_rna_way, "misc", $genome_lib, "nonCode", 
				$CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ);
}


=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:FAMILIES
$0	
		--help       	: this message
		--cfg		: the configuration file
		-kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--accession_number	: file which contains the names of the accession numbers of the strain of the organism
		--misc_file 	: the stockholm file wich contains RNA from NonCode database
		
END
}


=head2 function CreateMiscRNA

 Title        : CreateMiscRNA
 Usage        : CreateMiscRNA($misc_file, $searched_organism, $strain, $a_accession_numbers);
 Prerequisite : the input_file in parameter contains a family of miscRNA c
 Function     : Create the RNAs containing in the misc_file 
 Returns      : return a set of rna in a hashtable
 Args         : $misc_file : file containing the RNA in the stockholm format :
 					 STOCKHOLM 1.0 

					#=GF AC  RF50469
					#=GF ID  dsra 
					...
 					#=GS                n4884	OS Escherichia coli	AC U17136	DE	86 Weight: 1.00
					#=GS                n3832	OS Escherichia coli O157:H7	AC AP002559	DE	87 Weight: 1.00
					#=GS                n550	OS Shigella flexneri 2a str. 301	AC AE015217	DE	87 Weight: 1.00


					n4884               ---------AACACATCAGATTTCCTGGTGTAACGAATTTTTTAAGTGCT
					n3832               ---------AACACATCAGATTTCCTGGTGTAACGAATTTTTTAAGTGCT

					#=GC SS_cons        ............<<<<<<<.....>>>>>>>...................
		
		$searched_organism : the name of the organism of these rna
		$strain : the strain of the organism
		$a_accession_numbers : array composed of the accession numbers of the searched organism                    

 Globals      : none

=cut 


sub CreateMiscRNA
{
	my ($misc_file, $searched_organism, $strain, $a_accession_numbers) = @_;
	my %h_rna_accession;
	my %h_rfam_accession;
	my @a_rna;	# array containing the accession number and the sequence of an array
	my $family;	# the name of the miscRNA family
	my $structure = "";	# secondary structure
	my $accession_number;
	my $noncode_accession_number;
	my $ra_rnas;	# ref about an array containing rna objects
	
	# open the file
	open(MISC_FILE,"<$misc_file") or die " Impossible to open the file $misc_file";
	# file treatment
	# read the fist line and check the file has the stockholm format
	my $l = <MISC_FILE>;

	if (uc(substr($l,0,11)) eq '# STOCKHOLM') {
    	#for each line of the file
  		while (defined(my $line = <MISC_FILE>)) {
			if ( ( $line ne '\n' ) && ( $line ne '//' ) ){
				if ( substr($line, 0, 1) eq '#') {
					# if it's a line of description of a rna
					if (substr($line, 0, 4) eq "#=GS") {
						# search the accession number
						$accession_number = &GetRfamAccNumber($line);
						if( !$accession_number) {
							die "Problem in the structure of the file $misc_file !!\n";
						}
						# if the accession number belongs to the list of accession numbers
						if ( grep(/$accession_number/, @{$a_accession_numbers}) != 0 ) {
							$noncode_accession_number = &GetNonCodeAccNumber($line);
							# save the noncode accession number in the hashtable
							$h_rna_accession{$noncode_accession_number}  = "";
							$h_rfam_accession{$noncode_accession_number} = $accession_number;
						}
					} elsif ( substr($line, 0, 12) eq '#=GC SS_cons' ) {
						# save structure line
						$structure = $structure.GetStructure($line);
					} elsif (substr($line, 0, 7) eq "#=GF ID") {
						# save the rna family name 
						$family = &GetFamily($line);
					}
				} else {
					# case where it's a line of a rna
					@a_rna = split(/ +|\t+/,$line);
					if ( exists($h_rna_accession{$a_rna[0]}) ) {
						chomp($a_rna[1]);
						$h_rna_accession{$a_rna[0]} = $h_rna_accession{$a_rna[0]}.$a_rna[1];
					}
				}
			}
		}
		# create the rna objects
		$ra_rnas = &CreateRNAObjects(\%h_rna_accession, \%h_rfam_accession,$searched_organism, $strain, $family, $structure);
	}
	close(MISC_FILE);
	return ($ra_rnas);
}


=head2 function CreateRNAObjects

 Title        : CreateRNAObjects
 Usage        : CreateRNAObjects(rh_rna_accession, $rh_rfam_accession,$organism, $strain, $family, $structure);
 Prerequisite : none
 Function     : Create an array composed of rna objects
 Returns      : return a ref about an array composed of rna objects
 Args         : $rh_rna_accession : ref about an hashtab of sequences : 
 		key : noncode id
		value : the sequence		
		$rh_rfam_accession : ref about an hashtab composed of the rfam accession number
		$organism : the name of the organism of these rna
		$strain : the  strain of the organism
		$family : name of the family of the rnas
		$structure : the structure of the rna                   

 Globals      : none

=cut 
sub CreateRNAObjects
{
	my ($rh_rna_accession, $rh_rfam_accession,$organism, $strain, $family, $structure) = @_;
	
	my @a_rnas;		# array containing the objects Rna
	my $convert_seq;	# sequence in which T's are converted in U's
	my $seq;		# the sequence only
	my $seq_alignment;
	my $rna;
		 
	# creation of the rna
	foreach my $acc (keys %{$rh_rna_accession}) {
		$convert_seq = $rh_rna_accession->{$acc};
		$convert_seq =~s/T/U/g;	# T's -> U's
		($seq, $seq_alignment) 	= Rna::TreatSequence($convert_seq);
		$rna 			= Rna->New($seq,$seq_alignment,
					  	$organism, $strain, $family, $rh_rfam_accession->{$acc});
		$rna->CreateStructure2D($structure); #structure
		# save in the set on rna
		push(@a_rnas, $rna); 
	}
	return (\@a_rnas);
}


=head2 function GetRfamAccNumber

 Title        : GetRfamAccNumber
 Usage        : GetRfamAccNumber($line)
 Prerequisite : none
 Function     : extract the rfam accession number of the line 
 Returns      : the accession number
 Args         : $line : example #=GS      n4884	OS Escherichia coli	AC U17136	DE	86 Weight: 1.00
 Globals      : none

=cut 
sub GetRfamAccNumber
{
	my ($line) = @_;
	my @a_data 		= split(/[ +|\t+]AC[ +|\t+]/, $line);
	my @a_acc_number 	= split(/ +|\t+/, $a_data[1]);
	my $accession_number 	= $a_acc_number[0];
	return ($accession_number);
}


=head2 function GetNonCodeAccNumber

 Title        : GetNonCodeAccNumber
 Usage        : GetNonCodeAccNumber($line)
 Prerequisite : none
 Function     : extract the noncode accession number of the line 
 Returns      : the noncode accession number
 Args         : $line : example #=GS      n4884	OS Escherichia coli	AC U17136	DE	86 Weight: 1.00
 Globals      : none

=cut 
sub GetNonCodeAccNumber
{
	my ($line) = @_;
	my @a_data 			= split(/[ +|\t+]OS[ +|\t+]/, $line);
	my @a_acc_number 		= split(/ +|\t+/, $a_data[0]);
	my $noncode_accession_number 	= $a_acc_number[1];
	return ($noncode_accession_number);
}


=head2 function GetStructure

 Title        : GetStructure
 Usage        : GetStructure($line)
 Prerequisite : none
 Function     : extract the structure of the line
 Returns      : the structure
 Args         : $line #=GC SS_cons        ............<<<<<<<.....>>>>>>>...................
 Globals      : none

=cut 
sub GetStructure
{
	my ($line) = @_;
	# save structure line
	my @struct = split(/ +|\t+/,$line);
	chomp $struct[2];				# <<<<.<<<....<<<<...>>>>..<.<<<<....>
	return ($struct[2]);
}


=head2 function GetFamily

 Title        : GetFamily
 Usage        : GetFamily($line)
 Prerequisite : none
 Function     : extract the name of the family
 Returns      : the structure
 Args         : $line #=GF ID  dsra 
 Globals      : none

=cut 
sub GetFamily
{
	my ($line) = @_;
	my @family_name = split(/ +|\t+/,$line);
	return ($family_name[2]);
}

