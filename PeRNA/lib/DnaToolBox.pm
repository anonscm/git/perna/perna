
=pod 
=head1 NAME

ToolBox 

=head1  SYNOPSIS 

DNA.pm 

=head1 DESCRIPTION

DNA.pm is a library which contains procedures and functions about nucleotidic sequences 

=cut

use strict;
use warnings;
use Bio::SeqIO;
use Bio::Perl;
use ToolBox;



=head1 SUBROUTINES

=cut


=head2 function GetNucleotids

 Title        : GetNucleotids
 Usage        : GetNucleotids()
 Prerequisite : none
 Function     : return an array composed of the nucleotids
 Returns      : array of nucleotids
 Args         : none
 Globals      : none
=cut 

sub GetNucleotids
{
	my @a_alphabet = ('A','C','G','T');
	return (@a_alphabet);
}


=head2 function GetGenomicSequenceObject

 Title        : GetGenomicSequenceObject
 Usage        : GetGenomicSequenceObject($replicon_way)
 Prerequisite : none
 Function     : in the directory $replicon_way, search the fasta file, and return the genomic sequence object of the replicon  
 Returns      : the genomic sequence of the replicon
 Args         : $replicon_way : way of the directory of the replicon
 Globals      : none

=cut
sub GetGenomicSequenceObject
{
	my ($replicon_way) = @_;
	my $genome_file_way	= &GetFastaFileWay($replicon_way);
	my $inputstream		= Bio::SeqIO->new( -file =>$genome_file_way, 
	                           			-format => 'Fasta');				
	my $seqobj 		= $inputstream->next_seq();
	
	if ( !($seqobj)){
		die "Problem with $genome_file_way !!\n";
	}	
	return ($seqobj);
}

=head2 function GetFastaFileWay

 Title        : GetFastaFileWay
 Usage        : GetFastaFileWay($way)
 Function     : return the way of the fasta file 
 Returns      : the way of the fasta file
 Args         : $way : way of the directory where search the fasta file		
 Globals      : none

=cut

sub GetFastaFileWay
{
	my ($way) = @_;
	# search the fasta file
	my @a_file = &GetExtensionFiles($way, ".fna");
	
	if ( $#a_file < 0) {
		die "ERROR: no fasta file found in the directory $way !\n";
	} else {
		if ( $#a_file > 0 ){
			print "WARNING: there are several fasta file in the directory $way ; the first found wax return !\n";
		}
		my $file_way = $way.$a_file[0];
		return ($file_way);
	}
}

=head2 function GetRevCom

 Title        : GetRevCom
 Usage        : GetRevCom($seq)
 Prerequisite : none
 Function     : return the inverse complement sequence of the primary sequence $seq (example : "ATGGTCGGTACGT")
 Returns      : the inverse complement sequence of $seq
 Args         : $seq : a nucleotidic sequence: primary sequence or fasta sequence  
 Globals      : none

=cut

sub GetRevCom{
	my ($seq) = @_;
	my $seqrev ;
	# if it's a fasta string
	if ( $seq =~ /^\>/ ) {
		my @a_fasta_part= split(/\n/, $seq);
		$seqrev		= &reverse_complement_as_string($a_fasta_part[1]);
		$seqrev		= $a_fasta_part[0]."\n".$seqrev;
	}else {
		$seqrev = &reverse_complement_as_string($seq);
	}
	return ($seqrev);
}


=head2 function GetRNAPair

 Title        : GetRNAPair
 Usage        : GetRNAPair()
 Prerequisite : none
 Function     : return an array composed of the nucleotidic pairs in the rna structure
 Returns      : array of pairs
 Args         : none
 Globals      : none
=cut 
sub GetRNAPair
{
	my @a_pair = (
        [ "A", "U" ],
        [ "G", "U"],
        [ "G", "C"],
      );
	return (@a_pair);
}

=head2 function ArePaired

 Title        : ArePaired
 Usage        : ArePaired($nucl1, $nucl2)
 Prerequisite : none
 Function     : return 1 if nucleotids $nucl1 and $nucl2 are paired, else 0.
 Returns      : 1 if nucleotids $nucl1 and $nucl2 are paired, else 0
 Args         : $nucl1 : a nucleotid
 		$nucl2 : a nucleotid
 Globals      : none
=cut
sub ArePaired
{
	my ($nucl1, $nucl2) 	= @_;
	my @a_pair_set		= &GetRNAPair();

	# for each nucleotid pair
	for ( my $i = 0 ; $i <= $#a_pair_set ; $i++) {
	
		if (( ($a_pair_set[$i][0] eq $nucl1) && ($a_pair_set[$i][1] eq $nucl2) ) ||
		    ( ($a_pair_set[$i][1] eq $nucl1) && ($a_pair_set[$i][0] eq $nucl2) )) {	
			return 1;
		}
	}
	return 0;
}


=head2 function GetDNASeq

 Title        : GetDNASeq
 Usage        : GetDNASeq($fasta_file);
 Prerequisite : $fasta_file is a fasta file
 Function     : get a sequence in which U's are converted to T'
 Returns      : the converted sequence
 Args         : $fasta_file : way of the sequence file in which we want to convert U's to T's
 Globals      : none

=cut 
sub GetDNASeq
{
	my ($fasta_file) = @_;
	# convert U's to T's 
	my @a_convert_seq = `sed -e s/U/T/g $fasta_file`;
	if ($#a_convert_seq > -1 ){
		my $seq = $a_convert_seq[1];
		chomp($seq);
		return ($seq);
	}else{
		die "ERROR: execution of sed : $fasta_file didn't be convert !\n";
	}
}

###################################################################################################################"
#  Additional functions to compute the structure
###################################################################################################################"


=head2 function ExtractStructure

 Title        : ExtractStructure
 Usage        : ExtractStructure($align_seq, $struct)
 Prerequisite : none
 Function     : compute the numbers of paired nucleotids given by $struct in the sequence $align_seq
 		for each association of nucleotids, "(" and ")", check that the 2 nucleotids can be paired (formed A-U, G-C or G-U)
 Returns      : a reference of an array composed of the paired nucleotid numbers: 
 		Example: $struct =(((..)))..   $align_seq = AAAGAUUU.U --> return (1,8)(2,7)(3,6)
 Args         : $align_seq: a sequence in an alignment format: AAAGAUUU.U
 		$struct: a string which gives paired nucleotids: (((..)))..
 Globals      : none

=cut
sub ExtractStructure
{
	my ($align_seq, $struct) = @_;
	my $seq_char;		# current character in the sequence
	my $struct_char;	# current character in the structure
	my $nucleotid_nb = 0;	# current nucleotid number
	
	my @a_paired_pos;	# a temporary stack to save number of nucleotid to pair
	my @a_paired_nucl;	# a temporary stack to save nucleotid to pair 
	my $first_pos;		# number of the first nucleotid in a pair
	my $first_nucl;
	
	my @a_pairs;	# set of paired nucleotids 
	
	# check $align_seq and $struct have the same length
	if (length($align_seq) != length($struct)) {
		die "ERROR: $align_seq and $struct have different length. Check the data !\n";
	} else {
		my $length = length($struct);
		# for each character of the structure and the sequence
		for ( my $i = 0 ; $i < length($struct) ; $i++) {
		
			$seq_char 	= substr($align_seq, $i, 1);
			$struct_char 	= substr($struct, $i, 1);	
			
			# update the current nucleotid number: if the read character is a nucleotid
			if ( $seq_char =~ /A|U|G|C|T/ ){
				$nucleotid_nb++;
			}
			
			# if their is a pair in the structure
			if ( ($struct_char eq '<') || ($struct_char eq '(') ){
				# if the corresponding character is a nucleotid push the nucleotid nb, else push -1 
				if ($seq_char =~/A|U|G|C|T/) {
					push (@a_paired_pos, $nucleotid_nb);
					push (@a_paired_nucl, $seq_char);
				} else {
					push (@a_paired_pos, -1);
					push (@a_paired_nucl, "");
				}
				
			}elsif ( ($struct_char eq '>') || ($struct_char eq ')') ) {	# end of a pair
				$first_pos 	= pop(@a_paired_pos);
				$first_nucl 	= pop(@a_paired_nucl);
				
				if ( ($first_pos != -1) &&  ($seq_char=~ /A|U|G|C|T/) ) {
					# check the two nucleotids can be paired
					if ( &ArePaired($first_nucl, $seq_char) == 1) {
						push(@a_pairs, [($first_pos, $nucleotid_nb)] );
					}
				}
			}
		}
	}
	return (\@a_pairs);
}


=head2 function StructureToString

 Title        : StructureToString
 Usage        : StructureToString($ra_struct)
 Prerequisite : none
 Function     : return a string containing the structure of the rna
 		with this form, if nucleotids 1 and 12, 2 and 11, are paired:
			1 12
			2 11	
 Returns      : a string containing the structure of the rna
 Args         : $ra_struct : a reference of an array composed of the paired nucleotid numbers ((1,12)(2,11))
 Globals      : none

=cut
sub StructureToString
{
	my ($ra_struct) = @_;
	my $ch = "";
	
	for ( my $i = 0 ; $i < $#{$ra_struct}+1 ; $i++ ) {
		$ch = $ch.$ra_struct->[$i][0]." ".$ra_struct->[$i][1]."\n";
	}
	return $ch;	
}

=head2 function ExtractSeqWithoutN

 Title        : ExtractSeqWithoutN
 Usage        : ExtractSeqWithoutN($seq)
 Prerequisite : none
 Function     : return the sequence without N at the begin and at the end
 Returns      : the sequence without N at the begin and at the end 
 Args         : $seq : nucleotidic sequence
 Globals      : none

=cut
sub  ExtractSeqWithoutN
{
	my ($seq) = @_;
	$seq=~ s/^N*|N*$//g;
	return ($seq);
}

###################################################################################################################"
#  Additional functions not used
###################################################################################################################"

=head2 function ConvertDNASeq

 Title        : ConvertDNASeq
 Usage        : ConvertDNASeq($fasta_file, $fasta_file_dir, $output_dir);
 Prerequisite : none
 Function     : create a new fasta file $fasta_file.dna, in the directory $output_dir, in which U's are converted to T'
 Returns      : the way of the created fasta file 
 Args         : $fasta_file : name of the fasta file
 		$fasta_file_dir : directory of the fasta_file
 		$output_dir : directory where create the file composed of dna nucleotids
 Globals      : none

=cut 
sub ConvertDNASeq
{
	my ($fasta_file, $fasta_file_dir, $output_dir) = @_;
	# way of the created fasta file
	my $output_file_way = $output_dir.$fasta_file.".dna";
	
	# convert U's to T's in the file fasta_file.dna in the directory output_dir
	my $cmd = "sed -e s/U/T/g $fasta_file_dir$fasta_file > $output_file_way";
	my $res = system($cmd);
	
	if ($res == 0){
		return ($output_file_way);
	}else{
		die "ERROR: execution of sed : $fasta_file_dir$fasta_file didn't be convert in the file $output_file_way !\n";
	}
}	

1
