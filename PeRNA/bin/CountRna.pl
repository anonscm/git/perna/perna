#!/usr/local/bin/perl



=pod 
=head1 NAME

CountRna - This program counts the number of rna in the directory

=head1  SYNOPSIS 

./CountRna.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome

=head1 DESCRIPTION

CountRna.pl counts the number of rna in the directory of the replicon of the organism


=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;
use ToolBox;
use ParamParser;
use Logger;

our $DATA_DIR; 

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$DATA_DIR   = $rh_param->Get('DATA_DIR');
	
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');

	# compute the number of rnas and create a file which contains the number
	my $known_rna_way = &GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	print "RNA_NUMBER ".&CountRna($known_rna_way)."\n";
}


=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:FAMILIES
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon	
END
}



=head2 function CountRna

 Title        : CountRna
 Usage        : CountRna($way)
 Prerequisite : none
 Function	  : compute the number of rna in the directory $way
 				the directory has to be composed of directories ; one directory for one rna family
				knownRNA
					-- tRNA
					-- ssuRNA
					-- lsuRNA
					-- misc
				for each family, the script finds the number of this rna
 Returns      : the number of rnas
 Args         : $way : the directory in which are the rnas 
 Globals      : none

=cut 
sub CountRna
{
	my ($way) = @_;
	my $nb_rna = 0;	# the number of rna in th directory
	my $dir_number;
	my $dir_name ;

	opendir(DIR, $way ) or die "Can't open the directory $way !!!\n";
	
	# for each directory 
	while ( defined(my $dir_name = readdir DIR) ) {
		next if $dir_name =~ /^\.\.?/; # don't consider the dir . and ..
		next if !(-d $way.$dir_name); # don't consider the files 
		chdir($way.$dir_name."/");
		# calculate the number of rna in the current directory 
		$dir_number = `ls -l |wc -l`;
		$dir_number = $dir_number-1;

		# add the number of rna 
		$nb_rna = $nb_rna + $dir_number;
	}
	closedir(DIR);
	return $nb_rna;
}



