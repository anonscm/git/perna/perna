# $Id: Logger.pm,v 1.1.1.1 2007-07-19 08:25:30 cros Exp $
#

package Logger;

=pod

=head1 NAME

 Logger - An object to write some traces to a log file in a structured way

=head1 THE TIME STAMPS

This object generates a time stamp computed to be easily manageable (with sort and other unix commands).
Because of the last number (iteration), you are (nearly) sure that the generated stamp is UNIQUE to your program.

#04#11#09#13#16#27#01# => 9 nov 2004, 13h 16mn 27s iteration 01. I<(N.B. The nb of iterations is limited to 100)>

=head1 THE TIMER STAMPS

The timer stamps represents an duration: it is thus different from a time stamp, but the format is nearly the same:

#00#00#00#13#16#27#36# => the elapsed time was 13hours, 16 minutes, 27.36 seconds
If the module Time::HiRes is not available (not installed on system), the timer resolution will be 1s and the last 
number will be always 00

=head1 PUBLIC METHODS

=over 4

=item B<New> my $log = New Logger ($logfile,$log_level,$status);

=item B<CalcStamp> my $stamp = $log->CalcStamp()

=item B<Trace> $log->Trace($msg,$stamp,$log_type);

=item B<SetLogFile> $log->SetLogFile($logfile)

=item B<StartTimer> $log->StartTimer("timer")

=item B<StopTimer> my $elapsed = $log->StopTimer("timer")

=item B<CalcTimerStamp()> my $timer_stamp = $log->CalcStamp("timer")

=back

=head1 METHODS AND SUBROUTINES - DETAILED DESCRIPTION

=cut

use strict;
use warnings;
use Carp;
use IO::File;
our $HIRES = 1;
our $Time;

eval "use Time::HiRes";

if ($@ ne "")
{    # Something wrong in eval (may be Time::HiRes not available)
    $HIRES = 0;                         # Mark the module as Not Loaded
    $Time  = \&Logger::general_time;    # The time function is the default perl time function
}
else
{
    $Time = \&Time::HiRes::time;        # The Time function is given by Time::Hires
}

BEGIN
{
    our $VERSION = do {my @r = (q$Rev: 7 $ =~ /\d+/g); $r[0]};
}

=pod

=head2    New 

 Usage:     my $log = New Logger ($logfile,$log_level,$status);
 Procedure: Initialize an object, opening the logfile specified.
 Return:    the created object
 Args:      $logfile, the log file name
               "file.log" opens in write mode
               ">file.log" opens in write mode, too
               ">>file.log" opens in append mode
            $log_level = a string representing a log level (cf. Trace)
            $status = 'COMPRESSION'   -> gzip the logfile
		      'NOCOMPRESSION' -> normal (default status)

=cut

sub New
{
    my ($class, $logfile, $log_level, $status) = @_;
    my $self = bless {}, ref($class) || $class;

    confess("Please specify a logfile")   if (!defined($logfile)   or $logfile   eq "");
    confess("Please specify a log level") if (!defined($log_level) or $log_level eq "");

    if ($logfile !~ /\A>/)
    {
        $logfile = '>' . $logfile;
    }

    $status = 'NOCOMPRESSION' unless (defined $status);
    my $compression = ($status =~ /\ACOMP/) ? 1 : 0;

    my $logfh;
    if ($compression == 0)
    {
        $logfh = new IO::File($logfile) or die "Cannot open file $logfile for writing";
    }
    else
    {
        $logfile .= '.gz';
        $logfh = new IO::File("|gzip $logfile");
    }

    my $timer_resolution = ($HIRES == 1) ? "1e-2 s" : "1 s";
    $self->{__logfile}     = $logfile;
    $self->{__logfh}       = $logfh;
    $self->{__log_level}   = $log_level;
    $self->{__compression} = $compression;
    $self->Trace("STARTING LOGGING - LEVEL $log_level - $status - Timer resolution $timer_resolution\n", '*', '.');

    return $self;
}

=pod

=head2 SetLogFile 

 Usage: my $log->SetLogFile($logfile);
 Function: Close the current logfile and open a new file
 Return:   nothing
 Args:     $logfile, the new log file name
           "file.log" opens in write mode
           ">file.log" opens in write mode, too
           ">>file.log" opens in append mode

=cut

sub SetLogFile
{
    my $self        = shift;
    my $new_logfile = shift;
    $self->Trace("FINISHED LOGGING TO THIS FILE, NEXT LOG FILE IS $new_logfile\n", '*', '.');
    if ($new_logfile !~ /\A>/)
    {
        $new_logfile = '>' . $new_logfile;
    }
    my $logfh;
    my $compression = $$self{"__compression"};
    if ($compression == 0)
    {
        $logfh = new IO::File($new_logfile) or die "Cannot open file $new_logfile for writing";
    }
    else
    {
        $new_logfile .= '.gz';
        $logfh = new IO::File("|gzip $new_logfile");
    }
    $$self{"__logfile"} = $new_logfile;
    $$self{"__logfh"}   = $logfh;

    my $msg = "RESUMING LOGGING - LEVEL " . $self->{"__log_level"} . " - ";
    $msg .= ($compression == 1) ? 'COMPRESSION' : 'NOCOMPRESSION' . "\n";
    $self->Trace($msg, '*', '.');
}

=pod

=head2 StartTimer 
 
 Title: StartTimer
 Usage: $log->StartTimer('timer')
 Function: reset and start the timer called 'timer'
           Call gmtime() and keep the integer returned in a private member.
 Args:     The timer name
 On error: No error

=cut

sub StartTimer
{
    my $self  = shift;
    my $timer = shift;
    $self->{$timer} = &$Logger::Time() + 0.0;
}

=pod

=head2 StopTimer 

 Usage: $log->StopTimer('timer')
 Function: "stop" the timer called 'timer'
           Call gmtime() and returns the alapsed time since the number sotred by StartTimer
 Args:     The timer name
 Return:   The elapsed time
 On error: No error

=cut

sub StopTimer
{
    my $self  = shift;
    my $timer = shift;
    if (defined $self->{$timer})
    {
        my $now  = &$Logger::Time() + 0.0;
        my $then = $self->{$timer};
        return $now - $then;
    }
    else
    {
        croak "The timer $timer was NOT started";
    }
}

=pod

=head2 CalcTimerStamp 

 Usage: $log->CalcStamp("timer")
 Function: compute a timer stamp 
 Return:   the stamp
 Args:     The timer name
 On error: croak a message

=cut

sub CalcTimerStamp
{
    my $self    = shift;
    my $timer   = shift;
    my $elapsed = $self->StopTimer($timer) + 0.0;
    my $csec    = int(100 * ($elapsed - int($elapsed)));
    my $sec     = $elapsed % 60;
    $elapsed /= 60;
    my $min = $elapsed % 60;
    $elapsed /= 60;
    my $hour = $elapsed % 24;
    $elapsed /= 24;
    my $day = $elapsed;
    my $timer_stamp = sprintf("#00#00#%02i#%02i#%02i#%02i#%02i#", $day, $hour, $min, $sec, $csec);
    return $timer_stamp;
}

=pod
 
=head2 CalcStamp 

 Usage:    $log->CalcStamp()
 Function: compute an unique stamp
 Return:   the stamp
 Args:     none
 static:  stamp_old_time, stamp_seq

=cut

our $STAMP_OLD_TIME;
our $STAMP_SEQ;
sub BEGIN {$STAMP_OLD_TIME = ""; $STAMP_SEQ = 0;}

sub CalcStamp
{
    my $self = shift;
    my ($sec, $min, $hour, $mday, $mon, $year) = gmtime();
    $year = (1900 + $year) % 2000;    # We'll have to modify this before 2100
    $mon++;
    my $stamp_time = sprintf("#%02i#%02i#%02i#%02i#%02i#%02i#", $year, $mon, $mday, $hour, $min, $sec);
    if ($stamp_time eq $STAMP_OLD_TIME)
    {
        $STAMP_SEQ++;
    }
    else
    {
        $STAMP_SEQ      = 0;
        $STAMP_OLD_TIME = $stamp_time;
    }
    return sprintf("%s%02i#", $stamp_time, $STAMP_SEQ);
}

=pod

=head2 Trace 

 Usage: $log->Trace($msg,$stamp,$log_type)
 Function: write $msg to LOG only if $log_type matches with __log_level
           If $stamp is "", only $msg is printed
           If $stamp is ".", print a standard stamp
           If $stamp is "*", call CalcStamp
           If $stamp is a name but not a stamp ("timer"), call CalcTimerStamp("timer")
           If $stamp is a correct stamp (#\d\d# etc.), print this stamp
           The output is formatted as follows:
                 $stamp $log_type $msg (space is the separator)
           NO \n is written, so you must provide \n in $msg if needed. 
		   You may thus trace a line in two steps:
           $Trace("first part","#.....#","D");
           ...
           $Trace("second part\n","","D");
           $log_type is a regex, a match is done as follows:
           if ($log_level =~ /$log_type/) {
               display the line
           }
           TIPS ==> If you supply '.', the line is ALWAYS printed.

 Return:   none
 Args:     $msg      = the message to trace
           $stamp    = the stamp to print
           $log_type = type of log, compare with __log_level

=cut

sub Trace
{
    my $self      = shift;
    my $msg       = shift;
    my $stamp     = shift;
    my $log_type  = shift;
    my $log_level = $$self{"__log_level"};
    my $logfh     = $$self{"__logfh"};
    if ($log_level =~ /$log_type/)
    {

        if ($stamp eq "")
        {
            print $logfh $msg;
        }
        elsif ($stamp eq '.')
        {
            $stamp = '#' . ('.' x 20) . '#';
            print $logfh "$stamp" . ' _' . $log_type . '_ ' . $msg;
        }
        elsif ($stamp eq '*')
        {
            $stamp = $self->CalcStamp();
            print $logfh "$stamp" . ' _' . $log_type . '_ ' . $msg;
        }
        elsif ($stamp =~ /#\d\d#\d\d#\d\d#\d\d#\d\d#\d\d#\d\d#/)
        {
            print $logfh "$stamp" . ' _' . $log_type . '_ ' . $msg;
        }
        else
        {
            $stamp = $self->CalcTimerStamp($stamp);
            print $logfh "$stamp" . ' _' . $log_type . '_ ' . $msg;
        }
    }
}

sub general_time
{
    return time();
}

1;

