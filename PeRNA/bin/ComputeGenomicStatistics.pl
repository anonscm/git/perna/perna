#!/usr/local/bin/perl


=pod 
=head1 NAME

ComputeGenomicStatistics.pl - This program computes characteristics about the sequence of the replicon of the organism
it computes the k-nucleotid numbers of the complete sequence and complete the PeRNA bank with files

=head1  SYNOPSIS 

./ComputeGenomicStatistics.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome --word_max_length 2

=head1 DESCRIPTION

ComputeGenomicStatistics.pl computes characteristics about the sequence of the replicon of the organism (specific strain) :
the number of each k-nucleotid : 1 <= k <= word_max_length
if k=2, compute the number of A, C, G and T, AA, AC, AG, ...
it creates directories global_statistics, complet_genome_stat, and the files : one for each k-nucleotid
PeRNA bank is completed :

<replicon>
	global_statistics
		complet_genome_stat
			A-seq
			C-seq
			G-seq
			T-seq
			AA-seq
			AC-seq...
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use ToolBox;
use Stat;

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help:s", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s", "word_max_length=i"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain', 'replicon', 'word_max_length'));
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');

	# check that max_length is an integer
	$rh_param->AssertInteger('word_max_length');
	
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $word_max_length 	= $rh_param->Get('word_max_length');
	my $DATA_DIR   		= $rh_param->Get('DATA_DIR');

	# create directories which will contains the statistics data : global_statistics and complet_genome_stat
	my $genomic_stat_dir_name	= $rh_param->Get('GENOME_STAT_DIR_NAME'); 
	my $glob_stat_dir_name		= $rh_param->Get('GLOBAL_STAT_DIR_NAME'); 
	
	my $replicon_way 		= &GetRepliconDir($DATA_DIR, $kingdom, $organism, $strain,$replicon);
	my $way_genomic_statistics 	= $replicon_way.$glob_stat_dir_name."/".$genomic_stat_dir_name."/";
	&CreateStatDirectories($replicon_way, $glob_stat_dir_name, $genomic_stat_dir_name);
	
	# search the fasta file of the replicon
	my $genome_file 		= &GetFastaFileWay($replicon_way);
	# compute characteristics and create files 
	&CreateAllNucleotidFiles($genome_file, "seq", $way_genomic_statistics, $word_max_length);
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--word_max_length : max length of k-nucleotids to count ( 1 <= k <= word_max_length)
END
}


=head2 procedure CreateStatDirectories

 Title        : CreateStatDirectories
 Usage        : CreateStatDirectories($replicon_way, $glob_stat_dir_name, $genomic_stat_dir_name)
 Prerequisite : none
 Function     : create directories $glob_stat_dir_name in the directory $replicon_way and $genomic_stat_dir_name in $glob_stat_dir_name
 Returns      : none
 Args         : $replicon_way : way of the directory of the replicon
 		$glob_stat_dir_name : name of the global statistics directory
		$$genomic_stat_dir_name : name of the directory of the characteristics about the complete genomic sequence
 Globals      : none
 
=cut

sub CreateStatDirectories
{
	my ($replicon_way, $glob_stat_dir_name, $genomic_stat_dir_name) = @_;
	
	# creation of the directory global_statistics
	chdir($replicon_way);
	if ( !(&ExistDirectory($glob_stat_dir_name, $replicon_way)) ) {
		&CreateDirectory($glob_stat_dir_name, $replicon_way);
	}
	# creation of the directory complet_genome_stat
	my $glob_stat_way = $replicon_way.$glob_stat_dir_name."/";
	if ( !(&ExistDirectory($genomic_stat_dir_name, $glob_stat_way)) ) {
		&CreateDirectory($genomic_stat_dir_name, $glob_stat_way);
	}
}

