#!/usr/local/bin/perl

=pod 
=head1 NAME

CreateAccessionFile.pl - This program creates a file which contains all the accession numbers of one organism :


=head1  SYNOPSIS 


`pwd`/CreateAccessionFile.pl --cfg /home/esallet/prediction/work/cfg/conf.cfg  --accession_number U00096 --kingdom "Bacteria" --organism "Escherichia coli" --strain K12 --replicon "Chromosom1" 
./CreateAccessionFile.pl --cfg $PeRNA/cfg/conf.cfg  --accession_number U00096 --kingdom "Bacteria" --organism "Escherichia coli" --strain K12 --replicon "chromosom" 
			    

=head1 DESCRIPTION

CreateAccessionFile.pl creates a file which contains all the accession numbers of one organism :
the accession numbers of this replicon for example.
The script searches in the embl contig database (via the file Con.dat) all the accession numbers of the primary accession number in parameter
If there is no contig accession number, the script searches on the EMBL Release database the possible accession numbers. 
it extracts the numbers in the entry "http://srs.sanger.ac.uk/srsbin/cgi-bin/wgetz?-noSession+-e+[EMBLRELEASE-ACC:$accession_number]"
It creates in the directory $PeRNA/data/<kingdom>/<Organism>[_<strain>]/<replicon> the file "accession_number.ac" : 
one line = one accession_number

=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;

use ToolBox;

our $CON_DAT;
our $EXTERNAL_DATA_DIR;



MAIN:
{
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s", "accession_number=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg', 'kingdom', 'organism', 'strain', 'replicon', 'accession_number')); 
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	my $accession_number = $rh_param->Get('accession_number');
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my @a_acc_numbers;	# array which will contain all the accession numbers
	
	$EXTERNAL_DATA_DIR	= $rh_param->Get('EXTERNAL_DATA_DIR');
	$CON_DAT 		= $rh_param->Get('CON_DAT');
	
	# search the accession numbers in the CON_DAT : file which contains the contig accession numbers
	@a_acc_numbers = &GetEMBLConNumbers($accession_number);
	# if we didn't find accession numbers in the emblcon bank, we look for the accession number in the embl entry
	if ($#a_acc_numbers < 0 ){
		@a_acc_numbers = &GetEMBLReleaseNumbers($accession_number);
	}
	if ($#a_acc_numbers < 0) {
		print "WARNING : we didn't find others accession numbers for the accession number $accession_number\n";
	}
	
	my $way = &GetRepliconDir($EXTERNAL_DATA_DIR, $kingdom,$organism, $strain,$replicon);
	my $name_file = $accession_number.".ac";
	# create the file for the organism
	&CreateFileAccNumbers($accession_number,\@a_acc_numbers, $name_file, $way);
	

}


=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:FAMILIES
$0	
		--help       	: this message
		--cfg		: the configuration file
		--accession_number   : the primary accession number
		--kingdom	: kingdom of the organism
		--organism	: name of the organism
		--strain	: strain of the organism
		--replicon	: replicon of the organism
END
}


=head2 function GetEMBLConNumbers

 Title        : GetEMBLConNumbers
 Usage        : GetEMBLConNumbers($accession_number)
 Prerequisite : none
 Function     : return the accession numbers found in the contig file from emblcon bank
 Returns      : array which contains the accession number
 Args         : $accession_number : the primary accession number for which we looks for its contigs accession numbers
 
 Globals      : none

=cut 

sub GetEMBLConNumbers
{
	my ($accession_number) = @_;
	my $find_acc	= 0;
	my $stop	= 0;
	my @acc_numbers;
	
	open(CON_FILE,"<$CON_DAT") or die " Impossible to open the file $CON_DAT";
	
	# while we don't find the accession_number
	while ( (defined(my $line = <CON_FILE>)) && ($stop == 0 )) {
		# line of the accession number
		if ($line =~ /^AC[\t| ]+$accession_number.*/ ){
			$find_acc = 1;
		}elsif ( ($find_acc == 1) && ($line =~/^CO.*/ ) ) {
			# contig from the accession number
			push(@acc_numbers ,&GetAccNumbers($line));
		}elsif ( ($find_acc == 1) && ($line =~/^\/\/$/)) {
			# we found the data ; stop the searching
			$stop = 1;
		}
	}
	close(CON_FILE);
	return @acc_numbers;
}


=head2 function GetAccNumbers

 Title        : GetAccNumbers
 Usage        : GetAccNumbers($line)
 Prerequisite : none
 Function: return an array of the accession numbers
 Returns      : an array of the accession numbers
 Args         : $line : $line compodes of different accession numbers
 Globals      : none

=cut 
sub GetAccNumbers
{
	my ($line) = @_;
	my @a_acc;	# array to split the data
	my @a_acc_numbers;	# array of found accession numbers

	# delete the special caracters like join, (, ), CO ans spaces
	$line =~s/join| |CO|\(|\)//gm;
	
	my @a_data = split(/,/, $line);
	for ( my $i = 0 ; $i <= $#a_data ; $i++) {
		@a_acc = split(/\./, $a_data[$i]);
		chomp($a_acc[0]);
		push(@a_acc_numbers, $a_acc[0]);
	}
	return @a_acc_numbers;
}




=head2 function GetEMBLReleaseNumbers

 Title        : GetEMBLReleaseNumbers
 Usage        : GetEMBLReleaseNumbers($accession_number)
 Prerequisite : none
 Function     : return the accession numbers found in the  emblrelease bank
 Returns      : array containing the accession numbers found in the  emblrelease bank
 Args         : $accession_number : the primary accession number for which we looks for its secondary accession numbers
 
 Globals      : none

=cut 
sub GetEMBLReleaseNumbers
{
	my ($accession_number) = @_;
	my @a_acc_numbers;
	
	my $url		= "http://srs.sanger.ac.uk/srsbin/cgi-bin/wgetz?-noSession+-e+[EMBLRELEASE-ACC:$accession_number]";
	my $agent	= LWP::UserAgent->new;
	my $request	= HTTP::Request->new(GET => $url);
	my $response = $agent->request($request);
	if ($response->is_success) {
		my $content = $response->content;
		my $ch = ">Accession #</TD>";
		my @res = split(/$ch/, $content);
		@res    = split(/\<|\>/, $res[1]);
		# save the accession numbers
		@a_acc_numbers = split(/; /, $res[2]);
	}else {
		print "WARNING : we don't find accession number for the primary accession number $accession_number in the EMBLRELEASE bank\n";
	}
	return @a_acc_numbers;
}


=head2 procedure CreateFileAccNumbers

 Title        : CreateFileAccNumbers
 Usage        : CreateFileAccNumbers($acc_number, $a_acc_numbers, $name_file, $way);
 Prerequisite : none
 Function     : create a file containing the accession numbers,  in the directory $way
 Returns      : none
 Args         : $acc_number : the primary accession number
 				$a_acc_numbers : array containing the accession numbers
 				$name_file : name of the file that we want to create
 				$way : way where to create the file
 Globals      : none
=cut 

sub CreateFileAccNumbers
{
	my ($acc_number, $a_acc_numbers, $name_file, $way) = @_;
	# file which will contain the accession numbers and the corresponding organism
	my $out = new IO::File(">".$way.$name_file);
	
	# write the primary accession number in the file
	print $out $acc_number."\n";
	
	# each accession number is written in the file
	for ( my $i= 0 ; $i <= $#$a_acc_numbers ; $i++) {
		print $out $a_acc_numbers->[$i]."\n";
	}
}
