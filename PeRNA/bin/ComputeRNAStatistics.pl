#!/usr/local/bin/perl


=pod 
=head1 NAME

ComputeRNAStatistics.pl - This program computes characteristics about each rna of the replicon of the organism 

=head1  SYNOPSIS 

./ComputeRNAStatistics.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome --word_max_length 2 
=head1 DESCRIPTION

ComputeRNAStatistics.pl computes characteristics about each rna of the replicon
for each rna, it creates a directory RNA_stat in PeRNA, and files containing:
the k-nucleotid numbers (1 <= k <= word_max_length) in the sequence
the k-nucleotid numbers in the structure (paired parts)
the number of paired/unpaired nucleotids in the sequence
the number of each type of pair (A-U, U-A,G-C,C-G, U-G, G-U)
In PeRNA bank:
<replicon>
	known_RNA
	    <family>{1..m}
		RNA{1..n}
			RNA_stat	
				A_seq
				C_seq...
				AA_seq...
				A_struct
				C_struct...
				GC_pair...
				PairInformation
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use ToolBox;
use DnaToolBox;
use Stat;

our $RNA_STAT_DIR_NAME;
our $PAIR_INFO_FILE_NAME;
our $MULTI_FASTA_FILE_NAME;

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help:s", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s", "word_max_length=i"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain', 'replicon', 'word_max_length'));
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');

	# check that word_max_length is an integer
	$rh_param->AssertInteger('word_max_length');
	
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $word_max_length 	= $rh_param->Get('word_max_length');
	my $DATA_DIR   		= $rh_param->Get('DATA_DIR');
	$RNA_STAT_DIR_NAME	= $rh_param->Get('RNA_STAT_DIR_NAME');
	$PAIR_INFO_FILE_NAME	= $rh_param->Get('PAIR_INFO_FILE_NAME');
	$MULTI_FASTA_FILE_NAME	= $rh_param->Get('MULTI_FASTA_FILE_NAME');
	my $known_rna_dir 	= &GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	
	my $family_way;	# way of the directory of a rna family
	my $rna_way;	# way of the directory of a rna

	opendir(DIR, $known_rna_dir) or die "Can't open the directory $known_rna_dir !\n";
	
	# for each rna family
	while ( defined(my $family_dir = readdir DIR) ) {
		next if $family_dir =~ /^\.\.?/;		# don't compute the directories . and ..
		next if (-f $known_rna_dir.$family_dir);	# don't compute the files
	
		$family_way = $known_rna_dir.$family_dir."/";
		opendir(FAMILY_DIR, $family_way);
		
		# for each directory of a rna of the family 
		while ( defined(my $rna_dir = readdir FAMILY_DIR) ) {
			next if $rna_dir =~ /^\.\.?/; # don't treate the directories . and ..
			next if (-f $family_way.$rna_dir);	# don't compute the files
			$rna_way = $family_way.$rna_dir."/";
			# compute statistics about the rna
			&ComputeAnRNAStat($rna_way, $word_max_length);	
		}
		closedir(FAMILY_DIR);
	}
	closedir(DIR);
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--word_max_length : max length of k-nucleotids to count ( 1 <= k <= word_max_length)
END
}

=head2 procedure ComputeAnRNAStat


 Title        : ComputeAnRNAStat
 Usage        : ComputeAnRNAStat($rna_way,$max_length)
 Prerequisite : none
 Function     : compute characteristics about a rna: create the directory RNA_stat and create files about the characteristics 
 		(k-nucleotid number for the sequence, 
 		k-nucleotid number for the structure, paired base number, pair number)
 Returns      : none
 Args         : $rna_way : directory of the rna
 		$max_length : max length of the k-nucleotids to count
 Globals      : none

=cut
sub ComputeAnRNAStat
{
	my ($rna_way,$max_length) = @_;
	
	# create a directory for the rna statistics
	if ( !( &ExistDirectory($RNA_STAT_DIR_NAME, $rna_way) )) {
		&CreateDirectory($RNA_STAT_DIR_NAME, $rna_way);
	}
	my $rna_stat_dir	= &GetRNAStatDir($rna_way);	# directory of the statistic files
	my $tmp_way 		= &GetRNATmpDir($rna_way);	# directory for temporary data
	my $rna_file_way 	= &GetFastaFileWay($rna_way);	# way of the fasta of the rna
	
	# search the file "structure" of the rna
	my $struct_file_name = &GetStructureFileName();
	my @a_structure_file = &GetExtensionFiles($rna_way, $struct_file_name);
	if ($#a_structure_file < 0) {
		die "ERROR: there is no structure file in the directory $rna_way";
	}
	my $structure_file_way	= $rna_way.$a_structure_file[0];
	
	# compute the sequence in which U's are replaced by T'
	my $dna_seq 		= &GetDNASeq($rna_file_way);
	# compute the sequence of the rna in which N at the begin and at the end are deleted 
	my $seq_without_N 	= &ExtractSeqWithoutN($dna_seq);
	my $rna_length 		= length($seq_without_N);
	
	# compute the number of k-nucleotids in the sequence
	&CreateAllNucleotidFiles($seq_without_N, "seq", $rna_stat_dir, $max_length);
	
	# compute the number of paired nucleotids
	&CreateRNAPairNbFile($structure_file_way, $rna_length, $rna_stat_dir);

	# compute the number of k-nucleotids for the paired nucleotids of the sequence
	&CreateRNAKNuclStructFiles($structure_file_way, $dna_seq, $tmp_way, $rna_stat_dir, $max_length);
		
	# compute the pairs (with the rna fasta file)
	&CreateRNAPairFiles($structure_file_way, $rna_file_way, $rna_stat_dir);
}


=head2 procedure CreateRNAPairNbFile

 Title        : CreateRNAPairNbFile
 Usage        : CreateRNAPairNbFile($structure_file_way, $rna_length, $output_dir) 
 Prerequisite : none
 Function     : Create a file which contains the number of paired nucleotids and of unpaired nucleotids in the directory $output_dir
 Returns      : none. 
 Args         : $structure_file_way : way of the structure file
 		$rna_length : length of the part of rna which is computed (without N)
 		$output_dir : directory where create the files
 Globals      : none

=cut
sub CreateRNAPairNbFile
{
	my ($structure_file_way, $rna_length, $output_dir) = @_;
	my $nb_paired_nucleotid 	= &ComputePairedNuclNumber($structure_file_way);
	my $nb_unpaired_nucleotid	= $rna_length - $nb_paired_nucleotid;	
	my $data 			= $nb_paired_nucleotid." ".$nb_unpaired_nucleotid."\n";
	&CreateFile($PAIR_INFO_FILE_NAME, $output_dir, $data);		
}


=head2 procedure CreateRNAKNuclStructFiles

 Title        : CreateRNAKNuclStructFiles
 Usage        : CreateRNAKNuclStructFiles($struct_file_way, $seq, $tmp_dir, $output_dir, $max_length) 
 Prerequisite : none
 Function     : Create a multifasta file of the paired subsequences and 
		Compute the number of k-nucleotids (1<= k<= $max_length) 
		Resulting files are created in the directory $output_dir
 Returns      : none. 
 Args         : $struct_file_way : way of the structure file
 		$seq : sequence (composed of A, T, G, C) of the rna
 		$tmp_dir : directory where create the temporary multifasta file
 		$output_dir : directory where create the resulting files
		$max_length : max length of the k-nucleotids to count
 Globals      : none

=cut
sub CreateRNAKNuclStructFiles
{
	my ($struct_file_way, $seq, $tmp_dir, $output_dir, $max_length) = @_;
	# extract the regions of the sequences which are paired
	my $regions = &ExtractStructureRegions($struct_file_way);
	
	# if no region to extract	
	if( $regions eq "") {
		my @a_knucl;
		my @a_alphabet = ('A','C','G','T');
		my $file_name;
		for (my $length = 1 ; $length <= $max_length ; $length++ ) {
			push(@a_knucl, &GetCombination($length,\@a_alphabet));
			push(@a_knucl, "Other$length");
		}
			
		# save 0 for the number of each k-nucleotid in the structure
		for (my $i = 0 ; $i <= $#a_knucl ; $i++) {
			$file_name = &GetKNucleotidFileName($a_knucl[$i], "struct");
			# creation of the file for the k-nucleotid
			&CreateFile($file_name, $output_dir, "0\n");
		}
	} else {	
		# compute a multifasta file composed of the extracted regions
		&CreateMultiFastaFile($seq, $regions, $MULTI_FASTA_FILE_NAME, $tmp_dir);
		my $multifasta_way = $tmp_dir.$MULTI_FASTA_FILE_NAME;
		
		# compute k-nucleotid numbers about these regions
		&CreateAllNucleotidFiles($multifasta_way, "struct", $output_dir, $max_length);
		# delete the multifasta ????????
	}
}


=head2 procedure CreateRNAPairFiles

 Title        : CreateRNAPairFiles
 Usage        : CreateRNAPairFiles($structure_file_way, $seq, $output_dir)
 Prerequisite : none
 Function     : Create files which contains the number of each pair in the directory $output_dir
 Returns      : none. 
 Args         : $structure_file_way : way of the structure file
 		$seq : way of the fasta file of the rna
 		$output_dir : directory where create the files
 Globals      : none

=cut
sub CreateRNAPairFiles {
	my ($structure_file_way, $seq, $output_dir) = @_;
	my $file_name;
	
	# compute the pair number
	my $rh_pair = &ComputePair($structure_file_way, $seq);
	# for each pairs, create the corresponding file
	foreach my $pair (keys(%{$rh_pair})) {
   		$file_name = &GetKNucleotidFileName($pair, "pair");
		&CreateFile($file_name, $output_dir, $rh_pair->{$pair}."\n");
	}
}


=head2 function GetRNAStatDir

 Title        : GetRNAStatDir
 Usage        : GetRNAStatDir($rna_way)
 Function     : return the way of the directory which contains rna characteristics
 Returns      : the way of the directory which contains rna characteristics
 Args         : $rna_way : way of the directory of a RNA		
 Globals      : none

=cut
sub GetRNAStatDir
{
	my ($rna_way) = @_;
	my $rna_stat_dir = $rna_way.$RNA_STAT_DIR_NAME."/";
	return ($rna_stat_dir);
}

=head2 function GetRNATmpDir

 Title        : GetRNATmpDir
 Usage        : GetRNATmpDir($rna_way)
 Function     : return the way of the directory tmp of the rna
 Returns      : the way of the directory tmp of the rna
 Args         : $rna_way : way of the directory of a RNA		
 Globals      : none

=cut
sub GetRNATmpDir
{
	my ($rna_way) = @_;
	my $rna_tmp_dir	= $rna_way."tmp/";
	return ($rna_tmp_dir);
}
