#!/usr/local/bin/perl


=pod 
=head1 NAME

ComputeIntergenicStatistics.pl - This program computes characteristics about intergenic regions of a replicon of an organism (specific strain)

=head1  SYNOPSIS 

./ComputeIntergenicStatistics.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia_coli" --strain K12 --replicon chromosome --word_max_length 2

=head1 DESCRIPTION

ComputeIntergenicStatistics.pl computes characteristics about intergenic regions of the sequence of the replicon
it computes the intergenic regions : 
	on strand + and -, (complet) 
	only on strand +, (leading)
	only on strand - (lagging)
it saves them in the directory tmp in global_statistics
for each type of region, (complet, leading, lagging),
it computes the k-nucleotid numbers (1 <= k <= word_max_length) and creates the files 
in the directories ig_<type_region><number>_stat of PeRNA bank:

<replicon>
	global_statistics
		IgRegionNumber
		ig_complet{1..m}_stat
			Positions
			A_seq
			C_seq
			AA_seq
			AC_seq...
		ig_leading{1..n}_stat
		ig_lagging{1..l}_stat
		tmp
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;
use ToolBox;
use Stat;
use DnaToolBox;

# total intergenic, total intergenic strand -, intergenic strand +, intergenic strand -
our @intergenic_type = ("complet","leading","lagging");
our $IG_REGION_NB_FILE_NAME;
 	
MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help:s", "cfg=s","kingdom=s", "organism=s", "strain=s", "replicon=s","word_max_length=i"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	$rh_param->AssertDefined(('cfg','kingdom', 'organism', 'strain', 'replicon', 'word_max_length'));
	$rh_param->AssertFileExists('cfg');	# it has to be a file
	$rh_param->Update($rh_param->Get('cfg'),'A');
	$rh_param->AssertInteger('word_max_length');	#it has to be an interger
	
	my $DATA_DIR		= $rh_param->Get('DATA_DIR');
	my $EXTERNAL_DATA_DIR	= $rh_param->Get('EXTERNAL_DATA_DIR');
	my $EXTRACT_FROM_PTT	= $rh_param->Get('EXTRACT_FROM_PTT');
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $word_max_length 	= $rh_param->Get('word_max_length');
	my $GLOBAL_STAT_DIR_NAME= $rh_param->Get('GLOBAL_STAT_DIR_NAME');
	$IG_REGION_NB_FILE_NAME = $rh_param->Get('IG_REGION_NB_FILE_NAME');
	
	my $ext_data_replicon_dir	= &GetRepliconDir($EXTERNAL_DATA_DIR, $kingdom, $organism, $strain,$replicon);
	my $replicon_dir 		= &GetRepliconDir($DATA_DIR, $kingdom, $organism, $strain,$replicon);
	my $global_stat_dir 		= $replicon_dir.$GLOBAL_STAT_DIR_NAME."/";  

	# extract intergenic regions
	&ExtractIntergenicSeq($EXTRACT_FROM_PTT, $replicon_dir, $ext_data_replicon_dir, $global_stat_dir);
	
	# compute intergenic region characteristics
	&ComputeIntergenicStats($replicon_dir,$word_max_length, $global_stat_dir);
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--word_max_length : max length of k-nucleotids to count ( 1 <= k <= word_max_length)
END
}

=head2 procedure ExtractIntergenicSeq

 Title        : ExtractIntergenicSeq
 Usage        : ExtractIntergenicSeq($EXTRACT, $replicon_dir, $ext_data_replicon_dir, $global_stat_dir)
 Prerequisite : User has to get the ptt file about the replicon of the genome and put them in the directory 
 		external_data/<kingdom>/<organism_strain>/<replicon>/ !!
 Function     : extract intergenic regions from the fasta file with the ptt file, and create 3 files in the directory tmp:
 			1 contains intergenic region in the strand +
			1 in the strand -
			1 contains regions where there is no gene in a strand 
 Returns      : none
 Args         : $EXTRACT : command to extract intergenic regions
 		$replicon_dir : directory of the replicon
		$ext_data_replicon_dir : directory of the external data of the replicon in which there is genomic ptt file
		$global_stat_dir : directory of the global statistics
 Globals      : none
 
=cut 
sub ExtractIntergenicSeq
{
	my ($EXTRACT, $replicon_dir, $ext_data_replicon_dir, $global_stat_dir) = @_;
	my $fasta_file;
	my $ptt_file;
	my $cmde;

	# search the fasta file of the replicon
	$fasta_file 	= &GetFastaFileWay($replicon_dir);
	
	# compute ptt file which contains data about the cds regions
	my @a_ptt_file	= &GetExtensionFiles($ext_data_replicon_dir, ".ptt");
	if ($a_ptt_file[0]) {
		$ptt_file  = $ext_data_replicon_dir.$a_ptt_file[0];
	} else {
		die "Error: no ptt file in the directory $ext_data_replicon_dir !!\n";
	}
	
	# extract the strict intergenic region
	$cmde = $EXTRACT." -seq ".$fasta_file." -cds ".$ptt_file." --obj IG";
	print $cmde."\n";
	&SystemLaunch($cmde);
	
	# extract the intergenic region on the strand +
	$cmde = $EXTRACT." -seq ".$fasta_file." -cds ".$ptt_file." -obj IG -onestrand +";
	print $cmde."\n";
	&SystemLaunch($cmde);
	
	# extract the intergenic region on the strand -
	$cmde = $EXTRACT." -seq ".$fasta_file." -cds ".$ptt_file." -obj IG -onestrand -";
	print $cmde."\n";
	&SystemLaunch($cmde);

	# create tmp directory
	if ( !( &ExistDirectory("tmp", $global_stat_dir) )) {
		&CreateDirectory("tmp", $global_stat_dir);
	}
	my $tmp_dir = $global_stat_dir."tmp/";
	
	# move the generated files in the directory tmp
	chdir ($replicon_dir);
	$cmde = "mv *_- *_+ *IG $tmp_dir";
	&SystemLaunch($cmde);
}

=head2 procedure ComputeIntergenicStats

 Title        : ComputeIntergenicStats
 Usage        : ComputeIntergenicStats($replicon_dir,$max_length, $global_stat_dir)
 Prerequisite : none
 Function     : compute characteristics about all the intergenic regions: call the function to compute k-nucleotid number 
 		for 3 types of intergenic regions (complet, leading, lagging)
 		create the file IgRegionNumber in the directory global_statistics which 
		contains the number of intergenic regions for the 3 types (complet, leading, lagging)
 Returns      : none
 Args         : $replicon_dir : directory of the replicon
		$max_length : max length of k-nucleotids that we count (1 <= k <= max_length)
		$global_stat_dir : directory of the global statistics
 Globals      : none
=cut
sub ComputeIntergenicStats
{
	my ($replicon_dir,$max_length, $global_stat_dir) = @_;
	
	my $intergenic_files_dir = $global_stat_dir."tmp/";
	my $type;		# the type of regions: complet , leading or lagging
	my $region_number;	# number of this type of regions
	my $file_way;		# way of the multifasta file of the a type of intergenic regions
	my %h_interg_nb;
	
	$h_interg_nb{$intergenic_type[0]} = 0;
	$h_interg_nb{$intergenic_type[1]} = 0;
	$h_interg_nb{$intergenic_type[2]} = 0;
	
	# open the directory which contains the intergenic multifasta files
	opendir(DIR, $intergenic_files_dir) or die "Can't open the directory $intergenic_files_dir !!!\n";
	
	# for each multifasta file
	while ( defined(my $file = readdir DIR) ) {
		next if ($file =~ /^\.\.?/);	# don't compute the directories . and ..
		$file_way = $intergenic_files_dir.$file;
		next if (-d $file_way);	#don't compute files
		
		$type 			= &getRegionType($file);
		# compute characteristics for this type of region
		$region_number 		= &ComputeIntergenicRegions($file_way, $type, $global_stat_dir, $max_length);
		$h_interg_nb{$type} 	= $region_number; 
	}
	
	# create the file which contains the number of intergenic regions : +/-, +, - in this order
	my $data = $h_interg_nb{$intergenic_type[0]}."\n".$h_interg_nb{$intergenic_type[1]}."\n".$h_interg_nb{$intergenic_type[2]}."\n";
	&CreateFile($IG_REGION_NB_FILE_NAME, $global_stat_dir, $data);	
}

=head2 function getRegionType

 Title        : getRegionType
 Usage        : getRegionType($file)
 Prerequisite : none
 Function     : return the name of the intergenic region type (lagging, leading or complet): 
 		it corresponds to the end of the name of the file
 Returns      : the name of the intergenic region type (lagging, leading or complet)
 Args         : $file : file for which we look for the type of intergenic region
 Globals      : none
=cut

sub getRegionType
{
	my ($file) = @_;
	if ($file =~ /\+$/) {
		return ($intergenic_type[1]);
	}elsif ($file =~ /\-$/) {
		return ($intergenic_type[2]);
	}else {
		return ($intergenic_type[0]);
	}
}


=head2 function ComputeIntergenicRegions

 Title        : ComputeIntergenicRegions
 Usage        : ComputeIntergenicRegions($file, $type, $global_stat_dir, $max_length) 
 Prerequisite :
 Function     : compute characteristics for each intergenic region of the file
 Returns      : the number of intergenic regions extracted from the multifasta file
 Args         : $file : multifasta file of the intergenic regions
 		$type : type of the regions : complet, leading or lagging
		$global_stat_dir : directory of global statistics
		$max_length : max length of the k-nucleotids to count
 Globals      : none
=cut
 
sub ComputeIntergenicRegions
{
	my ($file, $type, $global_stat_dir, $max_length) = @_;
	my $seq = "";
	my $sequence_number = 0;
	
	open (INTER_FILE,"<$file") || die "Problem to open $file !\n";
	
	# for each line of the multi fasta file
 	while (defined(my $line = <INTER_FILE>) ) {
		# if it's the caracter ">" so the begin of a sub sequence
		if ($line =~ /^>/) {
			if ( $seq ne "") {
				$sequence_number++;
				# compute characteristics about this region
				&ComputeRegion($seq, $sequence_number, $type, $global_stat_dir, $max_length);
			}
			# prepare for the next sequence
			$seq = $line;
		} else {
			chomp($line);
			$seq = $seq.$line;
		}
	}
	
	# compute the last sequence
	$sequence_number++;
	&ComputeRegion($seq, $sequence_number, $type, $global_stat_dir, $max_length);
	close (INTER_FILE);
	
	#return the number of intergenic regions containing in the file
	return $sequence_number;
}


=head2 procedure ComputeRegion

 Title        : ComputeRegion
 Usage        : ComputeRegion($seq, $sequence_number, $type, $global_stat_dir, $max_length) 
 Prerequisite :
 Function     : create a directory for the region and compute statistics for 
 		the intergenic region in $seq. Create a file Positions 
 		which contains the positions of the region about the genomic sequence
 Returns      : none
 Args         : $seq : the sequence of the region
 		$seq_number : the number of the region (used to create its directory)
		$type : type of the regions : complet, leading or lagging 
		$global_stat_dir : directory of the global statistics
		$max_length : max length of the k-nucleotids to count
 Globals      : none
=cut
sub ComputeRegion
{
	my ($seq, $sequence_number,$type, $global_stat_dir, $max_length) = @_;
	
	# create directory for the region in $global_stat_dir
	my $region_dir_name = &GetRegionDirName($type,$sequence_number);
	if ( !(&ExistDirectory($region_dir_name, $global_stat_dir) )) {
		&CreateDirectory($region_dir_name, $global_stat_dir);
	}
	
	my $region_dir	= $global_stat_dir.$region_dir_name."/";
	my @a_data 	= split (/\n/, $seq);
	
	# extract the sequence
	my $only_seq	= $a_data[1];
	
	# extract the positions
	my @a_position	= split(/\./, $a_data[0]);	# >NC_000913.5531_5682_DOWN_b0006 Escherichia coli K12, complete genome. IG	
	@a_position	= split(/\_/, $a_position[1]); # 5531_5682_DOWN_b0006 Escherichia coli K12, complete genome. IG
	# search the nature of the sequence : primary sequence or reverse complement
	if ( $a_data[0] =~ /REVCOM$/ ) {
		$only_seq	= &reverse_complement_as_string($only_seq);
	}
	
	
	# create the file positions
	my $position_data = $a_position[0]." ".$a_position[1]."\n";
	&CreateFile("Positions",$region_dir, $position_data);

	# compute characteristics about the region
	&CreateAllNucleotidFiles($only_seq, "seq", $region_dir, $max_length);
}


=head2 function GetRegionDirName

 Title        : GetRegionDirName
 Usage        : GetRegionDirName($type, $seq_number)
 Function     : return the name of the directory for the region which has the number $seq_number 
 		and which is an intergenic region $type
 Returns      : the name of the directory for the region
 Args         : $type : type of intergenic region 
 		$seq_number : number of the region		
 Globals      : none

=cut
sub GetRegionDirName
{
	my ($type, $seq_number) = @_;
	my $region_dir_name = "ig_".$type.$seq_number."_stat";
	return ($region_dir_name);
}
