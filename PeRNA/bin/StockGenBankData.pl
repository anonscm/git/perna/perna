#!/usr/local/bin/perl



=pod 
=head1 NAME

StockGenBankData.pl - This program creates the tree structure for a organism and creates the specific files of its genome 

=head1  SYNOPSIS 

`pwd`/StockGenBankData.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom --organism "Escherichia coli" --strain K12 --replicon chromosom --file file.gbk 

=head1 DESCRIPTION

StockGenBankData.pl creates the tree structure for the strain of the organism
it creates files containing the data about the genome (genomic sequence, functional elements)
and the temporary files used to execute Blast in the directory tmp (formated genomic sequence, ...)

data
   <organism>[_<strain>]
	<replicon>
		known_rna
		tmp
			<organism>[_<strain>]_<replicon>_formated.fna
			<organism>[_<strain>]_<replicon>_formated.fna.csq
			<organism>[_<strain>]_<replicon>_formated.fna.nhd
			<organism>[_<strain>]_<replicon>_formated.fna.ntb
		<organism>[_<strain>]_<replicon>.fna
		genomic_information
		protein_genes
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use Bio::SeqIO;
use Bio::SeqFeatureI;
use Bio::Location::Simple;
use ParamParser;
use Logger;
use ToolBox;
use Rna;


our $DATA_DIR;
our $CMD_REFORMAT_DB;
our $CMD_PRESS_DB;
our $EXTERNAL_DATA_DIR;

MAIN:
{
 
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s", "kingdom=s", "organism=s", "strain=s", "replicon=s", "file=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$DATA_DIR   		= $rh_param->Get('DATA_DIR');
	$CMD_REFORMAT_DB	= $rh_param->Get('CMD_REFORMATDB');
	$CMD_PRESS_DB   	= $rh_param->Get('CMD_PRESSDB');
	$EXTERNAL_DATA_DIR	= $rh_param->Get('EXTERNAL_DATA_DIR');

	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $genbank_file 	= $rh_param->Get('file');
	
	# create and return the way for one organism : /organism/chromosom
	my $way = &CreateArborescence($kingdom, $organism, $strain, $replicon);
	
	# create the files of the genomes : the genomic fasta sequence,
	# the genomic information, the elements of the genomes ( cds repeat_region, and their position)
	&CreateGenomeFiles($genbank_file, $organism, $strain, $replicon, $way);
	
	# create the temporary files to allow the use of wu_Blast in the out_directory
	my $out_dir = $way."tmp/";
	&CreateTemporaryFiles($organism, $strain, $replicon, $way, $out_dir, $CMD_REFORMAT_DB, $CMD_PRESS_DB);
}

=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:FAMILIES
$0	
		--help		: this message
		--cfg		: the configuration file
		--organism	: the name of the searched organism
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--file		: a genbank file containing the data about the organism genome
END
}


=head2 function CreateArborescence

 Title        : CreateArborescence
 Usage        : CreateArborescence($kingdom, $organism, $strain, $replicon)
 Prerequisite : none
 Function     : Create the arborescence for the specific organism and its replicon in the directory $DATA_DIR
 Returns      : the way of the replicon directory
 Args         : $kingdom : kingdom of the organism
 		$organism : name of the organism
		$strain : strain of the organism
		$replicon : replicon of the organism
 Globals      : none
 
=cut 


sub CreateArborescence{
	my ($kingdom, $organism, $strain, $replicon) = @_;

	my $way;
	my $dir_name;

	# go in the directory of the kingdom
	$way = &GetKingdomDir($DATA_DIR,$kingdom); 
	chdir($way) or die "The directory $way doesn't exist. The kingdom $kingdom doesn't exist.\n";
	
	$dir_name = &GetDirName($organism, $strain);
	# create the directory for the organism
	if (&ExistDirectory($dir_name, $way) == 0) {
		&CreateDirectory($dir_name, $way);
	}
	
	$way = $way.$dir_name."/";
	$dir_name = &FormateString($replicon);
	# creation of the directory of the replicon
	if (&ExistDirectory($dir_name, $way) == 0) {
		&CreateDirectory($dir_name, $way);
	}
	
	$way = $way.$dir_name."/";
	
	# create the directory for the known rna
	&CreateDirectory("knownRNA", $way);
	
	# create the directory for the temporary data
	&CreateDirectory("tmp", $way);
	
	return ($way);
}


=head2 procedure CreateGenomeFiles

 Title        : CreateGenomeFiles
 Usage        : CreateGenomeFiles($genbank_file, $organism, $strain, $replicon, $way)
 Prerequisite : the genbank file of the complete genome exists
 Function     : Create files on the genome : a file containing the fasta sequence
 		a file containing information in the EMBL format 
			ID length BP
			AC accession number
		a file containing all the elements which composes the genome and their position : gene, CDS 
			FT CDS 1..210
			FT gene 1..110
 Returns      : none
 Args         : $genbank_file : the genbank file about the complete genome
 		$organism : the name of the searched organism
 		$strain : the strain of the organism
		$replicon : replicon 
		$way : the way where create the files
 Globals      : none
 
=cut 

sub CreateGenomeFiles
{
	my ($genbank_file, $organism, $strain, $replicon, $way) = @_;
	my $location;
	
	#read the genbank file
	my $seqio  = Bio::SeqIO->new( -format => 'genbank' ,
					-file   => $genbank_file);
	my $seqobj = $seqio->next_seq();
	
	chdir($way);
	
	# creation of the fasta file of the genomic sequence
	my $name_file = &GetNameFastaFile($organism, $strain, $replicon);
	my $out       = Bio::SeqIO->new( -format =>'fasta',
					-file   => ">".$name_file);
	$out->write_seq($seqobj);
	
	# creation of the genomic information file which contains the 
	# length of the sequence and the accession number
	open(F_SEQ, ">genomic_information");
	print F_SEQ "ID ".$seqobj->length()." BP\n";
	print F_SEQ "AC ".$seqobj->accession()."\n";
	
	close(F_SEQ);

	# creation of the protein genes file	
	open(F_GENES, ">protein_genes");
	foreach my $feat ($seqobj->top_SeqFeatures) {
		$location = $feat->location();
		print F_GENES "FT ".$feat->primary_tag." ";
		print F_GENES $location->start()."..".$location->end()." ";
		# if the strand is +
		if ($location->strand() == 1){
			print F_GENES Rna::GetStrandPlus();
		} elsif ($location->strand() == -1){
			#if the strand is -
			print F_GENES Rna::GetStrandLess();
		}else{
			print "WARNING : we don't know the strand of the feature $feat->primary_tag $location->start() $location->end()!\n";
		}
		print F_GENES "\n";
	}
	close(F_GENES);
}


=head2 procedure CreateTemporaryFiles

 Title        : CreateTemporaryFiles
 Usage        : CreateTemporaryFiles($organism, $strain, $replicon, $in_dir, $out_dir, $cmd_reformat_db, $cmd_press_db)
 Prerequisite : none
 Function     : Create temporary files used to execute wu-blast in the out_dir
 Returns      : none
 Args         : $organism : the name of the organism
 		$strain : the strain of the organism
		$replicon : replicon 
 		$in_dir : directory which contains the fasta sequence of the genome
		$out_dir : directory where are created the temporary files : often "$in_dir/tmp" 
 		$cmd_reformat_db : command reformat_db
		$cmd_press_db : command press_db
 Globals      : none
 
=cut 

sub CreateTemporaryFiles
{
	my ($organism,$strain, $replicon, $in_dir, $out_dir, $cmd_reformat_db, $cmd_press_db) = @_;
	
	my $in_file  = &GetNameFastaFile($organism, $strain, $replicon);
	my $out_file = &GetNameFormatedFastaFile($organism,$strain, $replicon);
	
	# creation of the files used to execute Wu-Blast int the out_dir
	my $cmde = $cmd_reformat_db." IN=".$in_dir.$in_file." OUT=".$out_dir.$out_file;
	&SystemLaunch($cmde);
	$cmde = $cmd_press_db.$out_dir.$out_file;
	print "Preparing temporary files for Blast: ";
	&SystemLaunch($cmde);
}

