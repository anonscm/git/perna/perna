#!/usr/local/bin/perl

=pod 
=head1 NAME

StockRfamData.pl - This program extracts the RNA from the rfam files

=head1  SYNOPSIS 

./StockRfamData.pl --cfg $PeRNA/cfg/Conf.cfg --kingdom Bacteria --organism "Escherichia coli" --strain K12 --replicon chromosome --accession_number $PeRNA/external_data/Bacteria/Escherichia_coli_K12/chromosome/U00096.ac --RNAfamilies $PeRNA/cfg/RNAfamiliesRfam.cfg 

=head1 DESCRIPTION

StockRfamData.pl extracts RNA from the file Rfam.full containing rna from rfam banks
for each rna, it creates the specific tree structure (in the directory knownRNA of the organism tree structure)

    data
     <organism>
	<replicon>
           knownRNA
	      RfamRNAfamily
		  RNA1
		  RNA2
		       RNA2.fna
		       structure
		       information
		       tmp
=cut

BEGIN
{
	my ($dirprg, $nameprg) = $0 =~/(.+)\/(.+)/;
	push(@INC, "$dirprg/../lib");	# @INC contains the directory for perl libraries
}

use strict;
use warnings;
use LWP;

use ParamParser;
use Logger;

use Rna;

# directories
our $DATA_DIR; 
#command
our $CMD_REFORMAT_DB;
our $CMD_WU_BLAST;
our $CMD_FILTER	;
#parameters
our $PCQ;
our $IDENTITY;


MAIN:
{
	my $rh_param = New ParamParser('GETOPTLONG',("help", "cfg=s", "kingdom=s","organism=s","strain=s", "replicon=s", "accession_number=s", "RNAfamilies=s"));
	$rh_param->SetUsage(my $usage=sub { &Usage(); } );
	
	$rh_param->AssertFileExists('cfg');
	$rh_param->Update($rh_param->Get('cfg'),'A');
	
	$DATA_DIR   		= $rh_param->Get('DATA_DIR');
	$CMD_REFORMAT_DB	= $rh_param->Get('CMD_REFORMATDB');
	$CMD_WU_BLAST		= $rh_param->Get('CMD_WU_BLAST');
	$CMD_FILTER		= $rh_param->Get('CMD_FILTER');
	$PCQ			= 97;
	$IDENTITY		= 100;
	
	my $kingdom  		= $rh_param->Get('kingdom');
	my $organism  		= $rh_param->Get('organism');
	my $strain  		= $rh_param->Get('strain');
	my $replicon  		= $rh_param->Get('replicon');
	my $accession_number_file  	= $rh_param->Get('accession_number');
	my $rna_families_file 		= $rh_param->Get('RNAfamilies');
	my $rfam_file 			= $rh_param->Get('RFAM_FULL');
	
	
	# create an array containing the name of the known families
	open(DF,"$rna_families_file") or die " Impossible to open the file $rna_families_file";
	my @a_known_family = <DF>;
	close(DF);
	
	# delete the empty line and the comment line (# comment)
	@a_known_family = &FormateArray(@a_known_family);
	
	&CreateRNARfam($kingdom, $organism, $strain, $replicon, $rfam_file, $accession_number_file, \@a_known_family);
}


=head1 SUBROUTINES

=cut

sub Usage
{
print STDERR<<END
Usage:
$0	
		--help		: this message
		--cfg		: the configuration file
		--kingdom	: the kingdom of the organism
		--organism	: the name of the searched organism	
		--strain	: the strain of the organism
		--replicon	: name of the replicon
		--accession_number	: file which contains the names of the accession numbers of the strain of the organism
		--RNAfamilies	: file which contains the rna families of Rfam and their corresponding family 
END
}


=head2 function FormateArray

 Title        : FormateArray
 Usage        : FormateArray(@a_array)
 Prerequisite : none
 Function     : return the array without the cells which begin by the character # and without the end of line
 Returns      : the array without the cells which begin by the character # and without the end of line
 Args         : @a_array : the array  
 Globals      : none

=cut 

sub FormateArray
{
	my (@a_array) = @_;	
	my @a_formated_array; # new array

	for ( my $i = 0 ; $i <= $#a_array ; $i++ ) {
		# if the line doesn't begin by "#" and isn't "\n", save it
		if (!($a_array[$i] =~/^[\#|\n]/ )) {
			push (@a_formated_array, $a_array[$i]);
		}
	}
	return (@a_formated_array);		
}


=head2 procedure CreateRNARfam

 Title        : CreateRNARfam
 Usage        : CreateRNARfam($kingdom, $organism, $strain, $replicon, $rfam_file,$accession_number_file, $a_known_family)
 Prerequisite : none
 Function     : Create the RNAs containing in the rfam_file and belonging to the organism 
 Returns      : none
 Args         : $misc_file : file which contains the rna
		$kingdom : the kingdom of the organism
		$organism : the name of the organism of these rna
		$strain : the searched strain of the organism   
		$replicon : the replicon       
		$rfam_file : file which contains the rfam data
		$accession_number_file : file which contains the accession numbers of the organism
		$a_known_family : array containing the name of the known families ( not misc family) 
 Globals      : none

=cut 


sub CreateRNARfam
{
	my ($kingdom, $organism, $strain, $replicon, $rfam_file,$accession_number_file, $a_known_family) = @_;
	my @a_rnas;		# array containing the objects Rna
	my $a_rna; 		# array of the sequences of a rna family
	my $ra_rna_acc;		# array of the accession numbers of the rnas
	
	my $consensus;		# structure sequence
	my $one_familie;	# data about one family of RNAs
	my $rfma_family_name;	# name of the Rfam family
	my $peRNA_family_name;	# name of the PeRNA family
	my $save_family = 0;
	

	my $known_rna_way = Rna::GetDirKnownRna($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	my $tmp_way       = Rna::GetDirTmp($DATA_DIR, $kingdom, $organism, $strain, $replicon);
	my $genome_lib 	  = $tmp_way.Rna::GetNameFormatedFastaFile($organism, $strain, $replicon);
	
	# open the file containing contig numbers
	open (CONTIG_FILE,"<$accession_number_file") || die "Problem to open file $accession_number_file ";
	# save all the lines in an array
	my @a_acc_nb = <CONTIG_FILE>;
	close (CONTIG_FILE);
	
	# open the file
	open(RFAM_FILE,"<$rfam_file") or die " Impossible to open the file $rfam_file";
	
	# file treatment
	while (defined(my $line = <RFAM_FILE>) ) {
		if ($line ne "\/\/\n"){
			$one_familie = $one_familie.$line;
		} else {
			# search the rfam family name and the corresponding PeRNA family name
			($rfma_family_name, $peRNA_family_name) = &SearchFamilyNames($one_familie, $a_known_family);
			
			# if the data of the family should be extract from Rfam 
			if ($peRNA_family_name ne "") {
				&ComputeFamily($one_familie, \@a_acc_nb, $organism, $strain, $rfma_family_name, $peRNA_family_name, $known_rna_way, $genome_lib);
			}
			
			# reinit data
			$one_familie="";
		}
	}
	close(RFAM_FILE);
}


=head2 procedure ComputeFamily

 Title        : ComputeFamily
 Usage        : ComputeFamily($data, $a_acc_numbers, $organism, $strain, $rfma_family_name, $peRNA_family_name, $known_rna_way, $genome_lib)
 Prerequisite : none
 Function     : compute the rnas of one family : create RNAs objects and the tree structure for one family
 Returns      : none
 Args         : $data : string for one rna family 
			# STOCKHOLM 1.0

			#=GF AC   RF00001
			#=GF ID   5S_rRNA
			#=GF DE   5S ribosomal RNA
			#=GF AU   Griffiths-Jones SR, Mifsud W
			#=GF SE   Szymanski et al, 5S ribosomal database, PMID:11752286
			#=GF SQ   602

			X07545.1/505-619                     ..ACCCGGC.CAUA...GUGGCCG.GGCAA.CAC.CCGG.U.C..UCGUU
			M21086.1/8-123                       ..ACCCGGC.CAUA...GCGGCCG.GGCAA.CAC.CCGG.A.C..UCAUG
			X01588.1/5-119                       ..ACCCGGU.CACA...GUGAGCG.GGCAA.CAC.CCGG.A.C..UCAUU
			M16530.1/8-123                       ..ACCCGGC.AAUA...GGCGCCGGUGCUA.CGC.CCGG.U.C..UCUUC 
		$a_acc_numbers: array containing the accession numbers of the searched organism
		$organism : the name of the organism of these rna
		$strain : the searched strain of the organism 
		$rfma_family_name : name of the rfam family
		$peRNA_family_name : name of the perna family
		$known_rna_way : way of the directory of the rna
		$genome_lib : library for blast
		               
 Globals      : none

=cut 
sub ComputeFamily
{
	my ($data, $a_acc_numbers, $organism, $strain, $rfam_family_name, $peRNA_family_name, $known_rna_way, $genome_lib)  = @_;
	my $ra_searched_rna_nb;		# ref about an array composed of the numbers of the sequences that have a correct accession number 
	my $ra_searched_rna_acc;	# ref about an array composed of the found accession numbers 
	my $ra_rna ;			# ref about an array composed of one rna
	my $line_number;
	my $acc_number;
	
	my @a_split 					= split(/\n\n/, $data);
	
	# search the lines which describe a searched rna (a rna which has a known accession number) and the accession numbers
	($ra_searched_rna_nb, $ra_searched_rna_acc) 	= &getNbSearchedSequence($a_split[2], $a_acc_numbers);
	
	# for each searched lines
	for (my $i = 0 ; $i < $#$ra_searched_rna_nb+1 ; $i++)
	{
		$line_number	= ${$ra_searched_rna_nb}[$i]; # we select the number of the searched line 
		$acc_number 	= ${$ra_searched_rna_acc}[$i]; # we select the accession number
		# compute the rna at this line
		$ra_rna 	= &ComputeAnRNA(\@a_split ,$line_number, $acc_number, $organism, $strain, $rfam_family_name);
		
		# creation of the tree structure for this RNA
		Rna::CreateTreeStructureRNAs($ra_rna, $known_rna_way, $peRNA_family_name, $genome_lib, "Rfam", 
		$CMD_REFORMAT_DB, $CMD_WU_BLAST,$CMD_FILTER, $IDENTITY, $PCQ);
	}
}



=head2 function ComputeAnRNA

 Title        : ComputeAnRNA
 Usage        : ComputeAnRNA($ra_family_data, $line_number, $acc_number, $organism,  $strain, $rfam_family_name)
 Prerequisite : none
 Function     : compute the rna witch is on the line $line_number
 Returns      : a ref about an array composed of an rna object
 Args         : $ra_family_data : ref about an array of 3 parts 
			# STOCKHOLM 1.0

			#=GF AC   RF00001
			#=GF ID   5S_rRNA
			#=GF DE   5S ribosomal RNA
			#=GF AU   Griffiths-Jones SR, Mifsud W
			#=GF SE   Szymanski et al, 5S ribosomal database, PMID:11752286
			#=GF SQ   602

			X07545.1/505-619                     ..ACCCGGC.CAUA...GUGGCCG.GGCAA.CAC.CCGG.U.C..UCGUU
			M21086.1/8-123                       ..ACCCGGC.CAUA...GCGGCCG.GGCAA.CAC.CCGG.A.C..UCAUG
			X01588.1/5-119                       ..ACCCGGU.CACA...GUGAGCG.GGCAA.CAC.CCGG.A.C..UCAUU
			M16530.1/8-123                       ..ACCCGGC.AAUA...GGCGCCGGUGCUA.CGC.CCGG.U.C..UCUUC 
		$line_number: the number of the line of the rna
		$acc_number : the accession number of the rna
		$organism : the name of the organism of these rna
		$strain : the strain of the organism 
		$rfam_family_name : name of the rfam family of the rna            
 Globals      : none

=cut 
sub ComputeAnRNA
{
	my ($ra_family_data, $line_number, $acc_number, $organism,  $strain, $rfam_family_name) = @_;
	my $sequence 	= "";
	my $consensus 	= "";
	my $line; # a line of data
	
	# use to split the data
	my $cons ;
	my @struct;
	my @a_lines;
	my @a_seq;
	my @a_rna;
	
	# for each bloc of data(third part of the array), concate the sequence of the rna (A..U..) 
	# and the consensus structure <<<...>>
	for ( my $i = 2 ; $i <= $#$ra_family_data ; $i++ ) {
		@a_lines	= split(/\n/, $ra_family_data->[$i]);
		$line 		= $a_lines[$line_number]; # we select the line of the rna in this bloc
		@a_seq 		= split(/ +|\t+/,$line);  # we select the sequence in this line
		chomp($a_seq[1]);
		$sequence = $sequence.$a_seq[1];	# concate the sequence
		
		@struct	= split(/ +/,$a_lines[$#a_lines-1]); # select the structure line : the last line -1
		$cons 	= $struct[2];
		chomp($cons);	# <<<<.<<<....<<<<...>>>>..<.<<<<....>
		$consensus 	= $consensus.$cons;	# concate the structure
	}
	
	# Create the rna object
	my ($seq, $seq_alignment) 	= Rna::TreatSequence($sequence);
	# creation of a new rna and save in the set on rna
	my $rna_obj			= Rna->New($seq,$seq_alignment, 
					$organism, $strain,$rfam_family_name, $acc_number);
	# add the structure
	$rna_obj->CreateStructure2D($consensus);
	push(@a_rna, $rna_obj);
	
	return \@a_rna;	
}

=head2 function SearchFamilyNames

 Title        : SearchFamilyNames
 Usage        : SearchFamilyNames($one_family, $a_known_family )
 Prerequisite : none
 Function     : search the Rfam family name and the corresponding PeRNA family name in the stockholm entry $one_family 
 		search the Rfam family name in the line #=GF ID of the entry $one_family
		search the PeRNA family name in the array $a_known_family
 Returns      : the name of the Rfam family and the name of the PeRNA family (tRNA or RNaseP)
 Args         : $one_family : data about one family
 			# STOCKHOLM 1.0

			#=GF AC   RF00001
			#=GF ID   5S_rRNA
			#=GF DE   5S ribosomal RNA
			#=GF AU   Griffiths-Jones SR, Mifsud W
			#=GF SE   Szymanski et al, 5S ribosomal database, PMID:11752286
			#=GF SQ   602

			X07545.1/505-619                     ..ACCCGGC.CAUA...GUGGCCG.GGCAA.CAC.CCGG.U.C..UCGUU
			M21086.1/8-123                       ..ACCCGGC.CAUA...GCGGCCG.GGCAA.CAC.CCGG.A.C..UCAUG
			X01588.1/5-119                       ..ACCCGGU.CACA...GUGAGCG.GGCAA.CAC.CCGG.A.C..UCAUU
			M16530.1/8-123                       ..ACCCGGC.AAUA...GGCGCCGGUGCUA.CGC.CCGG.U.C..UCUUC 
			
		$a_known_family : array containing the name of the rfam families and the name of the 
		associated family (snoRNA_CD_box, RNaseP, tRNA...)
			snoR44_J54 snoRNA_CD_box 
			snoR53 snoRNA_CD_box 
			snoR60 snoRNA_CD_box 
			snoR64 snoRNA_CD_box 
			snoR66 snoRNA_CD_box 
 Globals      : none

=cut 
sub SearchFamilyNames
{
	my ($one_family, $a_known_family) = @_;
	my $line;	# current line
	
	my @a_split 		= split(/\n\n/, $one_family);
	my $rfam_family_name	= &getFamilyName($a_split[1]); # find the name of the rfam family
	
	# for each family of the array
	for (my $i = 0 ; $i <= $#$a_known_family ; $i++) {
		$line = $a_known_family->[$i];
		# if we find the name of the rfam family in the file of the PeRNA families 
		if ($line =~/^$rfam_family_name[ |\n]/ ) {
			chomp($line);
			@a_split = split(/ /, $line);
			# return the associated name (= the PeRNA family name) 
			if ($a_split[1]) {
				return ($rfam_family_name, $a_split[1]);
			} else {
				return ($rfam_family_name,"");
			}		
		}
	}
	# the family doesn't belong to a known family so it's a misc family
	return ($rfam_family_name,"misc"); 
}


=head2 function getFamilyName

 Title        : getFamilyName
 Usage        : getFamilyName($data);
 Prerequisite : none
 Function     : return the name of the family
 Returns      : the name of the famil
 Args         :$data : string for one rna family 
			#=GF AC   RF00001
			#=GF ID   5S_rRNA
			#=GF DE   5S ribosomal RNA
			#=GF AU   Griffiths-Jones SR, Mifsud W
			#=GF SE   Szymanski et al, 5S ribosomal database, PMID:11752286
			#=GF SQ   602
                  
 Globals      : none

=cut 
sub getFamilyName
{
	my ($data) = @_;
	my @a_split 	= split("#=GF ID", $data);
	@a_split 	= split(/ +|\t+|\n/, $a_split[1]);
	return ($a_split[1]);
}

=head2 function getNbSearchedSequence

 Title        : getNbSearchedSequence
 Usage        : getNbSearchedSequence($data, $a_contig_acc_nb);
 Prerequisite : none
 Function     : return the numbers of the sequence that we have to search
 Returns      : references about an array containing the numbers of the sequence that we have to search,
 		and about an array containing the accession numbers
 Args         : $data : string for one rna family 
			X07545.1/505-619                     ..ACCCGGC.CAUA...GUGGCCG.GGCAA.CAC.CCGG.U.C..UCGUU
			M21086.1/8-123                       ..ACCCGGC.CAUA...GCGGCCG.GGCAA.CAC.CCGG.A.C..UCAUG
			X01588.1/5-119                       ..ACCCGGU.CACA...GUGAGCG.GGCAA.CAC.CCGG.A.C..UCAUU
			M16530.1/8-123                       ..ACCCGGC.AAUA...GGCGCCGGUGCUA.CGC.CCGG.U.C..UCUUC
		$a_contig_acc_nb : set of the searched accession numbers
                  
 Globals      : none

=cut 
sub getNbSearchedSequence
{
	my ($data, $a_contig_acc_nb) = @_;
	my @a_searched_seq_nb;	# numbers of the searched sequences
	my @a_searched_seq_acc; # acession numbers of the searched sequences
	
	my @a_acc;	# use to split
	my $accession_number; # use to split
	
	my @a_lines = split(/\n/, $data);
	
	for (my $i = 0 ; $i < $#a_lines ; $i++) {
		@a_acc 	= split(/ +|\t+/,$a_lines[$i]);	# AP000058.1/198449-198523   GG.G...C...C...C...U.C.
	    	@a_acc  = split(/\//, $a_acc[0]);	# AP000058.1/198449-198523 
		# accession number treatment
		@a_acc              	= split(/\./, $a_acc[0]);	# AP000058.1
		$accession_number 	= $a_acc[0];
		if ( grep(/$accession_number/, @{$a_contig_acc_nb}) != 0 ) {
			# save this number 
			push(@a_searched_seq_nb, $i);
			# save the accession number
			push(@a_searched_seq_acc, $accession_number);
		}
	}
	return (\@a_searched_seq_nb, \@a_searched_seq_acc);
}					
