#!/usr/local/bin/perl


##[
#>Project:      iANT integrated ANnotation Tool (1999-2000)
#>Release:      1.0
#>Lab:          INRA-CNRS LBMRPM, BP 27 Chemin de Borde Rouge, 31326 Castanet Cedex France
#>Authors:      Patricia.Thebault@toulouse.inra.fr and Jerome.Gouzy@toulouse.inra.fr
#>This script:  Patricia.Thebault@toulouse.inra.fr
#>Last updated: October 31, 2000
#>Citation:
#
#Aim: Filter out iANT results common format file
#
#
#Updated:       
#
#Who:   
#
##]

$script_ok = ( $0 =~ /\.ok\./ ) ? ".ok" : "";

require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jConfig$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/jLibPerl$script_ok.pl";
require "/bioinfo/www/bioinfo/perna/PeRNA/bin/WUBlast/pLibPerl$script_ok.pl";


#Definition des modes de fonctionnements
# JER 14/12/99 a mettre ds jConfig et a discuter ensemble
$NORMAL="NORMAL"; # pour fonctionnement normal
$STS="STS";   # pour les sts, representation differente	
$EST="EST";   # pour les est, representation differente # JER 14/12/99
# JER 7-7-2000 Une seule fonction pour STS, EST et CONTIG
$CONTIG="CONTIG"; # pour les contigs, comme ralsto, reconstitue le chemin des  differents fichiers impliques
$DB="DB";#pour db vs db
$GI="GI";
$AA = "AA";
$DEFAULT=4;

########################################################################
#[pFilterBlastUsage
#Aim:  Usage script
#Global:
#In: Error Msg to print out
#Out: 
#Update:
#Prerequisite:     
#]
sub pFilterBlastUsage 
{
local ($msg) = @_ ;

    print STDERR "$msg\n";    
    print STDERR 
	" USAGE : $0 

Required	
	FILE=Input File 
        ORGA_CONFIG=Configuration organism file (required if FORM_FRAME_SET=YES)

Optional
	BEGIN=Matchs with a begin value more than this value will be print out        [0]
	END=Matchs with a end value less than this value will be print out            [MAX]
	OVERLAP=Matchs with a region that overlaps this value will print out          []
        STRICT=This parameters works with Begin and End parameters 
               which delimit  a strict or no strict region VALUE=YES|NO               [NO]  

        MODE=Mode to print out matchs: VALUE=NORMAL|STS|EST|CONTIG|DB|GI              [NORMAL]	     
	SCORE=Matchs with a score more than this value will be print out	      [0]  
	PROBA=Matchs with a probability less than this value will be print put	      [1]
        PROBA_INF=Matchs with a probality more than this value will be print out      [0]
	IDENTITY=Matchs with an identity value more than this value will be print out [0]
	POS=Matchs with an positives value more than this value will be print out;    [0]
	GAP=Matchs with an gap value less than this value will be print out           [MAX]
	PCS=Matchs with a Percentage of the Subject sequence
            involved in the match more or equal than this value will be print out     [0]
	PCQ=Matchs with a Percentage of the Query sequence
            involved in the match more or equal than this value will be print out     [0]
	LENGTH_HSP=Matchs with a length value more or equal than this value will 
            be print out                                                              [0]

        FRAME_SHIFT=To predict Frame shifts thanks to the homology results 
            VALUE=ON|OFF|WithComment                                                  [OFF]       	
	LIGHT=To print out a light or complete ouput  VALUE=YES/NO                    [NO]
	REDUNDANCY=To suppress with 2 differents way the reduncdancy 
            VALUE=NO|Identity|Inclusion                                               [NO]
	SORT=To sort out match on their beggining position VALUE=YES|NO		      [NO]
	COM=Matchs which contains in their commentary those words will be print out
            One or several Keywords seperated by a comm   
	HTML=To print out or not on STDOUT output an HTML document                    [NO]
        FORM_FRAME_SET=For HTML output, to get the ViewNip output VALUE=YES|NO        [NO]
\n";
        exit(0);
}

########################################################################
#[pQuickFiltreLight
#Aim:  To pre-filter out the file in a LIGHT output : to collect header lines which contain com
#Global: $UNIXGREPBIS , $UNIXDEZIP
#In:  $com(:comment to search for, if equal to "" will give all header lines),$file(:file rp pre-filter)
#Out: $command(:command line with the pattern to search for a file) 
#Update:
#Prerequisite:        $GidxParamAnal{LIGHT}=$YES
#]
sub pQuickFilterLight
{
    my($com,$file)=@_;
    my(@mot_cles)= split (',', $com);
    my($trouve_com)=$NO;
    my($j);
    my($command)="-e PROGRAM -e QUERY -e \"^<\"";
    my($list_pattern)="";
    if ( $#mot_cles >=0 )
    {
	$list_pattern="-e \"^PROGRAM\" -e \"^QUERY\"";
        for( $j=0;$j<=$#mot_cles;$j++)
        {
                $list_pattern .=" -e $mot_cles[$j]";
        }        
    }
    if ($file =~ /$UNIXDEZIP/)
    {
	$command="$file $UNIXGREP  -i $command";
	$command="$command | $UNIXGREP -i $list_pattern" if ($list_pattern ne "");
	
    }
    else
    {
	$command="$UNIXGREP  -i $command $file";
	$command="$command | $UNIXGREP -i $list_pattern" if ($list_pattern ne "");
    }
    return ($command);
}

########################################################################
#[pPrintResultsIn2Frame
#Aim:    Print to STDOUT  Results in 2 Frame with in the Top one the DNA Nip View and the bottom one the filtered results      
#Global:$LIMIT_SIZE_TO_RUN_TRADUCTION , $WWW_SCRIPT_VIEW_WITH_NIP , $WWW_SCRIPT_AA_DNA_FILTER
#In: $nomFile (file to filter out) ,*P_Anal (Parameters to analize) ,*P_Filter (Parameters to filter out)
#Out: 
#Update:
#Prerequisite:  FORM_FRAME_SET = YES   
#]
sub pPrintResultsIn2Frame
{
    local($nomFile,*P_Anal,*P_Filter)=@_;
    my($cmd)="";
    my($key)="";
    my($long_BAC) = &pLongFastaFile($nomFile);
    $P_Filter{END}=$long_BAC if (&pIsEmpty($P_Filter{END}));
    foreach $key (keys %P_Anal)
    {
	if ($key eq "FORM_FRAME_SET")
	{
	   $cmd=$cmd."&$key=NO"; 
	}
	else
	{
	    $cmd=$cmd."&$key=$P_Anal{$key}" if ($P_Anal{$key} ne "");
	}
	
    }
    foreach $key (keys %P_Filter)
    {
	$cmd=$cmd."&$key=$GidxParamFilter{\"$key\"}"if ($P_Filter{$key} ne ""); 
    }
    #TEST the region size to run or not Usage Codon
    if (abs($P_Filter{END}-$P_Filter{BEGIN})<=$LIMIT_SIZE_TO_RUN_TRADUCTION)
    {
	$option="FORM_FRAME_SET=NO&FILE_BLASTX=$BLASTX_FILTRE_E1_EXTENSION";
    }
    else
    {
	$option="FORM_FRAME_SET=YES"; 
    }
print STDOUT <<END
<HTML>
<TITLE>iANT : BBA result </TITLE>
<FRAMESET ROWS="50%,*">
    <FRAME TARGET=new SRC="$WWW_SCRIPT_VIEW_WITH_NIP?&IN=$nomFile&BEGIN=$P_Filter{BEGIN}&END=$P_Filter{END}&WINDOW=21&$option&ORGA_CONFIG=$P_Anal{ORGA_CONFIG}";
       NAME="CodonUsage"
       SCROLLING="yes">
<FRAME TARGET=new SRC="$WWW_SCRIPT_AA_DNA_FILTER?$cmd"
       NAME="Filter"
       SCROLLING = "yes">
<FRAMESET FRAMEBORDER="NO">
</FRAMESET>
</FRAMESET>
</HTML>
END
}


sub jRedondanceINCLUSION_BLASTN
{
   local (*ALLMATCH,$returntrieparscore) = @_;
my($deb_red) = 0;
my($fin_red) = -1;
local(@ALLMATCHREDONDANCE) = ();        # ATTENTION il faut qu'il soit local pour etre lisible dans jRedondant
# my(@ALLMATCHTMP) = @_;  Encore pourquoi ??? 22/09/99
my(@ALLMATCHTMP) = (); 
my($courant);

        @ALLMATCHTMP = sort SortByScoreDecroissant @ALLMATCH;
        foreach $courant (@ALLMATCHTMP)
        {
                if ( ! &jRedondant("+",$courant,$deb_red,$fin_red,*ALLMATCHREDONDANCE) )
                {
                        $fin_red++;
                        $ALLMATCHREDONDANCE[$fin_red] = $courant;
                }
        }
        $deb_red = $fin_red+1;
        $fin_red = $fin_red;
        if ( $returntrieparscore )
        {
                return (sort SortByScoreDecroissant @ALLMATCHREDONDANCE);
        }
        else
        {
                return(@ALLMATCHREDONDANCE);
        }
}



#
#
#


sub jRedondanceINCLUSION
{
   local (*ALLMATCH,$returntrieparscore) = @_;
	my($deb_red) = 0;
my($fin_red) = -1;
local(@ALLMATCHREDONDANCE) = ();        # ATTENTION il faut qu'il soit local pour etre lisible dans jRedondant
# my(@ALLMATCHTMP) = @_;  Encore pourquoi ??? 22/09/99
my(@ALLMATCHTMP) = (); 
my($courant);

        @ALLMATCHTMP = sort SortByScoreDecroissant @ALLMATCH;
        foreach $courant (@ALLMATCHTMP)
        {
		print STDOUT "######$ALLMATCHTMP[$courant]########\n";
                if ( ! &jRedondant("+",$courant,$deb_red,$fin_red,*ALLMATCHREDONDANCE) )
                {
                        $fin_red++;
                        $ALLMATCHREDONDANCE[$fin_red] = $courant;
                }
        }
        $deb_red = $fin_red+1;
        $fin_red = $fin_red;
        if ( $returntrieparscore )
        {
                return (sort SortByScoreDecroissant @ALLMATCHREDONDANCE);
        }
        else
        {
                return(@ALLMATCHREDONDANCE);
        }
}



#
#
#


########################################################################
#[pInitParamBlastx_Filtre
#Aim:  Init Parameters 
#Global: $YES,$NO,$DEFAULT,$NORMAL
#In: 
#Out: 
#Update:*paramF(:Filter parameters),*paramA(:Annal parameters)
#Prerequisite:  
#] 
sub pInitParamBlastx_Filtre
{
local(*paramF,*paramA)=@_;
	
	$paramA{"FILE"} = "";
	$paramA{"MODE"} = $NORMAL; #$NORMAL $STS $CONTIG $DB $EST
	
	$paramA{"SORT"}=$DEFAULT;
	$paramA{"LIGHT"}=$NO;
	$paramA{"HTML"}=$NO;
	$paramA{"FRAME"}="";
	$paramA{"FRAME_SHIFT"}="OFF";
	$paramA{"FORM_FRAME_SET"}=$NO;
	$paramA{"REDUNDANCY"}=$NO;
	$paramA{"ORGA_CONFIG"}="";

	
	$paramF{"COM"}="";
	$paramF{"S_NAME"}="";

	$paramF{"LONG_HSP"}="";		#### PAT 26-07-2000 A DELETER
	$paramF{"LENGTH_HSP"}="";	

	$paramF{"SCORE"}="";
	$paramF{"PROBA"}="";
	$paramF{"PROBA_INF"}=""; 
	$paramF{"GAP"}="";

	$paramF{"IDENTITE"}="";	#### PAT 26-07-2000 A DELETER
	$paramF{"IDENTITY"}="";	

	$paramF{"PCS"}="";
	$paramF{"PCQ"}="";
	$paramF{"POS"}="";
	$paramF{"BEGIN"}="";
	$paramF{"END"}="";
	$paramF{"OVERLAP"}="";
	$paramF{"STRICT"}=$NO;
	    ############ Pour snoscan
    	$paramF{"C_SCORE"} ="";
    	$paramF{"D_SCORE"} ="";
    	$paramF{"Dp_SCORE"} ="";
    	$paramF{"STEM"} = "";
    	$paramF{"MISMATCH"} ="";
    	$paramF{"MATCH"} ="";
}
############################################################################################
######################################  MAIN ##########################
#######################################################################
&pInitParamBlastx_Filtre(*GidxParamFilter,*GidxParamAnal);

if ( $0 =~ /\.cgi\./ )
{	
    &InitWWW();
    ########### PAT 31-07-2000 TEMPORAIRE ATTENTION : FRAME_SHIFT etait de type umerique et est maintenant de type string
    if ($in{FRAME} ne "")
    {
    	if ($in{FRAME} =~ /\D+/)
    	{	
    		$in{FRAME_SHIFT}=$in{FRAME};
	}
    	else
    	{
		if  ($in{FRAME} == $NO)
		{
			$in{FRAME_SHIFT}="OFF";
		}
		elsif ($in{FRAME} == $YES)
		{
			$in{FRAME_SHIFT}="ON";
		}
    	}
    
    }	
    ########### END TEMPORAIRE
    &pFillParamBlastx(*in,*GidxParamFilter);
    &pFillParam(*in,*GidxParamAnal);
    $GidxParamAnal{"HTML"}=$YES;
}
else
{
    select(STDOUT);
    &pFilterBlastUsage() if (($#ARGV<0));
    %GidxArg=&pTab2Indx(@ARGV);
    ########### PAT 31-07-2000 TEMPORAIRE ATTENTION : FRAME_SHIFT etait de type umerique et est maintenant de type string
    if ($GidxArg{FRAME} ne "")
    {
    	if ($GidxArg{FRAME} =~ /\D+/)
    	{	
    		$GidxArg{FRAME_SHIFT}=$GidxArg{FRAME};
	}
    	else
    	{
		if  ({$GidxArgFRAME} == $NO)
		{
			$GidxArg{FRAME_SHIFT}="OFF";
		}
		elsif ($GidxArg{FRAME} == $YES)
		{
			$GidxArg{FRAME_SHIFT}="ON";
		}
    	}
    
    }	
    ########### END TEMPORAIRE
    &pFillParam(*GidxArg,*GidxParamAnal);
    $GidxParamAnal{FORM_FRAME_SET}=$NO	if (!$GidxParamAnal{HTML});
    &pFillParamBlastx(*GidxArg,*GidxParamFilter);
}

&pFilterBlastUsage("FATAL : Parameter FILE is required for cgi use\n") if (($GidxParamAnal{FILE} eq "")&&($0 =~ /\.cgi\./));
&pFilterBlastUsage("FATAL : Parameter ORGA_CONFIG is required\n") if (($GidxParamAnal{ORGA_CONFIG} eq "")&&($GidxParamAnal{FORM_FRAME_SET}));

############ PAT 27-07-2000 A_VIRER
$GidxParamFilter{IDENTITY}=$GidxParamFilter{IDENTITE} if ($GidxParamFilter{IDENTITE}ne "");
$GidxParamFilter{LENGTH_HSP}=$GidxParamFilter{LONG_HSP} if ($GidxParamFilter{LONG_HSP}ne "");
$GidxParamAnal{LIGHT}=$YES if (($GidxParamAnal{FRAME_SHIFT} eq "ON")||($GidxParamAnal{FRAME_SHIFT} eq "WithComment"));
############ END VIRER

#&pPrintLegende(*GidxParamFilter);
#&pPrintLegende(*GidxParamAnal) if (!$GidxParamAnal{FORM_FRAME_SET});

&OpenHTML("stdout BBA result") if ((!$GidxParamAnal{FORM_FRAME_SET})&&($GidxParamAnal{HTML})); # BUG PAT 20-01-2000

select STDOUT;
###### INITIALISATION DE VARIABLES
my ($FILE)="";
if ($GidxParamAnal{SORT} == $DEFAULT)
{
    if (($GidxParamAnal{MODE} eq $STS ||$GidxParamAnal{MODE} eq $CONTIG))
    {
	$GidxParamAnal{SORT} =$NO;
    }
    else
    {
	$GidxParamAnal{SORT} =$YES;
    }
}
if ($GidxParamAnal{FILE} =~ /$GZ_EXTENSION$/)
{
    $FILE ="$UNIXDEZIP $GidxParamAnal{FILE} |";
}
elsif (&pFileExiste("$GidxParamAnal{FILE}.$GZ_EXTENSION"))
{
    $FILE ="$UNIXDEZIP $GidxParamAnal{FILE}.$GZ_EXTENSION |";
}
else
{
    $FILE =$GidxParamAnal{FILE} ;
}

######### ORGA_CONFIG is mandatory if FORM_FRAME_SET=YES because is used with nip  
if (($GidxParamAnal{ORGA_CONFIG} eq "")&&($GidxParamAnal{FORM_FRAME_SET} ))#07-01-2000 # test de la negation car FORM_FRAME_SET peut etre egale a 2 au lieu de 1!!!!
{
    print  "#####\n##### WARNING ORGA_CONFIG is not set\n#####\n#####\n";
    exit;
}

my($nomFileSeq)=&pGetNameFileNoExt($GidxParamAnal{FILE});
my($prog_meth)="";

$GidxParamFilter{BEGIN}=0 if ( &pIsEmpty($GidxParamFilter{BEGIN}));
my($cpt_match)=0;
if ($GidxParamAnal{FORM_FRAME_SET} ) #### Double Frame with nip representation and blast results
{
    &pPrintResultsIn2Frame($nomFileSeq,*GidxParamAnal,*GidxParamFilter);
}
##################################################################
############################# BLASTX FILTRE 
##################################################################
else
{ 
    if (($GidxParamAnal{FILE} ne ""))
    {
	if ($GidxParamAnal{LIGHT})
	{
	    my($cmd)=&pQuickFilterLight($GidxParamFilter{COM},$FILE);
	    $GidxParamFilter{COM}=""; # INUTILE DE RETESTER SUR COM
	    open(STDIN,"$cmd|") || print STDOUT "## Error while running : $cmd\n";; 	  
	}
	else
	{
	    open(STDIN,"$FILE") || print STDOUT "ERROR opening $FILE \n" ;	    
	}
    }
    my($no_results)=$YES;
    my($header)=$NO;
    my($bak)="";
    my($query)="";
    my($queryfile)="";
    my($print_match)=$NO;
    my($light_ok)=$NO;
    local(%idxParseMATCH)=();
    local(@redundancy)=(["0","0","0"]);
    while (<STDIN>)
    {
	chop;
	my(@F)=split;
	if (/^QUERYFILE:/)
	{
	    $query=$F[1];
	    $query=~ s!/www!!;
	}
	elsif (/^QUERY:/)
	{
	    $queryfile=$F[1];
	}
	if ((!$GidxParamAnal{"HTML"}) && ((/^QUERY/)||(/^PROGRAM:/)||(/^DATABASE:/)))
	{
	    print "$_","\n";
	}
	if ( /^PROGRAM:/ )
	{
	    $prog_meth = $F[1];
	}
	if ( /^>/ )
	{ 
	    $bak = "\n";
	    $header = $YES;
	    $print_match =$NO;  
	}
 	$print_match = $NO if (/\/\//);# PAT 01-02-2000
	if (( /^</ )) 
	{      
	    $header = $NO;
	    $light_ok = $YES;
	    %idxParseMATCH=&pParseMATCH(); 
	   # print STDOUT "#--$idxParseMATCH{s_name}-$idxParseMATCH{id_query}------>";
	    $print_match = &pFiltreMatchBlast(*GidxParamFilter,*idxParseMATCH,$GidxParamAnal{"REDUNDANCY"},*redundancy);
	    $no_results = $NO if ( $print_match);
	   # print STDOUT "#print_match######$print_match####<BR>";
	    if ( ($print_match) && (!$GidxParamAnal{"LIGHT"}))
	    {
		print  "$bak";
		$bak="" ;
		s/^</< / if ( $GidxParamAnal{"HTML"});
		$_= "<B>$_</B>" if ( $GidxParamAnal{"HTML"});
	    }
	}
	else
	{
	    $light_ok=$NO;
	}	
	if ($header)
	{
	    $bak = "$bak$_\n";
	}
	if ( $print_match ==$YES)
	{
	    if ( $GidxParamAnal{"LIGHT"} && $light_ok)
	    {
		$ALLMATCH[$cpt_match] = $_;
		$cpt_match++;
	    }
	    if ( !$GidxParamAnal{"LIGHT"} )
	    {
		if ($GidxParamAnal{"HTML"})
		{
		    if (/^</)	
		    {
			$line=$_;
			pREfDB(*line); 
			##### 
			##### PAT 10-05-2000 A changer pb avec pREfDB
                        ##### 
			if ($prog_meth =~ /blastn/i)
			{
			    $line=~s/db=p/db=n/;
			}
			&pColorRed(*line);
			$_=$line;
		    }
		    elsif ( (!/Query:/) && (!/Sbject:/))
		    { 
			s/~/<B><FONT color=blue>~<\/FONT><\/B>/g;#PAT 21-01-99 colore gap
			}		    
		}
		print   "$_\n";
	    }
	    
	}
    } ########### END du while 	
    
##################################################
#############Impression des resultats#############
##################################################
    if (($GidxParamAnal{"REDUNDANCY"} eq "Inclusion")&&($GidxParamAnal{"LIGHT"} == $YES))
    {
	my($returntrieparscore) = ($GidxParamAnal{"SORT"}!=$YES ) ? $YES : $NO;
	#### AJOUT pour blastp
	if ($prog_meth =~ /blastp/i)
	{
	    @ALLMATCH = &jRedondanceINCLUSION_BLASTP(*ALLMATCH,$returntrieparscore);
	}
	elsif ($prog_meth =~ /blastn/i)
	{
	    @ALLMATCH = &jRedondanceINCLUSION_BLASTN(*ALLMATCH,$returntrieparscore);
	}
	else# pour blastx et blastn
	{
	    @ALLMATCH = &jRedondanceINCLUSION_BLASTN(*ALLMATCH,$returntrieparscore);
	}
    }
    if (($GidxParamAnal{"SORT"} == $YES) && ($GidxParamAnal{"LIGHT"} == $YES)&& ($GidxParamAnal{"FRAME_SHIFT"}eq "OFF")) ### si FRAME_SHIFT=ON ne sert a rien de toute facon
    {
	@ALLMATCH = sort SortByBeginPosition @ALLMATCH;
    }
    if ($GidxParamAnal{"FRAME_SHIFT"} eq "ON"  || $GidxParamAnal{"FRAME_SHIFT"} =~ /WithComment/i)
    { 
	@ALLMATCH = &pGetCandidateFrameShift($GidxParamAnal{FILE},*ALLMATCH);
	@ALLMATCH = &jScanFrameShift2(@ALLMATCH);
	$no_results=$YES if ($#ALLMATCH<=0);
    }
    if ($no_results)
    {
	&pExit($GidxParamAnal{"HTML"}," Your search produced no results \n");
    }
    if ( $GidxParamAnal{"FRAME_SHIFT"} =~ /WithComment/i )
    {
	$GidxParamAnal{"FRAME"}="WithComment";
	&jScanFrameShift3Print($prog_meth,*ALLMATCH);
    }
    else
    {
	my($last_name)="";
	foreach (@ALLMATCH)
	{
	    chomp;
	    my(@F)=split;
	    my($name)=$F[4];
	    if (($GidxParamAnal{"FRAME_SHIFT"} eq "ON") && ($name ne $last_name))
	    {
		print "<BR>\n";
		$last_name=$name;
	    } 
	    if ($GidxParamAnal{"HTML"} == $YES )	# JER 18/01/99
	    {	
		#	print  "## FUNCTION_to_PRINT_MCH ##$query et $queryfile ##\n";
		
		# JER 07-07-2000 simplification du traitemement de l'affichage des HIT
		if ($GidxParamAnal{MODE} eq $STS || $GidxParamAnal{MODE} eq $EST || $GidxParamAnal{MODE} eq $CONTIG || $GidxParamAnal{MODE} eq $AA )
		{
		    $GidxParam{ORGA_CONFIG} = $GidxParamAnal{ORGA_CONFIG};
		    &jPrintMATCH("$GidxParamAnal{MODE}",$prog_meth,$query,$queryfile,$_);
		}
		elsif ($GidxParamAnal{MODE} eq $DB)# PAT temporaire
		{		    
		    @T=split(" ",$_);
		    $courant_query=$T[0];
		    print "<BR>\n" if ($courant_query ne $last_query);
		    $last_query=$courant_query;
		    &pPrintMATCH_DBvsDB($_,$prog_meth,$GidxParamAnal{FILE},$GidxParamAnal{ORGA_CONFIG});
		}
		elsif ($GidxParamAnal{MODE} eq "IMG") # PAT temporaire
		{
		    @T=split(" ",$_);
		    $courant_query=$T[0];
		    print "<BR>\n" if ($courant_query ne $last_query);
		    $last_query=$courant_query;
		    &pPrintMATCH_with_IMG($_,$prog_meth,$GidxParamAnal{FILE},$GidxParamAnal{ORGA_CONFIG});
		}
		elsif ($GidxParamAnal{MODE} eq "DRAW") # PAT temporaire
		{
		    @T=split(" ",$_);
		    $courant_query=$T[0];
		    print "<BR>\n" if ($courant_query ne $last_query);
		    $last_query=$courant_query;
		    &pPrintMATCH_DrawMap($_,$prog_meth,$GidxParamAnal{FILE},$GidxParamAnal{ORGA_CONFIG});
		}
		else # PAR DEFAUT MODE=$NORMAL 
		{
		    &pPrintMATCH(0,$prog_meth,$_,$GidxParamAnal{FILE},$GidxParamAnal{ORGA_CONFIG});
		}   		
	    }
	    else
	    {
		print "$_\n";
	    }
	}
    }
	    

    if ( $GidxParamAnal{"HTML"})
    {
	print "</BODY></HTML>\n";
	&CloseHTML();
    }
}  #### END du else [!$GidxParamAnal{FORM_FRAME_SET}]


