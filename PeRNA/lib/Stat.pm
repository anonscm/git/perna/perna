
=pod 
=head1 NAME

ToolBox 

=head1  SYNOPSIS 

Stat.pm 

=head1 DESCRIPTION

Stat.pm is a library which contains different procedures and functions 
to compute characteristics about nucleotidic sequences

=cut

use strict;
use warnings;
use DnaToolBox;
use Bio::Tools::SeqStats;
use IPC::Open3;


=head1 SUBROUTINES

=cut


####################################################################################################
# Four functions which compute basic characteristics
####################################################################################################


=head2 function ComputeKNucl

 Title        : ComputeKNucl
 Usage        : ComputeKNucl($sequence,$word_length)
 Prerequisite : none
 Function     : compute the number of each k-nucleotid (k=$word_length) in the sequence 
 		with the software "compseq"
 Returns      : a reference about an hashtable {key=k-nucleotid, value=number of found k-nucleotids}
 Args         : $sequence : fasta file or directly the sequence
 		$word_length : value of k
 Globals      : none

=cut
sub ComputeKNucl
{
	my ($sequence, $word_length) = @_;
	my %h_knucl = ();
	my @a_knucl_nb; # array used to extract the k-nucleotid numbers
	
	my @a_result 	= &ComputeCompseq($sequence, $word_length);
	my $index 	= &getKNucleotidIndex(\@a_result,$word_length);
	
	# for each of k-nucleotid, complete the hashtable 
	for (my $i = $index ; $i < $#a_result-1 ; $i++) {
		@a_knucl_nb 			= split(/\s+| +/,$a_result[$i]);
		$h_knucl{$a_knucl_nb[0]} 	= $a_knucl_nb[1];
	}
	
	# compute the others: k-nucleotids which are unknown
	@a_knucl_nb 				= split(/\s+| +/,$a_result[$#a_result]);
	$h_knucl{$a_knucl_nb[0].$word_length} 	= $a_knucl_nb[1];
	return \%h_knucl;
}


=head2 function ComputePairedNuclNumber

 Title        : ComputePairedNuclNumber
 Usage        : ComputePairedNuclNumber($struct_file)
 Prerequisite : the file $struct_file has a specific format:
 		Example: 1 8
			 2 7
			 3 6
 Function     : count the number of paired nucleotids in the structure file
 Returns      : the number of words in the file = the number of paired nucleotids in the structure
 Args         : $struct_file: the structure file in which we count the words, so the number of paired nucleotids
 Globals      : none

=cut
sub ComputePairedNuclNumber
{
	my ($struct_file) = @_;
	my @a_words;
	my $word_nb = 0;
	
	open(FILE,"<$struct_file") || die "Can't open $struct_file !\n";
	while(defined (my $line = <FILE>)) {
		if ($line ne "\n") {
			chomp $line;
			@a_words = split(/ +/, $line);
			$word_nb = $word_nb+($#a_words+1);
		}
	}
	close(FILE);
	return ($word_nb);
}


=head2 function ComputePair

 Title        : ComputePair
 Usage        : ComputePair($structure_file, $seq)
 Prerequisite : none
 Function     : compute the number of each nucleotid pair (A-U, G-C...) in the sequence $seq.
 		It reads the structure file and extracts the pairs
 		Example: $structure_file	
				1 6
				2 5
				3 4 
			$seq : AAGCUU (composed of A, U, G and C)
			return {AU => 2, GC => 1, GU =>0 , ...}
 Returns      : return a reference about an hashtable {key=pair, value=number of time that the pair is found}
 Args         : $structure_file: way of the file which contains the paired nucleotid numbers
 		$seq: fasta file which contains the sequence, or directly the sequence
 Globals      : none

=cut
sub ComputePair
{
	my ($structure_file, $seq) = @_;
	my %h_pair = ();
	my $key;
	my @a_tmp;
	my $dinucleotid;
	
	# case where seq is a fasta file
	if ( -e $seq) {
		my $inputstream = Bio::SeqIO->new( -file   => $seq, 
	                           		   -format => 'Fasta');
		my $seqobj 	= $inputstream->next_seq();
		$seq 		= $seqobj->seq;
	}
	# save the sequence in an array
	my @a_seq = split(//,$seq);
	
	# init keys of the hashtable: a key of the hashtab is a dinucleotid : a pair
	my @a_pairs = &GetRNAPair(); 
	for (my $i = 0 ; $i <= $#a_pairs ; $i++){
		$key = $a_pairs[$i][0].$a_pairs[$i][1]; 
		$h_pair{$key} = 0;
		$key = $a_pairs[$i][1].$a_pairs[$i][0];
		$h_pair{$key} = 0;
	}
	open(STRUCT_FILE,"<$structure_file") || die "Can't open $structure_file !\n";
	
	# for each line of the structure file
	while(defined (my $line = <STRUCT_FILE>)) {
		if ($line ne "\n") {
			chomp ($line);
			@a_tmp = split(/ /, $line);
			# search the 2 nucleotids in the sequence
			if ( ($a_seq[$a_tmp[0]-1] ) && ($a_seq[$a_tmp[1]-1]) ) {
				$dinucleotid = $a_seq[$a_tmp[0]-1].$a_seq[$a_tmp[1]-1];
			} else {
				close(STRUCT_FILE);
				die "ERROR: can't extract nucleotids containing in $structure_file from the sequence $seq !!\nCheck the file refers the pairs of the sequence.\n";
			} 
			# if the pair between the 2 nucleotids exists, increase the count
			if ( exists($h_pair{$dinucleotid}) ) {
				$h_pair{$dinucleotid}++;
			}
		}
	}
	close(STRUCT_FILE);
	return \%h_pair;
}


=head2 function ComputeFreeEnergy

 Title        : ComputeFreeEnergy
 Usage        : ComputeFreeEnergy($seq)
 Prerequisite : none
 Function     : predict the structure of $seq with RNAfold: save the associated 
 		free energy and the structure
 Returns      : the free energy and the predicted structure
 Args         : $seq : fasta file or primary sequence (AAUGUUGCGU)
 Globals      : none

=cut
sub ComputeFreeEnergy
{
	my ($seq) = @_;
	my @a_rnafold_result = &ComputeRNAfold($seq);
	# if rnafold failed, return free energy = 0.00 and an empty structure
	if (!(@a_rnafold_result)){
		return ("0.00","");
	} else {
		my ($free_energy, $struct) = ExtractRNAfoldData($a_rnafold_result[1]);
		return ($free_energy, $struct);
	}
}


####################################################################################################
# sub-functions to compute k-nucleotid numbers
####################################################################################################

=head2 function ComputeCompseq

 Title        : ComputeCompseq
 Usage        : ComputeCompseq($sequence, $word_length)
 Prerequisite : none
 Function     : compute the number of k-nucleotids (k=$word_length) in the sequence 
 		with the software "compseq"
 Returns      : an array composed of the output of the compseq process with the sequence.
 		A cell is a line of the output
 Args         : $sequence: fasta file which contains the sequence, or directly the sequence
 		$word_length: k
 Globals      : none

=cut
sub ComputeCompseq
{
	my ($sequence, $word_length) = @_;
	# if the first parameter isn't a file, add asis:: at the start of the sequence
	if (!(-f $sequence)) {
		chomp($sequence);
		$sequence = "asis::".$sequence;
	} 
	# compute compseq
	my @result = `compseq $sequence $word_length -auto -stdout`;
	
	if (!(@result) ){
		die "\nERROR: execution of compseq!\nSequence : $sequence\n"; 
	}
	return @result;	
}

=head2 function getKNucleotidIndex

 Title        : getKNucleotidIndex
 Usage        : getKNucleotidIndex($a_result, $word_length)
 Prerequisite : none
 Function     : look for in the array $a_result the number of the first line in which appears a k-nucleotid 
 		(k=$word_length) ; the precedent lines are comments
 Returns      : the line number in which appears the first k-nucleotid
 Args         : $a_result : reference about an array of the output of compseq
		$word_length : k
 Globals      : none

=cut
sub getKNucleotidIndex
{
	my ($a_result, $word_length) = @_;
	my $index	= 0;	# line's counter
	my $first_word	= ""; 
		
	#search the first k-nucleotid which is calculated
	# if k is 2, the first 2-nucleotid is AA 
	for (my $i = 0 ; $i < $word_length ; $i++) {
		$first_word = $first_word."A";
	}	
	# read the first lines
	while( substr($a_result->[$index],0,$word_length) ne $first_word ) {
		$index++;
	}
	return $index;
}

####################################################################################################
# sub-functions to compute free energy
####################################################################################################

=head2 function ComputeRNAfold

 Title        : ComputeRNAfold
 Usage        : ComputeRNAfold($input)
 Prerequisite : none
 Function     : compute RNAfold: predict the structure of $input (fasta file or sequence) and the associated free energy
 		return the output of RNAfold
 Returns      : an array composed of the output of RNAfold
 Args         : $input : fasta file or sequence AAUGUUGCGU
 Globals      : none

=cut 
sub ComputeRNAfold
{
	my ($input) = @_;
	my @res;
	# case where input is fasta file
	if ( -e $input) {
		@res = `RNAfold < $input`;
	} else {
		my $cmd = "RNAfold";
		my $pid = open3(*CMD_IN, *CMD_OUT, *CMD_ERR, $cmd);
		print CMD_IN $input;
		close (CMD_IN);
	        if ( <CMD_ERR>) {
	        	print "RNAfold error with $input: ".<CMD_ERR>."\n";
			return @res;
	        }
		@res = <CMD_OUT>;
		close (CMD_OUT);
		close (CMD_ERR);
		waitpid($pid, 0);
	}
	return @res;
}


=head2 function ExtractRNAfoldData

 Title        : ExtractRNAfoldData
 Usage        : ExtractRNAfoldData($line)
 Prerequisite : none
 Function     : extract from $line the free energy and the structure
 		Example : $line = "...............((((((..((........)).....).)))))... ( -0.70)"
		Extract -0.70 and "...............((((((..((........)).....).)))))..."
 Returns      : the free energy and the structure
 Args         : $line : an output line from RNAfold
 Globals      : none

=cut
sub ExtractRNAfoldData
{
	my ($line) = @_;
	my $free_energy;
	my @a_data = split(/ +/, $line);
	
	if ($a_data[0] && $#a_data>=1) {
		$free_energy = $a_data[$#a_data];
		chomp($free_energy);
		$free_energy =~ s/\)|\(| //g;
		return ($free_energy, $a_data[0]);
	} else {
		die " ERROR: output of RNAfold isn't correct: \n$line\n";
	}
}


####################################################################################################
# general procedures use by different types of sequences
####################################################################################################

=head2 procedure CreateKNucleotidFiles

 Title        : CreateKNucleotidFiles
 Usage        : CreateKNucleotidFiles($seq, $type, $way_dir, $word_length)
 Prerequisite : none
 Function     : compute the number of the each k-nucleotid (k=$word_length) in the sequence $seq and
 		create files in the directory $way_dir which contains the results.
 Returns      : none
 Args         : $seq : the sequence (fasta file or nucleotidic sequence) to compute the composition in k-nucleotids
 		$type : seq/struct: used to create the name of the resulting files. it corresponds to the nature of the sequence
		creation of files named <k_nucl>_$type
 		$way_dir : way of the directory where the resulting files will be created
		$word_length : k
 Globals      : none

=cut
sub CreateKNucleotidFiles
{
	my ($seq, $type, $way_dir, $word_length) = @_;
	my $file_name = "";
	
	# compute the number of each k-nucleotid with k=$word_length
	my $rh_knucl = &ComputeKNucl($seq,$word_length); 
	
	# for each k-nucleotid, creation of a file which contains the number of time that the k-nucleotid is found
	foreach my $knucl (keys(%{$rh_knucl})) {
		# get the name of the file to create
   		$file_name = &GetKNucleotidFileName($knucl, $type);
		# creation of the file for the k-nucleotid
		&CreateFile($file_name, $way_dir, $rh_knucl->{$knucl}."\n");
	}
}


=head2 procedure CreateAllNucleotidFiles

 Title        : CreateAllNucleotidFiles
 Usage        : CreateAllNucleotidFiles($seq, $type, $way_dir, $max_length)
 Prerequisite : none
 Function     : compute the number of each k-nucleotid (1<=k<=$max_length) in the sequence.
 		Create files in the directory $way_dir which contain the results.
 Returns      : none
 Args         : $seq : the sequence (fasta file or nucleotidic sequence) to compute the composition in k-nucleotids
 		$type : seq/struct. used to create the name of the resulting files. 
		creation of files named <k_nucl>_$type
 		$way_dir : way of the directory where the resulting files will be created
		$max_length : max length of the searched words : procedure counts k-nucleotids 1<=k<=$max_length
 Globals      : none

=cut
sub CreateAllNucleotidFiles
{
	my ($seq, $type, $way_dir, $max_length) = @_;
	for (my $length = 1 ; $length <= $max_length ; $length++ ) {
		&CreateKNucleotidFiles($seq, $type, $way_dir, $length);
	}
}



####################################################################################################
# sub-functions to compute the paired regions of a sequence = the structure
####################################################################################################

=head2 function ExtractStructureRegions

 Title        : ExtractStructureRegions
 Usage        : ExtractStructureRegions($structure_file)
 Prerequisite : none
 Function     : select the positions of the regions of the sequence which are paired
 Returns      : a string which contains the positions of the regions which are paired in the sequence.
 		Example of output: 1-2 4-5 8-9
 Args         : $structure_file: way of the file which contains the paired nucleotid numbers
 		Example	1 5
			2 4
			8 9 
 Globals      : none

=cut
sub ExtractStructureRegions
{
	my ($structure_file) = @_;
	my @a_tmp;
	my @a_nucleotid;
	my $regions = "";
	my $begin_pos;
	
	open(STRUCT_FILE,"<$structure_file") ;
	# save in an array all the nucleotid numbers
	while(defined (my $line = <STRUCT_FILE>)) {
		if ($line ne "\n") {
			chomp $line;
			@a_tmp = split(/ /, $line);
			push(@a_nucleotid, @a_tmp);
		}
	}
	close(STRUCT_FILE);
	
	if ( $#a_nucleotid >= 0) {
		# sort the nucleotid numbers order by position
		@a_nucleotid = sort {$a <=> $b} @a_nucleotid;
	
		# save the extremities of the regions to extract
		$begin_pos = $a_nucleotid[0];
		for (my $i = 0 ; $i <$#a_nucleotid ; $i++) {
			if ( ($a_nucleotid[$i]+1) != $a_nucleotid[$i+1] ) {
				$regions	= $regions.$begin_pos."-".$a_nucleotid[$i]." ";
				$begin_pos	= $a_nucleotid[$i+1];
			}
		}
		$regions = $regions.$begin_pos."-".$a_nucleotid[$#a_nucleotid];
	} else {
		$regions = "";
	}
	return ($regions);
}

=head2 procedure CreateMultiFastaFile

 Title        : CreateMultiFastaFile
 Usage        : CreateMultiFastaFile($sequence, $regions, $output_file, $output_dir)
 Prerequisite : none
 Function     : create a multifasta file composed of the regions $regions of $sequence. 
 		The multifasta file $output_file is created in the directory $output_dir
		with the software "extractseq"
 Returns      : None
 Args         :	$sequence : the way of the fasta file or a nucleotidic sequence
		$regions : a string composed of the regions to extract. Example : "1-5 8-15"
		$output_file : name of the multifasta file to create
		$output_dir : directory where create the resulting multifasta file
			
 Globals      : none

=cut
sub CreateMultiFastaFile
{
	my ($sequence, $regions, $output_file, $output_dir) = @_;
	
	# if the fisrt parameter is a sequence
	if (!(-e $sequence)) {
		$sequence = "asis::".$sequence;
	}
	# execute extractseq : save the result of the extraction in the file output_file 
	my $cmd = "extractseq $sequence -regions \"$regions\" -separate -stdout -auto > $output_dir"."$output_file";
	my $res = system($cmd);
	if ($res != 0){
		die "ERROR execution of extractseq: impossible to extract regions $regions of the sequence $sequence in the file $output_dir$output_file !\n"; 
	}	
}

####################################################################################################
# functions used to generate the file "structure" for the windows 
####################################################################################################

=head2 function ComputePairedNucleotids

 Title        : ComputePairedNucleotids
 Usage        : ComputePairedNucleotids($struct)
 Prerequisite : none
 Function     : compute the numbers of paired nucleotids given by $struct
 		Example : $struct =(((..))).   
		                   123456789   
			 return (1,8)(2,7)(3,6)
 Returns      : a reference of an array composed of the paired nucleotid numbers: 
 Args         : $struct : a string which gives paired nucleotids : (((..)))..
 Globals      : none

=cut
sub ComputePairedNucleotids
{
	my ($struct) = @_;
	my $first;	# first nucleotid in a pair
	my $char;	# current read character
	
	my @a_couple;	# set of paired nucleotids 
	my @a_paired_nucleotids;	# a temporary stack to save nucleotid to pair 
	
	# for each character of the structure
	for ( my $nucleotid_nb = 0 ; $nucleotid_nb < length($struct) ; $nucleotid_nb++) {
		$char = substr($struct, $nucleotid_nb, 1);
		# first nucleotid in a pair
		if ( $char eq '('){	
			push (@a_paired_nucleotids, ($nucleotid_nb+1));
		}elsif ($char eq ')') {		# second nucleotid in a pair
			$first = pop(@a_paired_nucleotids);
			push(@a_couple, [($first, ($nucleotid_nb+1))] );
		}
	}
	return (\@a_couple);
}


#####################################################################################################
# File function
#####################################################################################################
 
=head2 function GetKNucleotidFileName

 Title        : GetKNucleotidFileName
 Usage        : GetKNucleotidFileName($knucleotid, $type)
 Prerequisite : none
 Function     : return the name of the file for the k-nucleotid and the type
 		name : $knucleotid_$type
 Returns      : the name of the file for the k-nucleotid and the type 
 Args         : $knucleotid : the k-nucleotid (example, AG)
 		$type : part of the file name  (seq, struct or pair)
 Globals      : none

=cut
sub GetKNucleotidFileName
{
	my ($knucleotid, $type) = @_;
	my $name_file = $knucleotid."_".$type;
	return $name_file;
}

1
